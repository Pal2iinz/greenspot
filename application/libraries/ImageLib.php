<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ImageLib
{

    protected $CI;

	public function __construct()
    {
        // Assign the CodeIgniter super-object
        $this->CI =& get_instance();
    }
    /**
    * Upload Image Multiple File And Create Thumbnail
    * @return Array
    */
    public function upload_mulitple_image($aFileImage)
    {
        $this->CI->load->library('upload');
        $config['upload_path'] = './assets/images/blog';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = '2048';
        $config['max_width'] = '1024';
        $config['max_height'] = '768';
        $this->CI->load->library('upload', $config);
        $this->CI->upload->initialize($config);

        // Change $_FILES to new vars and loop them
        foreach($aFileImage as $key=>$val) {
            $i = 1;
            foreach($val as $v) {
                $field_name = "file_".$i;
                $_FILES[$field_name][$key] = $v;
                $i++;
            }
        }
        // Unset the useless one ;)
        unset($_FILES['sFileImage']);
        $aFileUpload = array();
        // main action to upload each file
        foreach($_FILES as $field_name => $file) {
            if ( ! $this->CI->upload->do_upload($field_name)) {
                $error = array('error' => $this->CI->upload->display_errors());
                $this->CI->session->set_flashdata('error', $error["error"]);
            } else {
                $fResizeImage = $this->resizeImage($this->CI->upload->data());
                if($fResizeImage) {
                    $aFileUpload[] = $this->CI->upload->data();
                }
            }
        }
        return $aFileUpload;
    } // End Function Upload Image Multiple

    public function upload_mulitple($attach,$id,$sAccountFB)
    {

        echo "Have";

        //     $chkdata = $this->CI->Event_model->findById($id);
        //     if(!empty($chkdata)){
        //         foreach ($chkdata as $key => $cus) {
        //             $cuscode = $cus->code_cus;
        //         }
        //     }else{
        //         $sAccountFB = $this->CI->input->post("sAccountFB");
        //         $cuscode = $sAccountFB;
        //     }
        //     // retrieve the number of images uploaded;
        //     $number_of_files = count($_FILES['sAccountFB']['size']);
        //     $files =  $_FILES['attach_files'];
        //     $errors = array();
        //     $this->CI->load->library('upload');
        //     // set config for upload file
        //     $config['upload_path'] = "assets/images/vitamilk_club/".$cuscode;
        //     $config['allowed_types'] = "jpg|jpeg|gif|png|pdf|docx|xls|xlsx|pptx";
        //     $config['max_size']      = 1000000; // KB
        //     // load library upload
        //     $this->CI->upload->initialize($config);

        //     if(!is_dir("assets/documents/".$cuscode)) //create the folder if it's not already exists
        //     {
        //       mkdir("assets/documents/".$cuscode,0777,TRUE);
        //     }

        //     for ($i = 0 ; $i < $number_of_files; $i++) {
        //         $_FILES['sAccountFB']['name'] = $files['name'][$i];
        //         $_FILES['sAccountFB']['type'] = $files['type'][$i];
        //         $_FILES['sAccountFB']['tmp_name'] = $files['tmp_name'][$i];
        //         $_FILES['sAccountFB']['error'] = $files['error'][$i];
        //         $_FILES['sAccountFB']['size'] = $files['size'][$i];
        //         // we retrieve the number of files that were uploaded
        //     $document = $this->CI->upload->do_upload('attach_files');
        //     if($document){
        //         $data1 = $this->CI->upload->data();
        //         $name_array[$i] = date("dmYHis").$i.$data1['file_ext'];
        //         rename($data1['full_path'], $data1['file_path'].$name_array[$i]);
        //     } // end if

        //     if(!empty($data1)){
        //         $data = array(
        //             "attach"              => $name_array[$i],
        //             "registerEID"                   => $id,
        //             "createdate"                    => $this->CI->dateTimeNow,
        //         );
        //         $this->CI->Event_model->saveattach($data);
        //     }
        // } // end for
    }//end multifile

}
