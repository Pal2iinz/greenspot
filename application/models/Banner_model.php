<?php
/**
* Parses and verifies the doc comments for file.
*
* PHP version 5.6
*
* @category  PHP
* @package   Teerapuch
* @author    Teerapuch Kassakul <teerapuch@hotmail.com>
* @copyright 2016 Teerapuch
* @license   http://opensource.org/licenses/mit-license.php MIT License
* @link      https://www.teerapuch.com
*/
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* Permission Model is class for management department table
*
* @category  PHP
* @package   Teerapuch
* @author    Teerapuch Kassakul <teerapuch@hotmail.com>
* @copyright 2016 Teerapuch
* @license   http://opensource.org/licenses/mit-license.php MIT License
* @link      https://www.teerapuch.com
*/
class Banner_model extends CI_Model
{
    /**
    * Main construct function and variables to use in all class
    *
    * @return tablename department_tbl
    */
    public function __construct()
    {
        parent::__construct();
        $this->table = 'banner_tbl';
    }
    /**
    * find all data in hero_tbl
    *
    * @return $query
    */
    public function findAll()
    {
        $where = array('deleted !=' => '1');
        $query = $this->db->order_by('createdate', 'ASC')
            ->get_where($this->table, $where)->result();
        return $query;
    }
    /**
    * find some data filter from id in hero_tbl
    *
    * @param int $id hero id
    *
    * @return $query
    */
    public function findById($id)
    {
        $query = $this->db->get_where(
            $this->table,
            array(
                'id =' => $id,
                'deleted !=' => '1'
            )
        )->result();
        return $query;
    }
    /**
    * save data to hero_tbl
    *
    * @param array $data Group of data to save
    *
    * @return true
    */
    public function save($data)
    {
        // save to db
        $this->db->insert($this->table, $data);
        return true;
    }
    /**
    * update data to hero_tbl
    *
    * @param int   $id   hero id
    * @param array $data Group of data to update
    *
    * @return true
    */
    public function update($id,$data)
    {
        // update hero to db
        $this->db->where('id', $id);
        $this->db->update($this->table, $data);
        return true;
    }
    /**
    * update data to hero_tbl
    *
    * @param int $id hero id
    *
    * @return true
    */
    public function delete($id)
    {
        try {
            // update to db
            $this->db->set('deleted', 1);
            $this->db->set('updatedate', $this->dateTimeNow);
            $this->db->where('id', $id);
            $this->db->update($this->table);
            return true;
        } catch (Exception $e) {
            echo 'More exception: ',  $e->getMessage();
            return false;
        } // end try
    }
    /**
    * delete image data from hero_tbl
    *
    * @param int $id hero id
    *
    * @return true
    */
    public function deleteimage($id)
    {
        try {
            // update to db
            $this->db->set('image', '');
            $this->db->set('updatedate', $this->dateTimeNow);
            $this->db->where('id', $id);
            $this->db->update($this->table);
            return true;
        } catch (Exception $e) {
            echo 'More exception: ',  $e->getMessage();
            return false;
        } // end try
    }

    /**
    * content page 1
    *
    * @param int $id hero id
    *
    * @return true
    */
    public function findByPageOne()
    {
       $where = array('page' => '1','status' => '1','deleted !=' => '1');
        $query = $this->db->limit(1)->order_by('createdate', 'ASC')
        ->get_where($this->table, $where)->result();
        return $query;
    }
}
