<?php
/**
* Parses and verifies the doc comments for file.
*
* PHP version 5.6
*
* @category  PHP
* @package   Teerapuch
* @author    Teerapuch Kassakul <teerapuch@hotmail.com>
* @copyright 2016 Teerapuch
* @license   http://opensource.org/licenses/mit-license.php MIT License
* @link      https://www.teerapuch.com
*/
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* Permission Model is class for management department table
*
* @category  PHP
* @package   Teerapuch
* @author    Teerapuch Kassakul <teerapuch@hotmail.com>
* @copyright 2016 Teerapuch
* @license   http://opensource.org/licenses/mit-license.php MIT License
* @link      https://www.teerapuch.com
*/
class Hero_model extends CI_Model
{
    /**
    * Main construct function and variables to use in all class
    *
    * @return tablename department_tbl
    */
    public function __construct()
    {
        parent::__construct();
        $this->table = 'hero_tbl';
    }
    /**
    * find all data in hero_tbl
    *
    * @return $query
    */
    public function findAll()
    {
        $where = array('deleted !=' => '1');
        $query = $this->db->order_by('createdate', 'ASC')
            ->get_where($this->table, $where)->result();
        return $query;
    }
    /**
    * find some data filter from id in hero_tbl
    *
    * @param int $id hero id
    *
    * @return $query
    */
    public function findById($id)
    {
        $query = $this->db->get_where(
            $this->table,
            array(
                'id =' => $id,
                'deleted !=' => '1'
            )
        )->result();
        return $query;
    }
    /**
    * save data to hero_tbl
    *
    * @param array $data Group of data to save
    *
    * @return true
    */
    public function save($data)
    {
        // save to db
        $this->db->insert($this->table, $data);
        return true;
    }
    /**
    * update data to hero_tbl
    *
    * @param int   $id   hero id
    * @param array $data Group of data to update
    *
    * @return true
    */
    public function update($id,$data)
    {
        // update hero to db
        $this->db->where('id', $id);
        $this->db->update($this->table, $data);
        return true;
    }
    /**
    * update data to hero_tbl
    *
    * @param int $id hero id
    *
    * @return true
    */
    public function delete($id)
    {
        try {
            // update to db
            $this->db->set('deleted', 1);
            $this->db->set('updatedate', $this->dateTimeNow);
            $this->db->where('id', $id);
            $this->db->update($this->table);
            return true;
        } catch (Exception $e) {
            echo 'More exception: ',  $e->getMessage();
            return false;
        } // end try
    }
    /**
    * delete image data from hero_tbl
    *
    * @param int $id hero id
    *
    * @return true
    */
    public function deleteimage($id)
    {
        try {
            // update to db
            $this->db->set('hero', '');
            $this->db->set('updatedate', $this->dateTimeNow);
            $this->db->where('id', $id);
            $this->db->update($this->table);
            return true;
        } catch (Exception $e) {
            echo 'More exception: ',  $e->getMessage();
            return false;
        } // end try
    }
    /**
    * find all data in permission_tbl
    *
    * @return $query
    */
    public function findByHeroHome()
    {
        $where = array('page'=> '1','status' => '1','deleted !=' => '1');
        $query = $this->db->order_by('position', 'ASC')
            ->get_where($this->table, $where)->result();
        return $query;
    }

       /**
    * find all data in permission_tbl
    *
    * @return $query
    */
    public function findByHeroAbout()
    {
        $where = array('page'=> '2','status' => '1','deleted !=' => '1');
        $query = $this->db->order_by('position', 'ASC')
            ->get_where($this->table, $where)->result();
        return $query;
    }
      /**
    * find all data in permission_tbl
    *
    * @return $query
    */
    public function findByHeroVitamilk()
    {
        $where = array('page'=> '3','status' => '1','deleted !=' => '1');
        $query = $this->db->order_by('position', 'ASC')
            ->get_where($this->table, $where)->result();
        return $query;
    }
      /**
    * find all data in permission_tbl
    *
    * @return $query
    */
    public function findByHeroVsoy()
    {
        $where = array('page'=> '4','status' => '1','deleted !=' => '1');
        $query = $this->db->order_by('position', 'ASC')
            ->get_where($this->table, $where)->result();
        return $query;
    }
       /**
    * find all data in permission_tbl
    *
    * @return $query
    */
    public function findByHeroGreenspot()
    {
        $where = array('page'=> '5','status' => '1','deleted !=' => '1');
        $query = $this->db->order_by('position', 'ASC')
            ->get_where($this->table, $where)->result();
        return $query;
    }

    /**
    * find all data in permission_tbl
    *
    * @return $query
    */
    public function findByHeroHealthy()
    {
        $where = array('page'=> '6','status' => '1','deleted !=' => '1');
        $query = $this->db->order_by('position', 'ASC')
            ->get_where($this->table, $where)->result();
        return $query;
    }
     /**
    * find all data in permission_tbl
    *
    * @return $query
    */
    public function findByHeroHealthyMenu()
    {
        $where = array('page'=> '7','status' => '1','deleted !=' => '1');
        $query = $this->db->order_by('position', 'ASC')
            ->get_where($this->table, $where)->result();
        return $query;
    }
    /**
    * find for Jobcareer
    *
    * @return $query
    */
    public function findByJobcareer()
    {
        $where = array('page'=> '8','status' => '1','deleted !=' => '1');
        $query = $this->db->order_by('position', 'ASC')
            ->get_where($this->table, $where)->result();
        return $query;
    }

        /**
    * find for Jobcareer
    *
    * @return $query
    */
    public function findByHeroProvita()
    {
        $where = array('page'=> '9','status' => '1','deleted !=' => '1');
        $query = $this->db->order_by('position', 'ASC')
            ->get_where($this->table, $where)->result();
        return $query;
    }

}

