<?php
/**
* Parses and verifies the doc comments for file.
*
* PHP version 5.6
*
* @category  PHP
* @package   Digiday
* @author    Teerapuch Kassakul <teerapuch@hotmail.com>
* @copyright 2016 Digiday
* @license   http://opensource.org/licenses/mit-license.php MIT License
* @link      http://teerapuch.com
*/
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* Parses and verifies the doc comments for file.
*
* PHP version 5.6
*
* @category  PHP
* @package   Digiday
* @author    Teerapuch Kassakul <teerapuch@hotmail.com>
* @copyright 2016 Digiday
* @license   http://opensource.org/licenses/mit-license.php MIT License
* @link      http://digiday.co.th
*/
class User_model extends CI_Model
{
    /**
    * find all data in access_tbl
    *
    * @return $query
    */
    public function __construct()
    {
        parent::__construct();
        $this->table = 'user_tbl';
    }
    /**
    * find all data in access_tbl
    * @param array $aData from login to validate user
    * @return $query
    */
    public function validate($aData)
    {
        $this->db->select('
            id,
            firstname,
            lastname,
            email,
            password,
            avatar,
            role,
            department,
            ipaddress'
        );
        $this->db->from($this->table);
        $this->db->where('email', $aData['email']);
        $this->db->limit(1);
        // Run the query
        $query = $this->db->get();
        // if found email
        if ($query->num_rows() == 1) {
            // if there is a user, then create session data
            $row = $query->row();
            // if password pass after verify
            if (password_verify($aData['password'], $row->password)) {
                $getAccess = $this->db->select('perm,access')->get_where(
                    'access_tbl',
                    array(
                        'deleted !=' => '1',
                        'user =' => $row->id,
                    ))->result();
                $access = array();
                foreach ($getAccess as $key => $v) {
                    $access[$key] = $v;
                }
                $sess_array = array(
                    'id' => $row->id,
                    'firstname' => $row->firstname,
                    'lastname' => $row->lastname,
                    'email' => $row->email,
                    'avatar' => $row->avatar,
                    'role' => $row->role,
                    'department' => $row->department,
                    'ipaddress' => $row->ipaddress,
                    'perm' => $access,
                );
                // update ipaddress to db
                $data = array(
                    'ipaddress' => $this->input->ip_address()
                );
                $this->db->where('id', $row->id);
                $this->db->update($this->table, $data);
                // create session
                $this->session->set_userdata('logged_in_admin', $sess_array);
                return true;
            } else {
                return false;
            }
        } // end if
    }
    /**
    * find all data in access_tbl
    *
    * @return $query
    */
    public function findAll()
    {
        $query = $this->db->order_by('id','ASC')
        ->get_where($this->table, array('deleted !=' => '1'))->result();
        return $query;
    }
    /**
    * find all data in access_tbl
    *
    * @return $query
    */
    public function findById($id)
    {
        $query = $this->db->get_where($this->table,
            array('id =' => $id,'deleted !=' => '1')
            )->result();
        return $query;
    }
    /**
    * save data to user_tbl
    *
    * @param int   $id   user id
    * @param array $data Group of data to save
    *
    * @return true
    */
    public function save($data)
    {
        // save to db
        $this->db->insert($this->table, $data);
        return true;
    }
    /**
    * update data to user_tbl
    *
    * @param int   $id   user id
    * @param array $data Group of data to update
    *
    * @return true
    */
    public function update($id,$data)
    {
        // update order to db
        $this->db->where('id', $id);
        $this->db->update($this->table, $data);
        return true;
    }
    /**
    * Soft delete data to user_tbl
    *
    * @param int   $id   user id
    *
    * @return true
    */
    public function delete($id)
    {
        try {
            // update to db
            $this->db->set('deleted',1);
            $this->db->set('updatedate',$this->dateTimeNow);
            $this->db->where('id',$id);
            $this->db->update($this->table);
            return true;
        } catch (Exception $e) {
            echo 'More exception: ',  $e->getMessage();
            return false;
        } // end try
    }
}
