<?php
/**
* Parses and verifies the doc comments for file.
*
* PHP version 5.6
*
* @category  PHP
* @package   Digiday
* @author    Teerapuch Kassakul <teerapuch@hotmail.com>
* @copyright 2016 Digiday
* @license   http://opensource.org/licenses/mit-license.php MIT License
* @link      http://www.digiday.co.th
*/
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* Permission Model is class for management department table
*
* @category  PHP
* @package   Digiday
* @author    Teerapuch Kassakul <teerapuch@hotmail.com>
* @copyright 2016 Digiday
* @license   http://opensource.org/licenses/mit-license.php MIT License
* @link      http://www.digiday.co.th
*/
class Blog_model extends CI_Model
{
    /**
    * Main construct function and variables to use in all class
    *
    * @return tablename department_tbl
    */
    public function __construct()
    {
        parent::__construct();
        $this->table = 'blog_tbl';
    }
    /**
    * find all data in permission_tbl
    *
    * @return $query
    */
    public function findAll()
    {
        $where = array('deleted !=' => '1');
        $query = $this->db->order_by('createdate', 'ASC')
            ->get_where($this->table, $where)->result();
        return $query;
    }
     /**
    * find all data in healthy only
    *
    * @return $query
    */
    public function findByHealthy()
    {
        $where = array('categories'=>'1','deleted !=' => '1');
        $query = $this->db->order_by('createdate', 'ASC')
            ->get_where($this->table, $where)->result();
        return $query;
    }

     public function findByHealthyPager($config)
    {   
        $query = $this->db->order_by('createdate','desc')
                ->limit($config['per_page'],$this->uri->segment(2))
                ->get_where($this->table,array('categories'=>'1','deleted !=' => '1'))
                ->result();
        return $query;

    }

    /**
    * find some data filter from id in permission_tbl
    *
    * @param int $id permission id
    *
    * @return $query
    */
    public function findByIdHealthy($id)
    {
        $query = $this->db->get_where(
            $this->table,
            array(
                'id =' => $id,
                'categories'=>'1',
                'deleted !=' => '1'
            )
        )->result();
        return $query;
    }
     /**
    * find all data in healthy-menu only
    *
    * @return $query
    */
    public function findByHealthyMenu()
    {
       /* $where = array('categories'=>'2','deleted !=' => '1');
        $query = $this->db->order_by('createdate', 'ASC')
            ->get_where($this->table, $where)->result();
        return $query;*/
       $where = array('categories'=>'2','deleted !=' => '1');
        $query = $this->db->order_by('createdate', 'ASC')
            ->get_where($this->table, $where)->result();
        return $query;
    }
       public function findByHealthyMenuPager($config)
    {
        $query = $this->db->order_by('createdate','desc')
                ->limit($config['per_page'],$this->uri->segment(2))
                ->get_where($this->table,array('categories'=>'2','deleted !=' => '1'))
                ->result();
        return $query;
    }
     /**
    * find some data filter from id in permission_tbl
    *
    * @param int $id permission id
    *
    * @return $query
    */
    public function findByIdMenu($id)
    {
        $query = $this->db->get_where(
            $this->table,
            array(
                'id =' => $id,
                'categories'=>'2',
                'deleted !=' => '1'
            )
        )->result();
        return $query;
    }
    /**
    * find some data filter from id in permission_tbl
    *
    * @param int $id permission id
    *
    * @return $query
    */
    public function findById($id)
    {
        $query = $this->db->get_where(
            $this->table,
            array(
                'id =' => $id,
                'deleted !=' => '1'
            )
        )->result();
        return $query;
    }
    /**
    * save data to permission_tbl
    *
    * @param array $data Group of data to update
    *
    * @return true
    */
    public function save($data)
    {
        // save to db
        $this->db->insert($this->table, $data);
        return true;
    }
    /**
    * update data to permission_tbl
    *
    * @param int   $id   permission id
    * @param array $data Group of data to update
    *
    * @return true
    */
    public function update($id,$data)
    {
        // update permission to db
        $this->db->where('id', $id);
        $this->db->update($this->table, $data);
        return true;
    }
    /**
    * update data to permission_tbl
    *
    * @param int $id permission id
    *
    * @return true
    */
    public function delete($id)
    {
        try {
            // update to db
            $this->db->set('deleted', 1);
            $this->db->set('updatedate', $this->dateTimeNow);
            $this->db->where('id', $id);
            $this->db->update($this->table);
            return true;
        } catch (Exception $e) {
            echo 'More exception: ',  $e->getMessage();
            return false;
        } // end try
    }
    /**
    * update data to blog_tbl
    *
    * @param int $id blog id
    *
    * @return true
    */
    public function deleteimage($id,$position)
    {
        switch ($position) {
            case '1':
                try {
                    // update to db
                    $this->db->set('thumbnail', '');
                    $this->db->set('updatedate', $this->dateTimeNow);
                    $this->db->where('id', $id);
                    $this->db->update($this->table);
                    return true;
                } catch (Exception $e) {
                    echo 'More exception: ',  $e->getMessage();
                    return false;
                } // end try
                break;
            case '2':
                try {
                    // update to db
                    $this->db->set('image', '');
                    $this->db->set('updatedate', $this->dateTimeNow);
                    $this->db->where('id', $id);
                    $this->db->update($this->table);
                    return true;
                } catch (Exception $e) {
                    echo 'More exception: ',  $e->getMessage();
                    return false;
                } // end try
                break;
        } // end switch
    }
}
