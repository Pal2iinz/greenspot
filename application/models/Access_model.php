<?php
/**
* Parses and verifies the doc comments for file.
*
* PHP version 5.6
*
* @category  PHP
* @package   Digiday
* @author    Teerapuch Kassakul <teerapuch@hotmail.com>
* @copyright 2016 Digiday
* @license   http://opensource.org/licenses/mit-license.php MIT License
* @link      http://www.digiday.co.th
*/
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* Role Model is class for management department table
*
* @category  PHP
* @package   Digiday
* @author    Teerapuch Kassakul <teerapuch@hotmail.com>
* @copyright 2016 Digiday
* @license   http://opensource.org/licenses/mit-license.php MIT License
* @link      http://www.digiday.co.th
*/
class Access_Model extends CI_Model
{
    /**
    * Main construct function and variables to use in all class
    *
    * @return tablename access_tbl
    */
    public function __construct()
    {
        parent::__construct();
        $this->table = 'access_tbl';
    }
    /**
    * find all data in access_tbl
    *
    * @return $query
    */
    public function findAll()
    {
        $where = array('deleted !=' => '1');
        $query = $this->db->order_by('createdate', 'ASC')
            ->get_where($this->table, $where)->result();
        return $query;
    }
    /**
    * find some data filter from id in access_tbl
    *
    * @param int $id role id
    *
    * @return $query
    */
    public function findById($id)
    {
        $query = $this->db->get_where(
            $this->table,
            array(
                'id =' => $id,
                'deleted !=' => '1'
            )
        )->result();
        return $query;
    }
    /**
    * find some data filter from id in access_tbl
    *
    * @param int $id user id
    *
    * @return $query
    */
    public function findByUser($id)
    {
        $select =   $this->db->select('access_tbl.*, user_tbl.id');
                    $this->db->from('access_tbl');
                    $this->db->join(
                        'user_tbl',
                        'user_tbl.id = access_tbl.user',
                        'left'
                    );
                    $this->db->where('user_tbl.id =', $id);
                    $this->db->where('user_tbl.deleted !=', '1');
        $query = $this->db->get();
        if($query->num_rows() != 0) {
            return $query->result();
        } else {
            return false;
        }
    }
    /**
    * find some data filter from id in access_tbl
    *
    * @param int $id user id
    *
    * @return $query
    */
    public function findByUserPerm($user,$perm)
    {
        $query = $this->db->get_where(
            $this->table,
            array(
                'user =' => $user,
                'perm =' => $perm
            )
        )->result();
        return $query;
    }
    /**
    * save data to role_tbl
    *
    * @param array $data Group of data to update
    *
    * @return true
    */
    public function save($data)
    {
        // save to db
        $this->db->insert($this->table, $data);
        return true;
    }
    /**
    * update data to role_tbl
    *
    * @param int   $id   role id
    * @param array $data Group of data to update
    *
    * @return true
    */
    public function update($user,$perm,$data)
    {
        // update order to db
        $this->db->where('user', $user);
        $this->db->where('perm', $perm);
        $this->db->update($this->table, $data);
        return true;
    }
    /**
    * update data to role_tbl
    *
    * @param int $id role id
    *
    * @return true
    */
    public function delete($id)
    {
        try {
            // update to db
            $this->db->set('deleted', 1);
            $this->db->set('updatedate', $this->dateTimeNow);
            $this->db->where('id', $id);
            $this->db->update($this->table);
            return true;
        } catch (Exception $e) {
            echo 'More exception: ',  $e->getMessage();
            return false;
        } // end try
    }
}
