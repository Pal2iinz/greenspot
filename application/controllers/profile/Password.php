<?php
/**
* Parses and verifies the doc comments for file.
*
* PHP version 5.6
*
* @category  PHP
* @package   Digiday
* @author    Teerapuch Kassakul <teerapuch@hotmail.com>
* @copyright 2016 Digiday
* @license   http://opensource.org/licenses/mit-license.php MIT License
* @link      http://www.digiday.co.th
*/
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* Parses and verifies the doc comments for functions.
*
* @category  PHP
* @package   Digiday
* @author    Teerapuch Kassakul <teerapuch@hotmail.com>
* @copyright 2016 Digiday
* @license   http://opensource.org/licenses/mit-license.php MIT License
* @link      http://www.digiday.co.th
*/
class Password extends MY_Controller
{
    /**
    * Main construct function and variables to use in all class
    *
    * @return session is_logged_in_admin
    */
    public function __construct()
    {
        parent::__construct();
        // Verify Admin Login
        is_logged_in_admin();
    }
    /**
    * A summary informing the user what the associated element does.
    *
    * and to provide some background information or textual references.
    *
    * @return void
    */
    public function index()
    {
        $this->data['sTitle'] = 'Password Management';
        $aCustomCss = array(
            "assets/css/layouts/admin/user/index/style.css",
        );
        $aCustomJs = array(
            "assets/js/app/profile/change.js",
        );
        $this->main_css($aCustomCss);
        $this->main_js($aCustomJs);
        $this->main_layout("profile/change", $this->data);
    }
    /**
    * Change User Password
    *
    * @return void
    */
    public function change()
    {
        $this->form_validation->set_rules(
            'password',
            'Password',
            'trim|required|min_length[6]|xss_clean'
        );
        $this->form_validation->set_rules(
            'confirmpassword',
            'Confirm Password',
            'trim|required|min_length[6]|xss_clean|matches[password]'
        );
        if ($this->form_validation->run() === false) {
            $this->index();
        } else {
            $options = ['cost' => 12];
            $id = $this->security->xss_clean($this->input->post('id'));
            $crypt = $this->security->xss_clean($this->input->post('password'));
            $password = password_hash($crypt, PASSWORD_BCRYPT, $options);
            // Prepare Data To Array
            $data = array(
                "password" => $password,
                "updatedate" => $this->dateTimeNow,
                "createdate" => $this->dateTimeNow
            );
            $save = $this->user_model->update($id, $data);
            // redirect with session msessage
            $this->session->set_flashdata('msg-success', 'change success');
            redirect($this->agent->referrer());
        } // end else
    }

}
