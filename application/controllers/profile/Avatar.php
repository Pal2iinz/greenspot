<?php
/**
* Parses and verifies the doc comments for file.
*
* PHP version 5.6
*
* @category  PHP
* @package   Digiday
* @author    Teerapuch Kassakul <teerapuch@hotmail.com>
* @copyright 2016 Digiday
* @license   http://opensource.org/licenses/mit-license.php MIT License
* @link      http://www.digiday.co.th
*/
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* Parses and verifies the doc comments for functions.
*
* @category  PHP
* @package   Digiday
* @author    Teerapuch Kassakul <teerapuch@hotmail.com>
* @copyright 2016 Digiday
* @license   http://opensource.org/licenses/mit-license.php MIT License
* @link      http://www.digiday.co.th
*/
class Avatar extends MY_Controller
{
    /**
    * Main construct function and variables to use in all class
    *
    * @return session is_logged_in_admin
    */
    public function __construct()
    {
        parent::__construct();
        // Verify Admin Login
        is_logged_in_admin();
    }
    /**
    * A summary informing the user what the associated element does.
    *
    * and to provide some background information or textual references.
    *
    * @return void
    */
    public function index()
    {

        $this->data['sTitle'] = 'Avatar Management';
        $aCustomCss = array(
            "assets/css/layouts/admin/user/index/style.css",
        );
        $this->data['user'] = $this->User_model->findById(
            $this->session->userdata['logged_in_admin']['id']
        );
        $this->main_css($aCustomCss);
        $this->main_layout("profile/avatar", $this->data);
    }
    /**
    * Change User Avatar
    *
    * @return void
    */
    public function change()
    {
        $id = $this->security->xss_clean($this->input->post('id'));
        // set for upload
        $config['upload_path']   = 'assets/images/avatar/';
        $config['allowed_types'] = 'gif|jpg|jpeg|png';
        $config['max_size']      = '2048';
        $config['max_width']     = '1920';
        $config['max_height']    = '1280';
        $config['remove_spaces'] = TRUE;
        $this->load->library('upload', $config);
        // upload and crop profile to 120x120 px
        if($this->upload->do_upload('avatar')) {
            // Resize file
            $data = $this->upload->data();
            $sNewName = 'avatar_'.date("dmYHis").$data['file_ext'];
            $config['image_library'] = 'gd2';
            $config["source_image"] = $data["full_path"];
            $config['create_thumb'] = FALSE;
            $config['maintain_ratio'] = TRUE;
            $config['new_image'] = $data["file_path"].$sNewName;
            $config['quality'] = "100%";
            $config['width'] = 120;
            $config['height'] = 120;
            $dim = (intval($data["image_width"]) / intval($data["image_height"])) - ($config['width'] / $config['height']);
            $config['master_dim'] = ($dim > 0)? "height" : "width";

            $this->image_lib->initialize($config);
            if (!$this->image_lib->resize()) {
                //Resize image
                // redirect with session msessage
                $this->session->set_flashdata(
                    'msg-danger',
                    $this->image_lib->display_errors()
                );
                redirect($this->agent->referrer());
            } else {
                // Crop file
                $config['image_library'] = 'gd2';
                $config['source_image'] = $data["file_path"].$sNewName;
                $config['new_image'] = $data["file_path"].$sNewName;
                $config['quality'] = "100%";
                $config['maintain_ratio'] = FALSE;
                $config['width'] = 120;
                $config['height'] = 120;
                $config['x_axis'] = '0';
                $config['y_axis'] = '0';
                $this->image_lib->clear();
                $this->image_lib->initialize($config);
                if (!$this->image_lib->crop()) {
                    // redirect with session msessage
                    $this->session->set_flashdata('msg-danger', $this->image_lib->display_errors());
                    redirect($this->agent->referrer());
                } else {
                    $data = array(
                        'avatar'  => $sNewName,
                        'updatedate'  => $this->dateTimeNow
                    );
                    // update data to user_tbl
                    $this->User_model->update($id, $data);
                    $logged = $this->session->userdata('logged_in_admin');
                    $logged['avatar'] = $sNewName;
                    $this->session->set_userdata('logged_in_admin', $logged);
                    // redirect with session msessage
                    $this->session->set_flashdata(
                        'msg-success',
                        'Change Avatar Succsess'
                    );
                    redirect($this->agent->referrer());
                }
            } // end else
        } // end if upload and crop
    }

}
