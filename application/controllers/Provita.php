<?php
/**
* Parses and verifies the doc comments for file.
*
* PHP version 5.6
*
* @category  PHP
* @package   Teerapuch
* @author    Teerapuch Kassakul <teerapuch@hotmail.com>
* @copyright 2016 Teerapuch
* @license   http://opensource.org/licenses/mit-license.php MIT License
* @link      https://teerapuch.com
*/
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* Parses and verifies the doc comments for functions.
*
* @category  PHP
* @package   Teerapuch
* @author    Teerapuch Kassakul <teerapuch@hotmail.com>
* @copyright 2016 Teerapuch
* @license   http://opensource.org/licenses/mit-license.php MIT License
* @link      https://teerapuch.com
*/
class Provita extends MY_Controller
{
    /**
    * Main construct function and variables to use in all class
    *
    * @return session is_logged_in_admin
    */
    public function __construct()
    {
        parent::__construct();
        // Verify Admin Login
        // is_logged_in_admin();

    }
    /**
    * A summary informing the user what the associated element does.
    *
    * and to provide some background information or textual references.
    *
    * @return void
    */
    public function index()
    {

        // get data from db
 
        $title = 'Green Spot Co.,Ltd บริษัท กรีนสปอต จำกัด : ไวตามิ้ลค์ วีซอย กรีนสปอร์ต นมถั่วเหลือง อาหารเพื่อสุขภาพ';
        $style = array(
            "styles/provita.css",
        );
        $script = array(
             "front/healthy.js",
             "../plugins/isotope-docs/js/isotope-docs.min.js"

        );
        $hero       = $this->Hero_model->findByHeroProvita();
        $contents   = $this->Content_model->findByPageSeven();

        $data = array(
            'title'       => $title,
            'style'       => $style,
            'script'      => $script,
            'hero'        => $hero,
            'contents'  => $contents ,
        );
        //var_dump($contents);

        $this->load->view('header', $data);
        $this->load->view('pro-vita', $data);
        $this->load->view('footer', $data);
    }

}
