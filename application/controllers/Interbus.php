<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Interbus extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$title = 'Green Spot Co.,Ltd บริษัท กรีนสปอต จำกัด : ไวตามิ้ลค์ วีซอย กรีนสปอร์ต นมถั่วเหลือง อาหารเพื่อสุขภาพ';
        $style = array(
            "styles/carts.css",
        );
        $script = array(
        	"front/product/vitamilk.js",
        );

        $data = array(
            'title' => $title,
            'style' => $style,
            'script' => $script,
          
        );
		$this->load->view('header', $data);
		$this->load->view('interbus/index', $data);
		$this->load->view('footer', $data);
	}

    
	

}
