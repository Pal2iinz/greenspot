<?php
/**
* Parses and verifies the doc comments for file.
*
* PHP version 5.6
*
* @category  PHP
* @package   Teerapuch
* @author    Teerapuch Kassakul <teerapuch@hotmail.com>
* @copyright 2016 Teerapuch
* @license   http://opensource.org/licenses/mit-license.php MIT License
* @link      https://teerapuch.com
*/
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* Parses and verifies the doc comments for functions.
*
* @category  PHP
* @package   Teerapuch
* @author    Teerapuch Kassakul <teerapuch@hotmail.com>
* @copyright 2016 Teerapuch
* @license   http://opensource.org/licenses/mit-license.php MIT License
* @link      https://teerapuch.com
*/
class Home extends MY_Controller
{
    /**
    * Main construct function and variables to use in all class
    *
    * @return session is_logged_in_admin
    */
    public function __construct()
    {
        parent::__construct();
        // Verify Admin Login
        // is_logged_in_admin();
    }
    /**
    * A summary informing the user what the associated element does.
    *
    * and to provide some background information or textual references.
    *
    * @return void
    */
    public function index()
    {

        
        $title = 'Green Spot Co.,Ltd บริษัท กรีนสปอต จำกัด : ไวตามิ้ลค์ วีซอย กรีนสปอร์ต นมถั่วเหลือง อาหารเพื่อสุขภาพ';
        $style = array(
            "styles/home.css",
        );
        $script = array(
            "front/home.js",
        );

        $hero      = $this->Hero_model->findByHeroHome();
        $promotion = $this->Promotion_model->findByPromotion();
        $tvc       = $this->Promotion_model->findByTVC();
        $contents   = $this->Content_model->findByPageOne();
        $footer    = $this->Banner_model->findByPageOne();
        $social    = $this->Social_model->findBySocial();
        $intro  =   $this->Intro_model->findByIntro();
        $data = array(
            'title'         => $title,
            'style'         => $style,
            'script'        => $script,
            'hero'          => $hero,
            'promotion'     => $promotion,
            'tvc'           => $tvc,
            'contents'      => $contents,
            'footer'        => $footer,
            'social'        => $social,
            'intro' => $intro
        );
        $this->load->view('header', $data);
        $this->load->view('home', $data);
        $this->load->view('footer', $data);
    }

}
