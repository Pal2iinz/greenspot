<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$title = 'Green Spot Co.,Ltd บริษัท กรีนสปอต จำกัด : ไวตามิ้ลค์ วีซอย กรีนสปอร์ต นมถั่วเหลือง อาหารเพื่อสุขภาพ';
        $style = array(
            "styles/carts.css",
        );
        $script = array(
        	"front/product/vitamilk.js",
        );
		$hero      = $this->Hero_model->findByHeroVitamilk();
        $product   = $this->Product_model->findByVitamilk();
        $contents   = $this->Content_model->findByPageThree();
        $healthy    = $this->Blog_model->findByHealthy();
        $data = array(
            'title' => $title,
            'style' => $style,
            'script' => $script,
            'hero'   => $hero,
            'product' => $product,
            'contents' =>$contents,
            'healthy'  => $healthy,
        );
		$this->load->view('header', $data);
		$this->load->view('order/orderslist', $data);
		$this->load->view('footer', $data);
	}

    public function vitamilk()
    {
        $title = 'Green Spot Co.,Ltd บริษัท กรีนสปอต จำกัด : ไวตามิ้ลค์ วีซอย กรีนสปอร์ต นมถั่วเหลือง อาหารเพื่อสุขภาพ';
        $style = array(
            "styles/order/vitamilk.css",
        );
        $script = array(
            "front/product.js",
            "front/order/vitamilk.js",
        );
        $hero      = $this->Hero_model->findByHeroVitamilk();
        $product   = $this->Product_model->findByVitamilk();
        $contents   = $this->Content_model->findByPageThree();
        $healthy    = $this->Blog_model->findByHealthy();
        $data = array(
            'title' => $title,
            'style' => $style,
            'script' => $script,
            'hero'   => $hero,
            'product' => $product,
            'contents' =>$contents,
            'healthy'  => $healthy,
        );
        $this->load->view('header', $data);
        $this->load->view('order/vitamilk', $data);
        $this->load->view('footer', $data);
    }

    public function vsoy()
    {
        $title = 'Green Spot Co.,Ltd บริษัท กรีนสปอต จำกัด : ไวตามิ้ลค์ วีซอย กรีนสปอร์ต นมถั่วเหลือง อาหารเพื่อสุขภาพ';
        $style = array(
            "styles/order/vsoy.css",
        );
        $script = array(
        "front/product.js",
        "front/order/vsoy.js",
        );
        
        $hero      = $this->Hero_model->findByHeroVsoy();
        $contents   = $this->Content_model->findByPageFour();
        $product   = $this->Product_model->findByVsoy();
        $healthy    = $this->Blog_model->findByHealthyMenu();
        $data = array(
            'title' => $title,
            'style' => $style,
            'script' => $script,
            'hero'   => $hero,
            'contents' =>$contents,
            'healthy'  => $healthy,
            'product' => $product,
        );
        //var_dump($product);
        //die();
        $this->load->view('header', $data);
        $this->load->view('order/vsoy', $data);
        $this->load->view('footer', $data);
    }
    public function greenspot()
    {
        $title = 'Green Spot Co.,Ltd บริษัท กรีนสปอต จำกัด : ไวตามิ้ลค์ วีซอย กรีนสปอร์ต นมถั่วเหลือง อาหารเพื่อสุขภาพ';
        $style = array(
            "styles/product/greenspot.css",
        );
        $script = array(
        );
        $hero      = $this->Hero_model->findByHeroGreenspot();
        $contents     = $this->Content_model->findByPageFive();

        $data = array(
            'title' => $title,
            'style' => $style,
            'script'=> $script,
            'hero'           => $hero,
            'contents'          => $contents,
        );
        $this->load->view('header', $data);
        $this->load->view('order/greenspot', $data);
        $this->load->view('footer', $data);
    }

	

}
