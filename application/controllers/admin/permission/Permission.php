<?php
/**
* Parses and verifies the doc comments for file.
*
* PHP version 5.6
*
* @category  PHP
* @package   Digiday
* @author    Teerapuch Kassakul <teerapuch@hotmail.com>
* @copyright 2016 Digiday
* @license   http://opensource.org/licenses/mit-license.php MIT License
* @link      http://teerapuch.com
*/
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* Parses and verifies the doc comments for functions.
*
* @category  PHP
* @package   Digiday
* @author    Teerapuch Kassakul <teerapuch@hotmail.com>
* @copyright 2016 Digiday
* @license   http://opensource.org/licenses/mit-license.php MIT License
* @link      http://teerapuch.com
*/
class Permission extends MY_Controller
{
    /**
    * Main construct function and variables to use in all class
    *
    * @return session is_logged_in_admin
    */
    public function __construct()
    {
        parent::__construct();
        // Verify Admin Login
        is_logged_in_admin();
        // Verify Department User
        checkDep(1);
    }
    /**
    * First Page for show user to management Permission
    *
    * @return void
    */
    public function index()
    {
        $this->data['sTitle'] = 'Permission Management';
        $aCustomCss = array(
            "assets/css/admin/permission/style.css",
            "assets/plugins/datatable/css/bootstrap.datatable.min.css",
            "assets/plugins/datatable/css/jquery.dataTables.css",
        );
        $aCustomJs = array(
            'assets/js/bootstrap/modal.js',
            "assets/plugins/datatable/js/jquery.dataTables.js",
            "assets/plugins/datatable/js/bootstrap.datatable.js",
            "assets/js/app/admin/permission/custom.js"
        );
        $this->admin_css($aCustomCss);
        $this->admin_js($aCustomJs);
        $this->data['permission'] = $this->permission_model->findAll();
        $this->data['departments'] = $this->department_model->findAll();
        $this->admin_layout("permission/index", $this->data);
    }
    /**
    * save permission form data to database
    *
    * @param int $id role Id For Edit
    * @return true
    * @return msg
    */
    public function save($id = null)
    {
        $id = $this->security->xss_clean(
            $this->input->post("id")
        );
        $department = $this->security->xss_clean(
            $this->input->post("department")
        );
        $permission = $this->security->xss_clean(
            $this->input->post("permission")
        );
        if (empty($id)) {
            // Prepare Data To Array
            $data = array(
                "department" => $department,
                "permission" => $permission,
                "updatedate" => $this->dateTimeNow,
                "createdate" => $this->dateTimeNow
            );
            $save = $this->permission_model->save($data);
            // redirect with session msessage
            $this->session->set_flashdata(
                'msg-success',
                'success'
            );
            redirect($this->agent->referrer());
        } else {
            // Prepare Data To Array
            $data = array(
                "department" => $department,
                "permission" => $permission,
                "updatedate" => $this->dateTimeNow
            );
            $save = $this->permission_model->update($id, $data);
            // redirect with session msessage
            $this->session->set_flashdata(
                'msg-success',
                'success'
            );
            redirect($this->agent->referrer());
        }
    }
    /**
    * soft delete permission from database
    *
    * @param int $id id permission form index
    *
    * @return msg
    */
    public function delete($id)
    {
        if (isset($_POST['id'])) {
            // set id
            $id = $this->security->xss_clean(
                $this->input->post("id")
            );
            // check is numeric
            if (is_numeric($id)) {
                // update soft delete
                $this->permission_model->delete($id);
                // redirect with session msessage
                $this->session->set_flashdata('msg-success', 'success');
            } else {
                $this->session->set_flashdata('msg-danger', 'error deleteing');
                redirect($this->agent->referrer());
            } // end else
        } else {
            redirect($this->agent->referrer());
        } // end else
    }
}
