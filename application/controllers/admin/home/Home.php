<?php
/**
* Parses and verifies the doc comments for file.
*
* PHP version 5.6
*
* @category  PHP
* @package   Digiday
* @author    Teerapuch Kassakul <teerapuch@hotmail.com>
* @copyright 2016 Digiday
* @license   http://opensource.org/licenses/mit-license.php MIT License
* @link      http://teerapuch.com
*/
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* Parses and verifies the doc comments for functions.
*
* @category  PHP
* @package   Digiday
* @author    Teerapuch Kassakul <teerapuch@hotmail.com>
* @copyright 2016 Digiday
* @license   http://opensource.org/licenses/mit-license.php MIT License
* @link      http://teerapuch.com
*/
class Home extends MY_Controller
{
    /**
    * Main construct function and variables to use in all class
    *
    * @return session is_logged_in_admin
    */
    public function __construct()
    {
        parent::__construct();
        // Verify Admin Login
        is_logged_in_admin();
        // Verify Department User
        //checkDep(1);
    }
    /**
    * First Page for show user to management department
    *
    * @return void
    */
    public function index()
    {
        $this->data['sTitle'] = 'Dashboard';
        $aCustomCss = array(
            "assets/css/admin/department/index/style.css",
            "assets/plugins/datatable/css/bootstrap.datatable.min.css",
            "assets/plugins/datatable/css/jquery.dataTables.css",
        );
        $aCustomJs = array(
            'assets/js/bootstrap/modal.js',
            "assets/plugins/datatable/js/jquery.dataTables.js",
            "assets/plugins/datatable/js/bootstrap.datatable.js",
            "assets/js/app/admin/department/index/custom.js"
        );
        $this->admin_css($aCustomCss);
        $this->admin_js($aCustomJs);
        $this->data['user'] = $this->User_model->findAll();
        $this->admin_layout("home/index", $this->data);
    }
    /**
    * save department form data to database
    *
    * @param int $id Department Id For Edit
    * @return true
    * @return msg
    */
    public function save($id = null)
    {
        $id = $this->security->xss_clean(
            $this->input->post("id")
        );
        $department = $this->security->xss_clean(
            $this->input->post("department")
        );
        if (empty($id)) {
            // Prepare Data To Array
            $data = array(
                "department" => $department,
                "updatedate" => $this->dateTimeNow,
                "createdate" => $this->dateTimeNow
            );
            $save = $this->Department_model->save($data);
            // redirect with session msessage
            $this->session->set_flashdata(
                'msg-success',
                'success'
            );
            redirect($this->agent->referrer());
        } else {
            // Prepare Data To Array
            $data = array(
                "department" => $department,
                "updatedate" => $this->dateTimeNow
            );
            $save = $this->Department_model->update($id, $data);
            // redirect with session msessage
            $this->session->set_flashdata(
                'msg-success',
                'success'
            );
            redirect($this->agent->referrer());
        }
    }
    /**
    * soft delete department from database
    *
    * @param int $id id department form index
    *
    * @return msg
    */
    public function delete($id)
    {
        if (isset($_POST['id'])) {
            // set id
            $id = $this->security->xss_clean(
                $this->input->post("id")
            );
            // check is numeric
            if (is_numeric($id)) {
                // update soft delete
                $this->Department_model->delete($id);
                // redirect with session msessage
                $this->session->set_flashdata('msg-success', 'success');
            } else {
                $this->session->set_flashdata('msg-danger', 'error deleteing');
                redirect($this->agent->referrer());
            } // end else
        } else {
            redirect($this->agent->referrer());
        } // end else

    }

}
