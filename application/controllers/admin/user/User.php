<?php
/**
* Parses and verifies the doc comments for file.
*
* PHP version 5.6
*
* @category  PHP
* @package   Digiday
* @author    Teerapuch Kassakul <teerapuch@hotmail.com>
* @copyright 2016 Digiday
* @license   http://opensource.org/licenses/mit-license.php MIT License
* @link      http://www.digiday.co.th
*/
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* Parses and verifies the doc comments for functions.
*
* @category  PHP
* @package   Digiday
* @author    Teerapuch Kassakul <teerapuch@hotmail.com>
* @copyright 2016 Digiday
* @license   http://opensource.org/licenses/mit-license.php MIT License
* @link      http://www.digiday.co.th
*/
class User extends MY_Controller
{
    /**
    * Main construct function and variables to use in all class
    *
    * @return session is_logged_in_admin
    */
    public function __construct()
    {
        parent::__construct();
        // Verify Admin Login
        is_logged_in_admin();
        // Verify Department User
        checkDep(1);
    }
    /**
    * A summary informing the user what the associated element does.
    *
    * and to provide some background information or textual references.
    *
    * @return void
    */
    public function index()
    {
        $this->data['sTitle'] = 'User Management';
        $aCustomCss = array(
            "assets/plugins/datatable/css/bootstrap.datatable.min.css",
            "assets/plugins/datatable/css/jquery.dataTables.css",
            "assets/css/admin/user/index/style.css",
        );
        $aCustomJs = array(
            "assets/plugins/datatable/js/jquery.dataTables.js",
            "assets/plugins/datatable/js/bootstrap.datatable.js",
            "assets/js/app/admin/user/custom.js",
        );
        $this->admin_css($aCustomCss);
        $this->admin_js($aCustomJs);
        $this->data['user'] = $this->User_model->findAll();
        $this->data['role'] = $this->Role_model->findAll();
        $this->data['department'] = $this->Department_model->findAll();
        $this->admin_layout("user/index", $this->data);
    }
    /**
    * A summary informing the user what the associated element does.
    *
    * and to provide some background information or textual references.
    *
    * @return void
    */
    public function view()
    {
        $id = $this->uri->segment(4);
        $this->data['sTitle'] = 'View User Management';
        $aCustomCss = array(
            "assets/css/admin/user/index/style.css",
        );
        $this->admin_css($aCustomCss);
        $this->data['user'] = $this->User_model->findById($id);
        $this->data['role'] = $this->Role_model->findAll();
        $this->data['department'] = $this->Department_model->findAll($id);
        $this->admin_layout("user/view", $this->data);
    }
    /**
    * view form for create user informantion
    *
    * @return true
    */
    public function form()
    {
        $id = $this->uri->segment(4);
        $aCustomJs = array("assets/js/app/admin/user/form.js");
        $this->admin_js($aCustomJs);
        if (isset($id)) {
            $this->data['user'] = $this->User_model->findById($id);
        }
        $this->data['sTitle'] = 'Form User';
        $this->data['roles'] = $this->Role_model->findAll();
        $this->data['departments'] = $this->Department_model->findAll();
        $this->admin_layout("user/form", $this->data);
    }
    /**
    * validate data and save user data to database
    *
    * @return true
    */
    public function save()
    {
        $this->form_validation->set_rules(
            'firstname',
            'Firstname',
            'trim|required|xss_clean'
        );
        $this->form_validation->set_rules(
            'lastname',
            'Lastname',
            'trim|required|xss_clean'
        );
        $this->form_validation->set_rules(
            'email',
            'Email',
            'trim|required|xss_clean'
        );
        if (empty($this->input->post('id'))) {
            $this->form_validation->set_rules(
                'password',
                'Password',
                'trim|required|min_length[6]|xss_clean'
            );
        }
        if ($this->form_validation->run() == false) {
            $this->data['roles'] = $this->Role_model->findAll();
            $this->data['departments'] = $this->Department_model->findAll();
            $aCustomJs = array("assets/js/app/admin/user/form.js");
            $aCustomCss = array("assets/css/admin/user/form.css");
            $this->admin_js($aCustomJs);
            $this->admin_css($aCustomCss);
            $this->admin_layout("user/form",$this->data);
        } else {
            $id = $this->security->xss_clean(
                $this->input->post("id")
            );
            $options = ['cost' => 12];
            $crypt = $this->security->xss_clean($this->input->post('password'));
            $password = password_hash($crypt, PASSWORD_BCRYPT, $options);
            if (empty($id)) {
                // Prepare Data To Array
                $data = array(
                    "firstname" =>
                    $this->security->xss_clean(
                        $this->input->post("firstname")
                    ),
                    "lastname" => $this->security->xss_clean(
                        $this->input->post("lastname")
                    ),
                    "email" => $this->security->xss_clean(
                        $this->input->post("email")
                    ),
                    "password" => $password,
                    "department" => $this->security->xss_clean(
                        $this->input->post("department")
                    ),
                    "role" => $this->security->xss_clean(
                        $this->input->post("role")
                    ),
                    "updatedate" => $this->dateTimeNow,
                    "createdate" => $this->dateTimeNow
                );
                $save = $this->User_model->save($data);
                // redirect with session msessage
                $this->session->set_flashdata('msg-success', 'success');
                redirect('admin/user');
            } else {
                // Prepare Data To Array
                $data = array(
                    "firstname" => $this->security->xss_clean(
                        $this->input->post("firstname")
                    ),
                    "lastname" => $this->security->xss_clean(
                        $this->input->post("lastname")
                    ),
                    "email" => $this->security->xss_clean(
                        $this->input->post("email")
                    ),
                    "department" => $this->security->xss_clean(
                        $this->input->post("department")
                    ),
                    "role" => $this->security->xss_clean(
                        $this->input->post("role")
                    ),
                    "updatedate" => $this->dateTimeNow,
                    "createdate" => $this->dateTimeNow
                );
                $save = $this->User_model->update($id, $data);
                // redirect with session msessage
                $this->session->set_flashdata('msg-success', 'success');
                redirect('admin/user');
            } // end else empty id
        } // end else form validation == true
    }
    /**
    * soft delete department from database
    *
    * @param int $id id department form index
    *
    * @return msg
    */
    public function delete($id)
    {
        if (isset($_POST['id'])) {
            // set id
            $id = $this->security->xss_clean($_POST['id']);
            // check is numeric
            if (is_numeric($id)) {
                // update soft delete
                $this->User_model->delete($id);
                // redirect with session msessage
                $this->session->set_flashdata('msg-success', 'success');
            } else {
                $this->session->set_flashdata('msg-danger', 'error deleteing');
                redirect($this->agent->referrer());
            } // end else
        } else {
            redirect($this->agent->referrer());
        } // end else
    }

}
