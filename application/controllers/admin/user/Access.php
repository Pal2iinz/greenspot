<?php
/**
* Parses and verifies the doc comments for file.
*
* PHP version 5.6
*
* @category  PHP
* @package   Digiday
* @author    Teerapuch Kassakul <teerapuch@hotmail.com>
* @copyright 2016 Digiday
* @license   http://opensource.org/licenses/mit-license.php MIT License
* @link      http://teerapuch.com
*/
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* Parses and verifies the doc comments for functions.
*
* @category  PHP
* @package   Digiday
* @author    Teerapuch Kassakul <teerapuch@hotmail.com>
* @copyright 2016 Digiday
* @license   http://opensource.org/licenses/mit-license.php MIT License
* @link      http://teerapuch.com
*/
class Access extends MY_Controller
{
    /**
    * Main construct function and variables to use in all class
    *
    * @return session is_logged_in_admin
    */
    public function __construct()
    {
        parent::__construct();
        // Verify Admin Login
        is_logged_in_admin();
    }
    /**
    * First Page for show user to management department
    *
    * @return void
    */
    public function index()
    {
        $id = $this->uri->segment(4);
        $this->data['sTitle'] = 'Access Management';
        $aCustomCss = array(
            "assets/plugins/datatable/css/bootstrap.datatable.min.css",
            "assets/plugins/datatable/css/jquery.dataTables.css",
            "assets/css/admin/access/style.css",
        );
        $aCustomJs = array(
            'assets/js/bootstrap/modal.js',
            "assets/plugins/datatable/js/jquery.dataTables.js",
            "assets/plugins/datatable/js/bootstrap.datatable.js",
            "assets/js/app/admin/access/custom.js",
        );
        $this->admin_css($aCustomCss);
        $this->admin_js($aCustomJs);
        $this->data['user'] = $this->user_model->findById($id);
        //var_dump($this->data['user']);
        //die();
        $dep = $this->data['user'][0]->department;
        $this->data['permission'] = $this->permission_model->findByDep($dep);
        $this->data['dep'] = $this->department_model->findAll();
        $this->data['roles'] = $this->role_model->findAll();
        $this->data['access'] = $this->access_model->findByUser($id);
        $this->admin_layout("access/index", $this->data);
    }
    /**
    * save access form data to database
    *
    * @param int $id access Id For Edit
    * @return true
    * @return msg
    */
    public function save($id = null)
    {
        $id = $this->security->xss_clean(
            $this->input->post("id")
        );
        // Prepare Data To Array
        $temp = array();
        foreach($this->input->post('perm') as $key => $prem_id) {
            $temp[] = array(
                'user' => $id,
                'perm' => $key,
                'access' => $prem_id,
            );
        } // end foreach
        // rearrage array for save data
        foreach ($temp as $key => $value) {
            $data = array(
                'user' => $value['user'],
                'perm' => $value['perm'],
                'access' => $value['access'],
                "updatedate" => $this->dateTimeNow,
                "createdate" => $this->dateTimeNow
            );
            // find exits in access if have user and perm true
            $perm = $this->access_model->findByUserPerm(
                $value['user'],
                $value['perm']
            );
            if (count($perm) === 1) {
                // update to db
                $update = $this->access_model->update(
                    $value['user'],
                    $value['perm'],
                    $data
                );
            } else {
                // save to db
                $save = $this->access_model->save($data);
            } // end else
        } // end foreach
        redirect('admin/user/');
    }
}
