<?php
/**
* Parses and verifies the doc comments for file.
*
* PHP version 5.6
*
* @category  PHP
* @package   Digiday
* @author    Teerapuch Kassakul <teerapuch@hotmail.com>
* @copyright 2016 Digiday
* @license   http://opensource.org/licenses/mit-license.php MIT License
* @link      http://teerapuch.com
*/
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* Parses and verifies the doc comments for functions.
*
* @category  PHP
* @package   Digiday
* @author    Teerapuch Kassakul <teerapuch@hotmail.com>
* @copyright 2016 Digiday
* @license   http://opensource.org/licenses/mit-license.php MIT License
* @link      http://teerapuch.com
*/
class Role extends MY_Controller
{
    /**
    * Main construct function and variables to use in all class
    *
    * @return session is_logged_in_admin
    */
    public function __construct()
    {
        parent::__construct();
        // Verify Admin Login
        is_logged_in_admin();
        // Verify Department User
        checkDep(1);
    }
    /**
    * First Page for show user to management department
    *
    * @return void
    */
    public function index()
    {
        $this->data['sTitle'] = 'Role Management';
        $aCustomCss = array(
            "assets/css/admin/role/style.css",
            "assets/plugins/datatable/css/bootstrap.datatable.min.css",
            "assets/plugins/datatable/css/jquery.dataTables.css",
        );
        $aCustomJs = array(
            'assets/js/bootstrap/modal.js',
            "assets/plugins/datatable/js/jquery.dataTables.js",
            "assets/plugins/datatable/js/bootstrap.datatable.js",
            "assets/js/app/admin/role/custom.js"
        );
        $this->admin_css($aCustomCss);
        $this->admin_js($aCustomJs);
        $this->data['role'] = $this->role_model->findAll();
        $this->admin_layout("role/index", $this->data);
    }
    /**
    * save role form data to database
    *
    * @param int $id role Id For Edit
    * @return true
    * @return msg
    */
    public function save($id = null)
    {
        $id = $this->security->xss_clean(
            $this->input->post("id")
        );
        $role = $this->security->xss_clean(
            $this->input->post("role")
        );
        if (empty($id)) {
            // Prepare Data To Array
            $data = array(
                "role" => $role,
                "updatedate" => $this->dateTimeNow,
                "createdate" => $this->dateTimeNow
            );
            $save = $this->role_model->save($data);
            // redirect with session msessage
            $this->session->set_flashdata(
                'msg-success',
                'success'
            );
            redirect($this->agent->referrer());
        } else {
            // Prepare Data To Array
            $data = array(
                "role" => $role,
                "updatedate" => $this->dateTimeNow
            );
            $save = $this->role_model->update($id, $data);
            // redirect with session msessage
            $this->session->set_flashdata(
                'msg-success',
                'success'
            );
            redirect($this->agent->referrer());
        }
    }
    /**
    * soft delete role from database
    *
    * @param int $id id role form index
    *
    * @return msg
    */
    public function delete($id)
    {
        if (isset($_POST['id'])) {
            // set id
            $id = $this->security->xss_clean(
                $this->input->post("id")
            );
            // check is numeric
            if (is_numeric($id)) {
                // update soft delete
                $this->role_model->delete($id);
                // redirect with session msessage
                $this->session->set_flashdata('msg-success', 'success');
            } else {
                $this->session->set_flashdata('msg-danger', 'error deleteing');
                redirect($this->agent->referrer());
            } // end else
        } else {
            redirect($this->agent->referrer());
        } // end else
    }
}
