<?php
/**
* Parses and verifies the doc comments for file.
*
* PHP version 5.6
*
* @category  PHP
* @package   Teerapuch
* @author    Teerapuch Kassakul <teerapuch@hotmail.com>
* @copyright 2016 Teerapuch
* @license   http://opensource.org/licenses/mit-license.php MIT License
* @link      https://teerapuch.com
*/
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* Parses and verifies the doc comments for functions.
*
* @category  PHP
* @package   Teerapuch
* @author    Teerapuch Kassakul <teerapuch@hotmail.com>
* @copyright 2016 Teerapuch
* @license   http://opensource.org/licenses/mit-license.php MIT License
* @link      https://teerapuch.com
*/
class Healthy extends MY_Controller
{
    /**
    * Main construct function and variables to use in all class
    *
    * @return session is_logged_in_admin
    */
    public function __construct()
    {
        parent::__construct();
        // Verify Admin Login
        // is_logged_in_admin();
    }
    /**
    * A summary informing the user what the associated element does.
    *
    * and to provide some background information or textual references.
    *
    * @return void
    */
    public function index()
    {
        $title = 'Green Spot Co.,Ltd บริษัท กรีนสปอต จำกัด : ไวตามิ้ลค์ วีซอย กรีนสปอร์ต นมถั่วเหลือง อาหารเพื่อสุขภาพ';
        $style = array(
            "layouts/front/healthy.css",
            "layouts/front/component.css",
            "bootstrap/bootstrap_about.css",
            "layouts/front/nav.css",
        );
        $script = array(
            "jquery.min.js",
            "bootstrap.min.js",
            "app/front/global.js",
            "app/front/library.js",
            "app/front/healthy.js",
            "app/front/google-analytics.js",
        );
        $hero       = $this->Hero_model->findByHero();
        $healthy    = $this->Blog_model->findByHealthy();
        $categories = $this->Categories_model->findAll();

        $data = array(
            'title'       => $title,
            'style'       => $style,
            'script'      => $script,
            'healthy'     => $healthy,
            'hero'        => $hero,
            'categories'  => $categories ,
        );
        $this->load->view('front/header', $data);
        $this->load->view('front/healthy', $data);
        $this->load->view('front/footer', $data);
    }

    public function menu()
    {
         $title = 'Green Spot Co.,Ltd บริษัท กรีนสปอต จำกัด : ไวตามิ้ลค์ วีซอย กรีนสปอร์ต นมถั่วเหลือง อาหารเพื่อสุขภาพ';
        $style = array(
            "layouts/front/healthy-menu.css",
            "layouts/front/component.css",
            "bootstrap/bootstrap_about.css",
            "layouts/front/nav.css",
        );
        $script = array(
            "jquery.min.js",
            "bootstrap.min.js",
            "app/front/global.js",
            "app/front/library.js",
            "app/front/healthy.js",
            "app/front/google-analytics.js",
        );
        $hero       = $this->Hero_model->findByHero();
        $healthy    = $this->Blog_model->findByHealthyMenu();
        $categories = $this->Categories_model->findAll();
        $data = array(
            'title'      => $title,
            'style'      => $style,
            'script'     => $script,
            'healthy'    => $healthy,
            'hero'       => $hero,
            'categories' => $categories ,
        );
        $this->load->view('front/header', $data);
        $this->load->view('front/healthy-menu', $data);
        $this->load->view('front/footer', $data);
    }

    public function formhealthy()
    {
     $id = $this->uri->segment(2);
     $title = 'Green Spot Co.,Ltd บริษัท กรีนสปอต จำกัด : ไวตามิ้ลค์ วีซอย กรีนสปอร์ต นมถั่วเหลือง อาหารเพื่อสุขภาพ';
        $style = array(
            "layouts/front/healthy.css",
            "layouts/front/component.css",
            "bootstrap/bootstrap_about.css",
            "layouts/front/nav.css",
        );
        $script = array(
            "jquery.min.js",
            "bootstrap.min.js",
            "app/front/global.js",
            "app/front/library.js",
            "app/front/healthy.js",
            "app/front/google-analytics.js",
        );

        if (!isset($id)) {
            show_404();
        } else {
            $healthy = $this->Blog_model->findByIdHealthy($id);
        }
        if (empty($healthy)) {
            show_404();
        }
        $hero = $this->Hero_model->findByHero();
        $data = array(
            'title'   => $title,
            'style'   => $style,
            'script'  => $script,
            'hero'    => $hero,
            'healthy' => $healthy
        );
        $this->load->view('front/header', $data);
        $this->load->view('front/healthy/form', $data);
        $this->load->view('front/footer', $data);
    }

    public function formmenu()
    {
     $id = $this->uri->segment(2);
     $title = 'Green Spot Co.,Ltd บริษัท กรีนสปอต จำกัด : ไวตามิ้ลค์ วีซอย กรีนสปอร์ต นมถั่วเหลือง อาหารเพื่อสุขภาพ';
        $style = array(
            "layouts/front/healthy-menu.css",
            "layouts/front/component.css",
            "bootstrap/bootstrap_about.css",
            "layouts/front/nav.css",
        );
        $script = array(
            "jquery.min.js",
            "bootstrap.min.js",
            "app/front/global.js",
            "app/front/library.js",
            "app/front/healthy.js",
            "app/front/google-analytics.js",
        );
        if (!isset($id)) {
            show_404();
        } else {
            $healthy = $this->Blog_model->findByIdMenu($id);
        }
        $hero = $this->Hero_model->findByHero();
        $data = array(
            'title'   => $title,
            'style'   => $style,
            'script'  => $script,
            'hero'    => $hero,
            'healthy' => $healthy
        );
        $this->load->view('front/header', $data);
        $this->load->view('front/healthy/form', $data);
        $this->load->view('front/footer', $data);
    }
}
