<?php
/**
* Parses and verifies the doc comments for file.
*
* PHP version 5.6
*
* @category  PHP
* @package   Teerapuch
* @author    Teerapuch Kassakul <teerapuch@hotmail.com>
* @copyright 2016 Teerapuch
* @license   http://opensource.org/licenses/mit-license.php MIT License
* @link      https://teerapuch.com
*/
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* Parses and verifies the doc comments for functions.
*
* @category  PHP
* @package   Teerapuch
* @author    Teerapuch Kassakul <teerapuch@hotmail.com>
* @copyright 2016 Teerapuch
* @license   http://opensource.org/licenses/mit-license.php MIT License
* @link      https://teerapuch.com
*/
class About extends MY_Controller
{
    /**
    * Main construct function and variables to use in all class
    *
    * @return session is_logged_in_admin
    */
    public function __construct()
    {
        parent::__construct();
        // Verify Admin Login
        // is_logged_in_admin();
    }
    /**
    * A summary informing the user what the associated element does.
    *
    * and to provide some background information or textual references.
    *
    * @return void
    */
    public function index()
    {
        $title = 'Green Spot Co.,Ltd บริษัท กรีนสปอต จำกัด : ไวตามิ้ลค์ วีซอย กรีนสปอร์ต นมถั่วเหลือง อาหารเพื่อสุขภาพ';
        $style = array(
            "layouts/front/about.css",
            "layouts/front/component.css",
            "bootstrap/bootstrap_about.css",
            "layouts/front/nav.css",
        );
        $script = array(
            "jquery.min.js",
            "bootstrap.min.js",
            "app/front/global.js",
            "app/front/library.js",
            "app/front/google-analytics.js",
        );
        $hero      = $this->Hero_model->findByHero();
        $contents     = $this->Content_model->findByPageTwo();

        $data = array(
            'title'          => $title,
            'style'          => $style,
            'script'         => $script,
            'hero'           => $hero,
            'contents'          => $contents,
        );
        $this->load->view('front/header', $data);
        $this->load->view('front/about', $data);
        $this->load->view('front/footer', $data);
    }

}
