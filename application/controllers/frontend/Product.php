<?php
/**
* Parses and verifies the doc comments for file.
*
* PHP version 5.6
*
* @category  PHP
* @package   Teerapuch
* @author    Teerapuch Kassakul <teerapuch@hotmail.com>
* @copyright 2016 Teerapuch
* @license   http://opensource.org/licenses/mit-license.php MIT License
* @link      https://teerapuch.com
*/
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* Parses and verifies the doc comments for functions.
*
* @category  PHP
* @package   Teerapuch
* @author    Teerapuch Kassakul <teerapuch@hotmail.com>
* @copyright 2016 Teerapuch
* @license   http://opensource.org/licenses/mit-license.php MIT License
* @link      https://teerapuch.com
*/
class Product extends MY_Controller
{
    /**
    * Main construct function and variables to use in all class
    *
    * @return session is_logged_in_admin
    */
    public function __construct()
    {
        parent::__construct();
        // Verify Admin Login
        // is_logged_in_admin();
    }
    /**
    * A summary informing the user what the associated element does.
    *
    * and to provide some background information or textual references.
    *
    * @return void
    */
    public function index()
    {
        $title = 'Green Spot Co.,Ltd บริษัท กรีนสปอต จำกัด : ไวตามิ้ลค์ วีซอย กรีนสปอร์ต นมถั่วเหลือง อาหารเพื่อสุขภาพ';
        $style = array(
            "layouts/front/vitamilk.css",
            "layouts/front/component.css",
            "bootstrap/bootstrap_about.css",
            "layouts/front/nav.css",
        );
        $script = array(
            "jquery.min.js",
            "bootstrap.min.js",
            "app/front/global.js",
            "app/front/library.js",
            "app/front/vitamilk.js",
            "app/front/google-analytics.js",
        );
        $hero      = $this->Hero_model->findByHero();
        $product   = $this->Product_model->findByVitamilk();
        $contents   = $this->Content_model->findByPageThree();
        $healthy    = $this->Blog_model->findByHealthy();
        $data = array(
            'title' => $title,
            'style' => $style,
            'script' => $script,
            'hero'   => $hero,
            'product' => $product,
            'contents' =>$contents,
            'healthy'  => $healthy,
        );
        $this->load->view('front/header', $data);
        $this->load->view('front/vitamilk', $data);
        $this->load->view('front/footer', $data);
    }

    public function vsoy()
    {
        $title = 'Green Spot Co.,Ltd บริษัท กรีนสปอต จำกัด : ไวตามิ้ลค์ วีซอย กรีนสปอร์ต นมถั่วเหลือง อาหารเพื่อสุขภาพ';
        $style = array(
            "layouts/front/vsoy.css",
            "layouts/front/component.css",
            "bootstrap/bootstrap_about.css",
            "layouts/front/nav.css",
        );
        $script = array(
            "jquery.min.js",
            "bootstrap.min.js",
            "app/front/global.js",
            "app/front/library.js",
            "app/front/vsoy.js",
            "app/front/google-analytics.js",
        );
        $hero      = $this->Hero_model->findByHero();
        $contents   = $this->Content_model->findByPageFour();
        $product   = $this->Product_model->findByVsoy();
        $healthy    = $this->Blog_model->findByHealthyMenu();
        $data = array(
            'title' => $title,
            'style' => $style,
            'script' => $script,
            'hero'   => $hero,
             'contents' =>$contents,
             'healthy'  => $healthy,
             'product' => $product,
        );

        $this->load->view('front/header', $data);
        $this->load->view('front/vsoy', $data);
        $this->load->view('front/footer', $data);
    }

    public function greenspot()
    {
        $title = 'Green Spot Co.,Ltd บริษัท กรีนสปอต จำกัด : ไวตามิ้ลค์ วีซอย กรีนสปอร์ต นมถั่วเหลือง อาหารเพื่อสุขภาพ';
        $style = array(
            "layouts/front/greenspot.css",
            "layouts/front/component.css",
            "bootstrap/bootstrap_about.css",
            "layouts/front/nav.css",
        );
        $script = array(
            "jquery.min.js",
            "bootstrap.min.js",
            "app/front/global.js",
            "app/front/library.js",
            "app/front/vsoy.js",
            "app/front/google-analytics.js",
        );
        $hero      = $this->Hero_model->findByHero();
        $product   = $this->Product_model->findByGreenspot();
         $contents   = $this->Content_model->findByPageFive();
        $data = array(
            'title' => $title,
            'style' => $style,
            'script' => $script,
            'hero'   => $hero,
             'contents' =>$contents,
        );
        $this->load->view('front/header', $data);
        $this->load->view('front/greenspot', $data);
        $this->load->view('front/footer', $data);
    }



}
