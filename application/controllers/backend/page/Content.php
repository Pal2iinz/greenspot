<?php
/**
* Parses and verifies the doc comments for file.
*
* PHP version 5.6
*
* @category  PHP
* @package   Teerapuch
* @author    Teerapuch Kassakul <teerapuch@hotmail.com>
* @copyright 2016 Teerapuch
* @license   http://opensource.org/licenses/mit-license.php MIT License
* @link      https://teerapuch.com
*/
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* Parses and verifies the doc comments for functions.
*
* @category  PHP
* @package   Teerapuch
* @author    Teerapuch Kassakul <teerapuch@hotmail.com>
* @copyright 2016 Teerapuch
* @license   http://opensource.org/licenses/mit-license.php MIT License
* @link      https://teerapuch.com
*/
class Content extends MY_Controller
{
    /**
    * Main construct function and variables to use in all class
    *
    * @return session is_logged_in_admin
    */
    public function __construct()
    {
        parent::__construct();
        // Verify Admin Login
        is_logged_in_admin();
    }
    /**
    * A summary informing the user what the associated element does.
    *
    * and to provide some background information or textual references.
    *
    * @return void
    */
    public function index()
    {
        $this->data['sTitle'] = 'Content Management';
        $aCustomCss = array(
            'assets/css/backend/pages/content.css',
            "assets/plugins/datatable/css/bootstrap.datatable.min.css",
            "assets/plugins/datatable/css/jquery.dataTables.css",
        );
        $aCustomJs = array(
            "assets/plugins/datatable/js/jquery.dataTables.js",
            "assets/plugins/datatable/js/bootstrap.datatable.js",
            "assets/js/app/backend/pages/content.js",
        );
        $this->main_css($aCustomCss);
        $this->main_js($aCustomJs);
        $this->data['content'] = $this->Content_model->findAll();
        $this->main_layout("backend/content/index", $this->data);
    }
    /**
    * A summary informing the user what the associated element does.
    *
    * and to provide some background information or textual references.
    *
    * @return void
    */
    public function form()
    {
        $id = $this->uri->segment(4);
        $aCustomCss = array(
            'assets/plugins/summernote/summernote.css'
        );
        $aCustomJs = array(
            'assets/plugins/summernote/summernote.js',
            'assets/js/app/backend/pages/contentform.js',
        );
        $this->main_js($aCustomJs);
        $this->main_css($aCustomCss);
        if (isset($id)) {
            $this->data['content'] = $this->Content_model->findById($id);
        }
        $this->data['sTitle'] = 'Form Content';
        $this->main_layout("backend/content/form", $this->data);
    }
    /**
    * save content form data to database
    *
    * @param int $id content Id For Edit
    * @return true
    * @return msg
    */
    public function save($id = null)
    {
      
        $id = $this->security->xss_clean(
            $this->input->post("id")
        );
        $content =   $this->input->post("content");
          $page = $this->security->xss_clean(
            $this->input->post("page")
        );

        // set data to save
        $aData = array(
            "page" => $page,
            "content" => $content,            
            "updatedate" => $this->dateTimeNow,
        );
        //================//
        // set for upload //
        //================//
	        $this->load->library('upload');
	        $config['upload_path']   = 'assets/images/content/';
	        $config['allowed_types'] = 'gif|jpg|jpeg|png|mp4';
	        $config['max_size']      = '3000';
	        $config['max_width']     = '2480';
	        $config['max_height']    = '2480';
	        $config['remove_spaces'] = TRUE;
	        $this->upload->initialize($config);
	        //===================//
	        // Upload File Image Banner
	        //==================//
	        if($this->upload->do_upload('image')) {
	            // upload file and rename success
	            $data = $this->upload->data();
	            $sNewName = 'content_img_'.date("dmYHis").$data['file_ext'];
	            rename($data['full_path'], $data['file_path'].$sNewName);
	            $aData["image"]  = $sNewName;
	        }
         //================//
        // set for upload //
        //================//
	        $config2['upload_path']   = 'assets/images/filedata/';
	       	$config2['allowed_types'] = "jpg|jpeg|gif|png|pdf|docx|xls|xlsx|pptx";
	       	$config2['max_size']      = 500000; // KB
	        $this->upload->initialize($config2);
	        //===================//
	        // Upload FileUpload
	        //==================//
	        if($this->upload->do_upload('fileupload')) {
	            // upload file and rename success
	            $data2 = $this->upload->data();
	            $sFiledata = 'file_data_'.date("dmYHis").$data2['file_ext'];
	            rename($data2['full_path'], $data2['file_path'].$sFiledata);
	            $aData["fileupload"]  = $sFiledata;
	        }
        if (empty($id)) {
            // insert case
            $aData['createdate'] = $this->dateTimeNow;
            $save = $this->Content_model->save($aData);
            // redirect with session msessage
            $this->session->set_flashdata(
                'msg-success',
                'success'
            );
            redirect('backend/content');
        } else {
            // Update case
            $save = $this->Content_model->update($id, $aData);
            // redirect with session msessage
            $this->session->set_flashdata(
                'msg-success',
                'success'
            );
            redirect('backend/content');
        } // end else
    }
    /**
    * soft delete content from database
    *
    * @param int $id id content form index
    *
    * @return msg
    */
    public function delete($id)
    {
        if (isset($_POST['id'])) {
            // set id
            $id = $this->security->xss_clean(
                $this->input->post("id")
            );
            // check is numeric
            if (is_numeric($id)) {
                // update soft delete
                $this->Content_model->delete($id);
                // redirect with session msessage
                $this->session->set_flashdata('msg-success', 'success');
            } else {
                $this->session->set_flashdata('msg-danger', 'error deleteing');
                redirect($this->agent->referrer());
            } // end else
        } else {
            redirect($this->agent->referrer());
        } // end else
    }
    /**
    * soft delete image content from database
    *
    * @param int $id id image content form index
    *
    * @return msg
    */
    public function delimage($id)
    {
        if (isset($_POST['id'])) {
            // set id
            $id = $this->security->xss_clean(
                $this->input->post("id")
            );
            // check is numeric
            if (is_numeric($id)) {
                // update soft delete
                $this->Content_model->deleteimage($id);
                // redirect with session msessage
                $this->session->set_flashdata('msg-success', 'success');
            } else {
                $this->session->set_flashdata('msg-danger', 'error deleteing');
                redirect($this->agent->referrer());
            } // end else
        } else {
            redirect($this->agent->referrer());
        } // end else
    }

    /**
    * soft delete image content from database
    *
    * @param int $id id image content form index
    *
    * @return msg
    */
    public function delfile($id)
    {
        if (isset($_POST['id'])) {
            // set id
            $id = $this->security->xss_clean(
                $this->input->post("id")
            );
            // check is numeric
            if (is_numeric($id)) {
                // update soft delete
                $this->Content_model->deletefile($id);
                // redirect with session msessage
                $this->session->set_flashdata('msg-success', 'success');
            } else {
                $this->session->set_flashdata('msg-danger', 'error deleteing');
                redirect($this->agent->referrer());
            } // end else
        } else {
            redirect($this->agent->referrer());
        } // end else
    }

}
