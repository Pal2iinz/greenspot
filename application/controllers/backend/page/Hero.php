<?php
/**
* Parses and verifies the doc comments for file.
*
* PHP version 5.6
*
* @category  PHP
* @package   Teerapuch
* @author    Teerapuch Kassakul <teerapuch@hotmail.com>
* @copyright 2016 Teerapuch
* @license   http://opensource.org/licenses/mit-license.php MIT License
* @link      https://teerapuch.com
*/
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* Parses and verifies the doc comments for functions.
*
* @category  PHP
* @package   Teerapuch
* @author    Teerapuch Kassakul <teerapuch@hotmail.com>
* @copyright 2016 Teerapuch
* @license   http://opensource.org/licenses/mit-license.php MIT License
* @link      https://teerapuch.com
*/
class Hero extends MY_Controller
{
    /**
    * Main construct function and variables to use in all class
    *
    * @return session is_logged_in_admin
    */
    public function __construct()
    {
        parent::__construct();
        // Verify Admin Login
        is_logged_in_admin();
    }
    /**
    * A summary informing the user what the associated element does.
    *
    * and to provide some background information or textual references.
    *
    * @return void
    */
    public function index()
    {
        $this->data['sTitle'] = 'Hero Management';
        $aCustomCss = array(
            'assets/css/backend/pages/hero.css',
            "assets/plugins/datatable/css/bootstrap.datatable.min.css",
            "assets/plugins/datatable/css/jquery.dataTables.css",
        );
        $aCustomJs = array(
            "assets/plugins/datatable/js/jquery.dataTables.js",
            "assets/plugins/datatable/js/bootstrap.datatable.js",
            "assets/js/app/backend/pages/hero.js",
        );
        $this->main_css($aCustomCss);
        $this->main_js($aCustomJs);
        $this->data['hero'] = $this->Hero_model->findAll();
        $this->main_layout("backend/hero/index", $this->data);
    }
    /**
    * A summary informing the user what the associated element does.
    *
    * and to provide some background information or textual references.
    *
    * @return void
    */
    public function form()
    {
        $id = $this->uri->segment(4);
        $aCustomJs = array("assets/js/app/backend/pages/hero.js");
        $this->main_js($aCustomJs);
        if (isset($id)) {
            $this->data['hero'] = $this->Hero_model->findById($id);
        }
        $this->data['sTitle'] = 'Form Hero Banner';
        $this->main_layout("backend/hero/form", $this->data);
    }
    /**
    * save hero form data to database
    *
    * @param int $id hero Id For Edit
    * @return true
    * @return msg
    */
    public function save($id = null)
    {
        error_reporting(-1);
		ini_set('display_errors', 1);
        $id = $this->security->xss_clean(
            $this->input->post("id")
        );
        $title = $this->security->xss_clean(
            $this->input->post("title")
        );
        $page = $this->security->xss_clean(
            $this->input->post("page")
        );
        $position = $this->security->xss_clean(
            $this->input->post("position")
        );
        // set for upload
        $config['upload_path']   = 'assets/images/hero/';
        $config['allowed_types'] = 'gif|jpg|jpeg|png|avi|mov|mp4';
        $config['max_size']      = '25000';
        $config['remove_spaces'] = TRUE;
        $this->load->library('upload', $config);
        if($this->upload->do_upload('hero')) {
            $data = $this->upload->data();
            $name = $data['file_name'];
            //$name = 'hero_'.date("dmYHis").$data['file_ext'];
            /*
            var_dump("do upload");
            echo "<pre>";
            var_dump($data);
            echo "</pre>";
            var_dump($name);
            die();
            */

            // check empty id
            if (empty($id)) {
                // insert case
                $data = array(
                    "title" => $title,
                    "hero" => $name,
                    "page" => $page,
                    "position" => $position,
                    "status" => 1,
                    "updatedate" => $this->dateTimeNow,
                    "createdate" => $this->dateTimeNow
                );
                $save = $this->Hero_model->save($data);
                // redirect with session msessage
                $this->session->set_flashdata(
                    'msg-success',
                    'success'
                );
                /*
                var_dump("empty id");
                die();
                */
                redirect('backend/hero');
            } else {
                // Update case
                $data = array(
                    "title" => $title,
                    "hero" => $name,
                    "page" => $page,
                    "position" => $position,
                    "status" => 1,
                    "updatedate" => $this->dateTimeNow
                );
                $save = $this->Hero_model->update($id, $data);
                // redirect with session msessage
                $this->session->set_flashdata(
                    'msg-success',
                    'success'
                );
                /*
                var_dump("update case");
                die();
                */
                redirect('backend/hero');
            } // end else update case
        } // end if do upload
        else {
            // Not update image case
            /*
            var_dump("Not update");
            die();
            */
            $data = array(
                "title" => $title,
                "page" => $page,
                "position" => $position,
                "status" => 2,
                "updatedate" => $this->dateTimeNow
            );
            $save = $this->Hero_model->update($id, $data);
            // redirect with session msessage
            $this->session->set_flashdata(
                'msg-success',
                'success'
            );
            redirect('backend/hero');
        } // end else upload image

    }
    /**
    * open hero from database
    *
    * @param int $id id hero form index
    *
    * @return msg
    */
    public function open($id)
    {
        if (isset($_POST['id'])) {
            // set id
            $id = $this->security->xss_clean(
                $this->input->post("id")
            );
            // check is numeric
            if (is_numeric($id)) {
                $data = array(
                    "status" => 1,
                    "updatedate" => $this->dateTimeNow
                );
                // update open status
                $save = $this->Hero_model->update($id, $data);
                // redirect with session msessage
                $this->session->set_flashdata('msg-success', 'success');
            } else {
                $this->session->set_flashdata('msg-danger', 'error deleteing');
                redirect($this->agent->referrer());
            } // end else
        } else {
            redirect($this->agent->referrer());
        } // end else
    }
    /**
    * close hero from database
    *
    * @param int $id id hero form index
    *
    * @return msg
    */
    public function close($id)
    {
        if (isset($_POST['id'])) {
            // set id
            $id = $this->security->xss_clean(
                $this->input->post("id")
            );
            // check is numeric
            if (is_numeric($id)) {
                $data = array(
                    "status" => 2,
                    "updatedate" => $this->dateTimeNow
                );
                // update close status
                $save = $this->Hero_model->update($id, $data);
                // redirect with session msessage
                $this->session->set_flashdata('msg-success', 'success');
            } else {
                $this->session->set_flashdata('msg-danger', 'error deleteing');
                redirect($this->agent->referrer());
            } // end else
        } else {
            redirect($this->agent->referrer());
        } // end else
    }
    /**
    * soft delete hero from database
    *
    * @param int $id id hero form index
    *
    * @return msg
    */
    public function delete($id)
    {
        if (isset($_POST['id'])) {
            // set id
            $id = $this->security->xss_clean(
                $this->input->post("id")
            );
            // check is numeric
            if (is_numeric($id)) {
                // update soft delete
                $this->Hero_model->delete($id);
                // redirect with session msessage
                $this->session->set_flashdata('msg-success', 'success');
            } else {
                $this->session->set_flashdata('msg-danger', 'error deleteing');
                redirect($this->agent->referrer());
            } // end else
        } else {
            redirect($this->agent->referrer());
        } // end else
    }
    /**
    * soft delete image hero from database
    *
    * @param int $id id image hero form index
    *
    * @return msg
    */
    public function delimage($id)
    {
        if (isset($_POST['id'])) {
            // set id
            $id = $this->security->xss_clean(
                $this->input->post("id")
            );
            // check is numeric
            if (is_numeric($id)) {
                // update soft delete
                $this->Hero_model->deleteimage($id);
                // redirect with session msessage
                $this->session->set_flashdata('msg-success', 'success');
            } else {
                $this->session->set_flashdata('msg-danger', 'error deleteing');
                redirect($this->agent->referrer());
            } // end else
        } else {
            redirect($this->agent->referrer());
        } // end else
    }

}
