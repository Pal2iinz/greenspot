<?php
/**
* Parses and verifies the doc comments for file.
*
* PHP version 5.6
*
* @category  PHP
* @package   Teerapuch
* @author    Teerapuch Kassakul <teerapuch@hotmail.com>
* @copyright 2016 Teerapuch
* @license   http://opensource.org/licenses/mit-license.php MIT License
* @link      https://teerapuch.com
*/
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* Parses and verifies the doc comments for functions.
*
* @category  PHP
* @package   Teerapuch
* @author    Teerapuch Kassakul <teerapuch@hotmail.com>
* @copyright 2016 Teerapuch
* @license   http://opensource.org/licenses/mit-license.php MIT License
* @link      https://teerapuch.com
*/
class Jobs extends MY_Controller
{
    /**
    * Main construct function and variables to use in all class
    *
    * @return session is_logged_in_admin
    */
    public function __construct()
    {
        parent::__construct();
        // Verify Admin Login
        is_logged_in_admin();
    }
    /**
    * A summary informing the user what the associated element does.
    *
    * and to provide some background information or textual references.
    *
    * @return void
    */
    public function index()
    {
        $this->data['sTitle'] = 'Jobs Management';
        $aCustomCss = array(
            'assets/css/backend/pages/jobs.css',
            "assets/plugins/datatable/css/bootstrap.datatable.min.css",
            "assets/plugins/datatable/css/jquery.dataTables.css",
        );
        $aCustomJs = array(
            "assets/plugins/datatable/js/jquery.dataTables.js",
            "assets/plugins/datatable/js/bootstrap.datatable.js",
            "assets/js/app/backend/pages/jobs.js",
        );
        $this->main_css($aCustomCss);
        $this->main_js($aCustomJs);
        $this->data['jobs'] = $this->Jobs_model->findAll();
        $this->main_layout("backend/jobs/index", $this->data);
    }
    /**
    * A summary informing the user what the associated element does.
    *
    * and to provide some background information or textual references.
    *
    * @return void
    */
    public function form()
    {
        $id = $this->uri->segment(4);
         $aCustomCss = array(
            'assets/plugins/summernote/summernote.css'
        );
        
        $aCustomJs = array(
            'assets/plugins/summernote/summernote.js',
            "assets/js/app/backend/pages/jobsform.js"
        );
        $this->main_css($aCustomCss);
        $this->main_js($aCustomJs);
        if (isset($id)) {
            $this->data['jobs'] = $this->Jobs_model->findById($id);
        }
        $this->data['sTitle'] = 'Form Jobs';
        $this->main_layout("backend/jobs/form", $this->data);
    }
    /**
    * save banner form data to database
    *
    * @param int $id Banner Id For Edit
    * @return true
    * @return msg
    */
    public function save($id = null)
    {
        $id = $this->security->xss_clean(
            $this->input->post("id")
        );
        $position = $this->security->xss_clean(
            $this->input->post("position")
        );
        $department = $this->security->xss_clean(
            $this->input->post("department")
        );
        $workplace = $this->security->xss_clean(
            $this->input->post("workplace")
        );
        $number = $this->security->xss_clean(
            $this->input->post("number")
        );
        $content = $this->input->post("content");
        // set data to save
        $aData = array(
            "position" => $position,
            "department" => $department,
            "workplace" => $workplace,
            "number" => $number,
            "content" => $content,
            "status" => 1,
            "updatedate" => $this->dateTimeNow,
        );
        if (empty($id)) {
            // insert case
            $data['createdate'] = $this->dateTimeNow;
            $save = $this->Jobs_model->save($aData);
            // redirect with session msessage
            $this->session->set_flashdata(
                'msg-success',
                'success'
            );
            redirect('backend/jobs');
        } else {
            // Update case
            $save = $this->Jobs_model->update($id, $aData);
            // redirect with session msessage
            $this->session->set_flashdata(
                'msg-success',
                'success'
            );
            redirect('backend/jobs');
        } // end else
    }
    /**
    * open banner from database
    *
    * @param int $id id banner form index
    *
    * @return msg
    */
    public function open($id)
    {
        if (isset($_POST['id'])) {
            // set id
            $id = $this->security->xss_clean(
                $this->input->post("id")
            );
            // check is numeric
            if (is_numeric($id)) {
                $data = array(
                    "status" => 1,
                    "updatedate" => $this->dateTimeNow
                );
                // update open status
                $save = $this->Jobs_model->update($id, $data);
                // redirect with session msessage
                $this->session->set_flashdata('msg-success', 'success');
            } else {
                $this->session->set_flashdata('msg-danger', 'error deleteing');
                redirect($this->agent->referrer());
            } // end else
        } else {
            redirect($this->agent->referrer());
        } // end else
    }
    /**
    * close banner from database
    *
    * @param int $id id banner form index
    *
    * @return msg
    */
    public function close($id)
    {
        if (isset($_POST['id'])) {
            // set id
            $id = $this->security->xss_clean(
                $this->input->post("id")
            );
            // check is numeric
            if (is_numeric($id)) {
                $data = array(
                    "status" => 2,
                    "updatedate" => $this->dateTimeNow
                );
                // update close status
                $save = $this->Jobs_model->update($id, $data);
                // redirect with session msessage
                $this->session->set_flashdata('msg-success', 'success');
            } else {
                $this->session->set_flashdata('msg-danger', 'error deleteing');
                redirect($this->agent->referrer());
            } // end else
        } else {
            redirect($this->agent->referrer());
        } // end else
    }
    /**
    * soft delete banner from database
    *
    * @param int $id id banner form index
    *
    * @return msg
    */
    public function delete($id)
    {
        if (isset($_POST['id'])) {
            // set id
            $id = $this->security->xss_clean(
                $this->input->post("id")
            );
            // check is numeric
            if (is_numeric($id)) {
                // update soft delete
                $this->Jobs_model->delete($id);
                // redirect with session msessage
                $this->session->set_flashdata('msg-success', 'success');
            } else {
                $this->session->set_flashdata('msg-danger', 'error deleteing');
                redirect($this->agent->referrer());
            } // end else
        } else {
            redirect($this->agent->referrer());
        } // end else
    }


}
