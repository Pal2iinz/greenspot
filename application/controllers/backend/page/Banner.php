<?php
/**
* Parses and verifies the doc comments for file.
*
* PHP version 5.6
*
* @category  PHP
* @package   Teerapuch
* @author    Teerapuch Kassakul <teerapuch@hotmail.com>
* @copyright 2016 Teerapuch
* @license   http://opensource.org/licenses/mit-license.php MIT License
* @link      https://teerapuch.com
*/
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* Parses and verifies the doc comments for functions.
*
* @category  PHP
* @package   Teerapuch
* @author    Teerapuch Kassakul <teerapuch@hotmail.com>
* @copyright 2016 Teerapuch
* @license   http://opensource.org/licenses/mit-license.php MIT License
* @link      https://teerapuch.com
*/
class Banner extends MY_Controller
{
    /**
    * Main construct function and variables to use in all class
    *
    * @return session is_logged_in_admin
    */
    public function __construct()
    {
        parent::__construct();
        // Verify Admin Login
        is_logged_in_admin();
    }
    /**
    * A summary informing the user what the associated element does.
    *
    * and to provide some background information or textual references.
    *
    * @return void
    */
    public function index()
    {
        $this->data['sTitle'] = 'Banner Management';
        $aCustomCss = array(
            'assets/css/backend/pages/banner.css',
            "assets/plugins/datatable/css/bootstrap.datatable.min.css",
            "assets/plugins/datatable/css/jquery.dataTables.css",
        );
        $aCustomJs = array(
            "assets/plugins/datatable/js/jquery.dataTables.js",
            "assets/plugins/datatable/js/bootstrap.datatable.js",
            "assets/js/app/backend/pages/banner.js",
        );
        $this->main_css($aCustomCss);
        $this->main_js($aCustomJs);
        $this->data['banner'] = $this->Banner_model->findAll();
        $this->main_layout("backend/banner/index", $this->data);
    }
    /**
    * A summary informing the user what the associated element does.
    *
    * and to provide some background information or textual references.
    *
    * @return void
    */
    public function form()
    {
        $id = $this->uri->segment(4);
        $aCustomJs = array("assets/js/app/backend/pages/bannerform.js");
        $this->main_js($aCustomJs);
        if (isset($id)) {
            $this->data['banner'] = $this->Banner_model->findById($id);
        }
        $this->data['sTitle'] = 'Form Banner';
        $this->main_layout("backend/banner/form", $this->data);
    }
    /**
    * save banner form data to database
    *
    * @param int $id Banner Id For Edit
    * @return true
    * @return msg
    */
    public function save($id = null)
    {
        $id = $this->security->xss_clean(
            $this->input->post("id")
        );
        $url = $this->security->xss_clean(
            $this->input->post("url")
        );
        $page = $this->security->xss_clean(
            $this->input->post("page")
        );
        // set data to save
        $aData = array(
            "url" => $url,
            "page" => $page,
            "status" => 1,
            "updatedate" => $this->dateTimeNow,
        );
        //================//
        // set for upload //
        //================//
        $config['upload_path']   = 'assets/images/banner/';
        $config['allowed_types'] = 'gif|jpg|jpeg|png|mp4';
        $config['max_size']      = '3000';
        $config['max_width']     = '2480';
        $config['max_height']    = '2480';
        $config['remove_spaces'] = TRUE;
        $this->load->library('upload', $config);
        //===================//
        // Upload File Image Banner
        //==================//
        if($this->upload->do_upload('image')) {
            // upload file and rename success
            $data = $this->upload->data();
            $sNewName = 'banner_img_'.date("dmYHis").$data['file_ext'];
            rename($data['full_path'], $data['file_path'].$sNewName);
            $aData["image"]  = $sNewName;
        }
        if (empty($id)) {
            // insert case
            $data['createdate'] = $this->dateTimeNow;
            $save = $this->Banner_model->save($aData);
            // redirect with session msessage
            $this->session->set_flashdata(
                'msg-success',
                'success'
            );
            redirect('backend/banner');
        } else {
            // Update case
            $save = $this->Banner_model->update($id, $aData);
            // redirect with session msessage
            $this->session->set_flashdata(
                'msg-success',
                'success'
            );
            redirect('backend/banner');
        } // end else
    }
    /**
    * open banner from database
    *
    * @param int $id id banner form index
    *
    * @return msg
    */
    public function open($id)
    {
        if (isset($_POST['id'])) {
            // set id
            $id = $this->security->xss_clean(
                $this->input->post("id")
            );
            // check is numeric
            if (is_numeric($id)) {
                $data = array(
                    "status" => 1,
                    "updatedate" => $this->dateTimeNow
                );
                // update open status
                $save = $this->Banner_model->update($id, $data);
                // redirect with session msessage
                $this->session->set_flashdata('msg-success', 'success');
            } else {
                $this->session->set_flashdata('msg-danger', 'error deleteing');
                redirect($this->agent->referrer());
            } // end else
        } else {
            redirect($this->agent->referrer());
        } // end else
    }
    /**
    * close banner from database
    *
    * @param int $id id banner form index
    *
    * @return msg
    */
    public function close($id)
    {
        if (isset($_POST['id'])) {
            // set id
            $id = $this->security->xss_clean(
                $this->input->post("id")
            );
            // check is numeric
            if (is_numeric($id)) {
                $data = array(
                    "status" => 2,
                    "updatedate" => $this->dateTimeNow
                );
                // update close status
                $save = $this->Banner_model->update($id, $data);
                // redirect with session msessage
                $this->session->set_flashdata('msg-success', 'success');
            } else {
                $this->session->set_flashdata('msg-danger', 'error deleteing');
                redirect($this->agent->referrer());
            } // end else
        } else {
            redirect($this->agent->referrer());
        } // end else
    }
    /**
    * soft delete banner from database
    *
    * @param int $id id banner form index
    *
    * @return msg
    */
    public function delete($id)
    {
        if (isset($_POST['id'])) {
            // set id
            $id = $this->security->xss_clean(
                $this->input->post("id")
            );
            // check is numeric
            if (is_numeric($id)) {
                // update soft delete
                $this->Banner_model->delete($id);
                // redirect with session msessage
                $this->session->set_flashdata('msg-success', 'success');
            } else {
                $this->session->set_flashdata('msg-danger', 'error deleteing');
                redirect($this->agent->referrer());
            } // end else
        } else {
            redirect($this->agent->referrer());
        } // end else
    }
    /**
    * soft delete image banner from database
    *
    * @param int $id id image banner form index
    *
    * @return msg
    */
    public function delimage($id)
    {
        if (isset($_POST['id'])) {
            // set id
            $id = $this->security->xss_clean(
                $this->input->post("id")
            );
            // check is numeric
            if (is_numeric($id)) {
                // update soft delete
                $this->Banner_model->deleteimage($id);
                // redirect with session msessage
                $this->session->set_flashdata('msg-success', 'success');
            } else {
                $this->session->set_flashdata('msg-danger', 'error deleteing');
                redirect($this->agent->referrer());
            } // end else
        } else {
            redirect($this->agent->referrer());
        } // end else
    }

}
