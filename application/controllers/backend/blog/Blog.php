<?php
/**
* Parses and verifies the doc comments for file.
*
* PHP version 5.6
*
* @category  PHP
* @package   Teerapuch
* @author    Teerapuch Kassakul <teerapuch@hotmail.com>
* @copyright 2016 Teerapuch
* @license   http://opensource.org/licenses/mit-license.php MIT License
* @link      https://teerapuch.com
*/
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* Parses and verifies the doc comments for functions.
*
* @category  PHP
* @package   Teerapuch
* @author    Teerapuch Kassakul <teerapuch@hotmail.com>
* @copyright 2016 Teerapuch
* @license   http://opensource.org/licenses/mit-license.php MIT License
* @link      https://teerapuch.com
*/
class Blog extends MY_Controller
{
    /**
    * Main construct function and variables to use in all class
    *
    * @return session is_logged_in_admin
    */
    public function __construct()
    {
        parent::__construct();
        // Verify Admin Login
        is_logged_in_admin();
    }
    /**
    * A summary informing the user what the associated element does.
    *
    * and to provide some background information or textual references.
    *
    * @return void
    */
    public function index()
    {
        $this->data['sTitle'] = 'Blog Management';
        $aCustomCss = array(
            "assets/css/backend/blog/index.css",
            "assets/plugins/datatable/css/bootstrap.datatable.min.css",
            "assets/plugins/datatable/css/jquery.dataTables.css",
        );
        $aCustomJs = array(
            "assets/plugins/datatable/js/jquery.dataTables.js",
            "assets/plugins/datatable/js/bootstrap.datatable.js",
            "assets/js/app/backend/blog/index.js"
        );
        $this->main_css($aCustomCss);
        $this->main_js($aCustomJs);
        $this->data['blog'] = $this->Blog_model->findAll();
        $this->data['categories'] = $this->Categories_model->findAll();
        $this->main_layout("backend/blog/index", $this->data);
    }
    /**
    * view form for create product informantion
    *
    * @return true
    */
    public function form()
    {
        $id = $this->uri->segment(4);
        $aCustomCss = array(
            'assets/plugins/summernote/summernote.css',
            "assets/css/backend/blog/form.css",
        );
        $aCustomJs = array(
            'assets/plugins/summernote/summernote.js',
            'assets/js/app/backend/blog/form.js',
        );
        $this->main_css($aCustomCss);
        $this->main_js($aCustomJs);
        if (isset($id)) {
            $this->data['blog'] = $this->Blog_model->findById($id);
        }
        $this->data['sTitle'] = 'Form Blog';
        $this->data['cat'] = $this->Categories_model->findAll();
        $this->main_layout("backend/blog/form", $this->data);
    }

    public function save($id = null)
    {
        $id = $this->security->xss_clean(
            $this->input->post("id")
        );
        $title = $this->security->xss_clean(
            $this->input->post("title")
        );
        $categories = $this->security->xss_clean(
            $this->input->post("categories")
        );
        $subcat = $this->security->xss_clean(
            $this->input->post("subcat")
        );
        $post = $this->input->post("post");

        // set for upload
        $config['upload_path']   = 'assets/images/blog/';
        $config['allowed_types'] = 'gif|jpg|jpeg|png';
        $config['max_size']      = '2048';
        $config['max_width']     = '1920';
        $config['max_height']    = '1280';
        $config['remove_spaces'] = TRUE;
        $this->load->library('upload', $config);
        //===================//
        // Upload File Image
        //==================//
        if($this->upload->do_upload('thumbnail')) {
            // upload file and rename success
            $data = $this->upload->data();
            $thumbnail = 'thumbnail_'.date("dmYHis").$data['file_ext'];
            rename($data['full_path'], $data['file_path'].$thumbnail);
        } else {
            if($thumbnail == '') {
                $thumbnail = $this->input->post("thumbnail");
            }
        }
        if($this->upload->do_upload('image')) {
            // upload file and rename success
            $data = $this->upload->data();
            $image = 'blog_'.date("dmYHis").$data['file_ext'];
            rename($data['full_path'], $data['file_path'].$image);
        } else {
            if($image == '') {
                $image = $this->input->post("image");
            }
        }
        // set data to save
        $aData['title'] = $title;
        $aData['categories'] = $categories;
        $aData['subcat'] = $subcat;
        $aData['post'] = $post;
        $aData['updatedate'] = $this->dateTimeNow;
        $aData['thumbnail'] = $thumbnail;
        $aData['image'] = $image;

        if (empty($id)) {
            // echo 'Insert Case';
            $save = $this->Blog_model->save($aData);
            // redirect with session msessage
            $this->session->set_flashdata(
                'msg-success',
                'success'
            );
            redirect('backend/blog');
        } else {
            // echo 'Update Case';
            $save = $this->Blog_model->update($id, $aData);
            // redirect with session msessage
            $this->session->set_flashdata(
                'msg-success',
                'success'
            );
            redirect('backend/blog');
        } // end else empty id

    }
    /**
    * delete image from database
    *
    * @param int $id id image form form
    * @param int $position position image form form
    *
    * @return msg
    */
    public function deleteimage($id, $position)
    {
        if (isset($_POST['id'])) {
            // set id
            $id = $this->security->xss_clean(
                $this->input->post("id")
            );
            // set position
            $position = $this->security->xss_clean(
                $this->input->post("position")
            );
            // check is numeric
            if (is_numeric($id)) {
                // update soft delete
                $this->Blog_model->deleteimage($id,$position);
                // redirect with session msessage
                $this->session->set_flashdata('msg-success', 'success');
            } else {
                $this->session->set_flashdata('msg-danger', 'error deleteing');
                redirect($this->agent->referrer());
            } // end else
        } else {
            redirect($this->agent->referrer());
        } // end else
    }
    /**
    * save blog form data to database
    *
    * @param int $id blog Id For Edit
    * @return true
    * @return msg
    */
    public function oldsave($id = null)
    {
        /*
        // validate form
        $this->form_validation->set_rules(
            'title',
            'Title',
            'trim|required|xss_clean'
        );
        $this->form_validation->set_rules(
            'post',
            'post',
            'trim|required|xss_clean'
        );

        if ($this->form_validation->run() == false) {
            $this->form();
        } else {
        */
            $id = $this->security->xss_clean(
                $this->input->post("id")
            );
            $title = $this->security->xss_clean(
                $this->input->post("title")
            );
            $categories = $this->security->xss_clean(
                $this->input->post("categories")
            );
            $subcat = $this->security->xss_clean(
                $this->input->post("subcat")
            );
            $post = $this->input->post("post");

            // set for upload
            $config['upload_path']   = 'assets/images/blog/';
            $config['allowed_types'] = 'gif|jpg|jpeg|png';
            $config['max_size']      = '2048';
            $config['max_width']     = '1920';
            $config['max_height']    = '1280';
            $config['remove_spaces'] = TRUE;
            $this->load->library('upload', $config);
            //===================//
            // Create Thumbnail
            //==================//
            // upload and crop profile to 120x120 px
            if($this->upload->do_upload('image')) {
                // Resize file
                $data = $this->upload->data();
                $sNewName = 'thumbnail_'.date("dmYHis").$data['file_ext'];
                $config['image_library'] = 'gd2';
                $config["source_image"] = $data["full_path"];
                $config['create_thumb'] = FALSE;
                $config['maintain_ratio'] = TRUE;
                $config['new_image'] = $data["file_path"].$sNewName;
                $config['quality'] = "100%";
                $config['width'] = 310;
                $config['height'] = 190;
                $dim = (intval($data["image_width"]) / intval($data["image_height"])) - ($config['width'] / $config['height']);
                $config['master_dim'] = ($dim > 0)? "height" : "width";

                $this->image_lib->initialize($config);
                if (!$this->image_lib->resize()) {
                    // redirect with session msessage
                    $this->session->set_flashdata(
                        'msg-danger',
                        $this->image_lib->display_errors()
                    );
                    redirect($this->agent->referrer());
                } else {
                    // Crop file
                    $config['image_library'] = 'gd2';
                    $config['source_image'] = $data["file_path"].$sNewName;
                    $config['new_image'] = $data["file_path"].$sNewName;
                    $config['quality'] = "100%";
                    $config['maintain_ratio'] = FALSE;
                    $config['width'] = 310;
                    $config['height'] = 190;
                    $config['x_axis'] = '0';
                    $config['y_axis'] = '0';
                    $this->image_lib->clear();
                    $this->image_lib->initialize($config);

                    $aData['thumbnail'] = $sNewName;
                    /*
                    if (!$this->image_lib->crop()) {
                        // redirect with session msessage
                        $this->session->set_flashdata('msg-danger', $this->image_lib->display_errors());
                        redirect($this->agent->referrer());
                    } else {
                        $aData['thumbnail'] = $sNewName;
                    }
                    */
                } // end else
            /*
            } // end if upload and crop
            */

            //===================//
            // Upload File Image
            //==================//
            if($this->upload->do_upload('image')) {
                // upload file and rename success
                $data = $this->upload->data();
                $sNewName = 'blog_'.date("dmYHis").$data['file_ext'];
                rename($data['full_path'], $data['file_path'].$sNewName);
                $aData['image'] = $sNewName;
            } else {
                // redirect with session msessage
                $this->session->set_flashdata('msg-danger', $this->upload->display_errors());
                redirect($this->agent->referrer());
            }

            // set data to save
            $aData['title'] = $title;
            $aData['categories'] = $categories;
            $aData['subcat'] = $subcat;
            $aData['post'] = $post;
            $aData['updatedate'] = $this->dateTimeNow;

            if (empty($id)) {
                // echo 'Insert Case';
                $save = $this->Blog_model->save($aData);
                // redirect with session msessage
                $this->session->set_flashdata(
                    'msg-success',
                    'success'
                );
                redirect('backend/blog');
            } else {
                // echo 'Update Case';
                $save = $this->Blog_model->update($id, $aData);
                // redirect with session msessage
                $this->session->set_flashdata(
                    'msg-success',
                    'success'
                );
                redirect('backend/blog');
            } // end else empty id
        } // end else form validate
    }

    public function delete()
    {
        if (isset($_POST['id'])) {
            // set id
            $id = $this->security->xss_clean(
                $this->input->post("id")
            );
            // check is numeric
            if (is_numeric($id)) {
                // update soft delete
                $this->Blog_model->delete($id);
                // redirect with session msessage
                $this->session->set_flashdata('msg-success', 'success');
            } else {
                $this->session->set_flashdata('msg-danger', 'error deleteing');
                redirect($this->agent->referrer());
            } // end else
        } else {
            redirect($this->agent->referrer());
        } // end else

    }
}
