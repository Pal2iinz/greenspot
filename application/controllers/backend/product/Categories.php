<?php
/**
* Parses and verifies the doc comments for file.
*
* PHP version 5.6
*
* @category  PHP
* @package   Teerapuch
* @author    Teerapuch Kassakul <teerapuch@hotmail.com>
* @copyright 2016 Teerapuch
* @license   http://opensource.org/licenses/mit-license.php MIT License
* @link      https://teerapuch.com
*/
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* Parses and verifies the doc comments for functions.
*
* @category  PHP
* @package   Teerapuch
* @author    Teerapuch Kassakul <teerapuch@hotmail.com>
* @copyright 2016 Teerapuch
* @license   http://opensource.org/licenses/mit-license.php MIT License
* @link      https://teerapuch.com
*/
class Categories extends MY_Controller
{
    /**
    * Main construct function and variables to use in all class
    *
    * @return session is_logged_in_admin
    */
    public function __construct()
    {
        parent::__construct();
        // Verify Admin Login
        is_logged_in_admin();
    }
    /**
    * A summary informing the user what the associated element does.
    *
    * and to provide some background information or textual references.
    *
    * @return void
    */
    public function index()
    {
        $this->data['sTitle'] = 'Categories Management';
        $aCustomCss = array(
            "assets/css/backend/product/style.css",
            "assets/plugins/datatable/css/bootstrap.datatable.min.css",
            "assets/plugins/datatable/css/jquery.dataTables.css",
        );
        $aCustomJs = array(
            'assets/js/bootstrap/modal.js',
            "assets/plugins/datatable/js/jquery.dataTables.js",
            "assets/plugins/datatable/js/bootstrap.datatable.js",
            "assets/js/app/backend/product/categories/custom.js"
        );
        $this->main_css($aCustomCss);
        $this->main_js($aCustomJs);
        $this->data['categories'] = $this->Categories_model->findAll();
        $this->main_layout("backend/categories/index", $this->data);
    }
    /**
    * save categories form data to database
    *
    * @param int $id Product Id For Edit
    * @return true
    * @return msg
    */
    public function save($id = null)
    {
        $id = $this->security->xss_clean(
            $this->input->post("id")
        );
        $categories = $this->security->xss_clean(
            $this->input->post("categories")
        );
        if (empty($id)) {
            // insert case
            $data = array(
                "categories" => $categories,
                "updatedate" => $this->dateTimeNow,
                "createdate" => $this->dateTimeNow
            );
            $save = $this->Categories_model->save($data);
            // redirect with session msessage
            $this->session->set_flashdata(
                'msg-success',
                'success'
            );
            redirect($this->agent->referrer());
        } else {
            // Update case
            $data = array(
                "categories" => $categories,
                "updatedate" => $this->dateTimeNow
            );
            $save = $this->Categories_model->update($id, $data);
            // redirect with session msessage
            $this->session->set_flashdata(
                'msg-success',
                'success'
            );
            redirect($this->agent->referrer());
        }
    }
     /**
    * soft delete categories from database
    *
    * @param int $id id categories form index
    *
    * @return msg
    */
    public function delete($id)
    {
        if (isset($_POST['id'])) {
            // set id
            $id = $this->security->xss_clean(
                $this->input->post("id")
            );
            // check is numeric
            if (is_numeric($id)) {
                // update soft delete
                $this->Categories_model->delete($id);
                // redirect with session msessage
                $this->session->set_flashdata('msg-success', 'success');
            } else {
                $this->session->set_flashdata('msg-danger', 'error deleteing');
                redirect($this->agent->referrer());
            } // end else
        } else {
            redirect($this->agent->referrer());
        } // end else

    }


}
