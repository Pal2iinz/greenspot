<?php
/**
* Parses and verifies the doc comments for file.
*
* PHP version 5.6
*
* @category  PHP
* @package   Teerapuch
* @author    Teerapuch Kassakul <teerapuch@hotmail.com>
* @copyright 2016 Teerapuch
* @license   http://opensource.org/licenses/mit-license.php MIT License
* @link      https://teerapuch.com
*/
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* Parses and verifies the doc comments for functions.
*
* @category  PHP
* @package   Teerapuch
* @author    Teerapuch Kassakul <teerapuch@hotmail.com>
* @copyright 2016 Teerapuch
* @license   http://opensource.org/licenses/mit-license.php MIT License
* @link      https://teerapuch.com
*/
class Product extends MY_Controller
{
    /**
    * Main construct function and variables to use in all class
    *
    * @return session is_logged_in_admin
    */
    public function __construct()
    {
        parent::__construct();
        // Verify Admin Login
        is_logged_in_admin();
    }
    /**
    * A summary informing the user what the associated element does.
    *
    * and to provide some background information or textual references.
    *
    * @return void
    */
    public function index()
    {
        $this->data['sTitle'] = 'Product Management';
        $aCustomCss = array(
            "assets/css/backend/product/style.css",
            "assets/plugins/datatable/css/bootstrap.datatable.min.css",
            "assets/plugins/datatable/css/jquery.dataTables.css",
        );
        $aCustomJs = array(
            "assets/plugins/datatable/js/jquery.dataTables.js",
            "assets/plugins/datatable/js/bootstrap.datatable.js",
            "assets/js/app/backend/product/index.js"
        );
        $this->main_css($aCustomCss);
        $this->main_js($aCustomJs);
        $this->data['product'] = $this->Product_model->findAll();
        $this->data['categories'] = $this->Categories_model->findAll();
        $this->main_layout("backend/product/index", $this->data);
    }
    /**
    * view form for create product informantion
    *
    * @return true
    */
    public function form()
    {
        $id = $this->uri->segment(4);

        $aCustomCss = array(
            'assets/plugins/summernote/summernote.css'
        );
        $aCustomJs = array(
            'assets/plugins/summernote/summernote.js',
            'assets/js/app/backend/product/form.js',
        );
        $this->main_css($aCustomCss);
        $this->main_js($aCustomJs);
        if (isset($id)) {
            $this->data['product'] = $this->Product_model->findById($id);
        }
        $this->data['sTitle'] = 'Form Product';
        $this->data['categories'] = $this->Categories_model->findAll();
        $this->main_layout("backend/product/form", $this->data);
    }
    /**
    * save product form data to database
    *
    * @param int $id Product Id For Edit
    * @return true
    * @return msg
    */
    public function save($id = null)
    {
        /*
        // validate form
        $this->form_validation->set_rules(
            'title',
            'Title',
            'trim|required|xss_clean'
        );
        $this->form_validation->set_rules(
            'formula',
            'Formula',
            'trim|required|xss_clean'
        );
        $this->form_validation->set_rules(
            'benefit',
            'Benefit',
            'trim|required|xss_clean'
        );

        if ($this->form_validation->run() == false) {
            $this->form();
        } else {
        */
            // set data with form
            $id = $this->security->xss_clean(
                $this->input->post("id")
            );
            $title = $this->security->xss_clean(
                $this->input->post("title")
            );
            $formula = $this->security->xss_clean(
                $this->input->post("formula")
            );
            $benefit = $this->security->xss_clean(
                $this->input->post("benefit")
            );
            $category = $this->security->xss_clean(
                $this->input->post("categories")
            );
            $note = $this->security->xss_clean(
                $this->input->post("note")
            );

            //================//
            // set for upload //
            //================//
            $config['upload_path']   = 'assets/images/product/';
            $config['allowed_types'] = 'gif|jpg|jpeg|png';
            $config['max_size']      = '3000';
            $config['max_width']     = '1240';
            $config['max_height']    = '1000';
            $config['remove_spaces'] = TRUE;
            $this->load->library('upload', $config);

            //===================//
            // Upload File Image Product
            //==================//
            if($this->upload->do_upload('product_img')) {
                // upload file and rename success
                $data = $this->upload->data();
                $sNewName = 'product_img_'.date("dmYHis").$data['file_ext'];
                rename($data['full_path'], $data['file_path'].$sNewName);
                $product_img = $sNewName;
            } else {
                if($product_img == '') {
                    $product_img = $this->input->post("product_img");
                }
            }

            //===================//
            // Upload File Image Thumbnail Product
            //==================//
            if($this->upload->do_upload('product_thumb')) {
                // upload file and rename success
                $data = $this->upload->data();
                $sNewName = 'product_thumb_'.date("dmYHis").$data['file_ext'];
                rename($data['full_path'], $data['file_path'].$sNewName);
                $product_thumb = $sNewName;
            } else {
                if($product_thumb == '') {
                    $product_thumb = $this->input->post("product_thumb");
                }
            }

            //===================//
            // Upload File Image Thumbnail Product
            //==================//
            if($this->upload->do_upload('nutrition')) {
                // upload file and rename success
                $data = $this->upload->data();
                $sNewName = 'nutrition_'.date("dmYHis").$data['file_ext'];
                rename($data['full_path'], $data['file_path'].$sNewName);
                $nutrition = $sNewName;
            } else {
                if($nutrition == '') {
                    $nutrition = $this->input->post("nutrition");
                }
            }
            // set data to save
            $data = array(
                "title" => $title,
                "formula" => $formula,
                "benefit" => $benefit,
                "nutrition" => $nutrition,
                "category" => $category,
                "product_thumb" => $product_thumb,
                "product_img" => $product_img,
                "note" => $note,
                "status" => 1,
                "updatedate" => $this->dateTimeNow,
            );
            
            if (empty($id)) {
                // insert case
                $data['createdate'] = $this->dateTimeNow;
                $save = $this->Product_model->save($data);
                // redirect with session msessage
                $this->session->set_flashdata(
                    'msg-success',
                    'success'
                );
                redirect('backend/product/');
            } else {
                // Update case
                $save = $this->Product_model->update($id, $data);
                // redirect with session msessage
                $this->session->set_flashdata(
                    'msg-success',
                    'success'
                );
                redirect('backend/product/');
            }
        /*
        } // end else form validate
        */
    }
    /**
    * open Product from database
    *
    * @param int $id id Product form index
    *
    * @return msg
    */
    public function open($id)
    {
        if (isset($_POST['id'])) {
            // set id
            $id = $this->security->xss_clean(
                $this->input->post("id")
            );
            // check is numeric
            if (is_numeric($id)) {
                $data = array(
                    "status" => 1,
                    "updatedate" => $this->dateTimeNow
                );
                // update open status
                $save = $this->Product_model->update($id, $data);
                // redirect with session msessage
                $this->session->set_flashdata('msg-success', 'success');
            } else {
                $this->session->set_flashdata('msg-danger', 'error deleteing');
                redirect($this->agent->referrer());
            } // end else
        } else {
            redirect($this->agent->referrer());
        } // end else
    }
    /**
    * close product from database
    *
    * @param int $id id product form index
    *
    * @return msg
    */
    public function close($id)
    {
        if (isset($_POST['id'])) {
            // set id
            $id = $this->security->xss_clean(
                $this->input->post("id")
            );
            // check is numeric
            if (is_numeric($id)) {
                $data = array(
                    "status" => 2,
                    "updatedate" => $this->dateTimeNow
                );
                // update close status
                $save = $this->Product_model->update($id, $data);
                // redirect with session msessage
                $this->session->set_flashdata('msg-success', 'success');
            } else {
                $this->session->set_flashdata('msg-danger', 'error deleteing');
                redirect($this->agent->referrer());
            } // end else
        } else {
            redirect($this->agent->referrer());
        } // end else
    }
    /**
    * soft delete product from database
    *
    * @param int $id id product form index
    *
    * @return msg
    */
    public function delete($id)
    {
        if (isset($_POST['id'])) {
            // set id
            $id = $this->security->xss_clean(
                $this->input->post("id")
            );
            // check is numeric
            if (is_numeric($id)) {
                // update soft delete
                $this->Product_model->delete($id);
                // redirect with session msessage
                $this->session->set_flashdata('msg-success', 'success');
            } else {
                $this->session->set_flashdata('msg-danger', 'error deleteing');
                redirect($this->agent->referrer());
            } // end else
        } else {
            redirect($this->agent->referrer());
        } // end else
    }
    /**
    * delete image from database
    *
    * @param int $id id image form form
    * @param int $position position image form form
    *
    * @return msg
    */
    public function deleteimage($id, $position)
    {
        if (isset($_POST['id'])) {
            // set id
            $id = $this->security->xss_clean(
                $this->input->post("id")
            );
            // set position
            $position = $this->security->xss_clean(
                $this->input->post("position")
            );
            // check is numeric
            if (is_numeric($id)) {
                // update soft delete
                $this->Product_model->deleteimage($id,$position);
                // redirect with session msessage
                $this->session->set_flashdata('msg-success', 'success');
            } else {
                $this->session->set_flashdata('msg-danger', 'error deleteing');
                redirect($this->agent->referrer());
            } // end else
        } else {
            redirect($this->agent->referrer());
        } // end else
    }

}
