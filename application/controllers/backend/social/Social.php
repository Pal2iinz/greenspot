<?php
/**
* Parses and verifies the doc comments for file.
*
* PHP version 5.6
*
* @category  PHP
* @package   Teerapuch
* @author    Teerapuch Kassakul <teerapuch@hotmail.com>
* @copyright 2016 Teerapuch
* @license   http://opensource.org/licenses/mit-license.php MIT License
* @link      https://teerapuch.com
*/
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* Parses and verifies the doc comments for functions.
*
* @category  PHP
* @package   Teerapuch
* @author    Teerapuch Kassakul <teerapuch@hotmail.com>
* @copyright 2016 Teerapuch
* @license   http://opensource.org/licenses/mit-license.php MIT License
* @link      https://teerapuch.com
*/
class Social extends MY_Controller
{
    /**
    * Main construct function and variables to use in all class
    *
    * @return session is_logged_in_admin
    */
    public function __construct()
    {
        parent::__construct();
        // Verify Admin Login
        is_logged_in_admin();
    }
    /**
    * A summary informing the user what the associated element does.
    *
    * and to provide some background information or textual references.
    *
    * @return void
    */
    public function index()
    {
        $this->data['sTitle'] = 'Social Management';
        $aCustomCss = array(
            "assets/plugins/datatable/css/bootstrap.datatable.min.css",
            "assets/plugins/datatable/css/jquery.dataTables.css",
            "assets/css/backend/pages/homepage.css",
        );
        $aCustomJs = array(
            "assets/plugins/datatable/js/jquery.dataTables.js",
            "assets/plugins/datatable/js/bootstrap.datatable.js",
            "assets/js/app/backend/social/custom.js"
        );
        $this->main_css($aCustomCss);
        $this->main_js($aCustomJs);
        $this->data['social'] = $this->Social_model->findAll();
        $this->main_layout("backend/social/index", $this->data);
    }
    /**
    * A summary informing the user what the associated element does.
    *
    * and to provide some background information or textual references.
    *
    * @return void
    */
    public function form()
    {
        $id = $this->uri->segment(4);
        $aCustomJs = array(
            "assets/js/app/backend/social/form.js"
        );
        $this->main_js($aCustomJs);
        if (isset($id)) {
            $this->data['social'] = $this->Social_model->findById($id);
        }
        $this->data['sTitle'] = 'Form Social';
        $this->main_layout("backend/social/form", $this->data);
    }
    /**
    * save social form data to database
    *
    * @param int $id social Id For Edit
    * @return true
    * @return msg
    */
    public function save($id = null)
    {
        $id = $this->security->xss_clean(
            $this->input->post("id")
        );
        $title = $this->security->xss_clean(
            $this->input->post("title")
        );
        $details = $this->security->xss_clean(
            $this->input->post("details")
        );
        $url = $this->security->xss_clean(
            $this->input->post("url")
        );
        // set for upload
        $config['upload_path']   = 'assets/images/social/';
        $config['allowed_types'] = 'gif|jpg|jpeg|png';
        $config['max_size']      = '2048';
        $config['max_width']     = '1920';
        $config['max_height']    = '1280';
        $config['remove_spaces'] = TRUE;
        $this->load->library('upload', $config);
        // upload and crop profile to 120x120 px
        if($this->upload->do_upload('image')) {
            // Resize file
            $data = $this->upload->data();
            $sNewName = 'social_'.date("dmYHis").$data['file_ext'];
            $config['image_library'] = 'gd2';
            $config["source_image"] = $data["full_path"];
            $config['create_thumb'] = FALSE;
            $config['maintain_ratio'] = TRUE;
            $config['new_image'] = $data["file_path"].$sNewName;
            $config['quality'] = "100%";
            $config['width'] = 240;
            $config['height'] = 283;
            $dim = (intval($data["image_width"]) / intval($data["image_height"])) - ($config['width'] / $config['height']);
            $config['master_dim'] = ($dim > 0)? "height" : "width";

            $this->image_lib->initialize($config);
            if (!$this->image_lib->resize()) {
                //Resize image
                // redirect with session msessage
                $this->session->set_flashdata(
                    'msg-danger',
                    $this->image_lib->display_errors()
                );
                redirect($this->agent->referrer());
            } else {
                // Crop file
                $config['image_library'] = 'gd2';
                $config['source_image'] = $data["file_path"].$sNewName;
                $config['new_image'] = $data["file_path"].$sNewName;
                $config['quality'] = "100%";
                $config['maintain_ratio'] = FALSE;
                $config['width'] = 240;
                $config['height'] = 283;
                $config['x_axis'] = '0';
                $config['y_axis'] = '0';
                $this->image_lib->clear();
                $this->image_lib->initialize($config);
                if (!$this->image_lib->crop()) {
                    // redirect with session msessage
                    $this->session->set_flashdata(
                        'msg-danger',
                        $this->image_lib->display_errors()
                    );
                    redirect($this->agent->referrer());
                } else {
                    if (empty($id)) {
                        // insert case
                        $data = array(
                            "title" => $title,
                            "details" => $details,
                            'image' => $sNewName,
                            "url" => $url,
                            "status" => 1,
                            "updatedate" => $this->dateTimeNow,
                            "createdate" => $this->dateTimeNow
                        );
                        $save = $this->Social_model->save($data);
                        // redirect with session msessage
                        $this->session->set_flashdata(
                            'msg-success',
                            'success'
                        );
                        redirect('backend/social/');
                    } else {
                        // Update case
                        $data = array(
                            "title" => $title,
                            "details" => $details,
                            'image' => $sNewName,
                            "url" => $url,
                            "status" => 1,
                            "updatedate" => $this->dateTimeNow
                        );
                        $save = $this->Social_model->update($id, $data);
                        // redirect with session msessage
                        $this->session->set_flashdata(
                            'msg-success',
                            'success'
                        );
                        redirect('backend/social/');
                    }
                }
            } // end else
        } // end if upload and crop
        else {
            // Not update image case
            $data = array(
                "title" => $title,
                "details" => $details,
                "url" => $url,
                "status" => 1,
                "updatedate" => $this->dateTimeNow
            );
            $save = $this->Social_model->update($id, $data);
            // redirect with session msessage
            $this->session->set_flashdata(
                'msg-success',
                'success'
            );
            redirect('backend/social/');
        } // end else upload image
    }
    /**
    * open social from database
    *
    * @param int $id id social form index
    *
    * @return msg
    */
    public function open($id)
    {
        if (isset($_POST['id'])) {
            // set id
            $id = $this->security->xss_clean(
                $this->input->post("id")
            );
            // check is numeric
            if (is_numeric($id)) {
                $data = array(
                    "status" => 1,
                    "updatedate" => $this->dateTimeNow
                );
                // update open status
                $save = $this->Social_model->update($id, $data);
                // redirect with session msessage
                $this->session->set_flashdata('msg-success', 'success');
            } else {
                $this->session->set_flashdata('msg-danger', 'error deleteing');
                redirect($this->agent->referrer());
            } // end else
        } else {
            redirect($this->agent->referrer());
        } // end else
    }
    /**
    * close social from database
    *
    * @param int $id id social form index
    *
    * @return msg
    */
    public function close($id)
    {
        if (isset($_POST['id'])) {
            // set id
            $id = $this->security->xss_clean(
                $this->input->post("id")
            );
            // check is numeric
            if (is_numeric($id)) {
                $data = array(
                    "status" => 2,
                    "updatedate" => $this->dateTimeNow
                );
                // update close status
                $save = $this->Social_model->update($id, $data);
                // redirect with session msessage
                $this->session->set_flashdata('msg-success', 'success');
            } else {
                $this->session->set_flashdata('msg-danger', 'error deleteing');
                redirect($this->agent->referrer());
            } // end else
        } else {
            redirect($this->agent->referrer());
        } // end else
    }
    /**
    * soft delete social from database
    *
    * @param int $id id social form index
    *
    * @return msg
    */
    public function delete($id)
    {
        if (isset($_POST['id'])) {
            // set id
            $id = $this->security->xss_clean(
                $this->input->post("id")
            );
            // check is numeric
            if (is_numeric($id)) {
                // update soft delete
                $this->Social_model->delete($id);
                // redirect with session msessage
                $this->session->set_flashdata('msg-success', 'success');
            } else {
                $this->session->set_flashdata('msg-danger', 'error deleteing');
                redirect($this->agent->referrer());
            } // end else
        } else {
            redirect($this->agent->referrer());
        } // end else
    }
    /**
    * soft delete image from database
    *
    * @param int $id id image form index
    *
    * @return msg
    */
    public function deleteimage($id)
    {
        if (isset($_POST['id'])) {
            // set id
            $id = $this->security->xss_clean(
                $this->input->post("id")
            );
            // check is numeric
            if (is_numeric($id)) {
                // update soft delete
                $this->Social_model->deleteimage($id);
                // redirect with session msessage
                $this->session->set_flashdata('msg-success', 'success');
            } else {
                $this->session->set_flashdata('msg-danger', 'error deleteing');
                redirect($this->agent->referrer());
            } // end else
        } else {
            redirect($this->agent->referrer());
        } // end else
    }


}
