
<?php
/**
* Parses and verifies the doc comments for file.
*
* PHP version 5.6
*
* @category  PHP
* @package   Teerapuch
* @author    Teerapuch Kassakul <teerapuch@hotmail.com>
* @copyright 2016 Teerapuch
* @license   http://opensource.org/licenses/mit-license.php MIT License
* @link      https://teerapuch.com
*/
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* Parses and verifies the doc comments for functions.
*
* @category  PHP
* @package   Teerapuch
* @author    Teerapuch Kassakul <teerapuch@hotmail.com>
* @copyright 2016 Teerapuch
* @license   http://opensource.org/licenses/mit-license.php MIT License
* @link      https://teerapuch.com
*/
class Vsoy extends MY_Controller
{
    /**
    * Main construct function and variables to use in all class
    *
    * @return session is_logged_in_admin
    */
    public function __construct()
    {
        parent::__construct();
        // Verify Admin Login
        // is_logged_in_admin();
    }
 

        public function product($id)
    {

       $style = array(
            "styles/product/vsoy.css",
        );
        $script = array(
           "plugins/bxslider/jquery.bxslider.min.js",
            "front/home.js",
        );
        $product   = $this->Product_model->findById($id);
        $data = array(
            'style' => $style,
            'script' => $script,
            'product'   => $product,
        );
        $this->load->view('product/vsoy/product', $data);
    }

         public function order($id)
    {

       $style = array(
            "styles/order/vsoy.css",
        );
        $script = array(
           "plugins/bxslider/jquery.bxslider.min.js",
            "front/home.js",
        );
        $product   = $this->Product_model->findById($id);
        $data = array(
            'style' => $style,
            'script' => $script,
            'product'   => $product,
        );
        $this->load->view('product/vsoy/order', $data);
    }
 

}
