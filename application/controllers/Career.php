<?php
/**
* Parses and verifies the doc comments for file.
*
* PHP version 5.6
*
* @category  PHP
* @package   Teerapuch
* @author    Teerapuch Kassakul <teerapuch@hotmail.com>
* @copyright 2016 Teerapuch
* @license   http://opensource.org/licenses/mit-license.php MIT License
* @link      https://teerapuch.com
*/
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* Parses and verifies the doc comments for functions.
*
* @category  PHP
* @package   Teerapuch
* @author    Teerapuch Kassakul <teerapuch@hotmail.com>
* @copyright 2016 Teerapuch
* @license   http://opensource.org/licenses/mit-license.php MIT License
* @link      https://teerapuch.com
*/
class Career extends MY_Controller
{
    /**
    * Main construct function and variables to use in all class
    *
    * @return session is_logged_in_admin
    */
    public function __construct()
    {
        parent::__construct();
        // Verify Admin Login
        // is_logged_in_admin();

    }
    /**
    * A summary informing the user what the associated element does.
    *
    * and to provide some background information or textual references.
    *
    * @return void
    */

    public function index()
    {
    
     $title = 'Green Spot Co.,Ltd บริษัท กรีนสปอต จำกัด : ไวตามิ้ลค์ วีซอย กรีนสปอร์ต นมถั่วเหลือง อาหารเพื่อสุขภาพ';
        $style = array(
          "styles/lightbox.css",
					"styles/career.css",
        );
        $script = array(
          'assets/js/bootstrap/modal.js',
          "front/lightbox.js",
					"front/career.js",
					"../plugins/isotope-docs/js/isotope-docs.min.js"
        );

     $hero = $this->Hero_model->findByJobcareer();
     $jobs = $this->Jobs_model->findAll();
     $career = $this->Content_model->findByPageSix();
      $data = array(
          'title'   => $title,
          'style'   => $style,
          'script'  => $script,
          'hero'    => $hero,
          'career' => $career,
          'jobs'  => $jobs
      );
      $this->load->view('header', $data);
      $this->load->view('jobcareer', $data);
      $this->load->view('footer', $data);
    }

    public function getdatajob()
    {
      $id = $this->input->post('id');
      // Recieve Last Row Data
      $data = $this->Jobs_model->findById($id);
      //add the header here
      echo json_encode($data);
    }

}
