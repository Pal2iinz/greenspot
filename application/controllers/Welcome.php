<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{

		$title = 'Green Spot Co.,Ltd บริษัท กรีนสปอต จำกัด : ไวตามิ้ลค์ วีซอย กรีนสปอร์ต นมถั่วเหลือง อาหารเพื่อสุขภาพ';
        $style = array(
            "styles/home.css",
        );
        $script = array(
        	"front/home.js",
        );

		$hero      = $this->Hero_model->findByHeroHome();
        $promotion = $this->Promotion_model->findByPromotion();
        $tvc       = $this->Promotion_model->findByTVC();
        $contents   = $this->Content_model->findByPageOne();
        $footer    = $this->Banner_model->findByPageOne();
        $social    = $this->Social_model->findBySocial();
        $data = array(
       		'title'         => $title,
       		'style'         => $style,
       		'script'        => $script,
            'hero'          => $hero,
            'promotion'     => $promotion,
            'tvc'           => $tvc,
            'contents'      => $contents,
            'footer'        => $footer,
            'social'        => $social
        );
		$this->load->view('header', $data);
		$this->load->view('home', $data);
		$this->load->view('footer', $data);
	}

	

}
