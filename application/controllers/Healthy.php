<?php
/**
* Parses and verifies the doc comments for file.
*
* PHP version 5.6
*
* @category  PHP
* @package   Teerapuch
* @author    Teerapuch Kassakul <teerapuch@hotmail.com>
* @copyright 2016 Teerapuch
* @license   http://opensource.org/licenses/mit-license.php MIT License
* @link      https://teerapuch.com
*/
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* Parses and verifies the doc comments for functions.
*
* @category  PHP
* @package   Teerapuch
* @author    Teerapuch Kassakul <teerapuch@hotmail.com>
* @copyright 2016 Teerapuch
* @license   http://opensource.org/licenses/mit-license.php MIT License
* @link      https://teerapuch.com
*/
class Healthy extends MY_Controller
{
    /**
    * Main construct function and variables to use in all class
    *
    * @return session is_logged_in_admin
    */
    public function __construct()
    {
        parent::__construct();
        // Verify Admin Login
        // is_logged_in_admin();

    }
    /**
    * A summary informing the user what the associated element does.
    *
    * and to provide some background information or textual references.
    *
    * @return void
    */
    public function index()
    {

        // set pagination
        $config['base_url']   = base_url('healthy');
        $config['total_rows'] = count($this->Blog_model->findByHealthy());
        $config['per_page']   = 6;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';            
        $config['prev_link'] = '&laquo;';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo;';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="healthy">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config["num_links"] = round( $config["total_rows"] / $config["per_page"] );
        $this->pagination->initialize($config);
        // get data from db
 
        $title = 'Green Spot Co.,Ltd บริษัท กรีนสปอต จำกัด : ไวตามิ้ลค์ วีซอย กรีนสปอร์ต นมถั่วเหลือง อาหารเพื่อสุขภาพ';
        $style = array(
            "styles/healthy.css",
        );
        $script = array(
             "front/healthy.js",
             "../plugins/isotope-docs/js/isotope-docs.min.js"

        );
        $hero       = $this->Hero_model->findByHeroHealthy();
        $healthy  = $this->Blog_model->findByHealthyPager($config);
    /*    $healthy    = $this->Blog_model->findByHealthy();*/
        $categories = $this->Categories_model->findAll();

        $data = array(
            'title'       => $title,
            'style'       => $style,
            'script'      => $script,
            'healthy'     => $healthy,
            'hero'        => $hero,
            'categories'  => $categories ,
        );

        $this->load->view('header', $data);
        $this->load->view('healthy', $data);
        $this->load->view('footer', $data);
    }

    public function menu()
    {
        // set pagination
        $config['base_url']   = base_url('healthy-menu');
        $config['total_rows'] = count($this->Blog_model->findByHealthyMenu());
        $config['per_page']   = 9;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';            
        $config['prev_link'] = '&laquo;';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo;';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="healthy">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config["num_links"] = round( $config["total_rows"] / $config["per_page"] );
        $this->pagination->initialize($config);
        // get data from db

         $title = 'Green Spot Co.,Ltd บริษัท กรีนสปอต จำกัด : ไวตามิ้ลค์ วีซอย กรีนสปอร์ต นมถั่วเหลือง อาหารเพื่อสุขภาพ';
        $style = array(
            "styles/healthy-menu.css",
        );
        $script = array(
                 "front/healthy-menu.js",
             "../plugins/isotope-docs/js/isotope-docs.min.js"
        );
        $hero       = $this->Hero_model->findByHeroHealthyMenu();
        $healthy  = $this->Blog_model->findByHealthyMenuPager($config);
        /*$healthy   = $this->Blog_model->findByHealthyMenu();*/
        $categories = $this->Categories_model->findAll();
        $data = array(
            'title'      => $title,
            'style'      => $style,
            'script'     => $script,
            'healthy'    => $healthy,
            'hero'       => $hero,
            'categories' => $categories ,
        );
        $this->load->view('header', $data);
        $this->load->view('healthy-menu', $data);
        $this->load->view('footer', $data);
    }

    public function formhealthy()
    {
     $id = $this->uri->segment(3);
     $title = 'Green Spot Co.,Ltd บริษัท กรีนสปอต จำกัด : ไวตามิ้ลค์ วีซอย กรีนสปอร์ต นมถั่วเหลือง อาหารเพื่อสุขภาพ';
        $style = array(
           "styles/healthy.css",
        );
        $script = array(
                 "front/healthy.js",
             "../plugins/isotope-docs/js/isotope-docs.min.js"
        );

        if (!isset($id)) {
            show_404();
        } else {
            $healthy = $this->Blog_model->findByIdHealthy($id);
        }
        if (empty($healthy)) {
            show_404();
        }
       $hero = $this->Hero_model->findByHeroHealthy();
        $data = array(
            'title'   => $title,
            'style'   => $style,
            'script'  => $script,
            'hero'    => $hero,
            'healthy' => $healthy
        );
        $this->load->view('header', $data);
        $this->load->view('healthy/form', $data);
        $this->load->view('footer', $data);
    }

    public function formmenu()
    {
     $id = $this->uri->segment(3);
     $title = 'Green Spot Co.,Ltd บริษัท กรีนสปอต จำกัด : ไวตามิ้ลค์ วีซอย กรีนสปอร์ต นมถั่วเหลือง อาหารเพื่อสุขภาพ';
        $style = array(
           "styles/healthy-menu.css",
        );
        $script = array(
                  "front/healthy-menu.js",
             "../plugins/isotope-docs/js/isotope-docs.min.js"
        );
        if (!isset($id)) {
            show_404();
        } else {
            $healthy = $this->Blog_model->findByIdMenu($id);
        }
        $hero = $this->Hero_model->findByHeroHealthyMenu();
        $data = array(
            'title'   => $title,
            'style'   => $style,
            'script'  => $script,
            'hero'    => $hero,
            'healthy' => $healthy
        );
        $this->load->view('header', $data);
        $this->load->view('healthy-menu/form', $data);
        $this->load->view('footer', $data);
    }
}
