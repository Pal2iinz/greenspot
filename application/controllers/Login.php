<?php
/**
* Parses and verifies the doc comments for file.
*
* PHP version 5.6
*
* @category  PHP
* @package   Digiday
* @author    Teerapuch Kassakul <teerapuch@hotmail.com>
* @copyright 2016 Digiday
* @license   http://opensource.org/licenses/mit-license.php MIT License
* @link      http://teerapuch.com
*/
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* Parses and verifies the doc comments for file.
*
* PHP version 5.6
*
* @category  PHP
* @package   Digiday
* @author    Teerapuch Kassakul <teerapuch@hotmail.com>
* @copyright 2016 Digiday
* @license   http://opensource.org/licenses/mit-license.php MIT License
* @link      http://teerapuch.com
*/
class Login extends CI_Controller
{
    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * http://example.com/index.php/welcome
     * - or -
     * http://example.com/index.php/welcome/index
     * - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     *
     * @see https://codeigniter.com/user_guide/general/urls.html
     */

    public function __construct()
    {
        parent::__construct();
        $this->sTitle = 'Green Spot';
        // set date now
        $this->dateTimeNow = date("Y-m-d H:i:s");
        $this->dateNow = date("Y-m-d");
        $this->timeNow = date("H:i:s");
    }
    /**
    * First Page for show user to management department
    *
    * @return void
    */
    public function index()
    {
        $this->load->view('login');
    }
    /**
    * First Page for show user to management department
    *
    * @return void
    */
    public function verifyAdmin()
    {
        // Send Variable For Validate In Model [ admin_model/validate ]
        $this->form_validation->set_rules(
            'email',
            'Email',
            'trim|required|xss_clean|valid_email'
        );
        $this->form_validation->set_rules(
            'password',
            'Password',
            'trim|required|xss_clean'
        );
        if ($this->form_validation->run() == false) {
            $this->load->view('login');
        } else {
            $aData = array(
                'email' => $this->security->xss_clean(
                    $this->input->post('email')
                ),
                'password' => $this->security->xss_clean(
                    $this->input->post('password')
                )
            );
            $result = $this->User_model->validate($aData);
            if (!$result) {
                $this->session->set_flashdata(
                    'error',
                    'Email or Password is incorrect'
                );
                redirect("login", "refresh");
            } else {
            $dep = $this->session->userdata['logged_in_admin']['department'];
                switch ($dep) {
                    case '1':
                        redirect("admin/home/", "refresh");
                        break;
                    default:
                        echo 'Department Incorrect Contact Administartor!!';
                        break;
                } // end switch
            } // else
        }
    }
    /*
     * Logout Admin
    */
    public function logout()
    {
        $id = $this->session->userdata['logged_in_admin']['id'];
        $data = array(
            'ipaddress' => null,
            'updatedate' => $this->dateTimeNow
        );
        $save = $this->User_model->update($id, $data);
        $this->session->sess_destroy('logged_in_admin');
        redirect("login","refresh");
    }
}
