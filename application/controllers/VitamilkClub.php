<?php
/**
* Parses and verifies the doc comments for file.
*
* PHP version 5.6
*
* @category  PHP
* @package   Teerapuch
* @author    Teerapuch Kassakul <teerapuch@hotmail.com>
* @copyright 2016 Teerapuch
* @license   http://opensource.org/licenses/mit-license.php MIT License
* @link      https://teerapuch.com
*/
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* Parses and verifies the doc comments for functions.
*
* @category  PHP
* @package   Teerapuch
* @author    Teerapuch Kassakul <teerapuch@hotmail.com>
* @copyright 2016 Teerapuch
* @license   http://opensource.org/licenses/mit-license.php MIT License
* @link      https://teerapuch.com
*/
class VitamilkClub extends MY_Controller
{
    /**
    * Main construct function and variables to use in all class
    *
    * @return session is_logged_in_admin
    */
    public function __construct()
    {
        parent::__construct();
        // Verify Admin Login
        // is_logged_in_admin();

    }
    /**
    * A summary informing the user what the associated element does.
    *
    * and to provide some background information or textual references.
    *
    * @return void
    */
    public function index()
    {

        // get data from db
        $title = 'Green Spot Co.,Ltd บริษัท กรีนสปอต จำกัด : ไวตามิ้ลค์ วีซอย กรีนสปอร์ต นมถั่วเหลือง อาหารเพื่อสุขภาพ';
        $style = array(
            "styles/vitamilkclub/custom.css",
        );
        $script = array(
             "front/event/vitamilkclub.js",
             "../plugins/isotope-docs/js/isotope-docs.min.js"
        );
        $contents   = $this->Content_model->findByPageEight();

        $data = array(
            'title'       => $title,
            'style'       => $style,
            'script'      => $script,
            'contents'  => $contents ,
        );
        $this->load->view('header', $data);
        $this->load->view('vitamilkclub', $data);
        $this->load->view('footer', $data);
    }


    public function save(){
      $id = $this->security->xss_clean(
        $this->input->post("id")
      );
      if(empty($id)){
        $sFullname = $this->security->xss_clean($this->input->post("sFullname"));
        $sPhone = $this->security->xss_clean($this->input->post("sPhone"));
        $sAccountFB = $this->security->xss_clean($this->input->post("sAccountFB"));
        $sBirthdate = $this->security->xss_clean($this->input->post("sBirthdate"));
        $iSize1 = $this->security->xss_clean($this->input->post("iSize1"));
        $sFullname2 = $this->security->xss_clean($this->input->post("sFullname2"));
        $iSize2 = $this->security->xss_clean($this->input->post("iSize2"));
        $CheckDataAccount = $this->Event_model->CheckAccount($sPhone,$sFacebook);
        
        //die();
        if($CheckDataAccount){
          $this->session->set_flashdata('msg-danger', 'คุณได้ทำการลงทะเบียนเข้าร่วมกิจกรรมนี้ไปแล้ว');
          redirect($this->agent->referrer());
        }else{
          $data = array(
          'fullname' => $sFullname,
          'phone' => $sPhone,
          'facebook' => $sAccountFB,
          'birthdate' => $sBirthdate,
          'size' => $iSize1,
          'fullname2' => $sFullname2,
          'size2' => $iSize2,
          'createdate' => $this->dateTimeNow,
        );

        $this->Event_model->save($data);
        //INSERT LAST ID
        $insert_id = $this->db->insert_id();
        $id = $insert_id;
        /**
        * Upload Multiple Files Tor
        */
        $attach[] = $_FILES['sAttachFile'];
        if($attach !== ''){
            $chkdata = $this->Event_model->findById($id);
            foreach ($chkdata as $key => $val) {
              $iNo = $val->id;
            }
            $number_of_files = count($_FILES['sAttachFile']['size']);
            $files =  $_FILES['sAttachFile'];
            $errors = array();
            $this->load->library('upload');
            // set config for upload file
            $config['upload_path'] = "assets/images/events/vitamilkclub/".$iNo;
            $config['allowed_types'] = "jpg|jpeg|gif|png|pdf|docx|xls|xlsx|pptx";
            $config['max_size']      = 1000000; // KB
            // load library upload
            $this->upload->initialize($config);

            if(!is_dir("assets/images/events/vitamilkclub/".$iNo)) //create the folder if it's not already exists
            {
              mkdir("assets/images/events/vitamilkclub/".$iNo,0777,TRUE);
            }

            for ($i = 0 ; $i < $number_of_files; $i++) {
              $_FILES['sAttachFile']['name'] = $files['name'][$i];
              $_FILES['sAttachFile']['type'] = $files['type'][$i];
              $_FILES['sAttachFile']['tmp_name'] = $files['tmp_name'][$i];
              $_FILES['sAttachFile']['error'] = $files['error'][$i];
              $_FILES['sAttachFile']['size'] = $files['size'][$i];
              // we retrieve the number of files that were uploaded
              $document = $this->upload->do_upload('sAttachFile');
              if($document){
                $data1 = $this->upload->data();
                $name_array[$i] = date("dmYHis").$i.$data1['file_ext'];
                rename($data1['full_path'], $data1['file_path'].$name_array[$i]);
              } // end if

              if(!empty($data1)){
                $data = array(
                  "attach"        => $name_array[$i],
                  "registerEID"         => $id,
                  "createdate"          => $this->dateTimeNow,
                );
                $this->Event_model->saveattach($data);
              }
            } // end for
          }
          $this->session->set_flashdata('msg-success', 'คุณได้ลงทะเบียนเข้าร่วมกิจกรรมเรียบร้อยแล้ว');
          redirect($this->agent->referrer());
        }
      }  
    } //End public function

    public function CheckValidatorFacebook(){
       $sFacebook =  $this->input->post('sFacebook');
       $data = $this->Event_model->CheckFacebook($sFacebook);
       echo json_encode($data);
    }

    public function CheckValidatorPhone(){
       $sPhone =  $this->input->post('sPhone');
       $data = $this->Event_model->CheckPhone($sPhone);
       echo json_encode($data);
    }

}
