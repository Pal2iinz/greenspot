<?php
/**
* Parses and verifies the doc comments for file.
*
* PHP version 5.6
*
* @category  PHP
* @package   Digiday
* @author    Teerapuch Kassakul <teerapuch@hotmail.com>
* @copyright 2016 Digiday
* @license   http://opensource.org/licenses/mit-license.php MIT License
* @link      http://www.digiday.co.th
*/
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* Parses and verifies the doc comments for functions.
*
* @category  PHP
* @package   Digiday
* @author    Teerapuch Kassakul <teerapuch@hotmail.com>
* @copyright 2016 Digiday
* @license   http://opensource.org/licenses/mit-license.php MIT License
* @link      http://www.digiday.co.th
*/
class MY_Controller extends CI_Controller {

	protected $data = array();

	function __construct()
	{
		parent::__construct();
		$this->data['sTitle'] = '';
		// set date now
        $this->dateTimeNow = date("Y-m-d H:i:s");
        $this->dateNow = date("Y-m-d");
        $this->timeNow = date("H:i:s");
	}

	/*
	 * Template For Admin
	 */
	protected function admin_layout($content = NULL) {

		$this->data['content'] = (is_null($content)) ? '' : $this->load->view('admin/'.$content, $this->data, TRUE);
		$this->load->view('layout/_masterlayout', $this->data);

	}

	protected function admin_css($custom_css = NULL) {
		$this->data['custom_css'] = (is_null($custom_css)) ? '' : $custom_css;
		$this->load->view('layout/_masterlayout', $this->data, TRUE);

	}

	protected function admin_js($custom_js = NULL) {

		$this->data['custom_js'] = (is_null($custom_js)) ? '' : $custom_js;
		$this->load->view('layout/_masterlayout', $this->data, TRUE);
	}

    protected function main_layout($content = NULL)
    {
        $this->data['content'] = (is_null($content)) ? '' : $this->load->view($content, $this->data, TRUE);
        $this->load->view('layout/_masterlayout', $this->data);
    }
    protected function main_css($custom_css = NULL)
    {
        $this->data['custom_css'] = (is_null($custom_css)) ? '' : $custom_css;
        $this->load->view('layout/_masterlayout', $this->data, TRUE);
    }
    protected function main_js($custom_js = NULL)
    {
        $this->data['custom_js'] = (is_null($custom_js)) ? '' : $custom_js;
        $this->load->view('layout/_masterlayout', $this->data, TRUE);
    }

}
?>
