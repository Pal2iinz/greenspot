<?php
$route['about']	= 'About/index';
///Product Vitamilk
$route['vitamilk']	= 'Product/index';
$route['vitamilk/(:num)']	='Vitamilk/product/$1';

///Product Vsoy
$route['vsoy']	= 'Product/vsoy';
$route['vsoy/(:num)']	= 'Vsoy/product/$1';

///Product GreenSpot
$route['greenspot']	= 'Product/greenspot';
///Product Healthy
$route['healthy']	= 'Healthy/index';
$route['healthy/(:num)']	= 'Healthy/index/$1';
$route['healthy/form/(:num)']	= 'Healthy/formhealthy/$1';
///Product Healthy-Menu
$route['healthy-menu']	= 'Healthy/menu';
$route['healthy-menu/(:num)']	= 'Healthy/menu';
$route['healthy-menu/form/(:num)']  = 'Healthy/formmenu/$1';

//Order
$route['orderlist']	= 'Order/index';
$route['ordervitamilk']	= 'Order/vitamilk';
$route['ordervitamilk/(:num)']	= 'Order/vitamilk/$1';
$route['ordervsoy']	= 'Order/vsoy';
$route['ordervsoy/(:num)']	= 'Order/vsoy/$1';
$route['ordergreenspot']	= 'Order/greenspot';

$route['jobcareer']	= 'Career/index';
$route['jobcareer/(:num)']	= 'Career/index/$1';
//16/11/2560
$route['pro-vita']	= 'Provita/index';
//16/01/2561
$route['vitamilkclub']	= 'VitamilkClub/index';
$route['vsoyclub']	= 'Vsoyclub/index';
$route['greenspotclub']	= 'Greenspotclub/index';

//Event
$route['vitamilkclub/save']	= 'VitamilkClub/save';
$route['CheckValidatorFacebook']	= 'VitamilkClub/CheckValidatorFacebook';
$route['CheckValidatorPhone']	= 'VitamilkClub/CheckValidatorPhone';

// $route['interbus']	= 'Interbus/index';

?>
