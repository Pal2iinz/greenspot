<?php
// Social Routes
$route['backend/social'] = 'backend/social/Social/index';
$route['backend/social/form'] = 'backend/social/Social/form/';
$route['backend/social/form/(:num)'] = 'backend/social/Social/form/$1';
$route['backend/social/save'] = 'backend/social/Social/save';
$route['backend/social/open'] = 'backend/social/Social/open';
$route['backend/social/close'] = 'backend/social/Social/close';
$route['backend/social/delete'] = 'backend/social/Social/delete';
$route['backend/social/deleteimage'] = 'backend/social/Social/deleteimage';
?>
