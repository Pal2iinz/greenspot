<?php
// Admin Controller
$route['admin/home'] = 'admin/home/Home';
// route for user management
// user
$route['admin/user'] = 'admin/user/User';
$route['admin/user/form'] = 'admin/user/User/form';
$route['admin/user/form/(:num)'] = 'admin/user/User/form/$1';
$route['admin/user/save'] = 'admin/user/User/save';
$route['admin/user/delete'] = 'admin/user/User/delete';
$route['admin/user/view/(:num)'] = 'admin/user/User/view/$1';
// department
$route['admin/department'] = 'admin/department/Department';
$route['admin/department/form'] = 'admin/department/Department/form';
$route['admin/department/form/(:num)'] = 'admin/department/Department/form/$1';
$route['admin/department/save'] = 'admin/department/Department/save';
$route['admin/department/delete'] = 'admin/department/Department/delete';
// role
$route['admin/role'] = 'admin/role/Role';
$route['admin/role/form'] = 'admin/role/Role/form';
$route['admin/role/form/(:num)'] = 'admin/role/Role/form/$1';
$route['admin/role/save'] = 'admin/role/Role/save';
$route['admin/role/delete'] = 'admin/role/Role/delete';
// permission
$route['admin/permission']             = 'admin/permission/Permission';
$route['admin/permission/form']        = 'admin/permission/Permission/form';
$route['admin/permission/form/(:num)'] = 'admin/permission/Permission/form/$1';
$route['admin/permission/save']        = 'admin/permission/Permission/save';
$route['admin/permission/delete']      = 'admin/permission/Permission/delete';
// access
$route['admin/user/access/(:num)'] = 'admin/user/Access';
$route['admin/user/access/save']   = 'admin/user/Access/save';
$route['admin/user/access/delete'] = 'admin/user/Access/delete';
?>
