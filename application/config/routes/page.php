<?php
// Homepage Routes
$route['backend/page/homepage'] = 'backend/page/Homepage/index';
// Promotion Routes
$route['backend/page/homepage/form'] = 'backend/page/Homepage/form';
$route['backend/page/homepage/form/(:num)'] = 'backend/page/Homepage/form/(:num)';
$route['backend/page/homepage/openpro'] = 'backend/page/Homepage/openpro';
$route['backend/page/homepage/closepro'] = 'backend/page/Homepage/closepro';
$route['backend/page/homepage/deletepro'] = 'backend/page/Homepage/deletepro';
// Hero Routes
$route['backend/hero'] = 'backend/page/Hero';
$route['backend/hero/form'] = 'backend/page/Hero/form';
$route['backend/hero/form/(:num)'] = 'backend/page/Hero/form/(:num)';
$route['backend/hero/save'] = 'backend/page/Hero/save';
$route['backend/hero/open'] = 'backend/page/Hero/open';
$route['backend/hero/close'] = 'backend/page/Hero/close';
$route['backend/hero/delete'] = 'backend/page/Hero/delete';
$route['backend/hero/delimage'] = 'backend/page/Hero/delimage';
// Banner Routes
$route['backend/banner'] = 'backend/page/Banner';
$route['backend/banner/form'] = 'backend/page/Banner/form';
$route['backend/banner/form/(:num)'] = 'backend/page/Banner/form/$1';
$route['backend/banner/save'] = 'backend/page/Banner/save';
$route['backend/banner/close'] = 'backend/page/Banner/close';
$route['backend/banner/open'] = 'backend/page/Banner/open';
$route['backend/banner/delete'] = 'backend/page/Banner/delete';
$route['backend/banner/delimage'] = 'backend/page/Banner/delimage';
// Content Routes
$route['backend/content'] = 'backend/page/Content';
$route['backend/content/form'] = 'backend/page/Content/form';
$route['backend/content/form/(:num)'] = 'backend/page/Content/form/$1';
$route['backend/content/save'] = 'backend/page/Content/save';
$route['backend/content/delete'] = 'backend/page/Content/delete';


?>
