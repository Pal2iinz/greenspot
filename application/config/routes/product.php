<?php
// Product Routes
$route['backend/product'] = 'backend/product/product/index';
$route['backend/product/save'] = 'backend/product/product/save';
$route['backend/product/form'] = 'backend/product/product/form';
$route['backend/product/form/(:num)'] = 'backend/product/product/form/$1';
// Categories Routes
$route['backend/product/categories'] = 'backend/product/Categories/index';
$route['backend/product/categories/save'] = 'backend/product/Categories/save';
$route['backend/product/categories/delete'] = 'backend/product/Categories/delete';
?>
