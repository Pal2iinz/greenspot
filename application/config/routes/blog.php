<?php
$route['backend/blog'] = 'backend/blog/Blog';
$route['backend/blog/form'] = 'backend/blog/Blog/form';
$route['backend/blog/form/(:num)'] = 'backend/blog/Blog/form/$1';
$route['backend/blog/save'] = 'backend/blog/Blog/save';
$route['backend/blog/delete'] = 'backend/blog/Blog/delete';
?>
