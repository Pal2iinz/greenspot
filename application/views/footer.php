  
<footer>
    <div class="footer" id="footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-2  col-md-2 col-sm-4 col-xs-6">
                    <h3> หน้าหลัก </h3>
                </div>
                <div class="col-lg-2  col-md-2 col-sm-4 col-xs-6">
                    <h3> เกี่ยวกับเรา </h3>
                    <ul>
                        <li> 
                        <?php 
                            echo anchor('about#history','ข้อมูลเกี่ยวกับบริษัท');
                        ?>
                        </li>
                        <li> 
                        <?php 
                            echo anchor('about#vision','วิสัยทัศน์ พันธกิจ');
                        ?>
                        </li>
                        <li> 
                        <?php 
                            echo anchor('about#popularity','ค่านิยม');
                        ?>
                        </li>
                        <li> 
                         <?php 
                            echo anchor('about#manager','โครงสร้างผู้บริหาร');
                        ?>
                        </li>
                        <li>
                        <?php 
                            echo anchor('about#reward','รางวัลต่างๆ');
                        ?>
                        </li>
                        
                    </ul>
                </div>
                <div class="col-lg-2  col-md-2 col-sm-4 col-xs-6">
                    <h3>ผลิตภัณฑ์</h3>
                    <ul>
                      <li>
                        <?php 
                            echo anchor('vitamilk','ไวตามิ้ลค์');
                        ?>
                      </li>
                       <li>
                        <?php 
                            echo anchor('vsoy','วีซอย');
                        ?>
                      </li>
                       <li>
                        <?php 
                            echo anchor('greenspot','กรีนสปอต');
                        ?>
                      </li>
                    </ul>
                </div>
                <div class="col-lg-2  col-md-2 col-sm-4 col-xs-6">
                    <h3> มุมสุขภาพ </h3>
                     <ul>
                        <li> <?php echo anchor('healthy','สาระสุขภาพ');?></li>
                        <li><?php echo anchor('healthy-menu','เมนูเพื่อสุขภาพ');?></li>
                    <!--    <li> <a href="#"> กรีนสปอต </a> </li>-->
                    </ul>
                </div>
                  <div class="col-lg-2  col-md-2 col-sm-4 col-xs-6">
                    <h3> ติดต่อเรา </h3>
                    <ul>
                        <li>
                        <?php 
                            echo anchor('jobcareer','ร่วมงานกับเรา');
                        ?>
                        </li>
                    </ul>
                </div>
            </div>
            <!--/.row--> 
        </div>
        <!--/.container--> 
    </div>
    <!--/.footer-->
    <section id="copyright">
     <div class="container">
        <p class="pull-left">Copyright © 2015 Green Spot Co.,Ltd. all rights reserved </p>
    </div>
    </section>
</footer>

 
    <script src="<?php echo base_url();?>assets/js/jquery-3.2.1.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/plugins/owlcarousel/owl.carousel.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/front/main.js"></script>
     <script src="http://vjs.zencdn.net/6.1.0/video.js"></script>
     

    <script>
    $(document).ready(function() {
        var screensize = document.documentElement.clientWidth;
    if (screensize  < 1280) {
        
    } else {
        $("li").hover(function() { 
        $(this).find("ul").slideToggle("fast"); 
        });
    }
    });
    </script>

<?php
    if (isset($script)) {
        foreach ($script as $key => $js) {
            echo js_asset($js);
        }
    }
?>


   </body>
</html>