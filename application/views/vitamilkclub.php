<section id='vitamilkclub' class='clearfix'>
	<div class="col-xs-12 col-sm-12 col-md-12" style="position: absolute;">
	<?php
	    if ($this->session->flashdata('msg-success')) {
	?>
	    <div class="alert alert-success" role="alert">
	        <?php echo $this->session->flashdata('msg-success'); ?>
	    </div>
	<?php
	} // end if msg
	    if ($this->session->flashdata('msg-danger')) {
	?>
	    <div class="alert alert-danger alert-dismissible" role="alert">
	        <button type="button" class="close" data-dismiss="alert">x</button>
	        <?php echo $this->session->flashdata('msg-danger'); ?>
	    </div>
	<?php
		} // end if msg
	?>
</div>
		<div id="Bbackground">
			
		</div>
			<!-- <div id="frmbox">
				<div class="col-xs-12 col-md-12">
				<?php
					$img = array(
			        'src' => base_url().'assets/images/events/vitamilkclub/frmbox.png',
			        'class' => 'img-responsive',
			    );
			    echo img($img);
				?>
				</div>
			</div>
 -->


		<div class="container-fluid">
			<div class="row">
		<div id="moblie" class="col-xs-12 col-sm-12 visible-xs visible-sm mobile_regisform" >
			 <?php /*echo form_open_multipart('vitamilkclub/save',array('class'=>'form-horizontal'));*/ ?> 
			 <div class="form_setting">
			<form action="http://www.greenspot.co.th/vitamilkclub/save" class="form-horizontal" enctype="multipart/form-data" method="post" accept-charset="utf-8">
			<div>
				<div class="form-group">
					<div class="">
						<input type="text" name="sFullname" class="form-control textbox_vitamilk" required>
					</div>
				</div>
		 </div>
		 <div>
			<div class="form-group">
				<div class="">
					<input type="text" name="sPhone" class="form-control textbox_vitamilk" required>
				</div>
			</div>
		</div>
		<div>
			<div class="form-group">
				<div class="">
					<input type="text"  name="sAccountFB" class="form-control textbox_vitamilk" required="">
				</div>
			</div>
		</div>
		<div>
			<div class="form-group">
				<div class="">
					<input type="date" name="sBirthdate" class="form-control textbox_vitamilk" required>
				</div>
			</div>
		</div>
		<div>
			<div class="form-group">
					<div class="">
						<select name="iSize1" class="form-control textbox_vitamilk" required>
							<option value="1">S รอบอก 32" ความยาว 25"</option>
							<option value="2">M รอบอก 36" ความยาว 27"</option>
							<option value="3">L รอบอก 40" ความยาว 29"</option>
							<option value="4">XL รอบอก 44" ความยาว 31"</option>
							<option value="5">2XL รอบอก 48" ความยาว 32"</option>
							<option value="6">3XL รอบอก 52" ความยาว 33"</option>
						</select>
					</div>
				</div>
		</div>
		<div>
			<div class="form-group">
					<div class="">
						<input type="text" name="sFullname2" class="form-control textbox_vitamilk" required>
					</div>
				</div>
		</div>
		<div>
			<div class="form-group">
					<div class="">
						<select name="iSize2" class="form-control textbox_vitamilk" required>
							<option value="1">S รอบอก 32" ความยาว 25"</option>
							<option value="2">M รอบอก 36" ความยาว 27"</option>
							<option value="3">L รอบอก 40" ความยาว 29"</option>
							<option value="4">XL รอบอก 44" ความยาว 31"</option>
							<option value="5">2XL รอบอก 48" ความยาว 32"</option>
							<option value="6">3XL รอบอก 52" ความยาว 33"</option>
						</select>
					</div>
				</div>
		</div>
		<div>
				<div class="">
			  	<button class="btnn2" onclick="javascript:$('#inputfile').click();">Upload</button>
					<input style="display:none" id="inputfile" type="file" name="sAttachFile[]" multiple="" required/>
				</div>
		</div>
		<div class="share_submit">
		<div >
			<div class="bbsubmit">
				<button id="btnsubmit" type="submit">
					<?php
						$img = array(
				        'src' => base_url().'assets/images/events/vitamilkclub/btnsubmit.png',
				        'class' => 'img-responsive',
				    );
				    echo img($img);
					?>
				</button>
			</div>
		</div>
		<div >
				<div class="bbsharefb">
					<?php
						$current_url = "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
					?>
					<?php echo anchor("http://www.facebook.com/",img(array('src'=> base_url().'assets/images/events/vitamilkclub/btnfb.png',
						'class' => 'img-responsive')),
					array('id '=> 'btnsharefb','class'=>'fb','onclick'=>"popUp=window.open(
					'http://www.facebook.com/sharer.php?u=$current_url',
					'popupwindow',
					'scrollbars=yes,width=800,height=400');
					popUp.focus();
					return false")); 
					?>
				</div>
		 </div>
		</div>
	</form>
			<?php /*echo form_close();*/?>

		</div>
		</div>
	</div>
</div>

		<div id="desktop" class="visible-md visible-lg desktop_regisform">
			<div class="form_setting">
			<?php echo form_open_multipart('vitamilkclub/save',array('class'=>'form-horizontal'));?>
			<div id="sectionform1">
				<div class="form-group">
					<div class="">
						<input type="text" name="sFullname" class="form-control textbox_desktop" required="">
					</div>
				</div>
				<div class="form-group">
					<div class="">
						<input type="text" name="sPhone" class="form-control textbox_desktop" required="">
					</div>
				</div>
				<div class="form-group">
					<div class="">
						<input type="text"  name="sAccountFB" class="form-control textbox_desktop" required="">
					</div>
				</div>
				<div class="form-group">
					<div class="">
						<input type="date" name="sBirthdate" class="form-control textbox_desktop" required="">
					</div>
				</div>
			</div>
			<div id="sectionform2">
				<div class="form-group">
					<div class="">
						<select name="iSize1" class="form-control textbox_desktop">
							<option value="1">S รอบอก 32" ความยาว 25"</option>
							<option value="2">M รอบอก 36" ความยาว 27"</option>
							<option value="3">L รอบอก 40" ความยาว 29"</option>
							<option value="4">XL รอบอก 44" ความยาว 31"</option>
							<option value="5">2XL รอบอก 48" ความยาว 32"</option>
							<option value="6">3XL รอบอก 52" ความยาว 33"</option>
						</select>
					</div>
				</div>
			</div>
			<div id="sectionform3">
				<div class="form-group">
					<div class="">
						<input type="text" name="sFullname2" class="form-control textbox_desktop" required="">
					</div>
				</div>
			</div>
			<div id="sectionform4">
				<div class="form-group">
					<div class="">
						<select name="iSize2" class="form-control textbox_desktop">
							<option value="1">S รอบอก 32" ความยาว 25"</option>
							<option value="2">M รอบอก 36" ความยาว 27"</option>
							<option value="3">L รอบอก 40" ความยาว 29"</option>
							<option value="4">XL รอบอก 44" ความยาว 31"</option>
							<option value="5">2XL รอบอก 48" ความยาว 32"</option>
							<option value="6">3XL รอบอก 52" ความยาว 33"</option>
						</select>
					</div>
				</div>
			</div>
			<div class="">
				<div class="">
			  	<button class="btnn2" onclick="javascript:$('#inputfile_desktop').click();">Upload</button>
				<input style="display: none;" id="inputfile_desktop" type="file" name="sAttachFile[]" multiple="" required="" />
				</div>
			</div>
			<div class="share_submit">
			<div class="bbsubmit">
				<button id="btnsubmit" type="submit">
					<?php
						$img = array(
				        'src' => base_url().'assets/images/events/vitamilkclub/btnsubmit.png',
				        'class' => 'img-responsive',
				    );
				    echo img($img);
					?>
				</button>
			</div>

			<div class="bbsharefb">
				<?php
					$current_url = "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
				?>
				<?php echo anchor("http://www.facebook.com/",img(array('src'=> base_url().'assets/images/events/vitamilkclub/btnfb.png',
					'class' => 'img-responsive')),
				array('id '=> 'btnsharefb','class'=>'fb','onclick'=>"popUp=window.open(
				'http://www.facebook.com/sharer.php?u=$current_url',
				'popupwindow',
				'scrollbars=yes,width=800,height=400');
				popUp.focus();
				return false")); 
				?>
			</div>

		</div>
			<?php echo form_close();?>
		</div>


		</div>

				<div id="conditionform1" class="container">
			<div class="row">
			<div class="">
				<?php
					$img = array(
			        'src' => base_url().'assets/images/events/vitamilkclub/Condition1.png',
			        'class' => 'img-responsive',
			    );
			    echo img($img);
				?>
			</div>
		</div>
		</div>


		<div id="contentcndition">
			<div class="">
				<div class="content">
				<?php 
					foreach ($contents as $key => $c) {
						$content  = $c['content'];
					}
					 echo $content;
				?>
				</div>
			</div>
		</div>
		<div id="conditionform2">
			<div class="">
				<a id="btnCondition" data-toggle="modal" data-target="#frmCondition">
					<?php
						$img = array(
				        'src' => base_url().'assets/images/events/vitamilkclub/Condition2.png',
				        'class' => 'img-responsive',
				    );
				    echo img($img);
					?>
				</a>
			</div>
		</div>
</section>

<div class="modal fade" id="frmCondition" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
  <div class="modal-dialog modal-lg">
    <div class="modal-condition">
      <div class="modal-header">
        <?php
					$img = array(
			        'src' => base_url().'assets/images/events/vitamilkclub/ConditionHead.png',
			        'class' => 'img-responsive',
			    );
			    echo img($img);
				?>
      </div>
      <div class="modal-body">
						<div class="form-group">
        		<div class="col-md-12">
	          <ul class="frmcontent">
	          	<li>ผู้ร่วมกิจกรรมสามารถร่วมสนุกโดยการซื้อไวตามิ้ลค์ สูตรใดขนาดใดก็ได้  ที่ 7 – Eleven  ครบ 100 บาท  แล้วเก็บใบเสร็จไว้</li>
	          	<li>ถ่ายรูปคู่กับเพื่อนซี้หรือเดี่ยวในท่าดื่มไวตามิ้ลค์ที่คิดท่าว่าเท่ที่สุดในแบบคุณ และอัพโหลดพร้อมใบเสร็จที่ซื้อไวตามิ้ลค์จาก 7 – Eleven ครบ 100 บาท เข้ามาที่ website Vitamilk Club หรือ สแกนบาร์โค้ดจากข้างขวดหรือกล่องไวตามิ้ลค์ พร้อม ลงทะเบียนกรอกข้อมูลให้ครบถ้วน</li>
	          	<li>กดแชร์ Facebook ของตนเอง (ตั้งสถานะเป็น Public) พร้อม #ไวตามิ้ลค์  #กายพร้อมใจพร้อมเราทำได้</li>
	          	<li>ท่าดื่มของใคร โดนใจ 3 เจ้า มากที่สุดเตรียมไปท็อบฟอร์มกันเป็นคู่กับ 3 เจ้ากันเลย!!</li>
	          	<li>ประกาศรายชื่อผู้โชคดี ในวันที่ 11 เมษายน พ.ศ. 2561 ทาง Facebook Vitamilk Thailand</li>
	          	<li>ระยะเวลาร่วมกิจกรรม: วันที่ 13  มีนาคม ถึง 8 เมษายน พ.ศ. 2561</li>
	          	<li>ผู้โชคดีที่ได้รับการคัดเลือกจะได้สิทธิ์ในการเข้าร่วมแข่งขัน งาน VITAMILK SPORT DAY พร้อมมันส์กับมินิคอนเสิร์ตแบบใกล้ชิด กับ เจ้านาย เจ้าขุน และเจ้าสมุทร แบบใกล้ชิดจำนวน 150 สิทธิ สิทธิละ 2 ที่นั่ง ในวันที่ 21 เมษายน พ.ศ. 2561 ณ ลานกิจกรรมด้านหน้าศูนย์การค้า The Street รัชดา
	          	</li>
	          	<li>ผู้โชคดีที่ได้รับการคัดเลือกทั้งหมดพร้อมเพื่อนสนิทคุณจะถูกแบ่ง ออกเป็น 3 ทีม (ทีมเจ้านาย, ทีมเจ้าขุน และ ทีมเจ้าสมุทร) เพื่อเข่าร่วมการแข่งขันกีฬาสุดมันส์พร้อม 3 เจ้า  โดยทีมที่ชนะจะได้รับสิทธิไปมันส์ติดขอบเวที ในโซน VIP กับ 3 เจ้ามินิคอนเสิร์ต</li>
	          	<li>ผู้ที่ไม่ได้รับการคัดเลือกและประกาศรายชื่อมีสิทธิเข้าร่วมงานและมินิคอนเสิร์ต กับ 3 เจ้า ในโซน Fanclub แต่ไม่มีสิทธิเข้าร่วมการแข่งขันกีฬาสุดมันส์</li>
	          	<li>ผู้โชคดีที่ได้รับการคัดเลือกจะต้องยืนรับสิทธิ์ในการเข้าร่วมกิจกรรมภายในวันที่ 16 เมษายน พ.ศ. 2561 (หากเกินภายในเวลาดังกล่าวถือว่าสละสิทธิ์)</li>
	          	<li>ผู้ร่วมกิจกรรมต้องอัพโหลดภาพ 2 ภาพ คือ 1. ภาพดื่มไวตามิ้ลค์สุดเท่ในแบบคุณ และ 2. ภาพใบเสร็จจากการซื้อไวตามิ้ลค์ สูตรใด ขนาดก็ได้ ครบ 100 บาท จาก 7 – Eleven และ ต้องทำกิจกรรมครบทุกขั้นตอนและกรอกรายละเอียดให้ครบถ้วน ถึงจะมีสิทธิ์ในการรับของรางวัล </li>
	          	<li>ผู้ร่วมกิจกรรมสามารถลงทะเบียนโพสต์รูปกิจกรรม โดยทุกภาพและข้อความจะต้องถูกต้องตามกติกาและไม่ขัดต่อศีลธรรมอันดีของประชาชน หากพบภาพที่ไม่เหมาะสมทางคณะกรรมการมีสิทธิ์ขาดในการลบภาพและข้อความดังกล่าวออก โดยไม่ต้องแจ้งให้ทราบล่วงหน้า</li>
	          	<li>ผู้ร่วมกิจกรรมต้องทำกิจกรรมครบทุกขั้นตอนและกรอกรายละเอียดให้ครบถ้วน ถึงจะมีสิทธิ์ในการรับของรางวัล</li>
	          	<li>ขอสงวนสิทธิ์ให้รางวัล 1 คน/ 2 สิทธิ ตลอดระยะการร่วมกิจกรรมเท่านั้น </li>
	          	<li>สิทธิในการเข้าร่วมกิจกรรมดังกล่าวไม่สามารถเปลี่ยนแปลงเป็นเงินสดได้</li>
	          	<li>หากคณะกรรมการพิจารณาแล้วเห็นว่า ผู้ร่วมกิจกรรมท่านใดมิได้ปฏิบัติตาม กฎและข้อตกลงของกิจกรรม หรือมีเจตนาทำลายการดำเนินกิจกรรม คณะกรรมการมีสิทธิ์แต่เพียงผู้เดียวในการพิจารณามิให้ผู้นั้นเป็นผู้ร่วมกิจกรรมต่อไป และมีสิทธิ์เปลี่ยนแปลง แก้ไข หรือยกเลิกกฎ กติกา และการให้รางวัลตามความเหมาะสมเมื่อใดก็ได้</li>
	          	<li>ทางบริษัทฯ จะบันทึกภาพในรูปแบบของภาพถ่ายและคลิปวิดีโอ และทางบริษัทฯ มีสิทธิ์ในการนำไปเผยแพร่และดัดแปลงตามความเหมาะสม เพื่อประชาสัมพันธ์ในช่องทางต่างๆ หรือสื่อประเภทใดก็ตาม และไม่จำกัดเวลาการใช้งานไม่ว่าเพื่อวัตถุประสงค์ใดๆ รวมถึงไม่จำกัดเฉพาะในกิจกรรมส่งเสริมการขาย การตลาด หรือการประชาสัมพันธ์ในอนาคตโดยที่ไม่ต้องแจ้งให้ทราบล่วงหน้าและไม่มีค่าตอบแทนใดๆ ทั้งสิ้น</li>
	          	<li>ของรางวัลต่างๆ ที่มอบให้กับผู้ได้รับรางวัลไม่สามารถเปลี่ยนเป็นเงินสดได้</li>
	          	<li>คำตัดสินของคณะกรรมการถือเป็นที่สิ้นสุด</li>
	          </ul>
	        	</div>
        	</div>
        </div>
        	<div class="Linetab">
        	<?php
						$img = array(
				        'src' => base_url().'assets/images/events/vitamilkclub/linetab.png',
				        'class' => 'img-responsive',
				    );
				    echo img($img);
					?>
        </div>
	      <div class="modal-footer">
	      	<div class="col-md-2 col-sm-2 col-xs-3 col-xs-offset-10 col-md-offset-9">
			      <a data-dismiss='modal'>
			      	<?php
								$img = array(
					        'src' => base_url().'assets/images/events/vitamilkclub/btnclose.png',
					        'class' => 'img-responsive',
						    );
						    echo img($img);
							?>
						</a>
					</div>
	      </div><!-- /.modal-footer -->
    	</div><!-- /.modal-content -->
  	</div><!-- /.modal-doalog -->
	</div>
</div>

<div class="modal fade" id="VitamilkYoutube" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
	    <div class="modal-headeryt">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	     </div>
      <div class="modal-body">
        <div class="form-group">
           <iframe width="100%" height="560px" src="https://www.youtube.com/embed/tplssjSNJfA?autoplay=1" frameborder="0"  allowfullscreen></iframe>
        </div>
    	</div><!-- /.modal-content -->
  	</div><!-- /.modal-doalog -->
	</div>
</div>