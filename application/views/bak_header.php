<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Navbar Template for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/styles/header.css" rel="stylesheet">
    <link href="assets/css/styles/footer.css" rel="stylesheet">

      <?php
            if (isset($style)) {
                foreach ($style as $key => $css) {
                    echo css_asset($css);
                }
            }
        ?>
  </head>
  <body class="greenspot">
<div id="Hero">

<div id="Desktop" class=" visible-md visible-lg">

    <div class="container">
    <header class="head visible-md visible-lg">
      <!-- Static navbar -->
      <nav class="navbar navbar-desktop ">
        <div class="container-fluid">
          <div class="navbar-header">
            <a class="navbar-brand" href="#">
              <img src="http://greenspot.co.th/images/global/logo.png" class="desktop-logo img-responsive">
            </a>
          </div>
          <div  class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
              <li class="active">
              <a href="./">
                <img src="<?php echo base_url();?>/assets/images/nav/home.png">
                    หน้าหลัก
              </a>
              </li>
              <li>
              <a href="about">
                <img src="<?php echo base_url();?>/assets/images/nav/about.png">
                    เกี่ยวกับเรา
              </a>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <img src="<?php echo base_url();?>/assets/images/nav/product.png"> ผลิตภัณฑ์</a>
                <ul class="dropdown-menu">
                  <li><?php echo anchor('vitamilk','ไวตามิ้ลค์');?></li>
                  <li><?php echo anchor('vsoy','วีซอย');?></li>
                  <li><?php echo anchor('greenspot','กรีนสปอต');?></li>
                </ul>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <img src="<?php echo base_url();?>/assets/images/nav/menu.png"> มุมสุขภาพ</a>
            
                <ul class="dropdown-menu">
                    <div class="submenu">
                    <?php echo anchor('vitamilk','ไวตามิ้ลค์');?>
                 <!-- <li><?php echo anchor('vitamilk','ไวตามิ้ลค์');?></li>
                  <li><?php echo anchor('vsoy','วีซอย');?></li>
                  <li><?php echo anchor('greenspot','กรีนสปอต');?></li>-->
                  </div>
                </ul>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <img src="<?php echo base_url();?>/assets/images/nav/menu.png"> สั่งซื้อสินค้า</a>
                <ul class="dropdown-menu">
                  <li class="submenu"><?phpecho anchor('vitamilk','ไวตามิ้ลค์');?></li>
                  <li><?php echo anchor('vsoy','วีซอย');?></li>
                  <li><?php echo anchor('greenspot','กรีนสปอต');?></li>
                </ul>
              </li>
              <li> 
             <a href="http://www.greenspot.co.th/interbus/">
                <img src="<?php echo base_url();?>/assets/images/nav/world.png">
                    INTERNATIONAL BUSINESS
              </a>
              </li>
            </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </nav>
      </header>
    </div> <!-- /container -->
    
  </div><!-- /.visible-md .visible-lg-->
</div><!-- /#Desktop -->

<!-- Moblie --> 
<div id="Moblie" class="visible-xs visible-sm">

<nav class="navbar navbar-default navbar-fixed-top visible-xs visible-sm">
     <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#MenuMoblie" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        <a class="navbar-moblie " href="#">
              <img src="http://greenspot.co.th/images/global/logo.png" class="logo-moblie img-responsive">
            </a>
        </div>
        <div id="MenuMoblie" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="#">Home</a></li>
            <li><a href="#about">About</a></li>
            <li><a href="#contact">Contact</a></li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="#">Action</a></li>
                <li><a href="#">Another action</a></li>
                <li><a href="#">Something else here</a></li>
                <li role="separator" class="divider"></li>
                <li class="dropdown-header">Nav header</li>
                <li><a href="#">Separated link</a></li>
                <li><a href="#">One more separated link</a></li>
              </ul>
            </li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

    </div><!-- /#Moblie -->


