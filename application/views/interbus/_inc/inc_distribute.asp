
<script language="JavaScript">
			function CheckOther(objValue)
			{
					f = document.form;
					if(objValue == 'Other')
					{
						f.Other.style.display='';
					}
					else
					{
						f.Other.style.display='none';
					}
			}
</script>
<%
			UserId = Session("UserId")
			
			Set objUserData = New User
			Set oUserData = New UserData
		
			if UserId <> "" then
					oUserData.GetUserData UserId, objUserData
			end if
		
			Set oItems = Nothing
			With objUserData
		
%>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td align="right"><a href="default.asp?topic=distribute&tag=order" class="sub">Distributor Information</a> 
	| <a href="default.asp?topic=distribute&tag=changepassword" class="sub"> Change Password</a>
	| <a href="Logout.asp" class="sub"> Logout</a>
	</td>
  </tr>
</table>
<TABLE cellpadding="0" cellspacing="0"  border=0 width="100%">
  <form name="form" method="post" action="default.asp?topic=distribute&tag=order"> 
<TR><TD><TABLE width="100%" border=0 align="center" cellpadding="2" cellspacing="0" class="fontGrey12px"  >
          <td width="357"> 
          <TBODY>
		  <% IF Request("sedit") = 1 THEN %>
		    <TR align="center"> 
              <TD  colspan="4" class="sub_menu1" > Edit Information Complete </TD>
		  </TR>
		  <% END IF %>
            <TR bgcolor="#FFCC00"> 
              <TD  colspan="4" valign="top" bgcolor="#FFCC00" class="sub"><strong>Contact 
                Person</strong></TD>
            </TR>
            <tr> 
              <td align="right">Username</td>
              <td colspan="3"><b> 
                <input name="susername" value="<%=.UserName%>"  readonly class="inputBoxNormal" size="20" maxlength="20" />
                <font color="#FF6600">*</font></b></td>
            </tr>
            <TR class="label"> 
              <TD align="right" bgcolor="#F7F7F7">E-mail Address</TD>
              <TD colspan="3" bgcolor="#F7F7F7"><b> 
                <input size="20" class="inputBoxNormal" type="text" name="Email" value="<%=.Email%>">
                <font color="#FF6600">*</font></b></TD>
            </TR>
            <TR> 
              <td height="29" align="right" >Name- Lastname</td>
              <td colspan="3" > <select size=1 name=FName   Class="inputBoxNormal" style="font-family: Ms sans serif; font-size: 8pt;">
                  <% 	FName = str_blank(.FName)
						  			Call ListBoxArrayList(arrFName,FName) %>
                </select> <input name="NameEn" class="inputBoxNormal" size="20" maxlength="80" value="<%=.NameEn%>"> 
                <b> </b> <font color="#FF6600">* 
                <input name="LastNameEn" class="inputBoxNormal" size="20" maxlength="80" value="<%=.LastNameEn%>">
                <b> </b> <font color="#FF6600">* </font></font></td>
            </TR>
            <TR> 
              <td align="right" valign="top" bgcolor="#F7F7F7" >Address</td>
              <td colspan="3" bgcolor="#F7F7F7" > 
                <textarea name="Address" cols="50" rows="3" class="inputBoxNormal"><%=.Address%></textarea> 
                <b> </b> </td>
            </TR>
            <TR> 
              <td height="25" align="right" >Country</td>
              <td > <%
			CounId = Str_blank(.CounId)
			Call ListBoxFront("- - Country - -","TabCountry","","CounId","CounId","Country",CounId,"","")
			%> </td>
              <td align="right" >&nbsp;</td>
              <td >&nbsp;</td>
            </TR>
            <TR> 
              <td height="25" bgcolor="#F7F7F7" > 
                <div align="right">Street&nbsp;</div></td>
              <td width="149" bgcolor="#F7F7F7" > 
                <input name="Street" class="inputBoxNormal" value="<%=.Street%>" size="15"> 
                <b> </b> </td>
              <td width="129" align="right" bgcolor="#F7F7F7" >City</td>
              <td width="374" bgcolor="#F7F7F7" > 
                <input name="City" class="inputBoxNormal" value="<%=.City%>" size="15" maxlength="80"> 
              </td>
            </TR>
            <TR> 
              <TD height="25" align="right">District</TD>
              <TD><input name="District" class="inputBoxNormal" value="<%=.District%>" size="15"> 
              </TD>
              <TD align="right">Province</TD>
              <TD><input name="Province" class="inputBoxNormal" value="<%=.Province%>" size="15" maxlength="80"> 
                <b> </b> </TD>
            </TR>
            <TR> 
              <td height="25" align="right" bgcolor="#F7F7F7" >Post Code</td>
              <td colspan="3" bgcolor="#F7F7F7" > 
                <input name="PostCode" class="inputBoxNormal" value="<%=.PostCode%>" size="20" maxlength="5"> 
              </td>
            </TR>
            <TR> 
              <td height="27" align="right" >Telephone</td>
              <td colspan="3" > <input name="Tel" class="inputBoxNormal" value="<%=.Tel%>" size="20"> 
                <b> </b> <font color="#FF6600">*</font> &nbsp;Fax No&nbsp; <input name="Fax" class="inputBoxNormal" value="<%=.Fax%>" size="20"></td>
            </TR>
            <TR> 
              <td height="25" align="right" >&nbsp;</td>
              <td colspan="3" >&nbsp; </td>
            </TR>
            <TR bgcolor="#FFCC00"> 
              <TD colspan="4"  class="sub"><strong>Company Information</strong></TD>
            </TR>
            <TR> 
              <td height="25" align="right" >Name</td>
              <td colspan="3" > <input name="ComName" class="inputBoxNormal" size="30" maxlength="80" value="<%=.ComName%>"> 
                <font color="#FF6600">*</font> </td>
            </TR>
            <TR> 
              <td align="right" valign="top" bgcolor="#F7F7F7" > Address</td>
              <td colspan="3" bgcolor="#F7F7F7" > 
                <textarea name="ComAddress" cols="50" rows="3" class="inputBoxNormal"><%=.ComAddress%></textarea> 
                <font color="#FF6600">*</font> <b> </b> </td>
            </TR>
            <TR> 
              <td height="27" align="right" > Telephone</td>
              <td colspan="3" > <input name="ComTel" class="inputBoxNormal" value="<%=.ComTel%>" size="20"> 
                <font color="#FF6600">*</font> Fax No 
                <input name="ComFax" class="inputBoxNormal" value="<%=.ComFax%>" size="20"> 
              </td>
            </TR>
            <TR> 
              <td height="25" align="right" bgcolor="#F7F7F7" >Website (if any)</td>
              <td colspan="3" bgcolor="#F7F7F7" > 
                <input name="Website" class="inputBoxNormal" value="<%=.Website%>" size="20">
                E-mail Address 
                <input name="ComEmail" class="inputBoxNormal" value="<%=.ComEmail%>" size="20"></td>
            </TR>
            <TR> 
              <TD align="right">Nature of business/Company profile</TD>
              <td colspan="3" ><select size=1 name=ComBus  Class="inputBoxNormal" OnChange="CheckOther(this.value);">
                  <%
					ComBus = Str_blank(.ComBus)
			 	Call ListBoxArrayList(arrBus,ComBus) 
				%>
                </select> <input name="Other" class="inputBoxNormal" value="<%=.Other%>" size="50" <%if ComBus<>"Other" Then response.write "style='display:none'" End IF%>></td>
            </TR>
            <TR> 
              <TD align="right" bgcolor="#F7F7F7">How many years has the <br>
                company been in business?</TD>
              <td colspan="3" bgcolor="#F7F7F7" > 
                <input name="Years" class="inputBoxNormal" value="<%=.Years%>" size="20"></td>
            </TR>
            <TR> 
              <TD align="right">What is the size of your sales team? </TD>
              <td colspan="3" ><input name="SizeOfSalesTeam" class="inputBoxNormal" value="<%=.SizeOfSalesTeam%>" size="20"></td>
            </TR>
            <TR> 
              <TD align="right" bgcolor="#F7F7F7">How many sales trucks/ vans?</TD>
              <td colspan="3" bgcolor="#F7F7F7" > 
                <input name="SizeOfSalesTrucks" class="inputBoxNormal" value="<%=.SizeOfSalesTrucks%>" size="20"></td>
            </TR>
            <TR> 
              <TD align="right">Which geographical areas of the country <br>
                do you cover?</TD>
              <td colspan="3" ><input name="geographical" class="inputBoxNormal" value="<%=.geographical%>" size="50"> 
                <font color="#FF6600">*</font></td>
            </TR>
            <TR> 
              <TD align="right" bgcolor="#F7F7F7">Which types of outlets do you 
                cover<br>
                (modern markets, traditional markets, HORECA, etc.)? </TD>
              <td colspan="3" bgcolor="#F7F7F7" > 
                <input name="outlets" class="inputBoxNormal" value="<%=.outlets%>" size="50"></td>
            </TR>
            <TR> 
              <TD align="right">What are the product categories<br>
                that you currently distribute/handle? <br>
                Any brands from Thailand? </TD>
              <td colspan="3" ><input name="brands" class="inputBoxNormal" value="<%=.brands%>" size="50"></td>
            </TR>
            <TR> 
              <TD align="right" bgcolor="#F7F7F7">Countries that you would like 
                to sell our product to:</TD>
              <td colspan="3" bgcolor="#F7F7F7" > 
                <input name="Countries" class="inputBoxNormal" value="<%=.Countries%>" size="50"></td>
            </TR>
            <TR align="left"> 
              <TD align="right">Please specify product that you are interested 
                in <br>
                (brand, packaging, serving size):</TD>
              <TD colspan="3"><input name="interested" class="inputBoxNormal" value="<%=.interested%>" size="50"> 
                <font color="#FF6600">*</font></TD>
            </TR>
            <TR> 
              <TD align="right" bgcolor="#F7F7F7">Who is the target market for 
                this product?</TD>
              <td colspan="3" bgcolor="#F7F7F7" > 
                <input name="targetmarket" class="inputBoxNormal" value="<%=.targetmarket%>" size="50"></td>
            </TR>
            <TR> 
              <TD align="right">Estimated volume per order <br>
                (in bottle / serving unit)?</TD>
              <td colspan="3" ><input name="Estimated" class="inputBoxNormal" value="<%=.Estimated%>" size="50"></td>
            </TR>
            <TR> 
              <TD colspan="4" align="center"><input type="hidden" name="TypeProcess" value="Edit"> 
                <input type="hidden" name="Table" value="tabUser"> 
                <img src="bullet/submit.gif" alt="Submit" style="cursor:hand"
name="sub_but" 
width="129" height="36" border="0" id="sub_but" onclick="SendForm();" /> 
                  <img src="bullet/Reset.gif" alt="Reset"   style="cursor:hand"
name="sub_but" 
width="129" height="36" border="0" id="sub_but" onclick="document.form.reset()" /> 
              </TD>
            </TR>
            <TR> 
              <TD colspan="4">&nbsp;</TD>
            </TR>
          </TBODY>
        </TABLE>
  <br>
<table width="100%" border="1" class="FontNorMal" cellpadding="3" cellspacing="0" bordercolor="#eeeeee">
</table>
<%	End With %>
</TD></TR>
</form>
</TABLE>



