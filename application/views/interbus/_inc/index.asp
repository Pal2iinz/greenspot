<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>GreenSpotThailand.com : Contact Us</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-874">
<link href="../e-style.css" rel="stylesheet" type="text/css">
<script language="JavaScript" type="text/JavaScript">
<!--



function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}
//-->
</script>
</head>

<body topmargin="0">
<table width="777" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="7" background="../image/bg-shadow-left.gif">&nbsp;</td>
    <td width="763"><table width="763" height="100%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr bgcolor="369830">
        <td height="16" colspan="2"><!--#include virtual="top-e.asp" --></td>
      </tr>
      <tr bgcolor="369830">
        <td height="117" colspan="2"><img src="../image/eng-bigpix-contact-1.gif" width="444" height="117"><img src="../image/th-bigpix-contact-2.jpg" width="319" height="117"></td>
      </tr>
      <tr>
        <td width="178"><img src="../image/eng-top-green-menu-1.gif" width="178" height="35"></td>
        <td width="585"><!--#include virtual="menu-e.asp" --></td>
      </tr>
      <tr>
        <td valign="top" background="../image/th-bg-sidemenu.gif"><!--#include virtual="sidemenu-e.asp" --></td>
        <td rowspan="3" valign="top" bgcolor="#FFFFFF"><table width="520" border="0" align="center" cellpadding="0" cellspacing="0">
          <tr>
            <td><center>
                <center>
                  <br>
                  <br>
                  <img src="../image/eng-contact-pix-separate.gif" width="520" height="210" border="0" usemap="#MapMap" href="#hr">
                </center>
              <div align="left"><br>
                    <br>
                    <br>
                    <br>
                    <strong><br>
                    </strong><span class="first-word"><strong> <a name="domestic"></a><a name="corporate"></a><br>
                      Domestic Sales<br>
                    </strong></span><strong><a name="bkk"></a><br>
                      :: Bangkok Sales </strong><br>
                <br>
                </p>
                </div>
              <div align="left">
                  <table width="550" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="66"><div align="right"><span class="orange">Phone </span>:</div></td>
                      <td width="10">&nbsp;</td>
                      <td width="295" class="eng-black">02 374 0823 &#8211; 30, 02 377 5088 &#8211; 91 </td>
                      <td width="179" rowspan="4"><div align="center"><a href="../howto/form-order.asp?dev=bkk"><img src="b-online_order.gif" width="107" height="29" border="0"></a></div></td>
                    </tr>
                    <tr>
                      <td><div align="right"><span class="orange">Fax </span>:</div></td>
                      <td>&nbsp;</td>
                      <td class="eng-black">02 377 8588</td>
                    </tr>
                    <tr>
                      <td><div align="right"><span class="orange">E-mail </span>:</div></td>
                      <td>&nbsp;</td>
                      <td><a href="mailto:bkksales@greenspot.co.th">bkksales@greenspot.co.th</a> </td>
                    </tr>
                    <tr>
                      <td height="24" valign="top"><div align="right"><span class="orange">Address </span>:</div></td>
                      <td>&nbsp;</td>
                      <td>288 Srinagarindra Rd., Hua Mak, <br>
                        Bang Kapi, Bangkok 10240, Thailand</td>
                    </tr>
                  </table>
                <br>
                  <table width="550" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td height="1" background="../bg-line-hr.gif"></td>
                    </tr>
                  </table>
                <br>
                  <strong> :: Huamark </strong><br>
                  <br>
                  <table width="550" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="66"><div align="right"><span class="orange">Phone </span>:</div></td>
                      <td width="10">&nbsp;</td>
                      <td width="474" class="eng-black">02 725 9800 , 02 377 5088-91</td>
                      <td width="250" rowspan="5" valign="middle"><div align="center"><a href="../howto/form-order.asp?dev=bkk"><img src="b-online_order.gif" width="107" height="29" border="0"></a></div></td>
                    </tr>
                    <tr>
                      <td><div align="right"><span class="orange">Fax </span>:</div></td>
                      <td>&nbsp;</td>
                      <td class="eng-black">02 725 9801</td>
                    </tr>
                    <tr>
                      <td><div align="right"><span class="orange">E-mail </span>:</div></td>
                      <td>&nbsp;</td>
                      <td><a href="mailto:bkksales@greenspot.co.th">bkksales@greenspot.co.th</a> </td>
                    </tr>
                    <tr>
                      <td valign="top"><div align="right"><span class="orange">Address </span>:</div></td>
                      <td>&nbsp;</td>
                      <td>288 Srinagarindra Rd., Hua Mak, <br>
                        Bang Kapi, Bangkok 10240, Thailand</td>
                    </tr>
                    <tr>
                      <td valign="top">&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                  </table>
                <br>
                  <table width="550" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td height="1" background="../bg-line-hr.gif"></td>
                    </tr>
                  </table>
                <br>
                  <strong>:: Rangsit </strong><br>
                  <br>
                  <table width="550" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="66"><div align="right"><span class="orange">Phone </span>:</div></td>
                      <td width="10">&nbsp;</td>
                      <td width="474">02 533 0280 &#8211; 5 ext. 1111</td>
                      <td width="250" rowspan="3"><div align="center"><a href="../howto/form-order.asp?dev=bkk"><img src="b-online_order.gif" width="107" height="29" border="0"></a></div></td>
                    </tr>
                    <tr>
                      <td><div align="right"><span class="orange">E-mail </span>:</div></td>
                      <td>&nbsp;</td>
                      <td><a href="mailto:bkksales@greenspot.co.th">bkksales@greenspot.co.th</a> </td>
                    </tr>
                    <tr>
                      <td valign="top"><div align="right"><span class="orange">Address </span>:</div></td>
                      <td>&nbsp;</td>
                      <td>2 Soi Rangsit-Nakhon Nayok 46, Prachathipat<br>
                        Thanyaburi, Pathum Thani 12130, Thailand</td>
                    </tr>
                  </table>
                <br>
                  <table width="550" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td height="1" background="../bg-line-hr.gif"></td>
                    </tr>
                  </table>
                <br>
                  <strong> :: Kratumban<br>
                  </strong><br>
                  <table width="550" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="66"><div align="right"><span class="orange">Phone </span>:</div></td>
                      <td width="10">&nbsp;</td>
                      <td width="474">034 849 027 &#8211; 9 ext. 555</td>
                      <td width="250" rowspan="3"><div align="center"><a href="../howto/form-order.asp?dev=bkk"><img src="b-online_order.gif" width="107" height="29" border="0"></a></div></td>
                    </tr>
                    <tr>
                      <td><div align="right"><span class="orange">E-mail </span>:</div></td>
                      <td>&nbsp;</td>
                      <td><a href="mailto:bkksales@greenspot.co.th">bkksales@greenspot.co.th</a> </td>
                    </tr>
                    <tr>
                      <td valign="top"><div align="right"><span class="orange">Address </span>:</div></td>
                      <td>&nbsp;</td>
                      <td>4/2 Moo 8, Klong Madue, Kratumban, Samutsakorn</td>
                    </tr>
                  </table>
                <br>
                  <br>
                  <table width="550" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td height="1" background="../bg-line-hr.gif"></td>
                    </tr>
                  </table>
                <br>
                  <strong> :: Rajburana</strong> <br>
                  <br>
                  <table width="550" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="66"><div align="right"><span class="orange">Phone </span>:</div></td>
                      <td width="10">&nbsp;</td>
                      <td width="474">02 871 3399 ext. 11</td>
                      <td width="250" rowspan="3"><div align="center"><a href="../howto/form-order.asp?dev=bkk"><img src="b-online_order.gif" width="107" height="29" border="0"></a></div></td>
                    </tr>
                    <tr>
                      <td><div align="right"><span class="orange">E-mail </span>:</div></td>
                      <td>&nbsp;</td>
                      <td><a href="mailto:bkksales@greenspot.co.th">bkksales@greenspot.co.th</a> </td>
                    </tr>
                    <tr>
                      <td valign="top"><div align="right"><span class="orange">Address </span>:</div></td>
                      <td>&nbsp;</td>
                      <td>184 Moo1 Rajburana Rd., Bangpakok<br>
                        Rajburana, Bangkok 10140</td>
                    </tr>
                  </table>
                <br>
                  <strong><br>
                  </strong>
                  <table width="550" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td height="1" background="../bg-line-hr.gif"></td>
                    </tr>
                  </table>
                <strong><br>
                  :: Nongkham</strong> <br>
                <br>
                <table width="550" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="66"><div align="right"><span class="orange">Phone </span>:</div></td>
                    <td width="10">&nbsp;</td>
                    <td width="474">02 807 5288 ext. 11</td>
                    <td width="250" rowspan="3"><div align="center"><a href="../howto/form-order.asp?dev=bkk"><img src="b-online_order.gif" width="107" height="29" border="0"></a></div></td>
                  </tr>
                  <tr>
                    <td><div align="right"><span class="orange">E-mail </span>:</div></td>
                    <td>&nbsp;</td>
                    <td><a href="mailto:bkksales@greenspot.co.th">bkksales@greenspot.co.th</a> </td>
                  </tr>
                  <tr>
                    <td valign="top"><div align="right"><span class="orange">Address </span>:</div></td>
                    <td>&nbsp;</td>
                    <td>15/71 Petchkasem, Nhong Kangplu, Nhongkham, Bangkok 10160.</td>
                  </tr>
                </table>
                <br>
                <br>
                <table width="550" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td height="1" background="../bg-line-hr.gif"></td>
                  </tr>
                </table>
                <br>
                <strong>:: Theparak </strong><br>
                <br>
                <table width="550" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="66"><div align="right"><span class="orange">Phone </span>:</div></td>
                    <td width="10">&nbsp;</td>
                    <td width="474">02 754 9216 ext. 11</td>
                    <td width="250" rowspan="3"><div align="center"><a href="../howto/form-order.asp?dev=bkk"><img src="b-online_order.gif" width="107" height="29" border="0"></a></div></td>
                  </tr>
                  <tr>
                    <td><div align="right"><span class="orange">E-mail </span>:</div></td>
                    <td>&nbsp;</td>
                    <td><a href="mailto:bkksales@greenspot.co.th">bkksales@greenspot.co.th</a> </td>
                  </tr>
                  <tr>
                    <td valign="top"><div align="right"><span class="orange">Address </span>:</div></td>
                    <td>&nbsp;</td>
                    <td valign="top">1999/5 Theparak Rd., Theparak, Samutprakarn </td>
                  </tr>
                </table>
                <br>
                <br>
                <table width="550" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td height="1" background="../bg-line-hr.gif"></td>
                  </tr>
                </table>
                <br>
                <strong> :: Klongtoey</strong><br>
                <br>
                <table width="550" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="66"><div align="right"><span class="orange">Phone </span>:</div></td>
                    <td width="10">&nbsp;</td>
                    <td width="474">02 671 1471 ext. 11</td>
                    <td width="250" rowspan="3"><div align="center"><a href="../howto/form-order.asp?dev=bkk"><img src="b-online_order.gif" width="107" height="29" border="0"></a></div></td>
                  </tr>
                  <tr>
                    <td><div align="right"><span class="orange">E-mail </span>:</div></td>
                    <td>&nbsp;</td>
                    <td><a href="mailto:bkksales@greenspot.co.th">bkksales@greenspot.co.th</a> </td>
                  </tr>
                  <tr>
                    <td valign="top"><div align="right"><span class="orange">Address </span>:</div></td>
                    <td>&nbsp;</td>
                    <td valign="top">8 Soi Sri Aksorn, Chuepleng Rd., <br>
                      Tungmahamek, Satorn, Bangkok 10120 </td>
                  </tr>
                </table>
                <br>
                <strong> <br>
                  </strong>
                <table width="550" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td height="1" background="../bg-line-hr.gif"></td>
                  </tr>
                </table>
                <strong><br>
                  :: Pinklao</strong><br>
                <br>
                <table width="550" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="66"><div align="right"><span class="orange">Phone </span>:</div></td>
                    <td width="10">&nbsp;</td>
                    <td width="474"> 66 2 412 6730 ext. 11 </td>
                    <td width="250" rowspan="3"><div align="center"><a href="../howto/form-order.asp?dev=bkk"><img src="b-online_order.gif" width="107" height="29" border="0"></a></div></td>
                  </tr>
                  <tr>
                    <td><div align="right"><span class="orange">E-mail </span>:</div></td>
                    <td>&nbsp;</td>
                    <td><a href="mailto:bkksales@greenspot.co.th">bkksales@greenspot.co.th</a> </td>
                  </tr>
                  <tr>
                    <td valign="top"><div align="right"><span class="orange">Address </span>:</div></td>
                    <td>&nbsp;</td>
                    <td valign="top"> 72 Trok Wat Dongmullek, Ban ChangLor, Bangkok-Noi,
                      Bangkok 10700 </td>
                  </tr>
                </table>
                <strong><br>
                  <br>
                  </strong>
                <table width="550" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td height="1" background="../bg-line-hr.gif"></td>
                  </tr>
                </table>
                <strong><br>
                  :: 
                  
                  
                  Navamin</strong><br>
           <br>
           <table width="550" border="0" cellspacing="0" cellpadding="0">
             <tr>
               <td width="66"><div align="right"><span class="orange">Phone </span>:</div></td>
               <td width="10">&nbsp;</td>
               <td width="474"> 66 2 519 3782-3 ext. 11</td>
               <td width="250" rowspan="3"><div align="center"><a href="../howto/form-order.asp?dev=bkk"><img src="b-online_order.gif" width="107" height="29" border="0"></a></div></td>
             </tr>
             <tr>
               <td><div align="right"><span class="orange">E-mail </span>:</div></td>
               <td>&nbsp;</td>
               <td><a href="mailto:bkksales@greenspot.co.th">bkksales@greenspot.co.th</a> </td>
             </tr>
             <tr>
               <td valign="top"><div align="right"><span class="orange">Address </span>:</div></td>
               <td>&nbsp;</td>
               <td valign="top"> 4976 Ladprao 101, Bungkum,
                 
                 
                 Bangkok 10230</td>
             </tr>
           </table>
                <br>
           <strong><br>
           </strong>
           <table width="550" border="0" cellspacing="0" cellpadding="0">
             <tr>
               <td height="1" background="../bg-line-hr.gif"></td>
             </tr>
           </table>
                <strong><br>
                  :: 
                  
                  
                  Ladkrabung</strong><br>
           <br>
           <table width="550" border="0" cellspacing="0" cellpadding="0">
             <tr>
               <td width="66"><div align="right"><span class="orange">Phone </span>:</div></td>
               <td width="10">&nbsp;</td>
               <td width="474"> 66 2 727 8400 ext. 11</td>
               <td width="250" rowspan="3"><div align="center"><a href="../howto/form-order.asp?dev=bkk"><img src="b-online_order.gif" width="107" height="29" border="0"></a></div></td>
             </tr>
             <tr>
               <td><div align="right"><span class="orange">E-mail </span>:</div></td>
               <td>&nbsp;</td>
               <td><a href="mailto:bkksales@greenspot.co.th">bkksales@greenspot.co.th</a> </td>
             </tr>
             <tr>
               <td valign="top"><div align="right"><span class="orange">Address </span>:</div></td>
               <td>&nbsp;</td>
               <td valign="top"> 109/8 M.6 Onnut-Ladkrabung, Ladkrabung,
                 
                 
                 Bangkok 10520</td>
             </tr>
           </table>
                <strong><br>
           </strong>
           <table width="550" border="0" cellspacing="0" cellpadding="0">
             <tr>
               <td height="1" background="../bg-line-hr.gif"></td>
             </tr>
           </table>
                <strong><br>
                  :: 
                  
                  
                  Changwatana</strong><br>
           <br>
           <table width="550" border="0" cellspacing="0" cellpadding="0">
             <tr>
               <td width="66"><div align="right"><span class="orange">Phone </span>:</div></td>
               <td width="10">&nbsp;</td>
               <td width="474"> 66 2 573 5367-8 ext. 11</td>
               <td width="250" rowspan="3"><div align="center"><a href="../howto/form-order.asp?dev=bkk"><img src="b-online_order.gif" width="107" height="29" border="0"></a></div></td>
             </tr>
             <tr>
               <td><div align="right"><span class="orange">E-mail </span>:</div></td>
               <td>&nbsp;</td>
               <td><a href="mailto:bkksales@greenspot.co.th">bkksales@greenspot.co.th</a> </td>
             </tr>
             <tr>
               <td valign="top"><div align="right"><span class="orange">Address </span>:</div></td>
               <td>&nbsp;</td>
               <td valign="top"> 99/34 Changwatana, Laksi, Bangkok 10214</td>
             </tr>
           </table>
                <br>
           <table width="550" border="0" cellspacing="0" cellpadding="0">
             <tr>
               <td height="1" background="../bg-line-hr.gif"></td>
             </tr>
           </table>
                <a name="upcountry"></a><br>
           <strong> <span class="first-word">Upcountry Sales </span></strong><br>
           <br>
           <table width="550" border="0" cellspacing="0" cellpadding="0">
             <tr>
               <td width="66"><div align="right"><span class="orange">Phone </span>:</div></td>
               <td width="10">&nbsp;</td>
               <td width="474">66 2 377 8573</td>
               <td width="250" rowspan="4"><div align="center"><a href="../howto/form-order.asp?dev=upcountry"><img src="b-online_order.gif" width="107" height="29" border="0"></a></div></td>
             </tr>
             <tr>
               <td><div align="right"><span class="orange">Fax</span> :</div></td>
               <td>&nbsp;</td>
               <td>66 2 374 0825</td>
             </tr>
             <tr>
               <td><div align="right"><span class="orange">E-mail </span>:</div></td>
               <td>&nbsp;</td>
               <td><a href="mailto:upcadmin1@greenspot.co.th">upcadmin1@greenspot.co.th</a> </td>
             </tr>
             <tr>
               <td valign="top"><div align="right"><span class="orange">Address </span>:</div></td>
               <td>&nbsp;</td>
               <td valign="top">288 Srinagarindra Rd., Hua Mak, <br>
                 Bang Kapi, Bangkok 10240, Thailand</td>
             </tr>
           </table>
                <br>
           <br>
           <table width="550" border="0" cellspacing="0" cellpadding="0">
             <tr>
               <td height="1" background="../bg-line-hr.gif"></td>
             </tr>
           </table>
                <br>
           <a name="inter"></a> <br>
           <span class="first-word"><strong>International Business</strong></span><br>
           <br>
           <br>
           <table width="550" border="0" cellspacing="0" cellpadding="0">
             <tr>
               <td width="66"><div align="right"><span class="orange">Phone </span>:</div></td>
               <td width="10">&nbsp;</td>
               <td width="295">66 1 811 8351</td>
               <td width="179" rowspan="4"><div align="center"><a href="../howto/form-order.asp?dev=inter"><img src="b-online_order.gif" width="107" height="29" border="0"></a></div></td>
             </tr>
             <tr>
               <td><div align="right"><span class="orange">Fax </span>:</div></td>
               <td>&nbsp;</td>
               <td>66 2 375 2766</td>
             </tr>
             <tr>
               <td><div align="right"><span class="orange">E-mail </span>:</div></td>
               <td>&nbsp;</td>
               <td><a href="mailto:export@greenspot.co.th">export@greenspot.co.th</a> </td>
             </tr>
             <tr>
               <td height="24" valign="top"><div align="right"><span class="orange">Address </span>:</div></td>
               <td>&nbsp;</td>
               <td>288 Srinagarindra Rd., Hua Mak, <br>
                 Bang Kapi, Bangkok 10240, Thailand</td>
             </tr>
           </table>
                <br>
           <br>
           <table width="550" border="0" cellspacing="0" cellpadding="0">
             <tr>
               <td height="1" background="../bg-line-hr.gif"></td>
             </tr>
           </table>
                <a name="marketing"></a><br>
           <span class="first-word"><strong>Marketing</strong></span><br>
           <br>
           <table width="550" border="0" cellspacing="0" cellpadding="0">
             <tr>
               <td width="66"><div align="right"><span class="orange">Phone </span>:</div></td>
               <td width="10">&nbsp;</td>
               <td width="295"> 66 2 725 9800, 2 377 5088-91</td>
               <td width="179" rowspan="4"><div align="center"><a href="../contact/form-csc.asp?dev=marketing"><img src="b-contact.gif" width="119" height="32" border="0"></a></div></td>
             </tr>
             <tr>
               <td><div align="right"><span class="orange">Fax </span>:</div></td>
               <td>&nbsp;</td>
               <td>66 2 374 0823 </td>
             </tr>
             <tr>
               <td><div align="right"><span class="orange">E-mail </span>:</div></td>
               <td>&nbsp;</td>
               <td><a href="mailto:marketing@greenspot.co.th">marketing@greenspot.co.th </a> </td>
             </tr>
             <tr>
               <td height="24" valign="top"><div align="right"><span class="orange">Address </span>:</div></td>
               <td>&nbsp;</td>
               <td>288 Srinagarindra Rd., Hua Mak, <br>
                 Bang Kapi, Bangkok 10240, Thailand</td>
             </tr>
           </table>
                <br>
           <table width="550" border="0" cellspacing="0" cellpadding="0">
             <tr>
               <td height="1" background="../bg-line-hr.gif"></td>
             </tr>
           </table>
                <br>
           <a name="procurement"></a> <br>
           <span class="first-word"><strong>Procurement</strong></span><br>
           <br>
           <table width="550" border="0" cellspacing="0" cellpadding="0">
             <tr>
               <td width="66"><div align="right"><span class="orange">Phone </span>:</div></td>
               <td width="10">&nbsp;</td>
               <td width="295"> 66 2 533 0280 - 5 </td>
               <td width="179" rowspan="4"><div align="center"><a href="../contact/form-csc.asp?dev=procurement"><img src="b-contact.gif" width="119" height="32" border="0"></a></div></td>
             </tr>
             <tr>
               <td><div align="right"><span class="orange">Fax </span>:</div></td>
               <td>&nbsp;</td>
               <td>66 2 533 0288</td>
             </tr>
             <tr>
               <td><div align="right"><span class="orange">E-mail </span>:</div></td>
               <td>&nbsp;</td>
               <td><a href="mailto:procurement@greenspot.co.th">procurement@greenspot.co.th </a> </td>
             </tr>
             <tr>
               <td height="24" valign="top"><div align="right"><span class="orange">Address </span>:</div></td>
               <td>&nbsp;</td>
               <td>2 Soi Rangsit-Nakhon Nayok 46, Prachathipat<br>
                 Thanyaburi, Pathum Thani 12130, Thailand</td>
             </tr>
           </table>
                <br>
           <br>
           <table width="550" border="0" cellspacing="0" cellpadding="0">
             <tr>
               <td height="1" background="../bg-line-hr.gif"></td>
             </tr>
           </table>
                <br>
           <a name="hr"></a> <br>
           <span class="first-word"><strong>Human Resources</strong></span><br>
           <br>
           <table width="550" border="0" cellspacing="0" cellpadding="0">
             <tr>
               <td width="66"><div align="right"><span class="orange">Phone </span>:</div></td>
               <td width="10">&nbsp;</td>
               <td width="474" class="eng-black">66 2 725 9800, 2 377 5088-91</td>
               <td width="179" rowspan="4"><div align="center"><a href="../contact/form-csc.asp?dev=hr"><img src="b-contact.gif" width="119" height="32" border="0"></a></div></td>
             </tr>
             <tr>
               <td><div align="right"><span class="orange">Fax </span>:</div></td>
               <td>&nbsp;</td>
               <td class="eng-black">66 2 374 0829</td>
             </tr>
             <tr>
               <td><div align="right"><span class="orange">E-mail </span>:</div></td>
               <td>&nbsp;</td>
               <td><a href="mailto:hrrs@greenspot.co.th">hrrs@greenspot.co.th </a> </td>
             </tr>
             <tr>
               <td height="24" valign="top"><div align="right"><span class="orange">Address </span>:</div></td>
               <td>&nbsp;</td>
               <td>288 Srinagarindra Rd., Hua Mak, <br>
Bang Kapi, Bangkok 10240, Thailand</td>
             </tr>
           </table>
              </div>
              <p align="center">&nbsp; </p>
            </center></td>
          </tr>
          <tr>
            <td><br>
                <!--#include virtual="footer.asp"--></td>
          </tr>
        </table>
          <map name="MapMap">
            <area shape="poly" coords="21,72,61,6" href="#">
            <area shape="poly" coords="200,6,208,17,492,13,408,63,240,72,203,138,123,138,86,75,124,7" href="../contact/form-csc-t.asp?dev=csc">
            <area shape="rect" coords="339,76,430,88" href="#domestic">
            <area shape="rect" coords="360,93,448,105" href="#bkk">
            <area shape="rect" coords="368,110,465,125" href="#upcountry">
            <area shape="rect" coords="369,131,502,146" href="#inter">
            <area shape="rect" coords="363,153,429,169" href="#marketing">
            <area shape="rect" coords="356,173,439,186" href="#procurement">
            <area shape="rect" coords="343,193,450,215" href="#hr">
          </map></td>
      </tr>
      <tr>
        <td background="../image/th-bg-sidemenu.gif">&nbsp;</td>
      </tr>
      <tr>
        <td valign="bottom" background="../image/th-bg-sidemenu.gif"><img src="../image/th-end-sidemenu.gif" width="178" height="97"></td>
      </tr>
    </table></td>
    <td width="7" background="../image/bg-shadow-right.gif">&nbsp;</td>
  </tr>
</table>
<map name="Map">
  <area shape="poly" coords="21,72,61,6" href="#">
  <area shape="poly" coords="200,6,208,17,492,13,408,63,240,72,203,138,123,138,86,75,124,7" href="../contact/form-csc-t.asp?dev=csc">
  <area shape="rect" coords="339,76,430,88" href="#domestic">
  <area shape="rect" coords="360,93,448,105" href="#bkk">
  <area shape="rect" coords="368,110,465,125" href="#upcountry">
  <area shape="rect" coords="369,131,502,146" href="#inter">
  <area shape="rect" coords="363,153,429,169" href="#marketing">
  <area shape="rect" coords="356,173,439,186" href="#procurement">
  <area shape="rect" coords="343,193,450,215" href="#hr">
</map>
</body>
</html>
