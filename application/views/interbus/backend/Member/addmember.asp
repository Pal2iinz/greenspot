<title> ==  Member Description ===</title>
<meta http-equiv="Content-Type" content="text/html; charset=tis-620">
<form name="form" method="post" action="default.asp?topic=member&methods=addmember">
  <TABLE width="100%" border=0 align="center" cellpadding="2" cellspacing="0" class="label"  >
    <TBODY>
      <TR> 
        <TD  colspan="4" valign="top" class="colheader"><strong>User Information</strong> 
          (<%=Countrecord("TabUser","")%>) </TD>
      </TR>
      <TR align="center" class="label"> 
        <TD colspan="4"> <%
						  		IF  request("msg") = "Duplicate" Then
									response.write "<font color='#ff0000'> Username  "&request("Username")&" have been use already !!  <br> Please Choose Another Username !!"
								End IF
						  %> </TD>
      </TR>
      <tr> 
        <td align="right">Username</td>
        <td colspan="3"><b> 
          <input name="susername" class="inputTag" size="20" maxlength="20" />
          <font color="#FF6600">*</font></b></td>
      </tr>
      <tr bgcolor="#FFFFFF"> 
        <td align="right">Password</td>
        <td colspan="3"> <input name="sPassword"  class="inputTag" size="20" maxlength="20" /> 
          <b> </b> <font color="#FF6600">*</font> </td>
      </tr>
      <TR> 
        <TD align="right">E-mail Address</TD>
        <TD colspan="3"><b> 
          <input size="20" class="inputTag" type="text" name="Email">
          <font color="#FF6600">*</font></b></TD>
      </TR>
      <TR> 
        <td height="25" align="right" >Name- Lastname</td>
        <td colspan="3" > <select size=1 name=FName  style="font-family: Ms sans serif; font-size: 8pt;">
            <% 	
						  			Call ListBoxArrayList(arrFName,"") %>
          </select> <input name="NameEn" class="inputTag" size="30" maxlength="80"> 
          <b> </b> <font color="#FF6600">* 
          <input name="LastNameEn" class="inputTag" size="30" maxlength="80">
          <b> </b> <font color="#FF6600">* </font></font></td>
      </TR>
      <TR bgcolor="#FFFFFF"> 
        <td align="right" valign="top" >Address</td>
        <td colspan="3" > <textarea name="Address" cols="50" rows="3" class="inputTag"></textarea> 
          <b> </b> </td>
      </TR>
      <TR> 
        <td height="25" align="right" >Country</td>
        <td > <%
			Call ListBox("- - Country - -","TabCountry","","CounId","CounId","Country",CounId,"","")
			%> </td>
        <td align="right" >&nbsp;</td>
        <td >&nbsp;</td>
      </TR>
      <TR bgcolor="#FFFFFF"> 
        <td height="25" > <div align="right">Street&nbsp;</div></td>
        <td > <input name="Street" class="inputTag" size="15"> <b> </b> </td>
        <td align="right" >City</td>
        <td > <input name="City" class="inputTag" size="15" maxlength="80"> </td>
      </TR>
      <TR> 
        <TD height="25" align="right">District</TD>
        <TD><input name="District" class="inputTag" size="15"> </TD>
        <TD align="right">Province</TD>
        <TD><input name="Province" class="inputTag" size="15" maxlength="80"> 
          <b> </b> </TD>
      </TR>
      <TR bgcolor="#FFFFFF"> 
        <td height="25" align="right" >Post Code</td>
        <td colspan="3" > <input name="PostCode" class="inputTag" size="20" maxlength="5"> 
        </td>
      </TR>
      <TR> 
        <td height="27" align="right" >Telephone</td>
        <td colspan="3" > <input name="Tel" class="inputTag" size="20"> <b> </b> 
          <font color="#FF6600">*</font> &nbsp;Fax No&nbsp; <input name="Fax" class="inputTag" size="20"></td>
      </TR>
      <TR> 
        <td height="25" align="right" >&nbsp;</td>
        <td colspan="3" >&nbsp; </td>
      </TR>
      <TR> 
        <TD colspan="4" class="colheader"><strong>Company Information</strong></TD>
      </TR>
      <TR> 
        <td height="25" align="right" >Name</td>
        <td colspan="3" > <input name="ComName" class="inputTag" size="30" maxlength="80" value=""> 
          <font color="#FF6600">*</font> </td>
      </TR>
      <TR bgcolor="#FFFFFF"> 
        <td align="right" valign="top" > Address</td>
        <td colspan="3" > <textarea name="ComAddress" cols="50" rows="3" class="inputTag"></textarea> 
          <font color="#FF6600">*</font> <b> </b> </td>
      </TR>
      <TR> 
        <td height="27" align="right" > Telephone</td>
        <td colspan="3" > <input name="ComTel" class="inputTag" size="20"> <font color="#FF6600">*</font> 
          Fax No 
          <input name="ComFax" class="inputTag" size="20"> </td>
      </TR>
      <TR bgcolor="#FFFFFF"> 
        <td height="25" align="right" >Website (if any)</td>
        <td colspan="3" > <input name="Website" class="inputTag" size="20">
          E-mail Address 
          <input name="ComEmail" class="inputTag" size="20"></td>
      </TR>
      <TR> 
        <TD align="right">Nature of business/Company profile</TD>
        <td colspan="3" ><select size=1 name=ComBus  style="font-family: Ms sans serif; font-size: 8pt;" OnChange="CheckOther(this.value);">
            <%
			 	Call ListBoxArrayList(arrBus,"") 
				%>
          </select> <input name="Other" class="inputTag" size="50" <%if ComBus<>"Other" Then response.write "style='display:none'" End IF%>></td>
      </TR>
      <TR bgcolor="#FFFFFF"> 
        <TD align="right">How many years has the <br>
          company been in business?</TD>
        <td colspan="3" > <input name="Years" class="inputTag" size="20"></td>
      </TR>
      <TR> 
        <TD align="right">What is the size of your sales team? </TD>
        <td colspan="3" ><input name="SizeOfSalesTeam" class="inputTag" size="20"></td>
      </TR>
      <TR bgcolor="#FFFFFF"> 
        <TD align="right">How many sales trucks/ vans?</TD>
        <td colspan="3" > <input name="SizeOfSalesTrucks" class="inputTag" size="20"></td>
      </TR>
      <TR> 
        <TD align="right">Which geographical areas of the country <br>
          do you cover?</TD>
        <td colspan="3" ><input name="geographical" class="inputTag" size="50"> 
          <font color="#FF6600">*</font></td>
      </TR>
      <TR bgcolor="#FFFFFF"> 
        <TD align="right">Which types of outlets do you cover<br>
          (modern markets, traditional markets, HORECA, etc.)? </TD>
        <td colspan="3" > <input name="outlets" class="inputTag" size="50"></td>
      </TR>
      <TR> 
        <TD align="right">What are the product categories<br>
          that you currently distribute/handle? <br>
          Any brands from Thailand? </TD>
        <td colspan="3" ><input name="brands" class="inputTag" size="50"></td>
      </TR>
      <TR bgcolor="#FFFFFF"> 
        <TD align="right">Countries that you would like to sell our product to:</TD>
        <td colspan="3" > <input name="Countries" class="inputTag" size="50"></td>
      </TR>
      <TR align="left"> 
        <TD align="right">Please specify product that you are interested in <br>
          (brand, packaging, serving size):</TD>
        <TD colspan="3"><input name="interested" class="inputTag" size="50"> <font color="#FF6600">*</font></TD>
      </TR>
      <TR bgcolor="#FFFFFF"> 
        <TD align="right">Who is the target market for this product?</TD>
        <td colspan="3" > <input name="targetmarket" class="inputTag" size="50"></td>
      </TR>
      <TR> 
        <TD align="right">Estimated volume per order <br>
          (in bottle / serving unit)?</TD>
        <td colspan="3" ><input name="Estimated" class="inputTag" size="50"></td>
      </TR>
      <TR bgcolor="#FFFFFF"> 
        <TD align="right">���;�ѡ�ҹ������¡��</TD>
        <td colspan="3" > <input name="RecordBy" class="inputTag" size="20"> </td>
      </TR>
      <TR>
        <TD align="right">Permit User Login</TD>
        <TD colspan="3"><input type="checkbox" name="UserStatus" value="1" checked></TD>
      </TR>
      <TR> 
        <TD align="right">Permit Distributors</TD>
        <TD colspan="3"><input type="checkbox" name="DistStatus" value="1" > 
        </TD>
      </TR>
      <TR> 
        <TD colspan="4" align="center"><input type="hidden" name="TypeProcess" value="Add"> 
          <input type="hidden" name="str_check" value="1"> <input type="hidden" name="Table" value="tabUser"> 
          <input  type="button" class="buttonTag" style="font-family: Ms sans serif; font-size: 8pt;" 
		  onclick="SendFormeditA();"  value="Add Member"> <input  type="button" class="buttonTag" onclick="window.location.href='default.asp?topic=member&methods=memberinformation'" value="  Close  "></TD>
      </TR>
      <TR> 
        <TD colspan="4">&nbsp;</TD>
      </TR>
    </TBODY>
  </TABLE>
  <br>
<table width="100%" border="1" class="FontNorMal" cellpadding="3" cellspacing="0" bordercolor="#eeeeee">
</table>
</form>

<br><br>


