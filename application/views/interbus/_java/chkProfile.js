
function SendForm()
{
		var f = document.form;
		if(Trim(f.NameEn.value)){
				warninput(f.NameEn," Name","1")
		}else if(Trim(f.LastNameEn.value)){
				warninput(f.LastNameEn,"  Lastname","1")
		}else if(Trim(f.Tel.value)){
				warninput(f.Tel," Tel","1")
		}else if(Trim(f.Email.value)){
				warninput(f.Email," Email","1")
		}else if(!check_email(f.Email.value)){
				warncheck(f.Email,"  Email ","1")
		}else if(Trim(f.ComName.value)){
				warninput(f.ComName,"  Company Name","1")
		}else if(Trim(f.ComAddress.value)){
				warninput(f.ComAddress," Company Address","1")
		}else if(Trim(f.ComTel.value)){
				warninput(f.ComTel," Company Tel","1")
		}else if(Trim(f.geographical.value)){
				warninput(f.geographical," Which geographical areas of the country do you cover","1")
		}else if(Trim(f.interested.value)){
				warninput(f.interested," Please specify product that you are interested in (brand, packaging, serving size)","1")
		}
		else{
			f.method = "post"
			f.action = "SdbsResultCode.asp"
			f.submit();
		}
}

function SendFormContactDist()
{
		var f = document.form;
		if(Trim(f.Email.value)){
				warninput(f.Email," Email","1")
		}else if(!check_email(f.Email.value)){
				warncheck(f.Email,"  Email ","1")
		}else if(Trim(f.NameEn.value)){
				warninput(f.NameEn," Name","1")
		}else if(Trim(f.LastNameEn.value)){
				warninput(f.LastNameEn,"  Lastname","1")
		}else if(Trim(f.Tel.value)){
				warninput(f.Tel," Tel","1")
		}else if(Trim(f.ComName.value)){
				warninput(f.ComName,"  Company Name","1")
		}else if(Trim(f.ComAddress.value)){
				warninput(f.ComAddress," Company Address","1")
		}else if(Trim(f.ComTel.value)){
				warninput(f.ComTel," Company Tel","1")
		}else if(Trim(f.geographical.value)){
				warninput(f.geographical," Which geographical areas of the country do you cover","1")
		}else if(Trim(f.interested.value)){
				warninput(f.interested," Please specify product that you are interested in (brand, packaging, serving size)","1")
		}
		else{
			f.method = "post"
			f.submit();
		}
}

function SendFormedit()
{
		var f = document.form;
		 if(Trim(f.susername.value)){
				warninput(f.susername," UserName","1")
		}else if(Trim(f.sPassword.value)){
				warninput(f.sPassword," Password","1")
		}else if(Trim(f.NameEn.value)){
				warninput(f.NameEn," Name","1")
		}else if(Trim(f.LastNameEn.value)){
				warninput(f.LastNameEn,"  Lastname","1")
		}else if(Trim(f.Tel.value)){
				warninput(f.Tel," Tel","1")
		}else if(Trim(f.Email.value)){
				warninput(f.Email," Email","1")
		}else if(!check_email(f.Email.value)){
				warncheck(f.Email,"  Email ","1")
		}else if(Trim(f.ComName.value)){
				warninput(f.ComName,"  Company Name","1")
		}else if(Trim(f.ComAddress.value)){
				warninput(f.ComAddress," Company Address","1")
		}else if(Trim(f.ComTel.value)){
				warninput(f.ComTel," Company Tel","1")
		}else if(Trim(f.geographical.value)){
				warninput(f.geographical," Which geographical areas of the country do you cover","1")
		}else if(Trim(f.interested.value)){
				warninput(f.interested," Please specify product that you are interested in (brand, packaging, serving size)","1")
		}
		else{
			f.method = "post"
			f.action = "SdbsResultCode.asp"
			f.submit();
		}
}

function SendFormeditA()
{
		var f = document.form;
		 if(Trim(f.susername.value)){
				warninput(f.susername," UserName","1")
		}else if(Trim(f.sPassword.value)){
				warninput(f.sPassword," Password","1")
		}else if(Trim(f.NameEn.value)){
				warninput(f.NameEn," Name","1")
		}else if(Trim(f.LastNameEn.value)){
				warninput(f.LastNameEn,"  Lastname","1")
		}else if(Trim(f.Tel.value)){
				warninput(f.Tel," Tel","1")
		}else if(Trim(f.Email.value)){
				warninput(f.Email," Email","1")
		}else if(!check_email(f.Email.value)){
				warncheck(f.Email,"  Email ","1")
		}else if(Trim(f.ComName.value)){
				warninput(f.ComName,"  Company Name","1")
		}
		else{
			f.method = "post"
			f.action = "SdbsResultCode.asp"
			f.submit();
		}
}

function chk_changepassword()
{
		var f = document.formchangepasswd;
		if(Trim(f.OldPassword.value)){
				warninput(f.OldPassword," Old Password ","1")
		}else if(f.Password.value.length < 5){
				warninput(f.Password,"   more than 4 character","1")
		}else if(Trim(f.Password2.value)){
				warninput(f.Password2,"  Confirm password ","1")
		}else if(f.Password2.value.length < 5){
					warninput(f.Password2,"  Confirm password more than 4 character","1")
		}else if(f.Password.value != f.Password2.value){
				warncheck(f.Password2,"  Password and Confirm password ")
		}else{
			f.method = "post";
			f.submit();
		}
}
		function checknumber(val){
		var anum=/(^\d+$)|(^\d+\.\d+$)/
		if (anum.test(val))
		testresult=true
		else{
		testresult=false
		}
		return (testresult)
		}

function check_character(ch){
	var len, digit;
	if(ch == " "){ 
      		 len=0;
    	}else{
    		 len = ch.length;
	}
	for(var i=0 ; i<len ; i++)
	{
		digit = ch.charAt(i)
		if( (digit >= "a" && digit <= "z")  || (digit >="0" && digit <="9") || (digit >="A" && digit <="Z")){
			;	
		}else{
			return false;					
		}		
	}		
	return true;
}	

function CheckUserName()
{
    var NewWin;
    var f = document.form;
    id = f.UserName.value;
    src = "CheckDuplicateUser.asp?UserName="+id+"";
    NewWin = window.open(src,"chkid","toolbar=no,location=no,status=no,scrollbars=auto,top=40,left=40,width=300,height=200,resizable=no");
    NewWin.window.focus();
}

function chk_con()
		{
				f = document.form;
				f.action = "SdbsRegister.asp";
				f.method="post"
				f.submit();
		}


function chk_mailcontactus()
{
		f = document.formmailcontact;
		if(Trim(f.str_name.value))
		{
			warninput(f.str_name," Contact Name","1")
		}
		else if(f.str_sender.value.length <4){
				warncheck(f.str_sender ," Email " , "1")
		}
		else if(!check_email(f.str_sender.value)){
				warncheck(f.str_sender,"Email" , "1")
		}
		else if(Trim(f.CounId.value))
		{
				warnselbox(f.CounId,"Country","1")
		}
		else if(Trim(f.Tel.value))
		{
				warninput(f.Tel,"Telephone","1")
		}
		else if(Trim(f.str_comment.value))
		{
				warninput(f.str_comment,"Message","1")
		}
		else
		{
		document.formmailcontact.submit();
		}
}


