<%
				class User
				Public UserName 
				Public Email 
				Public Discount 
				Public Profit 
				Public FNameTh 
				Public NameTh 
				Public LastNameTh
				Public FName
				Public NameEn
				Public LastNameEn
				Public Address
				Public Street
				Public City
				Public District
				Public Province
				Public PostCode
				Public Tel
				Public Fax
				Public LogNow
				Public UserUpdate
				Public UserStatus
				Public UserId
				Public Passwd
				Public ComName
				Public Position
				Public ComAddress
				Public ComStreet
				Public ComCity
				Public ComDistrict
				Public ComProvince
				Public ComPostCode
				Public ComTel
				Public ComFax
				Public ComEmail
				Public Estimated
				Public Birthday
				Public Other
				Public Years
				Public ComBus
				Public SizeOfSalesTeam
				Public SizeOfSalesTrucks
				Public targetmarket
				Public Country1
				Public Country2
				Public Country3
				Public interested
				Public Countries
				Public geographical
				Public outlets
				Public brands
				Public RecordBy
				Public CheckDiscount
				Public LabelLanguage
				Public LabelStatus
				Public CounId
				Public Website
				Public DistStatus
				End class

				Class UserData
							Public Sub GetUserData (ByVal UserId, ByRef objUserData)
								Set RsUser = Server.CreateObject("ADODB.RECORDSET")
								StrSQL = 	" SELECT  * "&_
													"  FROM tabUser 	   "&_
													"  WHERE UserId = "&UserId
								RsUser.Open StrSQL,con,1,1
								if RsUser.RecordCount > 0 then
								With objUserData
								.UserName 		=				RsUser.Fields.Item("UserName").value
								.UserId 		=				RsUser.Fields.Item("UserId").value
								.Email 		=				RsUser.Fields.Item("Email").value
								.Discount 		=				RsUser.Fields.Item("Discount").value
								.Profit 		=				RsUser.Fields.Item("Profit").value
								.FNameTh 		=				RsUser.Fields.Item("FNameTh").value
								.NameTh 		=				RsUser.Fields.Item("NameTh").value
								.LastNameTh		=				RsUser.Fields.Item("LastNameTh").value
								.FName 		=				RsUser.Fields.Item("FName").value
								.NameEn		=				RsUser.Fields.Item("NameEn").value
								.LastNameEn		=				RsUser.Fields.Item("LastNameEn").value
								.Address		=				RsUser.Fields.Item("Address").value
								.Street		=				RsUser.Fields.Item("Street").value
								.City		=				RsUser.Fields.Item("City").value
								.District		=				RsUser.Fields.Item("District").value
								.Province		=				RsUser.Fields.Item("Province").value
								.PostCode		=				RsUser.Fields.Item("PostCode").value
								.Tel		=				RsUser.Fields.Item("Tel").value
								.Fax		=				RsUser.Fields.Item("Fax").value
								.Passwd		=				RsUser.Fields.Item("Passwd").value
								.LogNow		=				RsUser.Fields.Item("LogNow").value
								.UserUpdate	=				RsUser.Fields.Item("UserUpdate").value
								.UserStatus	=				RsUser.Fields.Item("UserStatus").value
								.ComName	=				RsUser.Fields.Item("ComName").value
								.Position		=				RsUser.Fields.Item("Position").value
								.ComAddress		=				RsUser.Fields.Item("ComAddress").value
								.ComStreet		=				RsUser.Fields.Item("ComStreet").value
								.ComCity		=				RsUser.Fields.Item("ComCity").value
								.ComDistrict		=				RsUser.Fields.Item("ComDistrict").value
								.ComProvince	=				RsUser.Fields.Item("ComProvince").value
								.ComPostCode	=				RsUser.Fields.Item("ComPostCode").value
								.ComTel		=				RsUser.Fields.Item("ComTel").value
								.ComFax		=				RsUser.Fields.Item("ComFax").value
								.ComEmail		=				RsUser.Fields.Item("ComEmail").value
								.Estimated		=				RsUser.Fields.Item("Estimated").value
								.Birthday		=				RsUser.Fields.Item("Birthday").value
								.Other		=				RsUser.Fields.Item("Other").value
								.Years		=				RsUser.Fields.Item("Years").value
								.ComBus		=				RsUser.Fields.Item("ComBus").value
								.SizeOfSalesTeam		=				RsUser.Fields.Item("SizeOfSalesTeam").value
								.SizeOfSalesTrucks	=				RsUser.Fields.Item("SizeOfSalesTrucks").value
								.targetmarket		=				RsUser.Fields.Item("targetmarket").value
								.interested		=				RsUser.Fields.Item("interested").value
								.Countries		=				RsUser.Fields.Item("Countries").value
								.geographical		=				RsUser.Fields.Item("geographical").value
								.outlets		=				RsUser.Fields.Item("outlets").value
								.brands	=				RsUser.Fields.Item("brands").value
								.RecordBy	=				RsUser.Fields.Item("RecordBy").value
								.CheckDiscount = 			RsUser.Fields.Item("CheckDiscount").value
								.LabelLanguage = 			RsUser.Fields.Item("LabelLanguage").value
								.LabelStatus = 			RsUser.Fields.Item("LabelStatus").value
								.CounId = 			RsUser.Fields.Item("CounId").value
								.Website = 			RsUser.Fields.Item("Website").value
								.DistStatus = 			RsUser.Fields.Item("DistStatus").value
								
								End With
								End if
								Set RsUser = Nothing
							end sub
				End Class
%>

