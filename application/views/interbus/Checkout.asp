<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<%


' *** Edit Operations: declare variables

Dim MM_editAction
Dim MM_abortEdit
Dim MM_editQuery
Dim MM_editCmd
Dim MM_editConnection

Dim MM_editTable
Dim MM_editRedirectUrl
Dim MM_editColumn
Dim MM_recordId

Dim MM_fieldsStr
Dim MM_columnsStr
Dim MM_fields
Dim MM_columns
Dim MM_typeArray
Dim MM_formVal
Dim MM_delim
Dim MM_altVal
Dim MM_emptyVal
Dim MM_i

MM_editAction = CStr(Request.ServerVariables("SCRIPT_NAME"))
If (Request.QueryString <> "") Then
  MM_editAction = MM_editAction & "?" & Request.QueryString
End If

' boolean to abort record edit
MM_abortEdit = false

' query string to execute
MM_editQuery = ""
%>
<%
' *** Insert Record: set variables

If (CStr(Request("MM_insert")) = "FR_CheckOut") Then
  MM_editConnection = MM_Home2_STRING
  MM_editTable = "dbo.Order"
  MM_editRedirectUrl = "MailOrder.asp"
  MM_fieldsStr  = "Total|value|F_CustCompany|value|F_CustFname|value|F_CustLname|value|F_Add1|value|F_Add2|value|F_CustCity|value|F_CustState|value|F_CustCountry|value|F_Zipcode|value|F_phone1|value|F_phone2|value|F_fax|value|F_email|value|F_AirCode|value|F_AirCity|value|F_AirState|value|F_AirCountry|value"
  MM_columnsStr = "Total|none,none,NULL|ShipCompany|',none,''|ShipFName|',none,''|ShipLName|',none,''|ShipAdd1|',none,''|ShipAdd2|',none,''|ShipCity|',none,''|ShipState|',none,''|ShipCountry|',none,''|ShipZipCode|',none,''|ShipPhone1|',none,''|ShipPhone2|',none,''|ShipFax|',none,''|ShipEmail|',none,''|AirportCode|',none,''|AirportCity|',none,''|AirportState|',none,''|AirportCountry|',none,''"

  ' create the MM_fields and MM_columns arrays
  MM_fields = Split(MM_fieldsStr, "|")
  MM_columns = Split(MM_columnsStr, "|")
  response.write Ubound(MM_fields)
  
  ' set the form values
  For MM_i = LBound(MM_fields) To UBound(MM_fields) Step 2
    MM_fields(MM_i+1) = CStr(Request.Form(MM_fields(MM_i)))
  Next

  ' append the query string to the redirect URL
  If (MM_editRedirectUrl <> "" And Request.QueryString <> "") Then
    If (InStr(1, MM_editRedirectUrl, "?", vbTextCompare) = 0 And Request.QueryString <> "") Then
      MM_editRedirectUrl = MM_editRedirectUrl & "?" & Request.QueryString
    Else
      MM_editRedirectUrl = MM_editRedirectUrl & "&" & Request.QueryString
    End If
  End If

End If
%>
<%
' *** Insert Record: construct a sql insert statement and execute it

Dim MM_tableValues
Dim MM_dbValues

If (CStr(Request("MM_insert")) <> "") Then
	'on error resume next
  ' create the sql insert statement
  MM_tableValues = ""
  MM_dbValues = ""
  For MM_i = LBound(MM_fields) To UBound(MM_fields) Step 2
    MM_formVal = MM_fields(MM_i+1)
    MM_typeArray = Split(MM_columns(MM_i+1),",")
    MM_delim = MM_typeArray(0)
    If (MM_delim = "none") Then MM_delim = ""
    MM_altVal = MM_typeArray(1)
    If (MM_altVal = "none") Then MM_altVal = ""
    MM_emptyVal = MM_typeArray(2)
    If (MM_emptyVal = "none") Then MM_emptyVal = ""
    If (MM_formVal = "") Then
      MM_formVal = MM_emptyVal
    Else
      If (MM_altVal <> "") Then
        MM_formVal = MM_altVal
      ElseIf (MM_delim = "'") Then  ' escape quotes
        MM_formVal = "'" & Replace(MM_formVal,"'","''") & "'"
      Else
        MM_formVal = MM_delim + MM_formVal + MM_delim
      End If
    End If
    If (MM_i <> LBound(MM_fields)) Then
      MM_tableValues = MM_tableValues & ","
      MM_dbValues = MM_dbValues & ","
    End If
    MM_tableValues = MM_tableValues & MM_columns(MM_i)
    MM_dbValues = MM_dbValues & MM_formVal
  Next
  
  MM_editQuery = "insert into ["& MM_editTable&"] (" & MM_tableValues & ") values (" & MM_dbValues & ")"
  If (Not MM_abortEdit) Then
    ' execute the insert
   ' Set MM_editCmd = Server.CreateObject("ADODB.Command")
  '  MM_editCmd.ActiveConnection = MM_editConnection
 '   MM_editCmd.CommandText = MM_editQuery
  '  MM_editCmd.Execute
   ' MM_editCmd.ActiveConnection.Close

    If (MM_editRedirectUrl <> "") Then
    
' SET UP MAIL FOR CUSTOMER
'==================================
	  Response.Redirect(MM_editRedirectUrl&"?shopBody=Company Name: "& request.form("F_CustCompany") &"<br>"&_
	 "First Name: "& request.form("F_CustFName") &"<br>"&_
	 "Last Name: " & request.form("F_CustLName") &"<br>"&_
	 "Address 1: "& request.form ("F_Add1") &"<br>"&_
	 "Address 2: "& request.form ("F_Add2") &"<br>"&_
	 "City / Provice: " & request.form ("F_CustCity") &"<br>"&_
	 "State: " & request.form ("F_CustState") &"<br>"&_
	 "Country: " & request.form ("F_CustCountry") &"<br>"&_
	 "Post Code / Zip: " & request.form ("F_Zipcode") &"<br>"&_
	 "Phone1: " & request.form ("F_phone1") &"<br>"&_
	 "Phone2: " & request.form ("F_phone2") &"<br>"&_
	 "Fax: " & request.form ("F_fax")&"<br>" &_
	 "Email: " & request.form ("F_email")&"<br>"&_
	 "Airport Code: " & request.form ("F_AirCode") &"<br>"&_
	 "Airport City/ Provice: " & request.form ("F_AirCity")&"<br>" &_
	 "Airport State: " & request.form ("F_AirState")&"<br>" &_
	 "Airport Country: " & request.form ("F_AirCountry") &"<br>"&_
	 "Order Contents: "  & request.form ("contents") &"<br>"&_
	 "Total Order Value: " & request.form ("total")&""&_
	 "&F_email="&request.form ("F_email")&"&F_CustFName="&request.form ("F_CustFName")&"&F_CustLName="&request.form ("F_CustLName"))
    End If
  End If

End If
%>
<%
' Define Constants
CONST CARTPID = 0
CONST CARTPNAME = 1
CONST CARTPPRICE = 2
CONST CARTPQUANTITY = 3

CONST Color = 4
CONST Fsize = 5

dim i, x,newQ,i_num,j_num

'------------------------------------------------

' GET THE SHOPPING CART AND OR SET UP SESSION VARIABLE
IF NOT isArray( Session( "Cart" ) ) THEN
	DIM localCart( 5, 20 )
ELSE
	localCart = Session( "Cart" )
END IF
'-------------------------------------------------------
' GET THE PRODUCT INFORMATION

Dim IDProd, ProductName, ProductPrice, ProductColor, ProductFSize ,cmd

IDProd = Trim(Request("PID"))
ProductName = Trim(Request("PName"))
ProductPrice = Trim(Request( "PPrice" ))
ProductColor = Trim(Request( "PColor" ))
ProductFSize = Trim(Request( "PFsize" ))
cmd = request("cmd")
IF cmd = "Empty Cart" Then
	FOR i_num = 0 TO UBOUND( localCart )
		For j_num =0 To 5
		localCart(j_num,i_num) = ""
		Next 
	Next
End IF
%>
<html>
<head>
<title>CheckOut</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_validateForm() { //v4.0
  var i,p,q,nm,test,num,min,max,errors='',args=MM_validateForm.arguments;
  for (i=0; i<(args.length-2); i+=3) { test=args[i+2]; val=MM_findObj(args[i]);
    if (val) { nm=val.name; if ((val=val.value)!="") {
      if (test.indexOf('isEmail')!=-1) { p=val.indexOf('@');
        if (p<1 || p==(val.length-1)) errors+='- '+nm+' must contain an e-mail address.\n';
      } else if (test!='R') { num = parseFloat(val);
        if (isNaN(val)) errors+='- '+nm+' must contain a number.\n';
        if (test.indexOf('inRange') != -1) { p=test.indexOf(':');
          min=test.substring(8,p); max=test.substring(p+1);
          if (num<min || max<num) errors+='- '+nm+' must contain a number between '+min+' and '+max+'.\n';
    } } } else if (test.charAt(0) == 'R') errors += '- '+nm+' is required.\n'; }
  } if (errors) alert('The following error(s) occurred:\n'+errors);
  document.MM_returnValue = (errors == '');
}
//-->
</script>
</head>

<body>
<h2><strong><font color="#666666">Check Out Page</font></strong></h2>
<form action="<%=MM_editAction%>" method="POST" name="FR_CheckOut" id="FR_CheckOut" onSubmit="MM_validateForm('F_CustFname','','R','F_CustLname','','R','F_Add1','','R','F_CustCountry','','R','F_Zipcode','','R','F_phone1','','R','F_fax','','R','F_email','','RisEmail','F_AirCode','','R','F_AirCity','','R','F_AirState','','R','F_AirCountry','','R');return document.MM_returnValue">
<p>&nbsp;</p>
<table width="95%" border="1">
  <tr align="center">
    <td colspan="2"><h2><strong><font color="#666699">Your Order Detail</font></strong></h2></td>
    </tr>
  <tr align="center">
    <td colspan="2">
	<table width="101%" border="1">
   <tr>
     <td width="23%" height="24"><strong>Prod Name</strong></td>
     <td width="13%"><strong>Color</strong></td>
     <td width="10%"><strong>Size</strong></td>
     <td width="12%"><strong>Quantity</strong></td>
     <td width="26%"><strong>Price</strong></td>
     <td width="16%"><strong>Total</strong></td>
     </tr>
   <%
'-- Add by Toang   
response.write "Cart has " & UBOUND( localCart, 2 ) & " slots<br>"
   on error resume next
FOR i = 0 TO UBOUND( localCart, 2 )
	IF localCart( CARTPID, i ) <> "" THEN
	
	
		'-- Add by Toang
		response.write "Slot " & i & " is filled by order<br>"
		
		SubTotal = SubTotal + ( localCart( CARTPPRICE, i ) * localCart( CARTPQUANTITY,i ) )
%>
   <tr>
     <td><%=localCart(CARTPNAME, i)%>&nbsp;</td>
     <td><%=localCart(Color, i)%>&nbsp;</td>
     <td><%=localCart(Fsize, i)%>&nbsp;</td>
     <td><input name="pq<%=localCart(CARTPID,i)%>" type="hidden"  value="<%=localCart(CARTPQUANTITY,i)%>" 
size="5"><%=localCart(CARTPQUANTITY,i)%></td>
     <td>$ <%=formatCurrency( localCart( CARTPPRICE, i ) )%>&nbsp;</td>
     <td>$ <%=(localCart(CARTPPRICE, i) * localCart(CARTPQUANTITY, i))%>&nbsp;</td>
     </tr>
   <%
	END IF
NEXT
%>

   <tr>
     <td>&nbsp;</td>
     <td>&nbsp;</td>
     <td>&nbsp;</td>
     <td>&nbsp;</td>
     <td><strong>SubTotal</strong></td>
     <td>$ <%=(SubTotal)%>&nbsp;</td>
     </tr>
   <tr>
     <td>&nbsp;</td>
     <td>&nbsp;</td>
     <td>&nbsp;</td>
     <td>&nbsp;</td>
     <td><strong>Doc. Fee</strong></td>
     <td>$ 40.00</td>
     </tr>
   <tr>
     <td>&nbsp;</td>
     <td>&nbsp;</td>
     <td>&nbsp;</td>
     <td>&nbsp;</td>
     <td><font color="#FF0000">*Fright (quote and sent later)</font></td>
     <td>&nbsp;</td>
     </tr>
</table>
	</td>
  </tr>
  <tr align="center">
    <td colspan="2"><strong><font color="#666699">Estimate Toltal:
      </font></strong>
      <div align="center"> <strong>$ <%=(orderTotal+40 )%></strong>
        <input name="Total" type="hidden" id="Total" value="<%=(orderTotal+40 )%>">
        <input name="contents" type="hidden" id="contents" value="contents">
    </div></td>
    </tr>
  <tr>
    <td colspan="2"><strong>Shipping Information:</strong></td>
    </tr>
  <tr>
    <td width="29%">Company Name</td>
    <td width="71%"><input name="F_CustCompany" type="text" id="F_CustCompany" size="50"></td>
  </tr>
  <tr>
    <td>First Name</td>
    <td><input name="F_CustFname" type="text" id="F_CustFname" size="50"></td>
  </tr>
  <tr>
    <td>Last Name</td>
    <td><input name="F_CustLname" type="text" id="F_CustLname" size="50"></td>
  </tr>
  <tr>
    <td>Address 1</td>
    <td><input name="F_Add1" type="text" id="F_Add1" size="50"></td>
  </tr>
  <tr>
    <td>Address 2</td>
    <td><input name="F_Add2" type="text" id="F_Add2" size="50"></td>
  </tr>
  <tr>
    <td>City / Province</td>
    <td><input name="F_CustCity" type="text" id="F_CustCity" size="50"></td>
  </tr>
  <tr>
    <td>State</td>
    <td><input name="F_CustState" type="text" id="F_CustState"></td>
  </tr>
  <tr>
    <td>Country</td>
    <td><input name="F_CustCountry" type="text" id="F_CustCountry" size="50"></td>
  </tr>
  <tr>
    <td>Zipcode</td>
    <td><input name="F_Zipcode" type="text" id="F_Zipcode"></td>
  </tr>
  <tr>
    <td>Telphone 1</td>
    <td><input name="F_phone1" type="text" id="F_phone1">
      <font color="#FF00FF" size="-2">    include area code</font></td>
  </tr>
  <tr>
    <td>Telphone 2</td>
    <td><input name="F_phone2" type="text" id="F_phone2">
      <font color="#FF00FF" size="-2">include area code</font></td>
  </tr>
  <tr>
    <td>Fax</td>
    <td><input name="F_fax" type="text" id="F_fax">
      <font color="#FF00FF" size="-2">include area code</font></td>
  </tr>
  <tr>
    <td>Email</td>
    <td><input name="F_email" type="text" id="F_email" size="50"></td>
  </tr>
  <tr>
    <td colspan="2"><strong>Airport Information</strong></td>
    </tr>
  <tr>
    <td>Airport Code</td>
    <td><input name="F_AirCode" type="text" id="F_AirCode"></td>
  </tr>
  <tr>
    <td>Airport City / Province</td>
    <td><input name="F_AirCity" type="text" id="F_AirCity" size="50"></td>
  </tr>
  <tr>
    <td>Airport State</td>
    <td><input name="F_AirState" type="text" id="F_AirState" size="50"></td>
  </tr>
  <tr>
    <td>Airport Country</td>
    <td><input name="F_AirCountry" type="text" id="F_AirCountry" size="50"></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><input name="Butt_Next" type="submit" id="Butt_Next" value="NEXT"></td>
  </tr>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>

<input type="hidden" name="MM_insert" value="FR_CheckOut">
</form>
<p>&nbsp; </p>
</body>
</html>
