<?php echo doctype('html5'); ?>
<html>
   <head>
      <?php
         $meta = array(
         array('name' => 'Content-type',
         'content' => 'text/html; charset=utf-8', 'type' => 'equiv'),
         array('name' => 'robots', 'content' => 'no-cache'),
         array('name' => 'description', 'content' => 'My Great Site'),
         array('name' => 'keywords', 'content' => 'love, passion, intrigue, deception'),
         array('name' => 'robots', 'content' => 'no-cache'),
         array('name' => 'viewport',
         'content' => 'width=device-width,initial-scale=1,
         maximum-scale=1,user-scalable=no')
         );
         echo meta($meta);
         ?>
      <title><?php echo $this->sTitle;?></title>
      <!--CSS STYLES-->
      <?php echo
         link_tag(array('href' => 'assets/css/login.css',
             'rel' => 'stylesheet',
             'type'  => 'text/css')
         );
      ?>
   </head>
   <body class="login tooltips">
      <!--
         ===========================================================
         BEGIN PAGE
         ===========================================================
         -->
      <div class="login-header text-center">
         <?php echo img(array(
            'src' => 'assets/images/global/logo.png',
            'alt' => 'Logo',
            'class' => 'logo',
            ));
         ?>
      </div>
      <div class="login-wrapper">
         <?php
            $attribute = array(
            'id' => 'sLogins',
            'required' => 'true',
            );
            echo form_open_multipart('login/verify',$attribute);
            ?>
            <?php
            if ($this->session->flashdata('error')) {
            ?>
            <div class="alert alert-danger alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert">x</button>
                <?php echo $this->session->flashdata('error'); ?>
            </div>
            <?php
            } // end if msg
            ?>
         <div class="form-group has-feedback lg left-feedback no-label">
            <?php echo form_input(array(
               'type' => 'email',
               'name' => 'email',
               'class' => 'form-control no-border input-lg rounded',
               'placeholder' => 'Enter email',
               'autofocus' => 'true',
               ));
               ?>
            <span class="fa fa-user form-control-feedback"></span>
         </div>
         <?php echo form_error('email'); ?>
         <div class="form-group has-feedback lg left-feedback no-label">
            <?php echo form_input(array(
               'type' => 'password',
               'name' => 'password',
               'class' => 'form-control no-border input-lg rounded',
               'placeholder' => 'Enter password',
               'autofocus' => 'true',
               ));
               ?>
            <span class="fa fa-unlock-alt form-control-feedback"></span>
         </div>
         <?php echo form_error('password'); ?>
         <div class="form-group">
            <button type="submit"
            class="btn btn-warning btn-lg btn-perspective btn-block">
                LOGIN
            </button>
         </div>
         </form>
      </div>
      <!-- /.login-wrapper -->
      <!-- MAIN JAVASRCIPT (REQUIRED ALL PAGE)-->
      <script src="assets/js/jquery.min.js"></script>
      <script src="assets/plugins/retina/retina.min.js"></script>
   </body>
</html>
