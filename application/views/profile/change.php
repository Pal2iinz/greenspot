<?php
$id = null;
$password = null;
?>
<!-- Begin page heading -->
<?php echo heading('Password Management',1,array('class' => 'page-heading'));?>
<!-- End page heading -->
<ol class="breadcrumb default square rsaquo sm">
   <li><a href="index.html"><i class="fa fa-home"></i></a></li>
   <li><a href="#fakelink">Profile</a></li>
   <li class="active">Change Password</li>
</ol>
<div class='the-box'>
    <?php
        if ($this->session->flashdata('msg-success')) {
    ?>
    <div class="alert alert-success alert-dismissible" role="alert">
        <?php echo $this->session->flashdata('msg-success'); ?>
    </div>
    <?php
    } // end if msg
    if ($this->session->flashdata('msg-danger')) {
    ?>
    <div class="alert alert-danger alert-dismissible" role="alert">
        <?php echo $this->session->flashdata('msg-danger'); ?>
    </div>
    <?php
    }
    ?>
    <div class="row">
        <div class="col-md-5">
            <?php
            $id = $this->session->userdata['logged_in_admin']['id'];
            echo form_open('profile/password/change/');
            echo form_hidden('id',$id);
            ?>
            <div class="form-group">
                <?php
                echo form_label(
                    'New Password',
                    'password'
                );
                echo form_password('password', $password);
                echo form_error('password', '<div class="alert alert-danger">', '</div>');
                ?>
            </div>
            <div class="form-group">
                <?php
                echo form_label(
                    'Confirm Password',
                    'Confirm Password'
                );
                echo form_password('confirmpassword');
                echo form_error('confirmpassword', '<div class="alert alert-danger">', '</div>');
                ?>
            </div>
            <div class="form-group">
                <?php
                echo form_submit('submit','Confirm Change',array('class'=>'btn btn-primary'));
                ?>
            </div>
            <?php
            echo form_close();
            ?>
        </div>
    </div>
