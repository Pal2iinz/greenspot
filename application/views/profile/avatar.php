<!-- Begin page heading -->
<?php echo heading('Avatar Management',1,array('class' => 'page-heading'));?>
<!-- End page heading -->
<ol class="breadcrumb default square rsaquo sm">
   <li><a href="index.html"><i class="fa fa-home"></i></a></li>
   <li><a href="#fakelink">Profile</a></li>
   <li class="active">Avatar</li>
</ol>
<div class='the-box'>
    <?php
        if ($this->session->flashdata('msg-success')) {
    ?>
    <div class="alert alert-success alert-dismissible" role="alert">
        <?php echo $this->session->flashdata('msg-success'); ?>
    </div>
    <?php
    } // end if msg
    if ($this->session->flashdata('msg-danger')) {
    ?>
    <div class="alert alert-danger alert-dismissible" role="alert">
        <?php echo $this->session->flashdata('msg-danger'); ?>
    </div>
    <?php
    }
    ?>
    <div class="row">
        <div class="col-md-5">
            <?php
            foreach ($user as $key => $u) {
                $id = $u->id;
                $avatar = $u->avatar;
            }
            if (empty($avatar)) {
                $img = array(
                   'src' => 'assets/images/avatar/avatar.jpg',
                   'class' => 'img-thumbnail',
                   'alt' => 'Avatar',
                );
                echo img($img);
            } else {
                $img = array(
                   'src' => 'assets/images/avatar/'.$avatar,
                   'class' => 'img-thumbnail',
                   'alt' => 'Avatar',
                );
                echo img($img);
            } // end else
            echo form_open_multipart('profile/avatar/change/');
            echo form_hidden('id',$id);
            ?>
            <div class="form-group">
                <?php
                echo form_label(
                    'Avatar',
                    'avatar'
                );
                echo form_upload('avatar','avatar');
                ?>
            </div>
            <div class="form-group">
                <?php
                echo form_submit('submit','Confirm Change',array('class'=>'btn btn-primary'));
                ?>
            </div>
            <?php
            echo form_close();
            ?>
        </div>
    </div>
