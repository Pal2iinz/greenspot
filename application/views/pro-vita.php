<?php 
	if(isset($contents)){
		foreach ($contents as $key => $cont) {
			$image[$key] =$cont['image'];
		}
		
	}
?>
<section id='provita' class='clearfix'>
	<div class="container">
		<div id="desktop" class="visible-md visible-lg">
			<div class="col-xs-12 col-md-12">
			<?php
				$test = count($image);
				
				for ($i=0; $i < $test ; $i++) { 
					$attr = array(
						'src' => 'assets/images/content/'.$image[$i],
						'class' => 'img-responsive',
						'alt' => $image[$i],
					);
					echo img($attr);
				}
				//echo img("assets/images/content/".$image[$key])
			;?>
			</div>
		</div>
		<div id="moblie" class="visible-xs visible-sm">
			<div class="col-xs-12 col-md-12">
			<?php
				$test = count($image);
				
				for ($i=0; $i < $test ; $i++) { 
					$attr = array(
						'src' => 'assets/images/content/'.$image[$i],
						'class' => 'img-responsive',
						'alt' => $image[$i],
					);
					echo img($attr);
				}
				//echo img("assets/images/content/".$image[$key])
			;?>
			</div>
		</div>
	</div>
	<!--<img src="images/provita/p1.png">
	<img src="images/provita/p2.png">
	<img src="images/provita/p3.png">
-->
</section>
	