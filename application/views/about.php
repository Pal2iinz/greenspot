
<!--history-->
<section id='history'>

<?php   if($contents){
          foreach($contents as $i => $c) {
                    $detail[$i] = $c['content'];
                    $images = $c['image'];
                     $img[$i] = array(
                            'src' => 'assets/images/content/'.$images,
                            'alt' => $images,
                            'class' => 'img-responsive playgif',
                        );
                }
        ?>

<div class="container">
<div class=" history-content">
<div class="col-xs-12 col-sm-6">
  <?php echo $detail[0];?>
</div>
<div class="col-xs-12 col-xs-6 visible-md visible-lg ">
  <?php echo img($img[0]); ?>
</div>
</div>
<?php }?>
</div>
</section>


<!--vision-->
<section id='vmp'>
<div id="boxcontent" class="container">
<div class="row">
<?php   if($contents){
          foreach($contents as $i => $c) {
                    $detail[$i] = $c['content'];
                    $images = $c['image'];
                     $img[$i] = array(
                            'src' => 'assets/images/content/'.$images,
                            'alt' => $images,
                            'class' => 'img-responsive text-center playgif',
                        );
                }
        ?>

  <div  class="col-xs-12 col-sm-12">
    <div id="imgres">
      <?php echo $detail[1]; ?>
    </div>
  </div>
</div>
</div>
<div class="container">

<div id="vmpdesk" class="visible-lg visible-md">
  <div class="col-xs-12 col-sm-12">
              <?php     echo img($img[1]); ?>
  </div>
</div>
<div id="moblie" class="visible-xs visible-sm">
  <div class="col-xs-12 col-sm-12">
    <?php     echo img($img[1]); ?>
  </div>
</div>

</div>
  <?php    } ?>
</section>






<!--reward-->
<div class="container">

<section id='reward' class="visible-lg visible-md">
 <div class="col-xs-12 col-sm-12">
    <?php 
            foreach($contents as $i => $c) {
                    $detail[$i] = $c['content'];
                     $images = $c['image'];
                } 
                  echo '<div class="row">';
                  echo $detail[2];
                  echo '</div>';
            ?>
      </div>

</section>
<section id='rewardmoblie' class="visible-xs visible-sm">
 <div class="col-xs-12 col-sm-12">
    <?php 
            foreach($contents as $i => $c) {
                    $detail[$i] = $c['content'];
                     $images = $c['image'];
                } 
                  echo '<div class="row">';
                  echo $detail[2];
                  echo '</div>';
            ?>
      </div>

</section>
      </div>
