<!-- Begin page heading -->
<?php echo heading('Permission',1,array('class' => 'page-heading'));?>
<!-- End page heading -->
<ol class="breadcrumb default square rsaquo sm">
   <li><a href="index.html"><i class="fa fa-home"></i></a></li>
   <li><a href="#fakelink">Permission</a></li>
   <li class="active">Index</li>
</ol>
<div class='the-box'>
    <div class="row">
        <div class="col-md-12 table-responsive">
            <table class='table datatable'>
                <thead>
                    <tr>
                        <th width="100">No.</th>
                        <th width="300">Department</th>
                        <th>Permission</th>
                        <th width="50">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if(isset($permission)) {
                        $i = 1;
                        foreach ($permission as $key => $p) {
                    ?>
                    <tr>
                        <td><?php echo $i; ?></td>
                        <td>
                            <?php
                            foreach ($departments as $key => $d) {
                                if ($d->id === $p->department) {
                                    echo $d->department;
                                }
                            }
                            ?>
                        </td>
                        <td><?php echo $p->permission; ?></td>
                        <td>
                            <div class="dropdown">
                                <button class="btn btn-primary dropdown-toggle" type="button" id="dropdown" data-toggle="dropdown">
                                    <i class="fa fa-cog"></i>
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="dropdown">
                                    <li>
                                        <a class="edit"
                                            data-id="<?php echo $p->id;?>"
                                            data-p="<?php
                                            echo $p->permission;
                                            ?>">
                                                Edit
                                        </a>
				                    </li>
			                        <li>
                                        <a class="delete"
                                        data-id="<?php echo $p->id;?>">
                                            Delete
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                    <?php
                        $i++;
                        } // end foreach
                    } // end if isset
                    ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <a class="btn btn-primary" id="addNew">Add Permission</a>
        </div>
    </div>

<!-- Modal -->
<div class="modal fade" id="modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Add Permission</h4>
            </div>
            <?php
            echo form_open('admin/permission/save');
            ?>
            <div class="modal-body">
                <input type="hidden" name="id" id="id" value="">
                <?php
                echo form_label(
                    'department',
                    'Department',
                    array(
                        'class' => 'form-group control-label'
                    )
                );
                $departmentop = array();
                foreach ($departments as $key => $d) {
                    $departmentop[$d->id] = $d->department;
                }
                echo form_dropdown('department', $departmentop, '1',
                array(
                    'class' => 'form-group form-control'
                ));
                echo form_label(
                    'permission',
                    'Permission',
                    array(
                        'class' => 'form-group control-label'
                    )
                );
                echo form_input(
                    array(
                        'name' => 'permission',
                        'id' => 'permission',
                        'class' => 'form-group form-control'
                    )
                );
                ?>
            </div><!-- /.modal-body -->
            <div class="modal-footer">
                <?php
                echo form_submit('submit','submit',array('class'=>'btn btn-primary'));
                ?>
            </div><!--/.modal-footer-->
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
