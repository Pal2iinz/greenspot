<!-- Begin page heading -->
<?php echo heading('USER Management',1,array('class' => 'page-heading'));?>
<!-- End page heading -->
<ol class="breadcrumb default square rsaquo sm">
   <li><a href="index.html"><i class="fa fa-home"></i></a></li>
   <li><a href="#fakelink">User</a></li>
   <li class="active">Index</li>
</ol>
<?php
    if ($this->session->flashdata('msg-success')) {
?>
    <div class="alert alert-success" role="alert">
        <?php echo $this->session->flashdata('msg-success'); ?>
    </div>
<?php
} // end if msg
    if ($this->session->flashdata('msg-danger')) {
?>
    <div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert">x</button>
        <?php echo $this->session->flashdata('msg-danger'); ?>
    </div>
<?php
} // end if msg
?>
<div class='the-box'>
    <div class="row">
        <div class="col-md-12 table-responsive">
            <table class='table datatable'>
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Email</th>
                        <th>Firstname</th>
                        <th>Lastname</th>
                        <th>Department</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if(isset($user)) {
                        $i = 1;
                        foreach ($user as $key => $u) {
                    ?>
                    <tr>
                        <td>
                            <?php echo $i; ?>
                        </td>
                        <td><?php echo $u->email; ?></td>
                        <td><?php echo $u->firstname; ?></td>
                        <td><?php echo $u->lastname; ?></td>
                        <td>
                            <?php
                            foreach ($department as $key => $d) {
                                if ($d->id === $u->department) {
                                    echo $d->department;
                                } // end if
                            } // end foreach
                            ?>
                        </td>
                        <td>
                            <?php
                            if ($u->ipaddress !== null) {
                            ?>
                            <span class="label label-danger">
                                Active
                            </span>
                            <?php
                            } else {
                            ?>
                            <span class="label label-info">
                                inactive
                            </span>
                            <?php
                            } // end else
                            ?>
                        </td>
                        <td>
                            <div class="dropdown">
                                <button class="btn btn-primary dropdown-toggle" type="button" id="dropdown" data-toggle="dropdown">
                                    <i class="fa fa-cog"></i>
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="dropdown">
                                    <li>
                                        <?php
                                        echo anchor(
                                            'admin/user/view/'.$u->id,
                                            'View'
                                        );
                                        ?>
                                    </li>
                                    <li>
                                        <?php
                                        $cl = array('class' => 'edit');
                                        echo anchor(
                                            'admin/user/form/'.$u->id,
                                            'Edit'
                                        );
                                        ?>
                                    </li>
                                    <li>
                                        <a class="delete"
                                        data-id="<?php echo $u->id;?>">
                                            Delete
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                    <?php
                        $i++;
                        } // end foreach
                    } // end if isset
                    ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?php
                echo anchor('admin/user/form',
                    'Add User',
                    array(
                        'class' => 'btn btn-primary'
                    )
                )
            ?>
        </div>
    </div>
