<?php
foreach ($user as $key => $u) {
    $firstname = $u->firstname;
    $lastname = $u->lastname;
    $email = $u->email;
    $department = $u->department;
    $role = $u->role;
    $ip = $u->ipaddress;
}
?>
<!-- Begin page heading -->
<?php echo heading('User View',1,array('class' => 'page-heading'));?>
<!-- End page heading -->
<ol class="breadcrumb default square rsaquo sm">
   <li><a href="index.html"><i class="fa fa-home"></i></a></li>
   <li><a href="#fakelink">User</a></li>
   <li class="active">View</li>
</ol>
<div class='the-box'>
    <div class="row">
        <div class="col-md-12">
            <table class='table table-bordered'>
                <tbody>
                    <tr>
                        <td>Firstname</td>
                        <td><?php echo $firstname; ?></td>
                        <td>Lastname</td>
                        <td><?php echo $lastname; ?></td>
                    </tr>
                    <tr>
                        <td>Email</td>
                        <td><?php echo $email; ?></td>
                        <td>Department</td>
                        <td><?php echo $department; ?></td>
                    </tr>
                    <tr>
                        <td>Role</td>
                        <td><?php echo $role; ?></td>
                        <td>Last IP</td>
                        <td><?php echo $ip; ?></td>
                    </tr>
                    <tr>
                        <td>Status</td>
                        <td>
                            <?php
                            if ($ip !== null) {
                            ?>
                            <span class="label label-danger">
                                Active
                            </span>
                            <?php
                            } else {
                            ?>
                            <span class="label label-info">
                                inactive
                            </span>
                            <?php
                            } // end else
                            ?>
                        </td>
                        <td></td>
                        <td></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
