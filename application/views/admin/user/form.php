<?php
if (isset($user)) {
    foreach ($user as $key => $u) {
        $id = $u->id;
        $firstname = $u->firstname;
        $lastname = $u->lastname;
        $email = $u->email;
        $department = $u->department;
    } // end foreach
} else {
    $id = null;
    $firstname = null;
    $lastname = null;
    $email = null;
    $department = null;
}
?>
<!-- Begin page heading -->
<?php echo heading('DASHBOARD',1,array('class' => 'page-heading'));?>
<!-- End page heading -->
<ol class="breadcrumb default square rsaquo sm">
   <li><a href="index.html"><i class="fa fa-home"></i></a></li>
   <li><a href="#fakelink">User</a></li>
   <li class="active">Form</li>
</ol>
<div class='the-box'>
    <?php
        if ($this->session->flashdata('msg-success')) {
    ?>
    <div class="alert alert-success alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert">x</button>
        <?php echo $this->session->flashdata('msg-success'); ?>
    </div>
    <?php
    } // end if msg
    if ($this->session->flashdata('msg-danger')) {
    ?>
    <div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert">x</button>
        <?php echo $this->session->flashdata('msg-danger'); ?>
    </div>
    <?php
    } // end if msg
    if ($this->session->flashdata('msg-update')) {
    ?>
    <div class="alert alert-info alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert">x</button>
        <?php echo $this->session->flashdata('msg-update'); ?>
    </div>
    <?php
    }// end if msg
    ?>
    <div class="row">
        <div class="col-md-12">
            <?php
            echo form_open('admin/user/save',array('class'=>'form-horizontal'));
            echo form_hidden('id',$id);
            ?>
            <div class="form-group">
                <?php
                echo form_label(
                    'Firstname',
                    'Firstname',
                    array(
                        'class' => 'col-sm-2 control-label'
                    )
                );
                ?>
                <div class="col-sm-5">
                    <?php
                    echo form_input('firstname', $firstname);
                    echo form_error('firstname', '<div class="alert alert-danger">', '</div>');
                    ?>
                </div>
            </div>
            <div class="form-group">
                <?php
                echo form_label(
                    'Lastname',
                    'Lastname',
                    array(
                        'class' => 'col-sm-2 control-label'
                    )
                );
                ?>
                <div class="col-sm-5">
                    <?php
                    echo form_input('lastname', $lastname);
                    echo form_error('lastname', '<div class="alert alert-danger">', '</div>');
                    ?>
                </div>
            </div>
            <div class="form-group">
                <?php
                echo form_label(
                    'Email',
                    'Email',
                    array(
                        'class' => 'col-sm-2 control-label'
                    )
                );
                ?>
                <div class="col-sm-5">
                    <?php
                    echo form_input('email', $email);
                    echo form_error('email', '<div class="alert alert-danger">', '</div>');
                    ?>
                </div>
            </div>
            <?php
            if (!isset($user)) {
            ?>
            <div class="form-group">
                <?php
                echo form_label(
                    'Password',
                    'Password',
                    array(
                        'class' => 'col-sm-2 control-label'
                    )
                );
                ?>
                <div class="col-sm-5">
                    <?php
                    echo form_password('password');
                    echo form_error('password', '<div class="alert alert-danger">', '</div>');
                    ?>
                </div>
            </div>
            <?php
            } // end if isset
            ?>
            <div class="form-group">
                <?php
                echo form_label(
                    'Department',
                    'Department',
                    array(
                        'class' => 'col-sm-2 control-label'
                    )
                );
                ?>
                <div class="col-sm-5">
                    <?php
                    $departmentop = array();
                    foreach ($departments as $key => $d) {
                        $departmentop[$d->id] = $d->department;
                    }
                    echo form_dropdown('department', $departmentop, $department,
                    array(
                        'class' => 'col-sm-2 control-label'
                    ));
                    ?>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-5 col-sm-offset-2">
                    <?php
                    echo form_submit('submit','submit',array('class'=>'btn btn-primary'));
                    ?>
                </div>
            </div>
            <?php
            echo form_close();
            ?>
        </div>
    </div>
