<!-- Begin page heading -->
<?php echo heading('ROLES',1,array('class' => 'page-heading'));?>
<!-- End page heading -->
<ol class="breadcrumb default square rsaquo sm">
   <li><a href="index.html"><i class="fa fa-home"></i></a></li>
   <li><a href="#fakelink">Role</a></li>
   <li class="active">Index</li>
</ol>
<div class='the-box'>
    <div class="row">
        <div class="col-md-12">
            <table class='table datatable'>
                <thead>
                    <tr>
                        <th width="100">No.</th>
                        <th>Role</th>
                        <th width="50">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if(isset($role)) {
                        $i = 1;
                        foreach ($role as $key => $r) {
                    ?>
                    <tr>
                        <td><?php echo $i; ?></td>
                        <td><?php echo $r->role; ?></td>
                        <td>
                            <div class="dropdown">
                                <button class="btn btn-primary dropdown-toggle" type="button" id="dropdown" data-toggle="dropdown">
                                    <i class="fa fa-cog"></i>
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="dropdown">
                                    <li>
                                        <a class="edit"
                                            data-id="<?php echo $r->id;?>"
                                            data-r="<?php
                                            echo $r->role;
                                            ?>" >
                                                Edit
                                        </a>
				                    </li>
			                        <li>
                                        <a class="delete"
                                        data-id="<?php echo $r->id;?>">
                                            Delete
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                    <?php
                        $i++;
                        } // end foreach
                    } // end if isset
                    ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <a class="btn btn-primary" id="addNew">Add Role</a>
        </div>
    </div>

<!-- Modal -->
<div class="modal fade" id="modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Add Role</h4>
            </div>
            <?php
            echo form_open('admin/role/save');
            ?>
            <div class="modal-body">
                <input type="hidden" name="id" id="id" value="">
                <?php
                echo form_label(
                    'role',
                    'Role',
                    array(
                        'class' => 'form-group control-label'
                    )
                );
                echo form_input(
                    array(
                        'name' => 'role',
                        'id' => 'role',
                        'class' => 'form-group form-control'
                    )
                );
                ?>
            </div><!-- /.modal-body -->
            <div class="modal-footer">
                <?php
                echo form_submit('submit','submit',array('class'=>'btn btn-primary'));
                ?>
            </div><!--/.modal-footer-->
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
