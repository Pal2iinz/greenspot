<!-- Begin page heading -->
<?php echo heading('DEPARTMENT',1,array('class' => 'page-heading'));?>
<!-- End page heading -->
<ol class="breadcrumb default square rsaquo sm">
   <li><a href="index.html"><i class="fa fa-home"></i></a></li>
   <li><a href="#fakelink">Department</a></li>
   <li class="active">Index</li>
</ol>
<div class='the-box'>
    <div class="row">
        <div class="col-md-12">
            <table class='table datatable'>
                <thead>
                    <tr>
                        <th width="100">No.</th>
                        <th>Department</th>
                        <th width="50">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if(isset($department)) {
                        $i = 1;
                        foreach ($department as $key => $d) {
                    ?>
                    <tr>
                        <td><?php echo $i; ?></td>
                        <td><?php echo $d->department; ?></td>
                        <td>
                            <div class="dropdown">
                                <button class="btn btn-primary dropdown-toggle" type="button" id="dropdown" data-toggle="dropdown">
                                    <i class="fa fa-cog"></i>
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="dropdown">
                                    <li>
                                        <a class="edit"
                                            data-id="<?php echo $d->id;?>"
                                            data-d="<?php
                                            echo $d->department;
                                            ?>" >
                                                Edit
                                        </a>
				                    </li>
			                        <li>
                                        <a class="delete"
                                        data-id="<?php echo $d->id;?>">
                                            Delete
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                    <?php
                        $i++;
                        } // end foreach
                    } // end if isset
                    ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <a class="btn btn-primary" id="addNew">Add Department</a>
        </div>
    </div>

<!-- Modal -->
<div class="modal fade" id="modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Add Department</h4>
            </div>
            <?php
            echo form_open('admin/department/save');
            ?>
            <div class="modal-body">
                <input type="hidden" name="id" id="id" value="">
                <?php
                echo form_label(
                    'department',
                    'Department',
                    array(
                        'class' => 'form-group control-label'
                    )
                );
                echo form_input(
                    array(
                        'name' => 'department',
                        'id' => 'department',
                        'class' => 'form-group form-control'
                    )
                );
                ?>
            </div><!-- /.modal-body -->
            <div class="modal-footer">
                <?php
                echo form_submit('submit','submit',array('class'=>'btn btn-primary'));
                ?>
            </div><!--/.modal-footer-->
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
