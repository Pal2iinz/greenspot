<?php
foreach ($user as $key => $u) {
    $userid = $u->id;
    $firstname = $u->firstname;
    $lastname = $u->lastname;
    $department = $u->department;
    $role = $u->role;
}
if ($access !== false) {
    foreach ($access as $key => $a) {
        $perm[$a->perm] = $a->access;
    }
}
?>
<!-- Begin page heading -->
<?php echo heading('ACCESS MANAGEMENT',1,array('class' => 'page-heading'));?>
<!-- End page heading -->
<ol class="breadcrumb default square rsaquo sm">
   <li><a href="index.html"><i class="fa fa-home"></i></a></li>
   <li><a href="#fakelink">Access</a></li>
   <li class="active">Index</li>
</ol>
<div class='the-box'>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-body">
                    Name :: <?php echo $firstname.' '.$lastname; ?>
                    <br>
                    Role ::
                    <?php
                    foreach ($roles as $key => $r) {
                        if ($r->id === $role) {
                            echo $r->role;
                        }
                    }
                    ?>
                    <br>
                    Department ::
                    <?php
                    foreach ($dep as $key => $d) {
                        if ($d->id === $department) {
                            echo $d->department;
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
    </div><!-- /.row -->
    <?php
    echo form_open('admin/user/access/save');
    echo form_hidden('id',$userid);
    ?>
    <div class="row">
        <div class="col-md-12 table-responsive">
            <table class='table datatable'>
                <thead>
                    <tr>
                        <th width="100">No.</th>
                        <th>Permission</th>
                        <th width="200">Access</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if(isset($permission)) {
                        $i = 1;
                        foreach ($permission as $key => $p) {
                    ?>
                    <tr>
                        <td><?php echo $i; ?></td>
                        <td><?php echo $p->permission; ?></td>
                        <td>
                            <?php
                            if (isset($perm[$p->id])) {
                                switch ($perm[$p->id]) {
                                    case '0':
                                        echo form_radio(
                                            'perm['.$p->id.']',
                                            '0',
                                            true
                                        );
                                        echo form_label('Yes');
                                        echo form_radio(
                                            'perm['.$p->id.']',
                                            '1'
                                        );
                                        echo form_label('No');
                                        break;
                                    case '1':
                                        echo form_radio(
                                            'perm['.$p->id.']',
                                            '0'
                                        );
                                        echo form_label('Yes');
                                        echo form_radio(
                                            'perm['.$p->id.']',
                                            '1',
                                            true
                                        );
                                        echo form_label('No');
                                        break;
                                    default:
                                        echo 'set access!!';
                                        break;
                                }
                            } else {
                                echo form_radio(
                                    'perm['.$p->id.']',
                                    '0'
                                );
                                echo form_label('Yes');
                                echo form_radio(
                                    'perm['.$p->id.']',
                                    '1'
                                );
                                echo form_label('No');
                            }
                            ?>
                        </td>
                    </tr>
                    <?php
                        $i++;
                        } // end foreach
                    } // end if isset
                    ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?php
            echo form_submit('submit','Save Access',array('class'=>'btn btn-primary'));
            ?>
        </div>
    </div>
    <?php
    echo form_close();
    ?>
