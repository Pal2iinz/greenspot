<section id ="Content" >
<div class="resize">
<div class="container">
<div class="col-xs-12 col-md-12">
<div class="img-center">
<div class="row">
	<div class=" col-xs-12 col-md-6">
	<div id="tvcBox">
		<h2 class="colorGreen">tvc</h2>
		<?php
			if ($tvc) {
				foreach ($tvc as $key => $t) {
				$id = $t->id;
				$title = $t->title;
				$images = $t->image;
				$url = $t->url;
			} // end foreach

			$file_parts = pathinfo($images);
			$ext = $file_parts['extension'];
			$supported_image = array('gif','jpg','jpeg','png');
			$img = array(
				'src' => 'assets/images/promotion/'.$images,
				'alt' => $title,
				'class' => 'img-responsive'
			);
			if (in_array($ext, $supported_image)) {
				$path = $url;
				echo anchor($path, img($img),array('onClick' => "pushEventStat('View_TVC')",'src' => $url,'target' => '_blank') );
			}
			} else {
					$id = null;
					$title = null;
					$images = 'http://fakeimg.pl/477x312/';
					$url = "#";
					echo img($images); 
			}
		?>
	</div>
	</div>
	<div class="col-xs-12 col-md-6">
	<div id="promotionBox">
		<h2 class="colorOrange">Promotion</h2>
		<?php
		if ($promotion) {
			foreach ($promotion as $key => $p) {
				$id = $p->id;
				$title = $p->title;
				$images = $p->image;
				$url = $p->url;
			} // end foreach
			$file_parts = pathinfo($images);
			$ext = $file_parts['extension'];
			$supported_image = array('gif','jpg','jpeg','png');
			$img = array(
				'src' => 'assets/images/promotion/'.$images,
				'alt' => $title,
				'class' => 'img-responsive '
			);
			if (in_array($ext, $supported_image)) {
				$path = $url;
				echo anchor($path, img($img),array('onClick' => "pushEventStat('View_TVC')",'src' => $url,'target' => '_blank') );
			}
		} else {
				$id = null;
				$title = null;
				$images = 'http://fakeimg.pl/477x312/';
				$url = "#";
				echo img($images); 
			}
		?>
	</div>
	</div>
</div>
</div>
</div>


		<div class="col-xs-12 col-md-6">
		<?php if($contents){
			foreach ($contents as $key => $c) {
				$id = $c->id;
				$content = $c->content;
				$images = $c->image;
			} // end foreach
		?>
			<div id="enjoyLife" class="visible-md visible-lg ">
				<img src="assets/images/content/<?php echo $images;?>" class="img-responsive playgif img-enjoylife" >
			</div>
		</div>
		<div class="col-xs-12 col-md-6">
			<div id="DenjoyLifeRandom" class="enjoyLifeRandom  visible-md visible-lg">
			<?php echo $content;?>
			<?php
				echo anchor('greenspot','ดูรายละเอียด',
				array('onClick' => "pushEventStat('View_EnjoyLife')",
				'class' => 'viewDetail')
			);
			?>
			</div>
			<div id="MenjoyLifeRandom" class="enjoyLifeRandom  visible-xs visible-sm ">
			<?php echo $content;?>
			<?php
				echo anchor('greenspot','ดูรายละเอียด',
				array('onClick' => "pushEventStat('View_EnjoyLife')",
				'class' => 'viewDetail')
			);
			?>
		</div>
		<?php }?>
		</div>

</div>
</div>
</section>

<!-- social network -->
<section id="socialNetwork">
 <div class="col-xs-12">
    <div class="inner" >
        <div class="row">
        <div class="col-xs-12">
        <h2 class="headingMain">
            <span>Social Network</span>
        </h2>
        </div>
        </div>
        <div class="row">
         <div id="socialNetworkSlide" class="owl-carousel owl-theme ">
          <?php if($social){
            foreach ($social as $key => $s) {
                    $id     = $s->id;
                    $title  = $s->title;
                    $details = $s->details;
                    $images = $s->image;
                    $url    = $s->url;
                $img = array(
                    'src' => 'assets/images/social/'.$images,
                    'alt' => $title,
                    'class' => 'img-responsive playgif'
                );
            ?>

          <div class="item box-shadow"> 
          <!-- Desktop -->
            <div class="box-content visible-md visible-lg ">
              <div class="col-md-6 img-box">
                <?php echo img($img);?>
              </div>
               <div class="col-md-6">
               <a target="_blank" href='<?php echo $url;?>'>
                    <span class="text"><?php echo $details;?> </span>
                </a>
               </div>
            </div>
            <!-- Moblie -->
             <div class="visible-xs visible-sm">
              <div class="col-sm-12">
                 <a target="_blank" href='<?php echo $url;?>'>
                    <?php echo img($img);?>
                  </a>
              </div>
            </div>


          </div>
          <?php }  } ?>
        </div>
        </div>
        </div>
</div>
</section>



<section id="delivery">
         <div class="row"> 
    <?php
      if ($footer) {
            foreach ($footer as $key => $f) {
                $id = $f->id;
                $images = $f->image;
                $url = $f->url;
            } // end foreach
        $img = array(
            'src' => 'assets/images/banner/'.$images,
            'alt' => $title,
            'class' => 'img-responsive center-block playgif',
          
        );
        $path = $url;
    ?>

      <div class="col-xs-12 col-md-12">
    <h2>
       <!-- <a onClick="pushEventStat('Follow_FB');" href="<?php echo $url;?>" target="_blank" class="img-responsive"></a>-->
       <a  href="#" target="_blank" >
        	<img src="assets/images/main/heading-follow.png" class="img-responsive center-block">
        </a>
    </h2>
    </div>
    </div>
    <div class="container">
    <div class="row"> 
      <div class="col-xs-12 col-md-12">
      <?php 
       echo anchor($path, img($img),
			array('onClick' => "pushEventStat('Delivery')",
			'target' => '_blank')
          );
      ?>
    </div>
    </div>
   </div>
      <?php } else{ ?>
 <div class="container">
 <div class="row"> 
  <div class="col-xs-12 col-md-12">
    <h2>
        <a  href="#" target="_blank" >
        	<img src="assets/images/main/heading-follow.png" class="img-responsive center-block">
        </a>
    </h2>

        <?php 
        $img = array(
            'src' => 'http://fakeimg.pl/511x116/',
            'class' => 'img-responsive center-block playgif',
          
        );

          echo anchor($path, img($img),
                array('onClick' => "pushEventStat('Delivery')",
                      'target' => '_blank')
            );
        ?>
            </div>
</div>
</div>
    <?php }?>

</section>


<?php if(isset($intro)) {
foreach ($intro as $key => $in) {?>
<div id="IntroPopup" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
      <?php if(empty($in->url)){?>
        <?php
					echo image_asset_att(
					'intro/'.$in->image,
					array(
						'id' => 'intro',
					    'class' => 'img-responsive',
					 
					)
					);
				?>
			<?php }else{?>
			<a href="<?php echo $in->url;?>" target="_blank">
			<?php
					echo image_asset_att(
					'intro/'.$in->image,
					array(
						'id' => 'intro',
					    'class' => 'img-responsive',
					
					)
					);
				?>
			</a>
			<?php } ?>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php } } ?>

