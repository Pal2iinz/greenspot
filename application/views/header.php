<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title><?php echo $title;?></title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url();?>assets/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/styles/header.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/styles/footer.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/moblie.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/plugins/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/plugins/owlcarousel/assets/owl.theme.default.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet"/>
    <link href="http://vjs.zencdn.net/6.1.0/video-js.css" rel="stylesheet">
    
    <style type="text/css">
    #HeroBanner .item img{
      display: block;
      width: 100%;
      height: auto;
    }
    </style>
      <?php
            if (isset($style)) {
                foreach ($style as $key => $css) {
                    echo css_asset($css);
                }
            }
        ?>

  </head>
  <body class="greenspot">
      <!-- Static navbar -->
      <nav class="navbar navbar-desktop visible-md visible-lg" style="padding: 20px 0 0 100px;">
        <div class="container-fluid">
        <div class="navmenu">
          <div class="navbar-header">
            <a class="navbar-brand" href="#">
              <img src="http://greenspot.co.th/images/global/logo.png" class="desktop-logo img-responsive">
            </a>
          </div>
          <div  class="navbar-collapse collapse" style="margin-top: 40px;">
            <ul class="nav navbar-nav">
              <li class="active">
              <a href="<?php echo base_url();?>">
                <img src="<?php echo base_url();?>/assets/images/nav/home.png">
                    หน้าหลัก
              </a>
              </li>
              <li>
              <a href="<?php echo base_url();?>about">
                <img src="<?php echo base_url();?>/assets/images/nav/about.png">
                    เกี่ยวกับเรา
              </a>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <img src="<?php echo base_url();?>/assets/images/nav/product.png"> ผลิตภัณฑ์</a>
                <ul class="dropdown-menu">
                  <li><?php echo anchor('vitamilk','ไวตามิ้ลค์');?></li>
                  <li><?php echo anchor('vsoy','วีซอย');?></li>
                  <li><?php echo anchor('greenspot','กรีนสปอต');?></li>
                </ul>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <img src="<?php echo base_url();?>/assets/images/nav/menu.png"> มุมสุขภาพ</a>
                <ul class="dropdown-menu">
                  <li class="submenu"><?php echo anchor('healthy','สาระสุขภาพ');?></li>
                  <!--<li><?php echo anchor('healthy-menu','เมนูเพื่อสุขภาพ');?></li>-->
                  <li><?php echo anchor('vitamilkclub','Pro-Vita');?></li>
                </ul>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <img src="<?php echo base_url();?>/assets/images/nav/activity.png"> กิจกรรม</a>
                <ul class="dropdown-menu">
                  <li class="submenu"><?php echo anchor('vitamilkclub','Vitamilk CLUB');?></li>
                  <li><?php echo anchor('#','V-SOY CLUB');?></li>
                  <li><?php echo anchor('#','Greenspot CLUB');?></li>
                </ul>
              </li>
              <li>
                <a href="<?php echo base_url();?>jobcareer">
                  <img src="<?php echo base_url();?>/assets/images/nav/work.png" style="width:39px;height:41px">
                      ร่วมงานกับเรา
                </a>
              </li>
          <!--    <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <img src="<?php echo base_url();?>/assets/images/nav/order.png"> สั่งซื้อสินค้า</a>
                <ul class="dropdown-menu">
                    <li><?php echo anchor('orderlist','สินค้าทั้งหมด');?></li>
                  <li><?php echo anchor('ordervitamilk#products','ไวตามิ้ลค์');?></li>
                  <li><?php echo anchor('ordervsoy#products','วีซอย');?></li>
                </ul>
              </li>-->
              <li> 
                <a href="<?php echo base_url();?>interbus">
               <!-- <a href="http://www.greenspot.co.th/interbus/">-->
                <img src="<?php echo base_url();?>/assets/images/nav/world.png">
                    <!-- INTERNATIONAL BUSINESS -->
                    Export
                  </a>
              </li>
              <!-- <a href="<?php echo base_url();?>orderlist">
                  <span class="badge badge-danger">
                    5
                  </span>
                 
                    <img src="<?php echo base_url();?>assets/carts/Cartmenu.png">
                       </a>
                -->
 
            </ul>
            </div>
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
        </nav>


<!-- Moblie --> 
<header id="MoblieMenu" class="visible-xs visible-sm" style="padding-bottom: 50px;">
<div class="container">
<nav class="navbar navbar-default navbar-fixed-top ">
     <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#MenuMoblie" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">
              <img src="http://greenspot.co.th/images/global/logo.png" class="moblie-logo img-responsive">
            </a>
        </div>
        <div id="MenuMoblie" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
              <li class="active">
              <a href="<?php echo base_url();?>">
                <img src="<?php echo base_url();?>/assets/images/nav/home.png">
                    หน้าหลัก
              </a>
              </li>
              <li>
              <a href="<?php echo base_url();?>about">
                <img src="<?php echo base_url();?>/assets/images/nav/about.png">
                    เกี่ยวกับเรา
              </a>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <img src="<?php echo base_url();?>/assets/images/nav/product.png"> ผลิตภัณฑ์</a>
                <ul class="dropdown-menu">
                 <li><?php echo anchor('vitamilk','ไวตามิ้ลค์');?></li>
                  <li><?php echo anchor('vsoy','วีซอย');?></li>
                  <li><?php echo anchor('greenspot','กรีนสปอต');?></li>
                </ul>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <img src="<?php echo base_url();?>/assets/images/nav/menu.png"> มุมสุขภาพ</a>
                <ul class="dropdown-menu">
                  <li class="submenu"><?php echo anchor('healthy','สาระสุขภาพ');?></li>
                  <!--<li><?php echo anchor('healthy-menu','เมนูเพื่อสุขภาพ');?></li>-->
                  <li><?php echo anchor('vitamilkclub','Pro-Vita');?></li>
                </ul>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <img src="<?php echo base_url();?>/assets/images/nav/activity.png"> กิจกรรม</a>
                <ul class="dropdown-menu">
                  <li class="submenu"><?php echo anchor('vitamilkclub','Vitamilk CLUB');?></li>
                  <li><?php echo anchor('#','V-SOY CLUB');?></li>
                  <li><?php echo anchor('#','Greenspot CLUB');?></li>
                </ul>
              </li>
              <li>
               <a href="<?php echo base_url();?>jobcareer">
                  <img src="<?php echo base_url();?>/assets/images/nav/work.png" style="width:39px;height:41px">
                      ร่วมงานกับเรา
                </a>
              </li>
          <!--    <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <img src="<?php echo base_url();?>/assets/images/nav/order.png"> สั่งซื้อสินค้า</a>
                <ul class="dropdown-menu">
                  <li><?php echo anchor('orderlist','สินค้าทั้งหมด');?></li>
                  <li><?php echo anchor('ordervitamilk#products','ไวตามิ้ลค์');?></li>
                  <li><?php echo anchor('ordervsoy#products','วีซอย');?></li>
                </ul>
              </li>-->
              <li> 
                <a href="<?php echo base_url();?>interbus">
               <!-- <a href="http://www.greenspot.co.th/interbus/">-->
                <img src="<?php echo base_url();?>/assets/images/nav/world.png">
                    <!-- INTERNATIONAL BUSINESS -->
                    Export
                  </a>
              </li>
            </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
</div>
</header>

<?php   
  $sSegment = $this->uri->segment(1);
  if($sSegment !== 'pro-vita' & $sSegment !== 'vitamilkclub' & $sSegment !== 'interbus'){ 
?>
<div class="visible-md visible-lg">
<section id="hero"  style="margin-top: -120px;">
<div id="HeroBanner" class="owl-carousel owl-theme desktop">
  <?php 
  if($hero){
       foreach ($hero as $key => $h) {
         $id     = $h->id;
                    $title  = $h->title;
                    $images = $h->hero;
                    $page = $h->page;
                    $img = array(
                        'src' => base_url().'assets/images/hero/'.$images,
                        'alt' => $title,
                        'class' => 'img-responsive playgif',
                        'data-swap' => base_url().'assets/images/hero/'.$images,
                    );
                    $file_parts = pathinfo($images);
                    $ext = $file_parts['extension'];
                    

?>
    <div class="item">
    <?php 
      $supported_image = array(
          'gif',
          'jpg',
          'jpeg',
          'png'
      );
      if (in_array($ext, $supported_image)) {
         echo img($img);
      }else{
       echo '<video id="my-video"  preload="auto" width="1349px" height="749px"  autoplay loop>
            <source src="assets/images/hero/' . $images . '" type="video/mp4">
            <source src="assets/images/hero/' . $images . '" type="video/webm">
      </video>'
      ;
    
      }
      ?>
   
   </div>
<?php } 
}else{

  $sSegment = $this->uri->segment(1);
  //  var_dump($sSegment);
  if($sSegment !== 'pro-vita'){
    echo img('assets/images/hero/default.png');  
  }

} ?>

</div>
</div>
</section>
<?php }?>

<?php   
  $sSegment = $this->uri->segment(1);
  if($sSegment !== 'pro-vita' & $sSegment !== 'vitamilkclub' & $sSegment !== 'interbus'){ 
?>
<div class="visible-xs">
<section id="hero" >
<div id="MoblieHero" class="owl-carousel owl-theme " style="height: 200px;">
     <?php if($hero){
       foreach ($hero as $key => $h) {
         $id     = $h->id;
                    $title  = $h->title;
                    $images = $h->hero;
                    $page = $h->page;
                    $img = array(
                        'src' => base_url().'assets/images/hero/'.$images,
                        'alt' => $title,
                        'class' => 'img-responsive'
                    );
                    $file_parts = pathinfo($images);
                    $ext = $file_parts['extension'];
                 //   var_dump($file_parts['extension']);
?>
    <div class="item">
    <?php 
      $supported_image = array(
          'gif',
          'jpg',
          'jpeg',
          'png'
      );
      if (in_array($ext, $supported_image)) {
         echo img($img);
      }else{
       echo '<video id="my-video"  preload="auto" width="1349px" height="749px"  autoplay loop>
      <source src="assets/images/hero/' . $images . '" type="video/mp4">
      <source src="assets/images/hero/' . $images . '" type="video/webm">
      </video>'
      ;
    
      }
      ?>
    
   
   </div>
<?php } } ?>
</div>
</div>
</section>
<?php }?>