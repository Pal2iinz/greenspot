<style>

#HeroDesktop {
    display: inline;
    transform: translate(0%, 25%)
}
.herodesktop{position: initial;height: 750px;}
.heromobile{
    width: 145%;
    position: initial;
    left: 50%;
    transform: translate(-50%, -10%);
    z-index: 1;
}


#Content{
    background: url(http://greenspot.co.th/images/main/content.png) 50% 100% no-repeat;
    margin: 0 auto 20px;
    max-width: 1507px;
    min-height: 698px;
}

 #Content #promotionBox,#Content #tvcBox {
    background: url(http://greenspot.co.th/images/global/bottom-shadow.png) 50% 100% no-repeat;
 
    margin: 0 22px;
    padding-bottom: 17px;
    width: 477px;
}

#Content #promotionBox h2, #Content #tvcBox h2 {
    font-size: 33px;
    text-transform: uppercase;
}

#Content #enjoyLife {
    position: relative;
    margin: 0 auto;
    padding-top: 20px;
    width: 952px;
}

.colorGreen {
    color: #00874e;
}
.colorOrange {
    color: #ff8300;
}

.enjoyLifeRandom {
    color: #fff;
}

#Content .enjoyLifeRandom .viewDetail {
    background: url(http://greenspot.co.th/images/global/icon-plus.png) 93% 50% no-repeat;
    border: 2px solid #fff;
    color: #fff;
    display: inline-block;
    font-size: 20px;
    line-height: 1;
    margin-top: 15px;
    padding: 4px 35px 4px 15px;
}
#Content .enjoyLifeRandom h3 {
    font-size: 28px;
    line-height: 1;
    margin-bottom: 15px;
}

#Content .enjoyLifeRandom p {
    font-size: 18px;
    line-height: 1;
}


#socialNetwork {
    margin-bottom: 10px;
}

#socialNetwork>.inner {
    width: 1000px;
}
.headingMain {
    border-top: 8px solid #ff8300;
    font-size: 40px;
    height: 8px;
    margin-top: 20px;
    text-align: center;
    transform: translateX(55%)
}
.headingMain span {
    background-color: #fff;
    color: #ff8300;
    display: inline-block;
    padding: 0 20px;
    position: relative;
    text-transform: uppercase;
    top: -31px;
}
#container {
    max-width: 2000px;
    position: relative;
}
#container, .inner {
    margin: 0 auto;
}

#socialNetwork .bx-wrapper {
    position: relative;
}

@media only screen and (min-device-width : 320px) and (max-device-width : 480px) {


    .headingMain {
     border-top: 8px solid #ff8300;
    font-size: 22px;
    height: 8px;
    padding: 15 15;
    text-align: center;
    vertical-align: middle;
    width: 300;
    transform: translateX(0);
    }
    
}
    

</style>


<section id="HeroDesktop">
        <div class="herodesktop visible-md visible-lg " style="background: url(http://greenspot.co.th/images/main/herobanner/vitamilk.png) 50% 0 no-repeat;" >
        </div>
</div>
</section>
<section id="HeroMoblie">
       <img src="http://greenspot.co.th/images/main/herobanner/vitamilk.png" class="heromobile visible-xs visible-sm">
</section>


<section id ="Content">
  <div class="inner">
    <div class="form-group">
        <div class="col-md-offset-1 col-xs-12 col-md-5">
            <div id="tvcBox">
                <h2 class="colorGreen">tvc</h2>
                  <a href="#">
                    <img src="https://fakeimg.pl/477x312" class="img-responsive">
                  </a>
            </div>
        </div>
         <div class="col-xs-12 col-md-6">
            <div id="promotionBox">
                <h2 class="colorOrange">Promotion</h2>
                <img src="https://fakeimg.pl/477x312" class="img-responsive">
            </div>
        </div>
    </div>
     <div class="form-group">
          <div class="col-md-offset-1 col-xs-12 col-md-4">
              <div id="enjoyLife">
                  <img src="http://greenspot.co.th/images/main/enjoy1.png" class="img-responsive">
              </div>
          </div>
          <div class="col-xs-12 col-md-6">
           <div class="enjoyLifeRandom  visible-md visible-lg ">

                        <h3>น้ำส้มกรีนสปอตเป็นเครื่องดื่มให้ความสดชื่น ที่มีคุณภาพ ทำจากน้ำส้มแท้ ไม่อัดลม เหมาะสำหรับดับกระหาย ให้ความสดชื่น</h3>
                        <p>สำหรับผลิตภัณฑ์ของกรีนสปอต เมื่อนำมาผสมให้เป็น Cocktail หรือ Mocktail นั้นจะให้รสชาติที่อร่อย แปลกใหม่ ไม่จำเจ</p>

                        <a onclick="pushEventStat('View_EnjoyLife');" class="viewDetail" href="greenspot.html">ดูรายละเอียด</a>
 
                </div>
              <div class="enjoyLifeRandom  visible-xs visible-sm ">

                        <h3>น้ำส้มกรีนสปอตเป็นเครื่องดื่มให้ความสดชื่น ที่มีคุณภาพ ทำจากน้ำส้มแท้ ไม่อัดลม เหมาะสำหรับดับกระหาย ให้ความสดชื่น</h3>
                        <p>สำหรับผลิตภัณฑ์ของกรีนสปอต เมื่อนำมาผสมให้เป็น Cocktail หรือ Mocktail นั้นจะให้รสชาติที่อร่อย แปลกใหม่ ไม่จำเจ</p>

                        <a onclick="pushEventStat('View_EnjoyLife');" class="viewDetail" href="greenspot.html">ดูรายละเอียด</a>
 
                </div>
        </div>
    </div>

</div>
</section>


<section id="socialNetwork">

<div class="col-xs-12 col-sm-6">
  <div class="form-group">
    <div class="inner">
        <h2 class="headingMain">
            <span>Social Network</span>
        </h2>
    </div>
    </div>
</div>

</section>

<!--<section id ="Content">
<div class="col-md-6">
 <div class="form-group  pull-right">
<div id="promotionBox">

      <div class="col-md-6"><img src="https://fakeimg.pl/477x312"></div>
    </div>
</div>
</div>
<div class="col-md-6">
     <div class="form-group">
     <div id="tvcBox">
      <div class="col-md-6"><img src="https://fakeimg.pl/477x312"></div>
      </div>
    </div>
</div>
</section>-->




<!-- 07/06/2560 -->
<!--<div class=" visible-md visible-lg">
<section id="HeroDesktop">
  <ul class="heroBannerSlide">
      <?php if($hero){
       foreach ($hero as $key => $h) {
         $id     = $h->id;
                    $title  = $h->title;
                    $images = $h->hero;
                    $page = $h->page;
                    $img = array(
                        'src' => 'assets/images/hero/'.$images,
                        'alt' => $title,
                    );

            ?>
          <li class="herodesktop  " style="background: url(<?php echo base_url();?>assets/images/hero/<?php echo $images;?>) 50% 0 no-repeat;" >
        </li>
      <?php } ?>
      <?php }else{?>
        <div class="herodesktop"  style="background: url(<?php echo base_url();?>assets/images/hero/default.png) 50% 0 no-repeat;" >
        </div>
      <?php } ?>
    </ul>
</div>
</section>
</div>

<div class=" visible-xs visible-sm">
<section id="HeroMoblie">
<ul class="heroBannerSlide">
  <?php if($hero){
       foreach ($hero as $key => $h) {
         $id     = $h->id;
                    $title  = $h->title;
                    $images = $h->hero;
                    $page = $h->page;
                    $img = array(
                        'src' => 'assets/images/hero/'.$images,
                        'alt' => $title,
                        'class' => 'img-responsive'
                    );

            ?>
       <!--   <li class="heromoblie  " style="background: url(<?php echo base_url();?>assets/images/hero/<?php echo $images;?>) ;background-size: cover; background-position: center; height: 200px; " >
        </li>-->
          <li class="heromoblie ">
                <?php echo img($img);?>
        </li>
      <?php } ?>
      <?php }else{?>
       <li class="heromoblie  ">
                 <?php echo img($img);?>
        </li>
       <!-- <div class="heromoblie"  style="background: url(<?php echo base_url();?>assets/images/hero/default.png) 50% center center fixed;" >
        </div>-->
      <?php } ?>
    </ul>
</section>

</div>


<section id ="Content">
<div class="container">
  <div class="inner">
        <div class="col-xs-12 col-sm-12 col-md-6"> 
            <div id="tvcBox">
                    <h2 class="colorGreen">tvc</h2>
                     <?php
                      if ($tvc) {
                        foreach ($tvc as $key => $t) {
                            $id = $t->id;
                            $title = $t->title;
                            $images = $t->image;
                            $url = $t->url;
                        } // end foreach
                        } else {
                            $id = null;
                            $title = null;
                            $images = 'http://fakeimg.pl/477x312/';
                            $url = "#";
                        }
                        $img = array(
                            'src' => $images,
                            'alt' => $title,
                            'class' => 'img-responsive'
                        );
                        $path = $url;
                        echo anchor($path, img($img),array('onClick' => "pushEventStat('View_TVC')",'src' => $url,'target' => '_blank') );
                ?>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6"> 
            <div id="promotionBox">
                <h2 class="colorOrange">Promotion</h2>
                    <?php
                  if ($promotion) {
                        foreach ($promotion as $key => $p) {
                            $id = $p->id;
                            $title = $p->title;
                            $images = $p->image;
                            $url = $p->url;
                        } // end foreach
                    } else {
                        $id = null;
                        $title = null;
                        $images = 'http://fakeimg.pl/477x312/';
                        $url = "#";
                    }
                    $img = array(
                        'src' => 'assets/images/promotion/'.$images,
                        'alt' => $title,
                         'class' => 'img-responsive'
                    );
                    $path = $url;
                    echo anchor($path, img($img),array('onClick' => "pushEventStat('View_Promotion')"));
                ?>
            </div> 
        </div>
      </div>
       <div class="inner">
        <div class="col-md-offset-1 col-xs-12 col-md-5"> 
            <div id="enjoyLife">
            <?php if($contents){
                foreach ($contents as $key => $c) {
                        $id = $c->id;
                        $content = $c->content;
                        $images = $c->image;
                    } // end foreach
            ?>
            <?php   if($images !== ""){
                        $image = array(
                            'src' =>'assets/images/content/'.$images,
                            'alt' => 'image',
                            'class' => 'img-responsive'
                        );
                        echo img($image);
                    }else{
                        $image = array(
                            'src' =>'http://greenspot.co.th/images/main/enjoy1.png',
                            'alt' => 'image',
                            'class' => 'img-responsive'
                         );
                        echo img($image);
                    }?>

         
            </div>
        </div>
        <div class="col-md-offset-1 col-xs-12 col-md-5"> 
            <div id="enjoyLifeRandom">
                 <?php   if($content !== ""){ ?>
                   <p><?php echo $content;?></p>
                    <?php
                      echo anchor('greenspot','ดูรายละเอียด',
                        array('onClick' => "pushEventStat('View_EnjoyLife')",
                              'class' => 'viewDetail')
                        );
                    ?>
                   <?php } }?>
            </div>
        </div>

</div>

</section>

<div class="headingMain">
    <span><h1 class="colorOrange">SOCIAL NETWORK</h1></span>
</div>

<!-- social network -->
<section id="socialNetwork">

<div class="container">
    <div class="inner">
    <div class="col-xs-12 col-sm-12 col-md-12"> 
        <ul class="socialNetworkSlide">
             <?php if($social){
            foreach ($social as $key => $s) {
                    $id     = $s->id;
                    $title  = $s->title;
                    $details = $s->details;
                    $images = $s->image;
                    $url    = $s->url;
                $img = array(
                    'src' => 'assets/images/social/'.$images,
                    'alt' => $title,
                
                );
            ?>
            <li>
                <a target="_blank" href='<?php echo $url;?>'>
                    <span class="crop">
                        <?php echo img($img);?>
                    </span>
                    <span class="text"><?php echo $details;?> </span>
                </a>
            </li>
            <?php }  } ?>
        </ul>
    </div>
    </div>
</div>

</section>

<section id="delivery">
    <div class="inner">
    <div class="container">
    <?php
      if ($footer) {
            foreach ($footer as $key => $f) {
                $id = $f->id;
                $images = $f->image;
                $url = $f->url;
            } // end foreach

        $img = array(
            'src' => 'assets/images/banner/'.$images,
            'alt' => $title,
            'class' => 'img-responsive center-block',
          
        );
        $path = $url;
    ?>
      <div class="col-xs-12 col-md-12">
    <h2>
        <a onClick="pushEventStat('Follow_FB');" href="<?php echo $url;?>" target="_blank"></a>
    </h2>
    </div>
    <div class="container">
      <div class="col-xs-12 col-md-12">
    <?php 
     echo anchor($path, img($img),
            array('onClick' => "pushEventStat('Delivery')",
                  'target' => '_blank')
        );
    ?>
    </div>
   </div>
      <?php } else{ ?>

    <div class="col-xs-12">
    <h2>
        <a onClick="pushEventStat('Follow_FB');" href="#" target="_blank"></a>
    </h2>
    <?php 
        echo anchor($path, img('http://fakeimg.pl/511x116/'),
        array('onClick' => "pushEventStat('Delivery')",
        'target' => '_blank')
        );
    ?>
    </div>
    <?php }?>
    </div>
</div>
</section>

