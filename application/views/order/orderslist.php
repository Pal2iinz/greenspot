     <!--hero banner-->
<div class=" visible-md visible-lg">
<section id="HeroDesktop">
  <ul class="heroBannerSlide">
  <?php if($hero){ ?>
     <?php       foreach ($hero as $key => $h) {
                    $id     = $h->id;
                    $title  = $h->title;
                    $images = $h->hero;
                    $page = $h->page;
                    $img = array(
                        'src' => 'assets/images/hero/'.$images,
                        'alt' => $title,
                    );

            ?>
      <?php if($page === '4') { ?>
        <div class="herodesktop  " style="background: url(<?php echo base_url();?>assets/images/hero/<?php echo $images;?>) 50% 0 no-repeat;" >
        </div>

      <?php }  } }?>

     <?php  if($hero == ''){?>
      <div class="herodesktop"  style="background: url(<?php echo base_url();?>assets/images/hero/default.png) 50% 0 no-repeat;" >
        </div>
    <?php }  ?>
    </ul>

</section>
</div>

     <!--products-->
        <section id='products'>
        <div class="container">
            <h2 class='colorGreen'>รายการที่ส่งซื้อ</h2>
          <div class="container">
            <table id="cart" class="table table-hover table-condensed">
                    <tbody>
                        <tr>
                            <td data-th="Product">
                                <div class="row">
                                    <div class="col-sm-2 hidden-xs"><img src="http://placehold.it/100x100" alt="..." class="img-responsive"/></div>
                                    <div class="col-sm-10">
                                        <h4 class="nomargin">นมถั่วเหลืองไวตามิ้ลค์ UHT 110ml</h4>
                                        <p class='colorGreen'>การจัดส่งพร้อมส่ง</p>
                                    </div>
                                </div>
                            </td>
                            <td data-th="Price">จำนวน</td>
                            <td data-th="Quantity">
                                <input type="number" class="form-control text-center" value="1">
                            </td>
                            <td data-th="Subtotal" class="text-center">ลัง</td>
                            <td class="actions" data-th="">
                             <?php echo anchor('#','ลบออก');?>       
                            </td>
                        </tr>
                    </tbody>
                    <tfoot>
                       <tr>
                            <td  colspan="3"  class="text-right">ยอดรวม</td>
                            <td class="text-center"><strong> 1,900</strong></td>
                            <td>บาท</td>
                        </tr>
                        <tr>
                            <td  colspan="3" class="text-right">บริการจัดส่งฟรี</td>
                            <td class="text-center"><strong> 0</strong></td>
                            <td>บาท</td>
                        </tr>
                        <hr>
                           <tr>
                            <td  colspan="3" class="text-right">รวม</td>
                            <td class="text-center"><strong> 0</strong></td>
                            <td>บาท</td>
                        </tr>
                        <tr>
                        <td colspan="3">
                         
                        </td>
                        <td align="center">
                        <a href="#">
                           <img src="<?php echo base_url();?>/assets/carts/Pay.png" style="width: 100px;">
                           </a>
                        </td>
                        <td></td>
                        </tr>
                    </tfoot>
                </table>
</div>
            </div>
        </section>
        