<section id='products'>
<div id="productcontent" class="container">
<div class="col-xs-12 col-md-12">
    <h2 class='colorGreen'>Products</h2>
       <div id="productSlide" class="owl-carousel owl-theme productSlide">
           <?php    
           if($product){
                   foreach ($product as $key => $p) {
                      $id         = $p->id;
                      $title      = $p->title;
                      $formula    = $p->formula;
                      $benefit    = $p->benefit;
                      $thumb      = $p->product_thumb;
                      $image        = $p->product_img;
                      $nutrition  = $p->nutrition;
                      $product_thumbnail = array(
                          'src' => 'assets/images/product/'.$thumb,
                          'alt' => $thumb,
                          'class' =>'product_thumbnail img-responsive playgif',
                          'style' => 'width:auto'

                      );
                      $img = array(
                          'src' => 'assets/images/product/'.$image,
                          'alt' => $image,
                          'class' =>'product img-responsive playgif'
                      );
                      $img_nutrition = array(
                          'src' => 'assets/images/product/'.$nutrition,
                          'alt' => $nutrition,
                          'class' =>'nutrition img-responsive playgif'
                      );

            ?>
              <div class="item">
              <?php 
              echo anchor('', 
                    img($product_thumbnail),
                    array("data-id" => $id,'class' =>'url_vsoy')); 
               ?>  
              </div>
          <?php 
          }
            } // end foreach
          ?>
        </div>
        </div>
        </div>
        </section>
    
  
    
<!--product content-->
<section id='boxContent'>
  <div >
    
  </div>
</section>
       
<section id='section1'>
  <div class="container">
    <div class="col-xs-12">
      <div class='inner'>
        <?php 
          if($contents){
                        foreach($contents as $i => $c) {
              $detail[$i] = $c['content'];
              $image= $c['image'];
              $img[$i] = array(
                'src' => 'assets/images/content/'.$image,
                'alt' => $image,
                'class'=>'bg-content visible-md visible-lg playgif'
              );
                        } 
        ?>
                 <div class="text-content visible-md visible-lg">          
                     <?php echo $detail[0]; ?>
                </div>
          <div class="visible-xs visible-sm">          
                     <?php echo $detail[0]; ?>
                </div>
                     <?php   echo img($img[0]);?>
                <?php 
          }   
        ?>
            </div>
    </div>
  </div>
</section>


<section id='enjoyLife'>
  <div class="container">
        <div class="col-xs-12">
            <div class='inner'>
        <?php 
        if($contents){
          foreach($contents as $i => $c) {
            $detail[$i] = $c['content'];
            $image= $c['image'];
            $img[$i] = array(
              'src' => 'assets/images/content/'.$image,
              'alt' => $image,
              'style'=> 'margin: -65px 0 0 100px;',
              'class' => 'img-responsive playgif'
            );
          }
          ?>
          <div class="col-xs-12 col-sm-6 visible-md visible-lg" style="height: 20px;">
            <?php  echo img($img[1]); ?>
          </div>
          <div class="row">
          <div class="col-xs-12 col-sm-6 visible-md visible-lg"  style="position: relative;right: 140px;padding: 20px;">
            <?php  echo $detail[1]; ?>
          </div>
          </div>
          <div class="col-xs-12 col-sm-6 visible-xs visible-sm">
            <?php  echo $detail[1]; ?>
          </div>
        <?php 
        }  // end if    
        ?>
            </div>
        </div>
  </div>
</section>

<section id='miniPack'>
  <?php 
    foreach($contents as $i => $c) {
      $detail[$i] = $c['content'];
            $image= $c['image'];
            $img[$i] = array(
        'src' => 'assets/images/content/'.$image,
                'alt' => $image,
                'class' => 'img-responsive  pull-right playgif'
            );
        } // end foeaxh
    ?>
    <div class="col-xs-12 col-sm-12">
    <?php echo img($img[2]); ?>
    </div>
  <?php echo $detail[2]; ?> 
</section>


         <!--healthy-->
<section id="healthy">
<div class="container">
 <div class="col-xs-12">
    <div class="inner">
        
        <h2 class="headingMain">
            <span>เมนูเพื่อสุขภาพ</span>
        </h2>
         <div id="healthySlide" class="owl-carousel owl-theme">
         <?php 
            if($healthy){
                foreach ($healthy as $key => $h) {
                    $id = $h->id;
                    $title =$h->title;
                    $image =$h->image;
                    $thumbnail =$h->thumbnail;
                    $category =$h->categories;
                    $subcat =$h->subcat;
                     $thumbnail = array(
                        'src' => 'assets/images/blog/'.$thumbnail,
                        'alt' => $title,
                         'class' => 'img-responsive playgif',

                    );
            ?>
          <div class="item"> 
          <div class="title-box">
            <a href="healthy-menu/form/<?php echo $id;?>">
                    <?php echo img($thumbnail);?>
                    <span class=text><?php echo $title;?></span>
                 </a>
          </div>
          </div>
          <?php }  } ?>
        </div>
        </div>
</div>
</div>
</section>
