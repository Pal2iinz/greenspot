<section id="healthy" class="clearfix">
<div class="container">
	<div class="col-sm-12">
		<h2 class='headingMain'>
		  <span>สาระสุขภาพ</span>
		</h2>
	<div class="row">
		<div id='filters' class='button-group'>
			<button class='all' data-filter=*>show all</button> <span class=pipe>|</span>
				<?php 
				if($categories){
				foreach ($categories as $key => $c) {
						$id = $c->id;
						$cate = $c->categories;
			
				?>
					<button class='<?php 
						switch ($id) {
									case $id:
										echo $cate;
										break;
									default:
										echo 'all';
										break;
								} 
						?>' 
						data-filter='.<?php 
						switch ($id) {
									case $id:
										echo $cate;
										break;
									default:
										echo 'all';
										break;
								} 
						?>'>
						<?php 
						switch ($id) {
									case $id:
										echo $cate;
										break;
									default:
										echo 'all';
										break;
								} 
						?>
					</button> 
					<span class='pipe'>|</span>
					<?php } } ?>

				</div>
			</div>
			<div class="row">
				<div class='wrapper'>
				<?php if($healthy){
					foreach ($healthy as $key => $h) {
						$id = $h->id;
						$title =$h->title;
						$image =$h->image;
						$thumbnail =$h->thumbnail;
						$category =$h->categories;
						$subcat =$h->subcat;
						if($thumbnail !== ''){
						 $thumbnail = array(
	                        'src' => 'assets/images/blog/'.$thumbnail,
	                        'alt' => $title
                    	);
						}else{
						$thumbnail = array(
							'src' => 'http://fakeimg.pl/310x190/',
	                        'alt' => $title
	                     );
						}
						
					?>
					<a href="<?php echo base_url();?>healthy/form/<?php echo $id;?>" 
					class="box <?php 
						switch ($subcat) {
								case '1':
									echo "vitamilk";
									break;
								case '2':
									echo 'vsoy';
									break;
								case '3':
									echo 'greenspot';
									break;
								default:
									echo 'all';
									break;
							} 
						?>"  
					>
						<?php echo img($thumbnail);?>
						<span class="name"><?php echo $title;?></span>
						<span class="wrapShare">
						<span>
						<span class="fb" data-title="<?php echo $title;?>" data-share="<?php echo $id;?>"></span>
						<span class="tw" data-title="<?php echo $title;?>" data-share="<?php echo $id;?>"></span>
						<span class="gp" data-title="<?php echo $title;?>" data-share="<?php echo $id;?>"></span>
						</span>
						</span>
					</a>
				<?php 
					}  
				  }
				?>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 text-center">
	          	<?php
	                echo $this->pagination->create_links();
	            ?>
	            </div>
			</div>
		</div>
	</div>
</div>
</section>

	