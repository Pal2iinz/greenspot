<div class=" visible-md visible-lg">
<section id="HeroDesktop">
  <ul class="heroBannerSlide">
  <?php if($hero){
            foreach ($hero as $key => $h) {
                    $id     = $h->id;
                    $title  = $h->title;
                    $images = $h->hero;
                    $page = $h->page;
                    $img = array(
                        'src' => 'assets/images/hero/'.$images,
                        'alt' => $title,
                    );
            ?>
      <?php if($page === '1') { ?>
        <li class="herodesktop  " style="background: url(<?php echo base_url();?>assets/images/hero/<?php echo $images;?>) 50% 0 no-repeat;" >
        </li>
        
      <?php }  }  } ?>
      </div>
    </ul>
</div>
</section>
</div>

<div class=" visible-xs visible-sm">
<div class="col-xs-4">
<section id="HeroMoblie">
<ul class="heroBannerSlide">
  <?php if($hero){
            foreach ($hero as $key => $h) {
                    $id     = $h->id;
                    $title  = $h->title;
                    $images = $h->hero;
                    $page = $h->page;
                    $img = array(
                        'src' => 'assets/images/hero/'.$images,
                        'alt' => $title,
                    );
            ?>
      <?php if($page === '1') { ?>
        <li class="heromobile" style="background: url(<?php echo base_url();?>assets/images/hero/<?php echo $images;?>) 50% center center fixed;" >
        </li>
      <?php }  }  } ?>
    
    </ul>
</section>
</div>
</div>

<section id ="Content">
  <div class="inner">
    <div class="form-group">
        <div class="col-md-offset-1 col-xs-12 col-md-5">
            <div id="tvcBox">
                <h2 class="colorGreen">tvc</h2>
                  <?php
                  if ($tvc) {
                    foreach ($tvc as $key => $t) {
                        $id = $t->id;
                        $title = $t->title;
                        $images = $t->image;
                        $url = $t->url;
                    } // end foreach
                    } else {
                        $id = null;
                        $title = null;
                        $images = 'http://fakeimg.pl/477x312/';
                        $url = "#";
                    }
                    $img = array(
                        'src' => $images,
                        'alt' => $title
                    );
                    $path = $url;
                    echo anchor($path, img($img),array('onClick' => "pushEventStat('View_TVC')",'src' => $url,'target' => '_blank'));
                ?>
            </div>
        </div>
         <div class="col-xs-12 col-md-6">
            <div id="promotionBox">
                <h2 class="colorOrange">Promotion</h2>
                  <?php
                  if ($promotion) {
                        foreach ($promotion as $key => $p) {
                            $id = $p->id;
                            $title = $p->title;
                            $images = $p->image;
                            $url = $p->url;
                        } // end foreach
                    } else {
                        $id = null;
                        $title = null;
                        $images = 'http://fakeimg.pl/477x312/';
                        $url = "#";
                    }
                    $img = array(
                        'src' => 'assets/images/promotion/'.$images,
                        'alt' => $title
                    );
                    $path = $url;
                    echo anchor($path, img($img),array('onClick' => "pushEventStat('View_Promotion')"));
                ?>
            </div>
        </div>
    </div>
     <div class="form-group">
          <div class="col-md-offset-1 col-xs-12 col-md-4">
                           <?php if($contents){
            foreach ($contents as $key => $c) {
                    $id = $c->id;
                    $content = $c->content;
                    $images = $c->image;
                } // end foreach
            ?>
              <div id="enjoyLife">
                <img src="assets/images/content/<?php echo $images;?>" class="img-responsive">

              </div>
          </div>
          <div class="col-xs-12 col-md-6">
           <div class="enjoyLifeRandom  visible-md visible-lg ">
               <h3><?php echo $content;?></h3>
               <?php
                  echo anchor('greenspot','ดูรายละเอียด',
                    array('onClick' => "pushEventStat('View_EnjoyLife')",
                          'class' => 'viewDetail')
                    );
               ?>
        
 
                </div>
            <?php }?>
              <div class="enjoyLifeRandom  visible-xs visible-sm ">

                        <h3>น้ำส้มกรีนสปอตเป็นเครื่องดื่มให้ความสดชื่น ที่มีคุณภาพ ทำจากน้ำส้มแท้ ไม่อัดลม เหมาะสำหรับดับกระหาย ให้ความสดชื่น</h3>
                        <p>สำหรับผลิตภัณฑ์ของกรีนสปอต เมื่อนำมาผสมให้เป็น Cocktail หรือ Mocktail นั้นจะให้รสชาติที่อร่อย แปลกใหม่ ไม่จำเจ</p>

                        <a onclick="pushEventStat('View_EnjoyLife');" class="viewDetail" href="greenspot.html">ดูรายละเอียด</a>
 
                </div>
        </div>
    </div>

</div>
</section>


<!-- social network -->
<section id="socialNetwork">
        <div class="container">

               <div class="col-xs-12">
    <div class="inner">
        
        <h2 class="headingMain">
            <span>Social Network</span>
        </h2>

        <ul class="socialNetworkSlide">
        <?php if($social){
            foreach ($social as $key => $s) {
                    $id     = $s->id;
                    $title  = $s->title;
                    $details = $s->details;
                    $images = $s->image;
                    $url    = $s->url;
                $img = array(
                    'src' => 'assets/images/social/'.$images,
                    'alt' => $title,
                    'class' => 'img-responsive'
                );
            ?>
            <li>
                <a target="_blank" href='<?php echo $url;?>'>
                    <span class="crop">
                    <?php echo img($img);?>
                    </span>
                    <span class="text"><?php echo $details;?> </span>
                 <!--   <span class="date">' + i.updated_time.substr(0, 10) + "</span>-->
                </a>
            </li>
            <?php }  } ?>
        </ul>
        </div>
        </div>
    </div>
</section>

<section id="delivery">
  <div class="container">
    <?php
      if ($footer) {
            foreach ($footer as $key => $f) {
                $id = $f->id;
                $images = $f->image;
                $url = $f->url;
            } // end foreach

        $img = array(
            'src' => 'assets/images/banner/'.$images,
            'alt' => $title,
            'class' => 'img-responsive',
          
        );
        $path = $url;
    ?>
    <div class="form-group">
     <div class="col-md-offset-1 col-xs-12 col-md-10">
    <h2>
        <a onClick="pushEventStat('Follow_FB');" href="<?php echo $url;?>" target="_blank"></a>
    </h2>
   <div class="col-md-offset-3 col-xs-12 col-md-6">
    <?php 
     echo anchor($path, img($img),
            array('onClick' => "pushEventStat('Delivery')",
                  'target' => '_blank')
        );
    ?>
    </div>
    </div>
    </div>
      <?php } else{ ?>

<div class="col-xs-12">
       <h2>
        <a onClick="pushEventStat('Follow_FB');" href="#" target="_blank"></a>
    </h2>

        <?php 
         echo anchor($path, img('http://fakeimg.pl/511x116/'),
                array('onClick' => "pushEventStat('Delivery')",
                      'target' => '_blank')
            );
        ?>
            </div>

    <?php }?>
</div>
</section>
