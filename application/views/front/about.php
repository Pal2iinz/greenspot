     <!--hero banner-->
<div class=" visible-md visible-lg">
<section id="HeroDesktop">

  <?php if($hero){
            foreach ($hero as $key => $h) {
                    $id     = $h->id;
                    $title  = $h->title;
                    $images = $h->hero;
                    $page = $h->page;
                    $img = array(
                        'src' => 'assets/images/hero/'.$images,
                        'alt' => $title,
                    );
            ?>
      <?php if($page === '2') { ?>
        <div class="herodesktop  " style="background: url(<?php echo base_url();?>assets/images/hero/<?php echo $images;?>)  50% 0 no-repeat;" >
        </div>
        
      <?php }  }  } ?>
      </div>

</div>
</section>
</div>

<div class=" visible-xs visible-sm">
<div class="col-xs-4">
<section id="HeroMoblie">

  <?php if($hero){
            foreach ($hero as $key => $h) {
                    $id     = $h->id;
                    $title  = $h->title;
                    $images = $h->hero;
                    $page = $h->page;
                    $img = array(
                        'src' => 'assets/images/hero/'.$images,
                        'alt' => $title,
                    );
            ?>
      <?php if($page === '2') { ?>
        <div class="heromobile" style="background: url(<?php echo base_url();?>assets/images/hero/<?php echo $images;?>) 50% center center fixed;" >
        </div>
      <?php }  }  } ?>

</section>
</div>
</div>
        </section>
<!--history-->
<section id='history'>

<?php 
foreach($contents as $i => $c) {
        $detail[$i] = $c['content'];
    }
      echo $detail[0];
?>
</section>

<!--vision-->
<section id='vmp'>
    <div class='inner'>
        <?php 
            foreach($contents as $i => $c) {
                    $detail[$i] = $c['content'];
                     $images = $c['image'];
                }
                  echo $detail[1];
            ?>
    </div>
</section>

<!--reward-->
<section id=reward>
    <?php 
            foreach($contents as $i => $c) {
                    $detail[$i] = $c['content'];
                     $images = $c['image'];
                }
                  echo $detail[2];
            ?>
    </section>
