	<?php if($healthy){
	//	var_dump($healthy);
		foreach ($healthy as $key => $h) {
			$id = $h->id;
			$title =$h->title;
			$image =$h->image;
			$category =$h->categories;
			$subcat =$h->subcat;
				$detail =$h->post;
			 $image = array(
                'src' => 'assets/images/blog/'.$image,
                'alt' => $title,
                'style' =>"position: relative;width: 100%;"
        	);
			}
		}
?>
	     <!--hero banner-->
<div class=" visible-md visible-lg">
<section id="HeroDesktop">

  <?php if($hero){
            foreach ($hero as $key => $h) {
                    $id     = $h->id;
                    $title  = $h->title;
                    $images = $h->hero;
                    $page = $h->page;
                    $img = array(
                        'src' => 'assets/images/hero/'.$images,
                        'alt' => $title,
                    );
            ?>
      <?php if($page === '7') { ?>
        <div class="herodesktop  " style="background: url(<?php echo base_url();?>assets/images/hero/<?php echo $images;?>) 75% 0 no-repeat;" >
        </div>
        
      <?php }  }  } ?>
      </div>

</div>
</section>
</div>

<div class=" visible-xs visible-sm">
<div class="col-xs-4">
<section id="HeroMoblie">

  <?php if($hero){
            foreach ($hero as $key => $h) {
                    $id     = $h->id;
                    $title  = $h->title;
                    $images = $h->hero;
                    $page = $h->page;
                    $img = array(
                        'src' => 'assets/images/hero/'.$images,
                        'alt' => $title,
                    );
            ?>
      <?php if($page === '7') { ?>
        <div class="heromobile" style="background: url(<?php echo base_url();?>assets/images/hero/<?php echo $images;?>) 75% center center fixed;" >
        </div>
      <?php }  }  } ?>

</section>
</div>
</div>
		</section>
		<section id='healthy' class=clearfix>
			<div class=inner>
				<h2 class=headingMain><span>สาระสุขภาพ</span></h2>
				<div id=filters class='button-group form-group'>
				<?php switch ($subcat) {
						case '1':
							$menu = 'vitamilk';
							break;
						case '2':
							$menu = 'vsoy';
							break;
						case '3':
							$menu = 'greenspot';
							break;
						
						default:
							# code...
							break;
					} 
				?>
				<button class='subcate'>สาระสุขภาพ</button> <span class=pipe>|</span>
				<button class='subcate'><?php echo $menu;?> </button>
				</div>
				<div class="form-group">
					<div class="col-md-12">
					<?php echo img($image);?>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
					<p><?php echo $detail;?></p>
					</div>
				</div>

			</div>	
		</section>
	