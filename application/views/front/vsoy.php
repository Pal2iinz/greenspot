    <!--hero banner-->
<div class=" visible-md visible-lg">
<section id="HeroDesktop">
  <ul class="heroBannerSlide">
  <?php if($hero){
            foreach ($hero as $key => $h) {
                    $id     = $h->id;
                    $title  = $h->title;
                    $images = $h->hero;
                    $page = $h->page;
                    $img = array(
                        'src' => 'assets/images/hero/'.$images,
                        'alt' => $title,
                    );
            ?>
      <?php if($page === '4') { ?>
        <div class="herodesktop  " style="background: url(<?php echo base_url();?>assets/images/hero/<?php echo $images;?>) 50% 0 no-repeat;" >
        </div>
        
      <?php }  }  } ?>
      </div>
    </ul>
</div>
</section>
</div>

<div class=" visible-xs visible-sm">
<div class="col-xs-4">
<section id="HeroMoblie">
<ul class="heroBannerSlide">
  <?php if($hero){
            foreach ($hero as $key => $h) {
                    $id     = $h->id;
                    $title  = $h->title;
                    $images = $h->hero;
                    $page = $h->page;
                    $img = array(
                        'src' => 'assets/images/hero/'.$images,
                        'alt' => $title,
                    );
            ?>
      <?php if($page === '4') { ?>
        <div class="heromobile" style="background: url(<?php echo base_url();?>assets/images/hero/<?php echo $images;?>) 50% center center fixed;" >
        </div>
      <?php }  }  } ?>
    
    </ul>
</section>
</div>
</div>


         <!--products-->
        <section id='products'>
        <div class="container">
            <h2 class='colorGreen'>Products</h2>
            <ul class='productSlide'>
            <?php 
                if($product){
                    foreach ($product as $key => $p) {
                      $id         = $p->id;
                      $title      = $p->title;
                      $formula    = $p->formula;
                      $benefit    = $p->benefit;
                      $thumb      = $p->product_thumb;
                      $image        = $p->product_img;
                      $nutrition  = $p->nutrition;
                $product_thumbnail = array(
                    'src' => 'assets/images/product/'.$thumb,
                    'alt' => $thumb,
                    'class' =>'product_thumbnail'
                );
                $img = array(
                    'src' => 'assets/images/product/'.$image,
                    'alt' => $image,
                    'class' =>'product'
                );
                $img_nutrition = array(
                    'src' => 'assets/images/product/'.$nutrition,
                    'alt' => $nutrition,
                    'class' =>'nutrition'
                );
            ?>
            <li>
                <?php echo anchor('', img($product_thumbnail),array('onClick' => "pushEventStat('Info');","data-id" => $id,'class' =>'url_vsoy')); ?>
            </li>
            <?php } } ?>
            </ul>
            </div>
        </section>
        <!--product content-->
        <section id=boxContent>
        <div></div>
        </section>
        <section id=section1>
            <div class=inner>
                  <?php 
                foreach($contents as $i => $c) {
                        $detail[$i] = $c['content'];
                    }
                      echo $detail[0];
            ?>
            </div>
        </section>
        <section id=enjoyLife>
            <div class=inner>
                <ul class=enjoyLifeSlide>
                  <li>
                        <?php 
                        foreach($contents as $i => $c) {
                                $detail[$i] = $c['content'];
                                $image= $c['image'];
                            $img[$i] = array(
                                'src' => 'assets/images/content/'.$image,
                                'alt' => $image,
                            );
                        }
                          echo img($img[1]);
                          echo $detail[1];
                    ?>
                        </li>
                   
                </ul>
            </div>
        </section>
       
                <section id='miniPack'>
                    <?php 
                        foreach($contents as $i => $c) {
                                $detail[$i] = $c['content'];
                                $image= $c['image'];
                            $img[$i] = array(
                                'src' => 'assets/images/content/'.$image,
                                'alt' => $image,
                            );

                        }
            ?>
                <?php echo img($img[2]); ?>
            <?php echo $detail[2]; ?>
     
    </section>
         <!--healthy-->
        <section id='healthy'>
            <h2 class='headingMain'><span>เมนูเพื่อสุขภาพ</span></h2>
            <ul class='healthySlide'>
            <?php 
            if($healthy){
                foreach ($healthy as $key => $h) {
                    $id = $h->id;
                    $title =$h->title;
                    $image =$h->image;
                    $thumbnail =$h->thumbnail;
                    $category =$h->categories;
                    $subcat =$h->subcat;
                     $thumbnail = array(
                        'src' => 'assets/images/blog/'.$thumbnail,
                        'alt' => $title
                    );
            ?>
            <li>
                <a href="healthy-menu/<?php echo $id;?>">
                    <?php echo img($thumbnail);?>
                    <span class=text><?php echo $title;?></span>
                 </a>
            </li>
            <?php    
               }
            }
            ?>
            </ul>
        </section>