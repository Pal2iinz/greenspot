  	     <!--hero banner-->
<div class=" visible-md visible-lg">
<section id="HeroDesktop">

  <?php if($hero){
            foreach ($hero as $key => $h) {
                    $id     = $h->id;
                    $title  = $h->title;
                    $images = $h->hero;
                    $page = $h->page;
                    $img = array(
                        'src' => 'assets/images/hero/'.$images,
                        'alt' => $title,
                    );
            ?>
      <?php if($page === '7') { ?>
        <div class="herodesktop  " style="background: url(<?php echo base_url();?>assets/images/hero/<?php echo $images;?>) 75% 0 no-repeat;" >
        </div>
        
      <?php }  }  } ?>
      </div>

</div>
</section>
</div>

<div class=" visible-xs visible-sm">
<div class="col-xs-4">
<section id="HeroMoblie">

  <?php if($hero){
            foreach ($hero as $key => $h) {
                    $id     = $h->id;
                    $title  = $h->title;
                    $images = $h->hero;
                    $page = $h->page;
                    $img = array(
                        'src' => 'assets/images/hero/'.$images,
                        'alt' => $title,
                    );
            ?>
      <?php if($page === '7') { ?>
        <div class="heromobile" style="background: url(<?php echo base_url();?>assets/images/hero/<?php echo $images;?>) 75% center center fixed;" >
        </div>
      <?php }  }  } ?>

</section>
</div>
</div>
		<section id=healthy class=clearfix>
			<div class=inner>
				<h2 class=headingMain><span>เมนูเพื่อสุขภาพ</span></h2>
				<div id=filters class=button-group>
				<button class='all' data-filter=*>show all</button> <span class=pipe>|</span>
				<?php 
				if($categories){
				foreach ($categories as $key => $c) {
						$id = $c->id;
						$cate = $c->categories;
			
				?>
					<button class='<?php 
						switch ($id) {
									case $id:
										echo $cate;
										break;
									default:
										echo 'all';
										break;
								} 
						?>' 
						data-filter='.<?php 
						switch ($id) {
									case $id:
										echo $cate;
										break;
									default:
										echo 'all';
										break;
								} 
						?>'>
						<?php 
						switch ($id) {
									case $id:
										echo $cate;
										break;
									default:
										echo 'all';
										break;
								} 
						?>
					</button> 
					<span class=pipe>|</span>
					<?php } } ?>

				</div>
				<div class=wrapper>
				<?php if($healthy){
					foreach ($healthy as $key => $h) {
						$id = $h->id;
						$title =$h->title;
						$image =$h->image;
						$thumbnail =$h->thumbnail;
						$category =$h->categories;
						$subcat =$h->subcat;
						 $thumbnail = array(
	                        'src' => 'assets/images/blog/'.$thumbnail,
	                        'alt' => $title
                    	);
						
					?>
					<a href="healthy-menu/<?php echo $id;?>" 
					class="box <?php 
						switch ($subcat) {
								case '1':
									echo "vitamilk";
									break;
								case '2':
									echo 'vsoy';
									break;
								case '3':
									echo 'greenspot';
									break;
								default:
									echo 'all';
									break;
							} 
						?>"  
					>
						<?php echo img($thumbnail);?>
						<span class=name><?php echo $title;?></span>
					</a>
				<?php 
					}  
				  }
				?>
				</div>
				<div class=paging>
					<a class=prev onclick="paging('healthy-page1', 1);"></a> <span class=page>1</span> / 2
					<a class=next onclick="paging('healthy-page2', 2);"></a>
				</div>
			</div>
		</section>
	