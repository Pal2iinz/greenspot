<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <title></title>
  <!-- SEO -->
  <meta name="description" content="ผู้ผลิตน้ำนมถั่วเหลืองไวตามิ้ลค์ วีซอย และเครื่องดื่มกรีนสปอตที่มีความมุ่งมั่นที่จะผลิตเครื่องดื่มเพื่อสุขภาพและได้มาตรฐานระดับสากล เพื่อสุขภาพที่ดีของคนไทย นมถั่วเหลือง อาหารเพื่อสุขภาพ">
  <meta name="keywords" content="นมถั่วเหลือง, อาหารเพื่อสุขภาพ">
  <!-- End SEO -->
  <!-- OGP -->
  <meta property="og:title" content="Greenspot Co.,Ltd บริษัท กรีนสปอต จำกัด">
  <meta property="og:image" content="http://www.greenspot.co.th/images/share/h2.jpg">
  <meta property="og:description" content="ผู้ผลิตน้ำนมถั่วเหลืองไวตามิ้ลค์ วีซอย และเครื่องดื่มกรีนสปอตที่มีความมุ่งมั่นที่จะผลิตเครื่องดื่มเพื่อสุขภาพและได้มาตรฐานระดับสากล เพื่อสุขภาพที่ดีของคนไทย นมถั่วเหลือง อาหารเพื่อสุขภาพ">
  <link href="favicon.ico" rel="shortcut icon">
  <!--    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>-->
  <!-- End OGP -->
  <?php
            if (isset($style)) {
                foreach ($style as $key => $css) {
                    echo css_asset($css);
                }
            }
        ?>
</head>

<body class='main '>
  <div id="container">
    <!-- Desktop -->
    <header class=" visible-md visible-lg">
      <div class="container">
        <div class="col-md-2"> <img src="http://greenspot.co.th/images/global/logo.png" class="img-responsive logo" /> </div>
        <div class="container-fluid">
          <div class=" visible-md visible-lg">
            <div class="col-sm-10 col-md-10">
              <!-- Collect the nav links, forms, and other content for toggling -->
              <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                  <li class="active">
                  <a href="./">
                  <img src="<?php echo base_url();?>/assets/images/nav/home.png">
                      หน้าหลัก
                  </a>
                   </li>
                  <li>
                    <a href="about">
                  <img src="<?php echo base_url();?>/assets/images/nav/about.png">
                      เกี่ยวกับเรา
                  </a>
                  </li>
                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <img src="<?php echo base_url();?>/assets/images/nav/product.png"> ผลิตภัณฑ์</a>
                    <ul class="dropdown-menu">
                      <li>
                        <?php
                                        echo anchor(
                                            'vitamilk',
                                            'ไวตามิ้ลค์'
                                        );
                                    ?>
                      </li>
                      <li>
                        <?php
                                        echo anchor(
                                            'vsoy',
                                            'วีซอย'
                                        );
                                    ?>
                      </li>
                      <li>
                        <?php
                                        echo anchor(
                                            'greenspot',
                                            'กรีนสปอต'
                                        );
                                    ?>
                      </li>
                    </ul>
                  </li>
                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <img src="<?php echo base_url();?>/assets/images/nav/menu.png"> มุมสุขภาพ</a>
                    <ul class="dropdown-menu">
                      <li>
                        <?php
                                            echo anchor(
                                                'healthy',
                                                'สาระสุขภาพ'
                                            );
                                        ?>
                      </li>
                      <li>
                        <?php
                                            echo anchor(
                                                'healthy-menu',
                                                'เมนูเพื่อสุขภาพ'
                                            );
                                        ?>
                      </li>
                    </ul>
                  </li>
                  <li>
                    <a href="http://www.greenspot.co.th/interbus/">
                  <img src="<?php echo base_url();?>/assets/images/nav/world.png">
                      INTERNATIONAL BUSINESS
                  </a>
</li>
                </ul>
              </div>
              <!-- /.navbar-collapse -->
            </div>
          </div>
        </div>
      </div>
      <!-- Desktop -->
    </header>
    <!-- Moblie -->
    <nav class="navbar navbar-default navbar-static-top visible-xs visible-sm">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button> <img src="http://greenspot.co.th/images/global/logo.png" class="img-responsive logo-responesive" /> </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="active">
              <?php echo anchor('./','หน้าหลัก' );?>
            </li>
            <li>
              <?php echo anchor('about',' เกี่ยวกับเรา');?>
            </li>
            <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">ผลิตภัณฑ์<span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li>
                  <?php
                                        echo anchor(
                                            'vitamilk',
                                            'ไวตามิ้ลค์'
                                        );
                                    ?>
                </li>
                <li>
                  <?php
                                        echo anchor(
                                            'vsoy',
                                            'วีซอย'
                                        );
                                    ?>
                </li>
                <li>
                  <?php
                                        echo anchor(
                                            'greenspot',
                                            'กรีนสปอต'
                                        );
                                    ?>
                </li>
              </ul>
            </li>
            <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">มุมสุขภาพ<span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li>
                  <?php
                                            echo anchor(
                                                'healthy',
                                                'สาระสุขภาพ'
                                            );
                                        ?>
                </li>
                <li>
                  <?php
                                            echo anchor(
                                                'healthy-menu',
                                                'เมนูเพื่อสุขภาพ'
                                            );
                                        ?>
                </li>
              </ul>
            </li>
            <li>
              <?php
                                echo anchor(
                                    'http://www.greenspot.co.th/interbus/',
                                    'INTERNATIONAL BUSINESS'
                                );
                                ?> </li>
          </ul>
        </div>
        <!--/.nav-collapse -->
      </div>
    </nav>
    <!-- Moblie -->