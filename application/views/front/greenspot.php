     <!--hero banner-->

    <div class=" visible-md visible-lg">
<section id="HeroDesktop">

  <?php if($hero){
            foreach ($hero as $key => $h) {
                    $id     = $h->id;
                    $title  = $h->title;
                    $images = $h->hero;
                    $page = $h->page;
                    $img = array(
                        'src' => 'assets/images/hero/'.$images,
                        'alt' => $title,
                    );
            ?>
      <?php if($page === '5') { ?>
        <div class="herodesktop  " style="background: url(<?php echo base_url();?>assets/images/hero/<?php echo $images;?>) 50% 0 no-repeat;" >
        </div>
        
      <?php }  }  } ?>
      </div>

</div>
</section>
</div>

<div class=" visible-xs visible-sm">
<div class="col-xs-4">
<section id="HeroMoblie">

  <?php if($hero){
            foreach ($hero as $key => $h) {
                    $id     = $h->id;
                    $title  = $h->title;
                    $images = $h->hero;
                    $page = $h->page;
                    $img = array(
                        'src' => 'assets/images/hero/'.$images,
                        'alt' => $title,
                    );
            ?>
      <?php if($page === '5') { ?>
        <div class="heromobile" style="background: url(<?php echo base_url();?>assets/images/hero/<?php echo $images;?>) 50% center center fixed;" >
        </div>
      <?php }  }  } ?>

</section>
</div>
</div>


        <section id=section1 class=clearfix>
          <?php 
            foreach($contents as $i => $c) {
                    $detail[$i] = $c['content'];
                    $image= $c['image'];
                $img[$i] = array(
                    'src' => 'assets/images/content/'.$image,
                    'alt' => $image,
                );
            }
              echo img($img[0]);
              echo $detail[0];
        ?>
        </section>


        <section id=enjoyLife>
            <div class=inner>
                <ul class=enjoyLifeSlide>
                    <li> 
                    <?php 
            foreach($contents as $i => $c) {
                    $detail[$i] = $c['content'];
                    $image= $c['image'];
                $img[$i] = array(
                    'src' => 'assets/images/content/'.$image,
                    'alt' => $image,
                );
            }
              echo img($img[1]);
              echo $detail[1];
        ?>
                    </li>
                
                </ul>
            </div>
        </section>


        <section id=section2>
       
            <div class=inner>
            <?php 
            foreach($contents as $i => $c) {
                    $detail[$i] = $c['content'];
                    $image= $c['image'];
                $img[$i] = array(
                    'src' => 'assets/images/content/'.$image,
                    'alt' => $image,
                );
            }
                ?>
               
                 <?php  echo img($img[2]); ?>
                <?php  echo $detail[2]; ?>

        </div>

        </section>
        <section id='healthy'>
            <?php 
                    foreach($contents as $i => $c) {
                            $detail[$i] = $c['content'];
                            $image= $c['image'];
                        $img[$i] = array(
                            'src' => 'assets/images/content/'.$image,
                            'alt' => $image,
                        );
                    }
                      echo img($img[3]);
                      echo $detail[3];
                ?>
        </section>