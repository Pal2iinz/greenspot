  <?php 
    if($product){
        foreach ($product as $key => $p) {
          $id         = $p->id;
          $title      = $p->title;
          $formula    = $p->formula;
          $benefit    = $p->benefit;
          $thumb      = $p->product_thumb;
          $image        = $p->product_img;
          $nutrition  = $p->nutrition;
    $product_thumbnail = array(
        'src' => 'assets/images/product/'.$thumb,
        'alt' => $thumb,
        'class' =>'product_thumbnail playgif'
    );
    $img = array(
        'src' => 'assets/images/product/'.$image,
        'alt' => $image,
        'class' =>'product img-responsive playgif',
        'style' => 'width:200px;margin:auto'
    );
    $img_nutrition = array(
        'src' => 'assets/images/product/'.$nutrition,
        'alt' => $nutrition,
         'class' =>'nutrition img-responsive playgif',
       'style' => 'width:350px;margin:auto;',
    );
?>

<div class="container">
<a class='close'></a>

<div class="col-sm-12">

    <?php echo img($img);?>
</div>

  <div id="nutrition" class="col-sm-4 text-right">
  <?php echo img($img_nutrition);?>
  </div>
    <div class="col-sm-8">
    <div class=productDetail>  
      <h2><?php echo $title;?><br><?php echo $formula;?></h2>
      <?php echo $benefit;?>
    </div>
    </div>
</div>


<?php } } ?>

<!--<script type="text/javascript">
$( "#nutrition img" ).each(function(){
    var $this = $(this); 
    $this.wrap('<a href="' + $this.attr('src') + '" data-lightbox="' + $this.attr('src') + '" ></a>');
});
</script>-->