<section id='products'>
				 <div class="container">

					<div class="col-xs-12 col-md-12">
					<h2 class='colorRed'>Products</h2>
					<div id="productSlide" class="owl-carousel owl-theme productSlide">
           <?php 
                if($product){
                    foreach ($product as $i => $p) {
                      $id         = $p->id;
                      $title      = $p->title;
                      $formula    = $p->formula;
                      $benefit    = $p->benefit;
                      $thumb      = $p->product_thumb;
                      $image        = $p->product_img;
                      $nutrition  = $p->nutrition;
                $product_thumbnail = array(
                    'src' => 'assets/images/product/'.$thumb,
                    'alt' => $thumb,
                    'class' =>'product_thumbnail playgif'
                );
                $img = array(
                    'src' => 'assets/images/product/'.$image,
                    'alt' => $image,
                    'class' =>'product playgif'
                );
                $img_nutrition = array(
                    'src' => 'assets/images/product/'.$nutrition,
                    'alt' => $nutrition,
                    'class' =>'nutrition playgif'
                );
            ?>
        		  <div class="item">
              <?php echo anchor('', img($product_thumbnail),array("data-id" => $id,'class' =>'url_product')); ?>  
              </div>
        	<?php } } ?>
        </div>


				</div>
				</div>
			

						</section>
				<!--product content-->
				<section id='boxContent'>
						<div>
							
						</div>
				</section>

				<section id='section1'>
				<div class="container ">
					<div class='inner visible-md visible-lg'>
						<div class="section-content col-md-offset-4 col-xs-12 col-sm-12 col-md-6"> 
						<?php 
						if($contents){
							foreach($contents as $i => $c) {
								$detail[$i] = $c['content'];
							}
								echo $detail[0];
							}
						?>
						</div>
					</div>

					<div class='product-moblie visible-xs visible-sm'>
						<div id="pmoblie" class="col-md-offset-4 col-xs-12 col-sm-12 col-md-6 "> 
						<?php 
						if($contents){
							foreach($contents as $i => $c) {
								$detail[$i] = $c['content'];
							}
								echo $detail[0];
							}
						?>
						</div>
					</div>

					</div>
				</section>


				<section id='enjoyLife' class="product-moblie">
				 <div class="container">
						<div class='inner'>
						 <div class="col-xs-12 col-sm-12 col-md-12"> 
							<?php 
								if($contents){
								foreach($contents as $i => $c) {
												$detail[$i] = $c['content'];
												$image= $c['image'];
										$img[$i] = array(
												'src' => 'assets/images/content/'.$image,
												'alt' => $image,
												'style' => 'margin-top: 28px;',
												'class' => 'img-responsive playgif',
										);
								}
								?>
									<div class="col-xs-12 col-sm-5 visible-md visible-lg"> 
								 <?php  echo img($img[1]); ?>
								 </div>
									<div class="col-xs-12 col-sm-6 content-enjoy"> 
									 <?php   echo $detail[1];   ?>
									 </div>
							<?php          }
							?>
							</div>
						</div>
					</div>
				</section>

		
				<section id='equation'>
						<div class="container">  
					<div  class="col-xs-12 col-sm-12 col-md-12 doubleblack"> 
						<?php 
						if($contents){
								foreach($contents as $i => $c) {
								$detail[$i] = $c['content'];
								$image= $c['image'];
								$img[$i] = array(
								'src' => 'assets/images/content/'.$image,
								'alt' => $image,
										 'class' => 'img-responsive playgif',
								);
						}
							 echo img($img[2]);

						} ?>
					</div>
							</div>
				</section>
		

				 <!--healthy-->

<section id="healthy">
<div class="container">
 <div class="col-xs-12">
		<div class="inner">
				
				<h2 class="headingMain">
						<span>เมนูเพื่อสุขภาพ</span>
				</h2>
				 <div id="healthySlide" class="owl-carousel owl-theme">
				 <?php 
						if($healthy){
								foreach ($healthy as $key => $h) {
										$id = $h->id;
										$title =$h->title;
										$image =$h->image;
										$thumbnail =$h->thumbnail;
										$category =$h->categories;
										$subcat =$h->subcat;
										 $thumbnail = array(
												'src' => 'assets/images/blog/'.$thumbnail,
												'alt' => $title,
												 'class' => 'img-responsive playgif',

										);
						?>
					<div class="item"> 
					<div class="title-box">
						<a href="healthy/form/<?php echo $id;?>">
										<?php echo img($thumbnail);?>
										<span class=text><?php echo $title;?></span>
								 </a>
					</div>
					</div>
					<?php }  } ?>
				</div>
				</div>
</div>
</div>
</section>
