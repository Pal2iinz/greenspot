
        <section id='section1' class='clearfix'>
            <div class="container">
            <div class="row">
              <div class="col-xs-12 col-sm-12 col-md-12"> 
              <?php 
              if ($contents){
                foreach($contents as $i => $c) {
                        $detail[$i] = $c['content'];
                        $image= $c['image'];
                    $img[$i] = array(
                        'src' => 'assets/images/content/'.$image,
                        'alt' => $image,
                        'class' => 'img-responsive playgif'
                    );
                }
                ?>
                <div class="col-sm-4">
                <?php  echo img($img[0]); ?>
                  </div>
                  <div class="col-sm-8">
                <?php  echo $detail[0]; ?>
                  </div>
          <?php }  ?>
            </div>
            </div>
            </div>
        </section>


        <section id='enjoyLife'>
          <div class="container">
              <div class="col-xs-12 col-sm-12 col-md-12"> 
            <div class='inner'>
                <div class='enjoyLifeSlide'>
    
        <?php 
            foreach($contents as $i => $c) {
                    $detail[$i] = $c['content'];
                    $image= $c['image'];
                $img[$i] = array(
                    'src' => 'assets/images/content/'.$image,
                    'alt' => $image,
                    'class' => 'img-responsive visible-md visible-lg playgif'
                );
            }
              echo img($img[1]);
              echo $detail[1];
        ?>
     
                </div>
            </div>
            </div>
            </div>
        </section>


        <section id='section2'>
       <div class="container">
              <div class="col-xs-12 col-sm-12 col-md-12"> 
            <div class=inner>
            <?php 
            if ($contents) {
                
            foreach($contents as $i => $c) {
                    $detail[$i] = $c['content'];
                    $image= $c['image'];
                $img[$i] = array(
                    'src' => 'assets/images/content/'.$image,
                    'alt' => $image,
                    'class'=> 'img-responsive visible-md visible-lg playgif'
                );
            }
                ?>
                <div class="col-xs-12 col-sm-12">
                 <?php  echo img($img[2]); ?>
                 </div>
                 <div class="col-xs-12 col-sm-5 col-md-5">
                 <div class="content-section2 visible-md visible-lg">
                        <?php  echo $detail[2]; ?>
                </div>
                <div class=" visible-xs visible-sm">
                        <?php  echo $detail[2]; ?>
                </div>
                </div>
<?php } ?>
        </div>
        </div>
        </div>
        </section>

        <section id='healthy'>
         <div class="container">
              <div class="col-xs-12 col-sm-12 col-md-12"> 
            <?php 
                    foreach($contents as $i => $c) {
                            $detail[$i] = $c['content'];
                            $image= $c['image'];
                        $img[$i] = array(
                            'src' => 'assets/images/content/'.$image,
                            'alt' => $image,
                            'class'=> 'img-responsive playgif'
                        );
                    }
                      echo img($img[3]);
                      echo $detail[3];
                ?>
                </div>
                </div>
        </section>