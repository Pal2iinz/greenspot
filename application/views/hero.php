<div class="herodesktop visible-md visible-lg">
<div class="container">
    <?php if($hero){
       foreach ($hero as $key => $h) {
         $id     = $h->id;
                    $title  = $h->title;
                    $images = $h->hero;
                    $page = $h->page;
                    $img = array(
                        'src' => 'assets/images/hero/'.$images,
                        'alt' => $title,
                        'class' => 'img-responsive'
                    );
                    
            ?>
    <!-- Swiper -->
    <div class="swiper-container">
 
        <div class="swiper-wrapper">
    
           <div class="swiper-slide"><?php echo img($img);?></div>
         <!--    <div class="swiper-slide"><img src="<?php echo base_url();?>/assets/images/hero/hero.png" class="img-responsive"></div>
            <div class="swiper-slide"><img src="<?php echo base_url();?>/assets/images/hero/hero.png" class="img-responsive"></div>-->
     
        </div>
        <!-- Add Pagination -->
        <div class="swiper-pagination"></div>
       
    </div>
          <?php } }?>
</div>
</div>

<div class="heromoblie visible-xs visible-sm">
<div class="container">
    <!-- Swiper -->
    <div class="swiper-container">
     <?php if($hero){
               foreach ($hero as $key => $h) {
                 $id     = $h->id;
                            $title  = $h->title;
                            $images = $h->hero;
                            $page = $h->page;
                            $img = array(
                                'src' => 'assets/images/hero/'.$images,
                                'alt' => $title,
                                'class' => 'img-responsive'
                            );
            ?>
        <div class="swiper-wrapper">
         <!--   <div class="swiper-slide"><?php echo img($img);?></div>-->
             <div class="swiper-slide"><img src="<?php echo base_url();?>/assets/images/hero/hero.png" class="img-responsive"></div>
            <div class="swiper-slide"><img src="<?php echo base_url();?>/assets/images/hero/hero.png" class="img-responsive"></div>
        </div>
        <!-- Add Pagination -->
        <div class="swiper-pagination-v"></div>
        <?php } }?>
    </div>
</div>
</div>


