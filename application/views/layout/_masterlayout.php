<?php echo doctype('html5'); ?>
<html>
    <head>
        <?php
        $meta = array(
            array('name' => 'Content-type', 'content' => 'text/html; charset=utf-8', 'type' => 'equiv'),
            array('name' => 'robots', 'content' => 'no-cache'),
            array('name' => 'description', 'content' => 'Green Spot Web Site'),
            array('name' => 'keywords', 'content' => 'love, passion, intrigue, deception'),
            array('name' => 'robots', 'content' => 'no-cache'),
            array('name' => 'viewport', 'content' => 'width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no')
        );
        echo meta($meta);
        echo '<title>'.$sTitle.'</title>';
        $uid = $this->session->userdata['logged_in_admin']['id'];
        $firstname = $this->session->userdata['logged_in_admin']['firstname'];
        $lastname = $this->session->userdata['logged_in_admin']['lastname'];
        $email = $this->session->userdata['logged_in_admin']['email'];
        $role = $this->session->userdata['logged_in_admin']['role'];
        $avatar = $this->session->userdata['logged_in_admin']['avatar'];
        $dep = $this->session->userdata['logged_in_admin']['department'];
        $perm = $this->session->userdata['logged_in_admin']['perm'];
        ?>
        <!--CSS STYLES-->
        <?php echo
        link_tag(array('href' => 'assets/css/admin/_masterlayout.css',
            'rel' => 'stylesheet',
            'type'  => 'text/css')
        );
        ?>
        <?php echo
        link_tag(array('href' => 'assets/css/bootstrap.css',
            'rel' => 'stylesheet',
            'type'  => 'text/css')
        );
        ?>
        <?php echo
        link_tag(array('href' => 'assets/css/fonts/font-awesome.css',
            'rel' => 'stylesheet',
            'type'  => 'text/css')
        );
        ?>
        <?php
            if (isset($custom_css)) {
                foreach ($custom_css as $key => $value) {
                    echo link_tag(
                        array(
                            'href' => base_url($value),
                            'rel' => 'stylesheet',
                            'type'  => 'text/css')
                    );
                }
           }
           ?>
    </head>
    <body class="tooltips">
      <!-- Page Content -->
      <div class="wrapper">
         <!-- BEGIN TOP NAV -->
         <div class="top-navbar">
            <div class="top-navbar-inner">
               <!-- Begin Logo brand -->
               <div class="logo-brand white-color">
                  <h1 style="margin-top: 10px;">Green Spot</h1>
              </div>
               <!-- /.logo-brand -->
               <!-- End Logo brand -->
               <div class="top-nav-content">
                  <!-- Begin button sidebar left toggle -->
                  <div class="btn-collapse-sidebar-left">
                     <i class="fa fa-bars"></i>
                  </div>
                  <!-- /.btn-collapse-sidebar-left -->
                  <!-- End button sidebar left toggle -->
                  <!-- Begin button nav toggle -->
                  <div class="btn-collapse-nav" data-toggle="collapse" data-target="#main-fixed-nav">
                  <i class="fa fa-plus icon-plus"></i>
               </div>
               <!-- /.btn-collapse-sidebar-right -->
               <!-- End button nav toggle -->
               <!-- Begin user session nav -->
               <ul class="nav-user navbar-right">
                  <li class="dropdown">
                     <a href="#fakelink" class="dropdown-toggle" data-toggle="dropdown">
                        <?php
                        if (empty($avatar)) {
                            $img = array(
                                'src' => 'assets/images/avatar/avatar.jpg',
                                'class' => 'avatar img-circle',
                                'alt' => 'Avatar',
                            );
                            echo img($img);
                        } else {
                            $img = array(
                                'src' => 'assets/images/avatar/'.$avatar,
                                'class' => 'avatar img-circle',
                                'alt' => 'Avatar',
                            );
                            echo img($img);
                        } // end else
                        ?>
                     <strong>
                        <?php
                        echo 'Hi, '.$firstname.' '.$lastname;
                        ?>
                    </strong>
                     </a>
                     <ul class="dropdown-menu square danger margin-list-rounded with-triangle">
                        <li>
                            <?php
                            echo anchor(
                                'profile/avatar/',
                                'Avatar setting'
                            );
                            ?>
                        </li>
                        <li>
                            <?php
                            echo anchor(
                                'profile/change/',
                                'Change password'
                            );
                            ?>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <?php
                            echo anchor(
                                'logout',
                                'Log out'
                            );
                            ?>
                        </li>
                     </ul>
                  </li>
               </ul>
               <!-- End user session nav -->
            </div>
            <!-- /.top-nav-content -->
         </div>
         <!-- /.top-navbar-inner -->
      </div>
      <!-- /.top-navbar -->
      <!-- END TOP NAV -->
      <!-- BEGIN SIDEBAR LEFT -->
      <div class="sidebar-left success-color sidebar-nicescroller">
         <ul class="sidebar-menu">
            <li class="static left-profile-summary">
               <div class="media">
                    <p class="pull-left">
                        <?php
                        if (empty($avatar)) {
                            $img = array(
                                'src' => 'assets/images/avatar/avatar.jpg',
                                'class' => 'avatar img-circle',
                                'alt' => 'Avatar',
                            );
                            echo img($img);
                        } else {
                            $img = array(
                                'src' => 'assets/images/avatar/'.$avatar,
                                'class' => 'avatar img-circle',
                                'alt' => 'Avatar',
                            );
                            echo img($img);
                        } // end else
                        ?>
                    </p>
                  <div class="media-body">
                     <h4>Hello, <br />
                        <strong>
                        <?php
                            echo $firstname.' '.$lastname;
                        ?>
                        </strong>
                    </h4>
                  </div>
               </div>
            </li>
            <li>
               <?php
               echo anchor('admin/home','Dashboard<i class="fa fa-dashboard icon-sidebar"></i>');
               ?>
            </li>
            <?php
            if ($dep === '1') {
            ?>
                <li>
                    <?php
                    echo anchor('admin/user','User Management<i class="fa fa-user icon-sidebar"></i> <i class="fa fa-angle-right chevron-icon-sidebar"></i>');
                    ?>
                    <ul class="submenu">
                        <li>
                            <?php
                            echo anchor('admin/user','User');
                            ?>
                        </li>
                        <li>
                            <?php
                            echo anchor('admin/department','Department');
                            ?>
                        </li>
                    </ul>
                </li>
                <li>
                    <?php
                    echo anchor(
                        'backend/product/',
                        'Product Management
                        <i class="fa fa-shopping-basket icon-sidebar"></i>
                        <i class="fa fa-angle-right chevron-icon-sidebar"></i>'
                    );
                    ?>
                    <ul class="submenu">
                        <li>
                            <?php
                            echo anchor('backend/product/','Product');
                            ?>
                        </li>
                        <li>
                            <?php
                            echo anchor('backend/product/categories/','Categories');
                            ?>
                        </li>
                    </ul>
                </li>
            <?php
            } // end if
            ?>
            <li>
               <a href="#fakelink">
               <i class="fa fa-television icon-sidebar"></i>
               <i class="fa fa-angle-right chevron-icon-sidebar"></i>
               Pages Management
               </a>
               <ul class="submenu">
                    <li>
                        <?php
                        echo anchor('backend/page/homepage/','Homepage');
                        ?>
                    </li>
                    <li>
                        <?php
                        echo anchor('backend/content/','Content');
                        ?>
                    </li>
                    <li>
                        <?php
                        echo anchor('backend/hero/','Hero Banner');
                        ?>
                    </li>
                    <li>
                        <?php
                        echo anchor('backend/banner/','Footer Banner');
                        ?>
                    </li>
                    <li>
                        <?php
                        echo anchor('backend/jobs/','Jobs');
                        ?>
                    </li>
               </ul>
            </li>
            <li>
                <?php
                echo anchor(
                    'backend/blog/',
                    'Blog Management <i class="fa fa-book icon-sidebar"></i>'
                );
                ?>
            </li>
            <li>
               <a href="#fakelink">
               <i class="fa fa-calendar icon-sidebar"></i>
               <i class="fa fa-angle-right chevron-icon-sidebar"></i>
               Event Management
               </a>
               <ul class="submenu">
                  <li>
                    <?php
                      echo anchor('backend/event/vitamilk/','Vitalmilk Club');
                    ?>
                  </li>
                  <li>
                    <?php
                      echo anchor('backend/event/vsoy/','Vsoy Club');
                    ?>
                  </li>
                  <li>
                    <?php
                      echo anchor('backend/event/greenspot/','Greenspot Club');
                    ?>
                  </li>
               </ul>
            </li>

            <li>
                <?php
                echo anchor(
                    'backend/social/',
                    'Social Network <i class="fa fa-facebook-square icon-sidebar"></i>'
                );
                ?>
            </li>
            <li>
                <?php
                echo anchor(
                    'logout',
                    'Log out <i class="fa fa-sign-out icon-sidebar"></i>'
                );
                ?>
            </li>
         </ul>
      </div>
      <!-- /.sidebar-left -->
      <!-- END SIDEBAR LEFT -->
      <!-- BEGIN PAGE CONTENT -->
      <div class="page-content">
         <div class="container-fluid">
                <?php echo $content; ?>
            </div>
         </div>
         <!-- BEGIN FOOTER -->
         <footer>
            &copy; 2017 <a href="#fakelink">Teerapuch Web Studio.</a>
            <?php echo br(1);?>
            All rights reserved.
         </footer>
         <!-- END FOOTER -->
      </div>
      <!-- /.page-content -->
      </div>
      <!-- /.wrapper -->
      <!-- END PAGE CONTENT -->
      <!-- BEGIN BACK TO TOP BUTTON -->
      <div id="back-top">
         <a href="#top"><i class="fa fa-chevron-up"></i></a>
      </div>
      <!-- END BACK TO TOP -->
      <!--END PAGE-->
      <!-- MAIN JAVASRCIPT (REQUIRED ALL PAGE)-->
      <?php echo script_tag('assets/js/jquery.min.js');?>
      <?php echo script_tag('assets/js/bootstrap.min.js');?>
      <?php //echo script_tag('assets/js/framework/bootstrap/transition.js');?>
      <?php //echo script_tag('assets/js/framework/bootstrap/dropdown.js');?>
      <?php //echo script_tag('assets/js/framework/bootstrap/collapse.js');?>
      <?php echo script_tag('assets/plugins/retina/retina.min.js');?>
      <?php echo script_tag('assets/plugins/nicescroll/jquery.nicescroll.js');?>
      <?php echo script_tag('assets/plugins/slimscroll/jquery.slimscroll.min.js');?>
      <?php echo script_tag('assets/plugins/backstretch/jquery.backstretch.min.js');?>
      <!-- PLUGINS -->
      <?php
         if (isset($custom_js)) {
             foreach ($custom_js as $key => $value) {
                 echo '<script src='.base_url($value).' ></script>';
             }
         }

         ?>
      <!-- MAIN APPS JS -->
      <?php echo script_tag('assets/js/apps.js');?>
   </body>
</html>
