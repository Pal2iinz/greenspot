<?php echo doctype('html5'); ?>
<html>
    <head>
        <?php
        $meta = array(
            array('name' => 'Content-type', 'content' => 'text/html; charset=utf-8', 'type' => 'equiv'),
            array('name' => 'robots', 'content' => 'no-cache'),
            array('name' => 'description', 'content' => 'My Great Site'),
            array('name' => 'keywords', 'content' => 'love, passion, intrigue, deception'),
            array('name' => 'robots', 'content' => 'no-cache'),
            array('name' => 'viewport', 'content' => 'width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no')
        );
        echo meta($meta);
        echo '<title>'.$sTitle.'</title>';
        $firstname = $this->session->userdata['logged_in_admin']['firstname'];
        $lastname = $this->session->userdata['logged_in_admin']['lastname'];
        $email = $this->session->userdata['logged_in_admin']['email'];
        $avatar = $this->session->userdata['logged_in_admin']['avatar'];
        $role = $this->session->userdata['logged_in_admin']['role'];
        $dep = $this->session->userdata['logged_in_admin']['department'];
        $perm = $this->session->userdata['logged_in_admin']['perm'];
        ?>
        <!--CSS STYLES-->
        <?php echo
        link_tag(array('href' => 'assets/css/admin/_masterlayout.css',
            'rel' => 'stylesheet',
            'type'  => 'text/css')
        );
        ?>
        <?php echo
        link_tag(array('href' => 'assets/css/fonts/font-awesome.css',
            'rel' => 'stylesheet',
            'type'  => 'text/css')
        );
        ?>
        <?php
            if (isset($custom_css)) {
                foreach ($custom_css as $key => $value) {
                    echo link_tag(
                        array(
                            'href' => base_url($value),
                            'rel' => 'stylesheet',
                            'type'  => 'text/css')
                    );
                }
           }
           ?>
    </head>
    <body class="tooltips">
      <!-- Page Content -->
      <div class="wrapper">
         <!-- BEGIN TOP NAV -->
         <div class="top-navbar">
            <div class="top-navbar-inner">
               <!-- Begin Logo brand -->
               <div class="logo-brand white-color">
                  <?php
                     $img = array(
                       'src' => 'assets/images/layouts/true-logo.jpg',
                       'alt' => 'Sentir logo',
                     );
                     echo img($img);
                     ?>
               </div>
               <!-- /.logo-brand -->
               <!-- End Logo brand -->
               <div class="top-nav-content">
                  <!-- Begin button sidebar left toggle -->
                  <div class="btn-collapse-sidebar-left">
                     <i class="fa fa-bars"></i>
                  </div>
                  <!-- /.btn-collapse-sidebar-left -->
                  <!-- End button sidebar left toggle -->
                  <!-- Begin button nav toggle -->
                  <div class="btn-collapse-nav" data-toggle="collapse" data-target="#main-fixed-nav">
                  <i class="fa fa-plus icon-plus"></i>
               </div>
               <!-- /.btn-collapse-sidebar-right -->
               <!-- End button nav toggle -->
               <!-- Begin user session nav -->
               <ul class="nav-user navbar-right">
                  <li class="dropdown">
                     <a href="#fakelink" class="dropdown-toggle" data-toggle="dropdown">
                     <?php
                        $img = array(
                           'src' => 'assets/images/avatar/avatar.jpg',
                           'class' => 'avatar img-circle',
                           'alt' => 'Avatar',
                        );
                        echo img($img);
                        ?>
                     <strong>
                        <?php
                        echo 'Hi, '.$firstname.' '.$lastname;
                        ?>
                    </strong>
                     </a>
                     <ul class="dropdown-menu square danger margin-list-rounded with-triangle">
                        <li><a href="#fakelink">Account setting</a></li>
                        <li><a href="#fakelink">Avatar setting</a></li>
                        <li>
                            <?php
                            echo anchor(
                                'admin/user/change/',
                                'Change password'
                            );
                            ?>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <?php
                            echo anchor(
                                'logout',
                                'Log out'
                            );
                            ?>
                        </li>
                     </ul>
                  </li>
               </ul>
               <!-- End user session nav -->
               <!-- Begin Collapse menu nav -->
               <div class="collapse navbar-collapse" id="main-fixed-nav">
                  <!-- Begin nav search form -->
                  <form class="navbar-form navbar-left" role="search">
                     <div class="form-group">
                        <input type="text" class="form-control" placeholder="Search">
                     </div>
                  </form>
                  <!-- End nav search form -->
                  <ul class="nav navbar-nav navbar-left">
                     <!-- Begin nav notification -->
                     <li class="dropdown">
                        <a href="#fakelink" class="dropdown-toggle" data-toggle="dropdown">
                        <span class="badge badge-danger icon-count">7</span>
                        <i class="fa fa-bell"></i>
                        </a>
                        <ul class="dropdown-menu square with-triangle">
                           <li>
                              <div class="nav-dropdown-heading">
                                 Notifications
                              </div>
                              <!-- /.nav-dropdown-heading -->
                              <div class="nav-dropdown-content scroll-nav-dropdown">
                                 <ul>
                                    <li class="unread"><a href="#fakelink">
                                       <?php
                                          $img = array(
                                             'src' => 'assets/images/avatar/avatar.jpg',
                                             'class' => 'absolute-left-content img-circle',
                                             'alt' => 'Avatar',
                                          );
                                          echo img($img);
                                          ?>
                                       <strong>Thomas White</strong> posted on your profile page
                                       <span class="small-caps">17 seconds ago</span>
                                       </a>
                                    </li>
                                    <li class="unread"><a href="#fakelink">
                                       <?php
                                          $img = array(
                                             'src' => 'assets/images/avatar/avatar.jpg',
                                             'class' => 'absolute-left-content img-circle',
                                             'alt' => 'Avatar',
                                          );
                                          echo img($img);
                                          ?>
                                       <strong>Doina Slaivici</strong> uploaded photo
                                       <span class="small-caps">10 minutes ago</span>
                                       </a>
                                    </li>
                                    <li><a href="#fakelink">
                                       <?php
                                          $img = array(
                                             'src' => 'assets/images/avatar/avatar.jpg',
                                             'class' => 'absolute-left-content img-circle',
                                             'alt' => 'Avatar',
                                          );
                                          echo img($img);
                                          ?>
                                       <strong>Harry Nichols</strong> commented on your post
                                       <span class="small-caps">40 minutes ago</span>
                                       </a>
                                    </li>
                                    <li class="unread"><a href="#fakelink">
                                       <?php
                                          $img = array(
                                             'src' => 'assets/images/avatar/avatar.jpg',
                                             'class' => 'absolute-left-content img-circle',
                                             'alt' => 'Avatar',
                                          );
                                          echo img($img);
                                          ?>
                                       <strong>Mihaela Cihac</strong> send you a message
                                       <span class="small-caps">2 hours ago</span>
                                       </a>
                                    </li>
                                    <li class="unread"><a href="#fakelink">
                                       <?php
                                          $img = array(
                                             'src' => 'assets/images/avatar/avatar.jpg',
                                             'class' => 'absolute-left-content img-circle',
                                             'alt' => 'Avatar',
                                          );
                                          echo img($img);
                                          ?>
                                       <strong>Harold Chavez</strong> change his avatar
                                       <span class="small-caps">Yesterday</span>
                                       </a>
                                    </li>
                                    <li class="unread"><a href="#fakelink">
                                       <?php
                                          $img = array(
                                             'src' => 'assets/images/avatar/avatar.jpg',
                                             'class' => 'absolute-left-content img-circle',
                                             'alt' => 'Avatar',
                                          );
                                          echo img($img);
                                          ?>
                                       <strong>Elizabeth Owens</strong> posted on your profile page
                                       <span class="small-caps">Yesterday</span>
                                       </a>
                                    </li>
                                    <li class="unread"><a href="#fakelink">
                                       <?php
                                          $img = array(
                                             'src' => 'assets/images/avatar/avatar.jpg',
                                             'class' => 'absolute-left-content img-circle',
                                             'alt' => 'Avatar',
                                          );
                                          echo img($img);
                                          ?>
                                       <strong>Frank Oliver</strong> commented on your post
                                       <span class="small-caps">A week ago</span>
                                       </a>
                                    </li>
                                    <li><a href="#fakelink">
                                       <?php
                                          $img = array(
                                             'src' => 'assets/images/avatar/avatar.jpg',
                                             'class' => 'absolute-left-content img-circle',
                                             'alt' => 'Avatar',
                                          );
                                          echo img($img);
                                          ?>
                                       <strong>Mya Weastell</strong> send you a message
                                       <span class="small-caps">April 15, 2014</span>
                                       </a>
                                    </li>
                                    <li><a href="#fakelink">
                                       <?php
                                          $img = array(
                                             'src' => 'assets/images/avatar/avatar.jpg',
                                             'class' => 'absolute-left-content img-circle',
                                             'alt' => 'Avatar',
                                          );
                                          echo img($img);
                                          ?>
                                       <strong>Carl Rodriguez</strong> joined your weekend party
                                       <span class="small-caps">April 01, 2014</span>
                                       </a>
                                    </li>
                                 </ul>
                              </div>
                              <!-- /.nav-dropdown-content scroll-nav-dropdown -->
                              <button class="btn btn-primary btn-square btn-block">See all notifications</button>
                           </li>
                        </ul>
                     </li>
                     <!-- End nav notification -->
                     <!-- Begin nav task -->
                     <li class="dropdown">
                        <a href="#fakelink" class="dropdown-toggle" data-toggle="dropdown">
                        <span class="badge badge-warning icon-count">3</span>
                        <i class="fa fa-tasks"></i>
                        </a>
                        <ul class="dropdown-menu square margin-list-rounded with-triangle">
                           <li>
                              <div class="nav-dropdown-heading">
                                 Tasks
                              </div>
                              <!-- /.nav-dropdown-heading -->
                              <div class="nav-dropdown-content scroll-nav-dropdown">
                                 <ul>
                                    <li class="unread"><a href="#fakelink">
                                       <i class="fa fa-check-circle-o absolute-left-content icon-task completed"></i>
                                       Creating documentation
                                       <span class="small-caps">Completed : Yesterday</span>
                                       </a>
                                    </li>
                                    <li><a href="#fakelink">
                                       <i class="fa fa-clock-o absolute-left-content icon-task progress"></i>
                                       Eating sands
                                       <span class="small-caps">Deadline : Tomorrow</span>
                                       </a>
                                    </li>
                                    <li><a href="#fakelink">
                                       <i class="fa fa-clock-o absolute-left-content icon-task progress"></i>
                                       Sending payment
                                       <span class="small-caps">Deadline : Next week</span>
                                       </a>
                                    </li>
                                    <li><a href="#fakelink">
                                       <i class="fa fa-exclamation-circle absolute-left-content icon-task uncompleted"></i>
                                       Uploading new version
                                       <span class="small-caps">Deadline: 2 seconds ago</span>
                                       </a>
                                    </li>
                                    <li><a href="#fakelink">
                                       <i class="fa fa-exclamation-circle absolute-left-content icon-task uncompleted"></i>
                                       Drinking coffee
                                       <span class="small-caps">Deadline : 2 hours ago</span>
                                       </a>
                                    </li>
                                    <li class="unread"><a href="#fakelink">
                                       <i class="fa fa-check-circle-o absolute-left-content icon-task completed"></i>
                                       Walking to nowhere
                                       <span class="small-caps">Completed : over a year ago</span>
                                       </a>
                                    </li>
                                    <li class="unread"><a href="#fakelink">
                                       <i class="fa fa-check-circle-o absolute-left-content icon-task completed"></i>
                                       Sleeping under bridge
                                       <span class="small-caps">Completed : Dec 31, 2013</span>
                                       </a>
                                    </li>
                                    <li class="unread"><a href="#fakelink">
                                       <i class="fa fa-check-circle-o absolute-left-content icon-task completed"></i>
                                       Buying some cigarettes
                                       <span class="small-caps">Completed : 2 days ago</span>
                                       </a>
                                    </li>
                                 </ul>
                              </div>
                              <!-- /.nav-dropdown-content scroll-nav-dropdown -->
                              <button class="btn btn-primary btn-square btn-block">See all task</button>
                           </li>
                        </ul>
                     </li>
                     <!-- End nav task -->
                     <!-- Begin nav message -->
                     <li class="dropdown">
                        <a href="#fakelink" class="dropdown-toggle" data-toggle="dropdown">
                        <span class="badge badge-success icon-count">9</span>
                        <i class="fa fa-envelope"></i>
                        </a>
                        <ul class="dropdown-menu square margin-list-rounded with-triangle">
                           <li>
                              <div class="nav-dropdown-heading">
                                 Messages
                              </div>
                              <!-- /.nav-dropdown-heading -->
                              <div class="nav-dropdown-content scroll-nav-dropdown">
                                 <ul>
                                    <li class="unread"><a href="#fakelink">
                                       <?php
                                          $img = array(
                                             'src' => 'assets/images/avatar/avatar.jpg',
                                             'class' => 'absolute-left-content img-circle',
                                             'alt' => 'Avatar',
                                          );
                                          echo img($img);
                                          ?>
                                       Lorem ipsum dolor sit amet, consectetuer adipiscing elit
                                       <span class="small-caps">17 seconds ago</span>
                                       </a>
                                    </li>
                                    <li class="unread"><a href="#fakelink">
                                       <?php
                                          $img = array(
                                             'src' => 'assets/images/avatar/avatar.jpg',
                                             'class' => 'absolute-left-content img-circle',
                                             'alt' => 'Avatar',
                                          );
                                          echo img($img);
                                          ?>
                                       Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat
                                       <span class="small-caps">10 minutes ago</span>
                                       </a>
                                    </li>
                                    <li><a href="#fakelink">
                                       <?php
                                          $img = array(
                                             'src' => 'assets/images/avatar/avatar.jpg',
                                             'class' => 'absolute-left-content img-circle',
                                             'alt' => 'Avatar',
                                          );
                                          echo img($img);
                                          ?>
                                       I think so
                                       <span class="small-caps">40 minutes ago</span>
                                       </a>
                                    </li>
                                    <li class="unread"><a href="#fakelink">
                                       <?php
                                          $img = array(
                                             'src' => 'assets/images/avatar/avatar.jpg',
                                             'class' => 'absolute-left-content img-circle',
                                             'alt' => 'Avatar',
                                          );
                                          echo img($img);
                                          ?>
                                       Yes, I'll be waiting
                                       <span class="small-caps">2 hours ago</span>
                                       </a>
                                    </li>
                                    <li class="unread"><a href="#fakelink">
                                       <?php
                                          $img = array(
                                             'src' => 'assets/images/avatar/avatar.jpg',
                                             'class' => 'absolute-left-content img-circle',
                                             'alt' => 'Avatar',
                                          );
                                          echo img($img);
                                          ?>
                                       Thank you!
                                       <span class="small-caps">Yesterday</span>
                                       </a>
                                    </li>
                                    <li class="unread"><a href="#fakelink">
                                       <?php
                                          $img = array(
                                             'src' => 'assets/images/avatar/avatar.jpg',
                                             'class' => 'absolute-left-content img-circle',
                                             'alt' => 'Avatar',
                                          );
                                          echo img($img);
                                          ?>
                                       No problem! I will never remember that
                                       <span class="small-caps">Yesterday</span>
                                       </a>
                                    </li>
                                    <li class="unread"><a href="#fakelink">
                                       <?php
                                          $img = array(
                                             'src' => 'assets/images/avatar/avatar.jpg',
                                             'class' => 'absolute-left-content img-circle',
                                             'alt' => 'Avatar',
                                          );
                                          echo img($img);
                                          ?>
                                       Tak gepuk ndasmu sisan lho dab!
                                       <span class="small-caps">A week ago</span>
                                       </a>
                                    </li>
                                    <li><a href="#fakelink">
                                       <?php
                                          $img = array(
                                             'src' => 'assets/images/avatar/avatar.jpg',
                                             'class' => 'absolute-left-content img-circle',
                                             'alt' => 'Avatar',
                                          );
                                          echo img($img);
                                          ?>
                                       Sorry bro, aku or atau sing jenenge ngono kui
                                       <span class="small-caps">April 15, 2014</span>
                                       </a>
                                    </li>
                                    <li><a href="#fakelink">
                                       <?php
                                          $img = array(
                                             'src' => 'assets/images/avatar/avatar.jpg',
                                             'class' => 'absolute-left-content img-circle',
                                             'alt' => 'Avatar',
                                          );
                                          echo img($img);
                                          ?>
                                       Will you send me an invitation for your weeding party?
                                       <span class="small-caps">April 01, 2014</span>
                                       </a>
                                    </li>
                                 </ul>
                              </div>
                              <!-- /.nav-dropdown-content scroll-nav-dropdown -->
                              <button class="btn btn-primary btn-square btn-block">See all message</button>
                           </li>
                        </ul>
                     </li>
                     <!-- End nav message -->
                     <!-- Begin nav friend requuest -->
                     <li class="dropdown">
                        <a href="#fakelink" class="dropdown-toggle" data-toggle="dropdown">
                        <span class="badge badge-info icon-count">2</span>
                        <i class="fa fa-users"></i>
                        </a>
                        <ul class="dropdown-menu square margin-list-rounded with-triangle">
                           <li>
                              <div class="nav-dropdown-heading">
                                 Friend requests
                              </div>
                              <!-- /.nav-dropdown-heading -->
                              <div class="nav-dropdown-content static-list scroll-nav-dropdown">
                                 <ul>
                                    <li>
                                       <?php
                                          $img = array(
                                             'src' => 'assets/images/avatar/avatar.jpg',
                                             'class' => 'absolute-left-content img-circle',
                                             'alt' => 'Avatar',
                                          );
                                          echo img($img);
                                          ?>
                                       <div class="row">
                                          <div class="col-xs-6">
                                             <strong>Craig Dixon</strong>
                                             <span class="small-caps">2 murtual friends</span>
                                          </div>
                                          <div class="col-xs-6 text-right btn-action">
                                             <button class="btn btn-success btn-xs">Accept</button><button class="btn btn-danger btn-xs">Ignore</button>
                                          </div>
                                          <!-- /.col-xs-5 text-right btn-cation -->
                                       </div>
                                       <!-- /.row -->
                                    </li>
                                    <li>
                                       <?php
                                          $img = array(
                                             'src' => 'assets/images/avatar/avatar.jpg',
                                             'class' => 'absolute-left-content img-circle',
                                             'alt' => 'Avatar',
                                          );
                                          echo img($img);
                                          ?>
                                       <div class="row">
                                          <div class="col-xs-6">
                                             <strong>Mikayla King</strong>
                                             <span class="small-caps">20 murtual friends</span>
                                          </div>
                                          <div class="col-xs-6 text-right btn-action">
                                             <button class="btn btn-success btn-xs">Accept</button><button class="btn btn-danger btn-xs">Ignore</button>
                                          </div>
                                          <!-- /.col-xs-5 text-right btn-cation -->
                                       </div>
                                       <!-- /.row -->
                                    </li>
                                    <li>
                                       <?php
                                          $img = array(
                                             'src' => 'assets/images/avatar/avatar.jpg',
                                             'class' => 'absolute-left-content img-circle',
                                             'alt' => 'Avatar',
                                          );
                                          echo img($img);
                                          ?>
                                       <div class="row">
                                          <div class="col-xs-6">
                                             <strong>Richard Dixon</strong>
                                             <span class="small-caps">1 murtual friend</span>
                                          </div>
                                          <div class="col-xs-6 text-right btn-action">
                                             <button class="btn btn-success btn-xs">Accept</button><button class="btn btn-danger btn-xs">Ignore</button>
                                          </div>
                                          <!-- /.col-xs-5 text-right btn-cation -->
                                       </div>
                                       <!-- /.row -->
                                    </li>
                                    <li>
                                       <?php
                                          $img = array(
                                             'src' => 'assets/images/avatar/avatar.jpg',
                                             'class' => 'absolute-left-content img-circle',
                                             'alt' => 'Avatar',
                                          );
                                          echo img($img);
                                          ?>
                                       <div class="row">
                                          <div class="col-xs-6">
                                             <strong>Brenda Fuller</strong>
                                             <span class="small-caps">8 murtual friends</span>
                                          </div>
                                          <div class="col-xs-6 text-right btn-action">
                                             <button class="btn btn-success btn-xs">Accept</button><button class="btn btn-danger btn-xs">Ignore</button>
                                          </div>
                                          <!-- /.col-xs-5 text-right btn-cation -->
                                       </div>
                                       <!-- /.row -->
                                    </li>
                                    <li>
                                       <?php
                                          $img = array(
                                             'src' => 'assets/images/avatar/avatar.jpg',
                                             'class' => 'absolute-left-content img-circle',
                                             'alt' => 'Avatar',
                                          );
                                          echo img($img);
                                          ?>
                                       <div class="row">
                                          <div class="col-xs-6">
                                             <strong>Ryan Ortega</strong>
                                             <span class="small-caps">122 murtual friends</span>
                                          </div>
                                          <div class="col-xs-6 text-right btn-action">
                                             <button class="btn btn-success btn-xs">Accept</button><button class="btn btn-danger btn-xs">Ignore</button>
                                          </div>
                                          <!-- /.col-xs-5 text-right btn-cation -->
                                       </div>
                                       <!-- /.row -->
                                    </li>
                                    <li>
                                       <?php
                                          $img = array(
                                             'src' => 'assets/images/avatar/avatar.jpg',
                                             'class' => 'absolute-left-content img-circle',
                                             'alt' => 'Avatar',
                                          );
                                          echo img($img);
                                          ?>
                                       <?php
                                          $img = array(
                                             'src' => 'assets/images/avatar/avatar.jpg',
                                             'class' => 'absolute-left-content img-circle',
                                             'alt' => 'Avatar',
                                          );
                                          echo img($img);
                                          ?>
                                       <div class="row">
                                          <div class="col-xs-6">
                                             <strong>Jessica Gutierrez</strong>
                                             <span class="small-caps">45 murtual friends</span>
                                          </div>
                                          <div class="col-xs-6 text-right btn-action">
                                             <button class="btn btn-success btn-xs">Accept</button><button class="btn btn-danger btn-xs">Ignore</button>
                                          </div>
                                          <!-- /.col-xs-5 text-right btn-cation -->
                                       </div>
                                       <!-- /.row -->
                                    </li>
                                 </ul>
                              </div>
                              <!-- /.nav-dropdown-content scroll-nav-dropdown -->
                              <button class="btn btn-primary btn-square btn-block">See all request</button>
                           </li>
                        </ul>
                     </li>
                     <!-- End nav friend requuest -->
                  </ul>
               </div>
               <!-- /.navbar-collapse -->
               <!-- End Collapse menu nav -->
            </div>
            <!-- /.top-nav-content -->
         </div>
         <!-- /.top-navbar-inner -->
      </div>
      <!-- /.top-navbar -->
      <!-- END TOP NAV -->
      <!-- BEGIN SIDEBAR LEFT -->
      <div class="sidebar-left danger-color sidebar-nicescroller">
         <ul class="sidebar-menu">
            <li class="static left-profile-summary">
               <div class="media">
                  <p class="pull-left">
                     <?php
                        $img = array(
                           'src' => 'assets/images/avatar/avatar.jpg',
                           'class' => 'avatar img-circle media-object',
                           'alt' => 'Avatar',
                        );
                        echo img($img);
                        ?>
                  </p>
                  <div class="media-body">
                     <h4>Hello, <br />
                        <strong>
                        <?php
                            echo $firstname.' '.$lastname;
                        ?>
                        </strong>
                    </h4>
                  </div>
               </div>
            </li>
            <li>
               <?php
               echo anchor('admin/home','Dashboard<i class="fa fa-dashboard icon-sidebar"></i>');
               ?>
            </li>
            <li>
                <?php
                echo anchor('admin/user','User Management<i class="fa fa-user icon-sidebar"></i> <i class="fa fa-angle-right chevron-icon-sidebar"></i>');
                ?>
                <ul class="submenu">
                    <li>
                        <?php
                        echo anchor('admin/user','User');
                        ?>
                    </li>
                    <li>
                        <?php
                        echo anchor('admin/role','Role');
                        ?>
                    </li>
                    <li>
                        <?php
                        echo anchor('admin/permission','Permission');
                        ?>
                    </li>
                    <li>
                        <?php
                        echo anchor('admin/department','Department');
                        ?>
                    </li>
                </ul>
            </li>
            <li>
               <a href="front-end.html">
               <i class="fa fa-bomb icon-sidebar"></i>
               Web templates
               </a>
            </li>
            <li>
               <a href="#fakelink">
               <i class="fa fa-desktop icon-sidebar"></i>
               <i class="fa fa-angle-right chevron-icon-sidebar"></i>
               Layout template
               </a>
               <ul class="submenu">
                  <li><a href="#fakelink">Default layout<span class="label label-success span-sidebar">CURRENT</span></a></li>
                  <li><a href="layout-no-sidebar-right.html">No sidebar right</a></li>
                  <li><a href="layout-profile-left.html">Profile summary left</a></li>
                  <li><a href="layout-no-sidebar-left.html">No sidebar left</a></li>
                  <li><a href="layout-shrink-navbar.html">Shrink navbar</a></li>
                  <li><a href="layout-top-navigation.html">Top navigation</a></li>
                  <li><a href="layout-tour.html">Tour layout</a></li>
                  <li><a href="layout-hidden-sidebar-left.html">Hidden sidebar left<span class="label label-danger span-sidebar">NEW</span></a></li>
                  <li><a href="layout-top-notification.html">Top notification<span class="label label-danger span-sidebar">NEW</span></a></li>
               </ul>
            </li>
            <li>
               <a href="#fakelink">
               <i class="fa fa-flask icon-sidebar"></i>
               <i class="fa fa-angle-right chevron-icon-sidebar"></i>
               Widget UI kits
               </a>
               <ul class="submenu">
                  <li><a href="widget-default.html">Default<span class="label label-success span-sidebar">UPDATED</span></a></li>
                  <li><a href="widget-store.html">Store</a></li>
                  <li><a href="widget-real-estate.html">Real estate <span class="label label-warning span-sidebar">HOT</span></a></li>
                  <li><a href="widget-blog.html">Blog</a></li>
                  <li><a href="widget-social.html">Social <span class="label label-warning span-sidebar">HOT</span></a></li>
               </ul>
            </li>
            <li>
               <a href="#fakelink">
               <i class="fa fa-folder icon-sidebar"></i>
               <i class="fa fa-angle-right chevron-icon-sidebar"></i>
               Basic elements
               </a>
               <ul class="submenu">
                  <li><a href="element-typography.html">Typography</a></li>
                  <li><a href="element-form.html">Form element</a></li>
                  <li><a href="element-form-example.html">Form example</a></li>
                  <li><a href="element-wyswyg.html">Form WYSWYG</a></li>
                  <li><a href="element-validation.html">Form validation</a></li>
                  <li><a href="element-button.html">Button</a></li>
               </ul>
            </li>
            <li>
               <a href="#fakelink">
               <i class="fa fa-folder-open icon-sidebar"></i>
               <i class="fa fa-angle-right chevron-icon-sidebar"></i>
               More elements
               </a>
               <ul class="submenu">
                  <li><a href="element-icon.html">Icons <span class="badge badge-info span-sidebar">3</span></a></li>
                  <li><a href="element-box-panel.html">Box &amp; panel</a></li>
                  <li><a href="element-nav-dropdown.html">Nav &amp; dropdown</a></li>
                  <li><a href="element-breadcrumb-pagination.html">Breadcrumb &amp; pagination</a></li>
                  <li><a href="element-thumbnail-jumbotron.html">Jumbotron &amp; thumbnail</a></li>
                  <li><a href="element-alert-progress-bar.html">Alert &amp; progress</a></li>
                  <li><a href="element-list-media.html">List group &amp; media object</a></li>
                  <li><a href="element-collapse.html">Collapse</a></li>
                  <li><a href="element-grid-masonry.html">Grid &amp; masonry</a></li>
                  <li><a href="element-masonry-js.html">Masonry JS</a></li>
                  <li><a href="element-toastr.html">Toastr notifications</a></li>
                  <li><a href="element-carousel.html">Carousel</a></li>
                  <li><a href="element-calendar.html">Calendar</a></li>
                  <li><a href="element-extra.html">Extra elements</a></li>
               </ul>
            </li>
            <li>
               <a href="#fakelink">
               <i class="fa fa-table icon-sidebar"></i>
               <i class="fa fa-angle-right chevron-icon-sidebar"></i>
               Tables
               </a>
               <ul class="submenu">
                  <li><a href="table-static.html">Static table</a></li>
                  <li><a href="table-color.html">Table color</a></li>
                  <li><a href="table-datatable.html">Jquery datatable</a></li>
               </ul>
            </li>
            <li>
               <a href="#fakelink">
               <i class="fa fa-bar-chart-o icon-sidebar"></i>
               <i class="fa fa-angle-right chevron-icon-sidebar"></i>
               Chart or Graph
               </a>
               <ul class="submenu">
                  <li><a href="chart-morris.html">Morris chart</a></li>
                  <li><a href="chart-c3.html">C3 chart</a></li>
                  <li><a href="chart-flot.html">Flot chart</a></li>
                  <li><a href="chart-easy-knob.html">Easy pie chart &amp; knob</a></li>
               </ul>
            </li>
         </ul>
      </div>
      <!-- /.sidebar-left -->
      <!-- END SIDEBAR LEFT -->
      <!-- BEGIN PAGE CONTENT -->
      <div class="page-content">
         <div class="container-fluid">
                <?php var_dump($this->session->userdata['logged_in_admin']); ?>
               <?php echo $content; ?>
            </div>
         </div>
         <!-- BEGIN FOOTER -->
         <footer>
            &copy; 2017 <a href="#fakelink">Digiday Co.,Ltd.</a>
            <?php echo br(1);?>
            All rights reserved.
         </footer>
         <!-- END FOOTER -->
      </div>
      <!-- /.page-content -->
      </div>
      <!-- /.wrapper -->
      <!-- END PAGE CONTENT -->
      <!-- BEGIN BACK TO TOP BUTTON -->
      <div id="back-top">
         <a href="#top"><i class="fa fa-chevron-up"></i></a>
      </div>
      <!-- END BACK TO TOP -->
      <!--END PAGE-->
      <!-- MAIN JAVASRCIPT (REQUIRED ALL PAGE)-->
      <?php echo script_tag('assets/js/jquery.min.js');?>
      <?php echo script_tag('assets/js/framework/bootstrap/transition.js');?>
      <?php echo script_tag('assets/js/framework/bootstrap/dropdown.js');?>
      <?php echo script_tag('assets/js/framework/bootstrap/collapse.js');?>
      <?php echo script_tag('assets/plugins/retina/retina.min.js');?>
      <?php echo script_tag('assets/plugins/nicescroll/jquery.nicescroll.js');?>
      <?php echo script_tag('assets/plugins/slimscroll/jquery.slimscroll.min.js');?>
      <?php echo script_tag('assets/plugins/backstretch/jquery.backstretch.min.js');?>
      <!-- PLUGINS -->
      <?php
         if (isset($custom_js)) {
             foreach ($custom_js as $key => $value) {
                 echo '<script src='.base_url($value).' ></script>';
             }
         }

         ?>
      <!-- MAIN APPS JS -->
      <?php echo script_tag('assets/js/apps.js');?>
   </body>
</html>
