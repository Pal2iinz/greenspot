	<?php if($career){
		foreach ($career as $key => $ca) {
			$image =$ca['image'];
			$detail = $ca['content'];
			$filedata = $ca['fileupload'];
			$applyjob = $ca['applyjob'];
			$jobthai = $ca['jobthai'];
			$jobdb = $ca['jobdb'];
			$jobtopgun = $ca['jobtopgun'];
			if($image !== ''){
			 $img[$i] = array(
                'src' => 'assets/images/content/'.$images,
                'alt' => $image,
                'style' =>"position: relative;width: 100%;"
        	);
		}else{
			  $img[$i]  = array(
                'src' => 'http://fakeimg.pl/900x340/',
                'alt' => $images,
                'style' =>"position: relative;width: 100%;"
        	);
		}
	
?>
		<section id='career' class=clearfix>
			<div class="inner">
				<h2 class=headingMain><span>ร่วมงานกับเรา</span></h2>
				<div class="form-group">
					<div class="col-md-12">
						<div class="table-responsive">
						<table id="jobscareer" class="table">
							<thead>
								<tr>
									<th>ตำแหน่ง</th>
									<th>หน่วยงาน</th>
									<th>สถานที่ทำงาน</th>
									<th>จำนวน</th>
								</tr>
							</thead>
							<tbody>
							<?php 
								if (isset($jobs)) {
								foreach ($jobs as $key => $jo) {
							?>
								<tr>
									<td>
										<?php if (empty($jo->content)) {?>
										<?php echo $jo->position; ?>
										<?php }else{ ?>
										<a class="detail" data-toggle="modal" data-target="#detailfrm" data-id='<?php echo $jo->id; ?>'><?php echo $jo->position; ?></a>	
										<?php } ?>
									</td>
									<!--<td><?php echo $jo->position;?></td>-->
									<td><?php echo $jo->department;?></td>
									<td><?php echo $jo->workplace;?></td>
									<td><?php echo $jo->number;?></td>
								</tr>
							<?php 
								}
							}
							?>
							</tbody>
						</table>
						</div>
					</div>
				</div>
				<div class="row">
				<div class="form-group">
					<div class='col-sm-12 col-md-2'>
					<?php 
						if(!empty($filedata)){
							echo anchor('assets/images/filedata/'.$filedata,'<img src="assets/images/jobcareer/download.png" class="center-block" alt="Download">');
						}else{
							echo anchor('#','<img src="assets/images/jobcareer/download.png"  class="center-block" alt="Download">');
						}?>
					</div>
					<div class='col-sm-12 col-md-2'>
					<!--<?php echo anchor('mailto:admin@greenspot.co.th?subject=Greenspot',
							'<img src="assets/images/jobcareer/sent.png" class="center-block" alt="application form">',
							array('target'=> '_blank')
						);?>-->
						<?php 
							$attributes = array('title' => 'application','target' => '_top');
							echo mailto('admin@greenspot.co.th?subject=Greenspot', '<img src="assets/images/jobcareer/sent.png" class="center-block" alt="application form">', $attributes);
						?>	
					</div>
				</div>
				</div>
				<div class="col-sm-12 col-md-9">
				<div class="row">
					<div class="form-group">
					<div class='col-md-3'>
						<img src="assets/images/jobcareer/topic.png" class="center-block" alt="topic">
					</div>
					</div>
				</div>
				<div class="row">
					<div class="form-group">
						<div class='col-sm-12 col-md-2'>
						<?php echo anchor('http://www.jobthai.com/company/1,58675.html',
							'<img src="assets/images/jobcareer/jobthai.png" class="center-block" alt="jobthai">',
							array('target'=> '_blank')
						);?>
						</div>
						<div class='col-sm-12 col-md-2'>
						<?php echo anchor('https://th.jobsdb.com/th/jobs/companies/green-spot-co-ltd',
							'<img src="assets/images/jobcareer/jobdb.png" class="center-block" alt="jobdb">',
							array('target'=> '_blank')
						);?>
						</div>
						<div class='col-sm-12 col-md-2'>
						<?php echo anchor('https://www.jobtopgun.com/%E0%B8%AB%E0%B8%B2%E0%B8%87%E0%B8%B2%E0%B8%99/Green%20Spot%20Co.,%20Ltd.%20Jobs/company/252',
							'<img src="assets/images/jobcareer/jobtop.png" class="center-block" alt="jobtop">',
							array('target'=> '_blank')
						);?>
						</div>

					</div>
				</div>
			</div>
			<div class="col-sm-12 col-md-3">
				<div class="row">
					<div class="form-group">
						<div class="career">
							<img src="assets/images/jobcareer/contact.png" alt="contact">
						</div>
					</div>
				</div>
			</div>
				<div class="row">
				<div class="form-group">
					<div class="col-md-12 career">
					<?php 
						echo img($img);
					?>
					</div>
				</div>
				</div>
				<div class="row">
					<div class="form-group">
						<div class="col-md-12 career">
						<?php echo $detail;?>
						</div>
					</div>
				</div>
			</div>	
		</section>
<?php } }?>

<div class="modal fade" id="detailfrm" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">รายละเอียด</h4>
      </div>
      <div class="modal-body">
       <?php echo form_open('#',array('id'=>'detailmodal','class'=>'form-horizontal')); ?>
	       	<div class="row">
	       		<div class="col-sm-12">
	          	<input type="hidden" name="id" id="id">
	          	<div id="content" class="content" style="padding:20px; ">
	          	</div>
	          </div>
	        </div>
       </div>
	      <div class="modal-footer">
	        <?php echo form_button('close','ปิดหน้านี้',array('type' => 'button','class' => 'btn btn-danger','data-dismiss'=>'modal'));?>
	      </div><!-- /.modal-footer -->

			<?php echo form_close();?>
    	</div><!-- /.modal-content -->
  	</div><!-- /.modal-doalog -->
	</div>
</div>