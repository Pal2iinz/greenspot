<?php
if (isset($promotion)) {
    foreach ($promotion as $key => $p) {
        $id = $p->id;
        $title = $p->title;
        $image = $p->image;
        $url = $p->url;
        $type = $p->type;
        $status = $p->status;
    } // end foreach
} else {
    $id = null;
    $title = null;
    $image = null;
    $url = null;
    $type = null;
    $status = null;
}
?>
<!-- Begin page heading -->
<?php echo heading('Form Promotion',1,array('class' => 'page-heading'));?>
<!-- End page heading -->
<ol class="breadcrumb default square rsaquo sm">
   <li><a href="index.html"><i class="fa fa-home"></i></a></li>
   <li><a href="#fakelink">Promotion</a></li>
   <li class="active">Form</li>
</ol>
<div class='the-box'>
    <?php
        if ($this->session->flashdata('msg-success')) {
    ?>
    <div class="alert alert-success alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert">x</button>
        <?php echo $this->session->flashdata('msg-success'); ?>
    </div>
    <?php
    } // end if msg
    if ($this->session->flashdata('msg-danger')) {
    ?>
    <div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert">x</button>
        <?php echo $this->session->flashdata('msg-danger'); ?>
    </div>
    <?php
    } // end if msg
    if ($this->session->flashdata('msg-update')) {
    ?>
    <div class="alert alert-info alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert">x</button>
        <?php echo $this->session->flashdata('msg-update'); ?>
    </div>
    <?php
    }// end if msg
    ?>
    <div class="row">
        <div class="col-md-12">
            <?php
            echo form_open_multipart(
                'backend/page/homepage/savepro',
                    array(
                        'class'=>'form-horizontal'
                    )
                );
            echo form_hidden('id',$id);
            ?>
            <div class="form-group">
                <?php
                echo form_label(
                    'Title',
                    'title',
                    array(
                        'class' => 'col-sm-2 control-label'
                    )
                );
                ?>
                <div class="col-sm-5">
                    <?php
                    echo form_input('title', $title);
                    echo form_error('title', '<div class="alert alert-danger">', '</div>');
                    ?>
                </div>
            </div>
            <div class="form-group">
                <?php
                echo form_label(
                    'Url',
                    'url',
                    array(
                        'class' => 'col-sm-2 control-label'
                    )
                );
                ?>
                <div class="col-sm-5">
                    <?php
                    echo form_input('url', $url);
                    echo form_error('url', '<div class="alert alert-danger">', '</div>');
                    ?>
                </div>
            </div>
            <div class="form-group">
                <?php
                echo form_label(
                    'Type',
                    'type',
                    array(
                        'class' => 'col-sm-2 control-label'
                    )
                );
                ?>
                <div class="col-sm-5">
                    <select class="" name="type" id="type">
                        <<option value="1">Promotion</option>
                        <<option value="2">TVC</option>
                    </select>
                    <?php
                    echo form_error('type', '<div class="alert alert-danger">', '</div>');
                    ?>
                </div>
            </div>
            <div class="form-group">
                <?php
                if (!empty($image)) {
                    echo '<div class="col-sm-5 col-sm-offset-2">';
                    echo image_asset_att(
                        'promotion/'.$p->image,
                        array(
                            'class' => 'img-thumbnail img-responsive',
                            'style' => 'width: 150px;'
                        )
                    );
                    echo '</div>';
                    echo '<div class="col-sm-5 col-sm-offset-2">';
                    echo '<i class="fa fa-trash fa-2x del" title="Delete Image" data-id="'.$p->id.'"></i>';
                    echo '</div>';
                } else {
                    echo form_label(
                        'Image',
                        'image',
                        array(
                            'class' => 'col-sm-2 control-label'
                        )
                    );
                ?>
                <div class="col-sm-5">
                    <input type="file" name="image">
                </div>
                <?php
                }  // end else
                ?>
            </div>
            <div class="form-group">
                <div class="col-sm-5 col-sm-offset-2">
                    <?php
                    echo form_submit('submit','submit',array('class'=>'btn btn-primary'));
                    ?>
                </div>;
            <?php
            echo form_close();
            ?>
        </div>
    </div>
