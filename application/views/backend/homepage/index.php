<!-- Begin page heading -->
<?php echo heading('Homepage Management',1,array('class' => 'page-heading'));?>
<!-- End page heading -->
<ol class="breadcrumb default square rsaquo sm">
   <li><a href="index.html"><i class="fa fa-home"></i></a></li>
   <li><a href="#fakelink">Homepage</a></li>
   <li class="active">Index</li>
</ol>
<?php
    if ($this->session->flashdata('msg-success')) {
?>
    <div class="alert alert-success" role="alert">
        <?php echo $this->session->flashdata('msg-success'); ?>
    </div>
<?php
} // end if msg
    if ($this->session->flashdata('msg-danger')) {
?>
    <div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert">x</button>
        <?php echo $this->session->flashdata('msg-danger'); ?>
    </div>
<?php
} // end if msg
?>
<div class='the-box'>
    <div class="row">
        <div class="col-md-12">
            <h4>Promotion</h4>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 table-responsive">
            <table class='table datatable'>
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Image</th>
                        <th>Title</th>
                        <th>Url</th>
                        <th>Type</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if (isset($promotion)) {
                        $i = 1;
                        foreach ($promotion as $key => $p) {
                        ?>
                        <tr>
                            <td><?php echo $i; ?></td>
                            <td>
                                <?php
                                echo image_asset_att(
                                    'promotion/'.$p->image,
                                    array(
                                        'class' => 'img-thumbnail img-responsive',
                                        'style' => 'width: 150px;'
                                    )
                                );
                                ?>
                            </td>
                            <td><?php echo $p->title; ?></td>
                            <td>
                                <?php
                                echo anchor($p->url, 'Link');
                                ?>
                            </td>
                            <td>
                                <?php
                                if ($p->type == 1) {
                                    echo 'Promotion';
                                } else {
                                    echo 'TVC';
                                }
                                ?>
                            </td>
                            <td>
                                <?php
                                if ($p->status == 1) {
                                    echo '<span class="label label-danger">Open</span>';
                                } else {
                                    echo '<span class="label label-default">Close</span>';
                                }
                                ?>
                            </td>
                            <td>
                                <div class="dropdown">
                                    <button class="btn btn-primary dropdown-toggle"
                                    type="button" id="dropdown" data-toggle="dropdown">
                                        <i class="fa fa-cog"></i>
                                    </button>
                                    <ul class="dropdown-menu" aria-labelledby="dropdown">
                                        <li>
                                            <?php
                                            $cl = array('class' => 'edit');
                                            echo anchor(
                                                'backend/page/homepage/form/'.$p->id,
                                                'Edit'
                                            );
                                            ?>
                                        </li>
                                        <li>
                                            <?php
                                            if ($p->status == 1) {
                                            ?>
                                            <a class="close"
                                            data-id="<?php echo $p->id;?>">Close</a>
                                            <?php
                                            } else {
                                            ?>
                                            <a class="open"
                                            data-id="<?php echo $p->id;?>">Open</a>
                                            <?php
                                            } // end if
                                            ?>
                                        </li>
                                        <li>
                                            <a class="delete"
                                            data-id="<?php echo $p->id;?>">
                                                Delete
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                        <?php
                        $i++;
                        } // end foreach
                    } // end if
                    ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?php
                echo anchor('backend/page/homepage/form',
                    'Add Promotion',
                    array(
                        'class' => 'btn btn-primary'
                    )
                )
            ?>
        </div>
    </div>
</div>
