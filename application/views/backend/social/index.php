<!-- Begin page heading -->
<?php echo heading('Social Management',1,array('class' => 'page-heading'));?>
<!-- End page heading -->
<ol class="breadcrumb default square rsaquo sm">
   <li><a href="index.html"><i class="fa fa-home"></i></a></li>
   <li><a href="#fakelink">Social</a></li>
   <li class="active">Index</li>
</ol>
<?php
    if ($this->session->flashdata('msg-success')) {
?>
    <div class="alert alert-success" role="alert">
        <?php echo $this->session->flashdata('msg-success'); ?>
    </div>
<?php
} // end if msg
    if ($this->session->flashdata('msg-danger')) {
?>
    <div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert">x</button>
        <?php echo $this->session->flashdata('msg-danger'); ?>
    </div>
<?php
} // end if msg
?>
<div class='the-box'>
    <div class="row">
        <div class="col-md-12 table-responsive">
            <table class='table datatable'>
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Image</th>
                        <th>Title</th>
                        <th>Details</th>
                        <th>Url</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if (isset($social)) {
                        $i = 1;
                        foreach ($social as $key => $s) {
                        ?>
                        <tr>
                            <td><?php echo $i; ?></td>
                            <td>
                                <?php
                                echo image_asset_att(
                                    'social/'.$s->image,
                                    array(
                                        'class' => 'img-thumbnail img-responsive',
                                        'style' => 'width: 150px;'
                                    )
                                );
                                ?>
                            </td>
                            <td><?php echo $s->title; ?></td>
                            <td><?php echo $s->details; ?></td>
                            <td>
                                <?php
                                echo anchor($s->url, 'Link');
                                ?>
                            </td>
                            <td>
                                <?php
                                if ($s->status == 1) {
                                    echo '<span class="label label-danger">Open</span>';
                                } else {
                                    echo '<span class="label label-default">Close</span>';
                                }
                                ?>
                            </td>
                            <td>
                                <div class="dropdown">
                                    <button class="btn btn-primary dropdown-toggle"
                                    type="button" id="dropdown" data-toggle="dropdown">
                                        <i class="fa fa-cog"></i>
                                    </button>
                                    <ul class="dropdown-menu" aria-labelledby="dropdown">
                                        <li>
                                            <?php
                                            $cl = array('class' => 'edit');
                                            echo anchor(
                                                'backend/social/form/'.$s->id,
                                                'Edit'
                                            );
                                            ?>
                                        </li>
                                        <li>
                                            <?php
                                            if ($s->status == 1) {
                                            ?>
                                            <a class="closed"
                                            data-id="<?php echo $s->id;?>">Close</a>
                                            <?php
                                            } else {
                                            ?>
                                            <a class="open"
                                            data-id="<?php echo $s->id;?>">Open</a>
                                            <?php
                                            } // end if
                                            ?>
                                        </li>
                                        <li>
                                            <a class="delete"
                                            data-id="<?php echo $s->id;?>">
                                                Delete
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                        <?php
                        $i++;
                        }
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?php
                echo anchor('backend/social/form',
                    'Add Social',
                    array(
                        'class' => 'btn btn-primary'
                    )
                )
            ?>
        </div>
    </div>
