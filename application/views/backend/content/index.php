<!-- Begin page heading -->
<?php echo heading('Content Management',1,array('class' => 'page-heading'));?>
<!-- End page heading -->
<ol class="breadcrumb default square rsaquo sm">
   <li><a href="index.html"><i class="fa fa-home"></i></a></li>
   <li><a href="#fakelink">Content</a></li>
   <li class="active">Index</li>
</ol>
<?php
    if ($this->session->flashdata('msg-success')) {
?>
    <div class="alert alert-success" role="alert">
        <?php echo $this->session->flashdata('msg-success'); ?>
    </div>
<?php
} // end if msg
    if ($this->session->flashdata('msg-danger')) {
?>
    <div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert">x</button>
        <?php echo $this->session->flashdata('msg-danger'); ?>
    </div>
<?php
} // end if msg
?>

<div class='the-box'>
    <div class="row">
        <div class="col-md-12">
            <h4>Content</h4>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 table-responsive">
            <table class='table datatable'>
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Image</th>
                        <th>Page</th>
                        <th>Content</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        if (isset($content)) {
                            $i = 1;
                            foreach ($content as $key => $c) {
                        ?>
                        <tr>
                            <td><?php echo $i; ?></td>
                            <td>
                                <?php
                                if($c->image !== ''){
                                echo image_asset_att(
                                    'content/'.$c->image,
                                    array(
                                        'class' => 'img-thumbnail img-responsive',
                                        'style' => 'width: 150px;'
                                    )
                                );
                            }else{
                                 echo img(
                                    'http://fakeimg.pl/150x157/',
                                    array(
                                        'class' => 'img-thumbnail img-responsive',
                                        'style' => 'width: 150px;'
                                    )
                                );
                            }
                            ?>
                            </td>
                            <td>
                                <?php
                                switch ($c->page) {
                                    case '1':
                                        echo 'homepage';
                                        break;
                                    case '2':
                                        echo 'about';
                                        break;
                                    case '3':
                                        echo 'vitamilk';
                                        break;
                                    case '4':
                                        echo 'vsoy';
                                        break;
                                    case '5':
                                        echo 'greenspot';
                                        break;
                                    case '6':
                                        echo 'job career';
                                        break;
                                    case '7':
                                        echo 'Pro-Vita';
                                        break;
                                    case '8':
                                        echo 'VitamilkClub';
                                        break;
                                    case '9':
                                        echo 'VsoyClub';
                                        break;
                                    case '10':
                                        echo 'GreenspotClub';
                                        break;
                                    default:
                                        echo 'ไมได้ตั้งค่า';
                                        break;
                                }
                                ?>
                            </td>
                            <td><?php echo $c->content; ?></td>
                            <td>
                                <div class="dropdown">
                                    <button class="btn btn-primary dropdown-toggle"
                                    type="button" id="dropdown" data-toggle="dropdown">
                                        <i class="fa fa-cog"></i>
                                    </button>
                                    <ul class="dropdown-menu" aria-labelledby="dropdown">
                                        <li>
                                            <?php
                                            $cl = array('class' => 'edit');
                                            echo anchor(
                                                'backend/content/form/'.$c->id,
                                                'Edit'
                                            );
                                            ?>
                                        </li>
                                        <li>
                                            <a class="delete"
                                            data-id="<?php echo $c->id;?>">
                                                Delete
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                        <?php
                            $i++;
                            }
                        }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?php
                echo anchor('backend/content/form',
                    'Add Content',
                    array(
                        'class' => 'btn btn-primary'
                    )
                )
            ?>
        </div>
    </div>
</div>
