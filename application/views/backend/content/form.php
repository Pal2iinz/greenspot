<?php
if (isset($content)) {
    foreach ($content as $key => $c) {
        $id = $c->id;
        $image = $c->image;
        $content = $c->content;
        $page = $c->page;
        $fileupload = $c->fileupload;
    } // end foreach
} else {
    $id = null;
    $image = null;
    $content = null;
    $page = null;
    $fileupload = null;

}
?>
<!-- Begin page heading -->
<?php echo heading('Form Content',1,array('class' => 'page-heading'));?>
<!-- End page heading -->
<ol class="breadcrumb default square rsaquo sm">
   <li><a href="index.html"><i class="fa fa-home"></i></a></li>
   <li><a href="#fakelink">Content</a></li>
   <li class="active">Form</li>
</ol>
<div class='the-box'>
    <?php
        if ($this->session->flashdata('msg-success')) {
    ?>
    <div class="alert alert-success alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert">x</button>
        <?php echo $this->session->flashdata('msg-success'); ?>
    </div>
    <?php
    } // end if msg
    if ($this->session->flashdata('msg-danger')) {
    ?>
    <div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert">x</button>
        <?php echo $this->session->flashdata('msg-danger'); ?>
    </div>
    <?php
    } // end if msg
    if ($this->session->flashdata('msg-update')) {
    ?>
    <div class="alert alert-info alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert">x</button>
        <?php echo $this->session->flashdata('msg-update'); ?>
    </div>
    <?php
    }// end if msg
    ?>
    <div class="row">
        <div class="col-md-12">
            <?php
            echo form_open_multipart(
                'backend/content/save',
                array(
                    'class'=>'form-horizontal'
                )
            );
            echo form_hidden('id', $id);
            ?>
            <div class="form-group">
                <?php
                if (!empty($image)) {
                    echo '<div class="col-sm-5 col-sm-offset-2">';
                    echo image_asset_att(
                        'content/'.$image,
                        array(
                            'class' => 'img-thumbnail img-responsive',
                            'style' => 'width: 150px;'
                        )
                    );
                    echo '</div>';
                    echo '<div class="col-sm-5 col-sm-offset-2">';
                    echo '<i class="fa fa-trash fa-2x delimage" title="Delete Image" data-id="'.$id.'"></i>';
                    echo '</div>';
                } else {
                    echo form_label(
                        'Image',
                        'image',
                        array(
                            'class' => 'col-sm-2 control-label'
                        )
                    );
                ?>
                <div class="col-sm-5">
                    <input type="file" name="image">
                </div>
                <?php
                }  // end else
                ?>
            </div>
            <div class="form-group">
                <?php
                echo form_label(
                    'Pages',
                    'pages',
                    array(
                        'class' => 'col-sm-2 control-label'
                    )
                );
                ?>
                <div class="col-sm-5">
                <?php 
                    $option = array(
                        '' =>'กรุณาเลือก Pages',
                        '1'=>'Homepage',
                        '2'=>'About',
                        '3'=>'Vitamilk',
                        '4'=>'Vsoy',
                        '5'=>'Greenspot',
                        '6'=>'Job Career',
                        '7'=>'Pro-Vita',
                        '8'=>'VitamilkClub',
                        '9'=>'VsoyClub',
                        '10'=>'GreenspotClub',
                    );
                    echo form_dropdown('page',$option,$page);
                ?> 
                </div>
            </div>
            <div class="form-group">
                <?php
                echo form_label(
                    'Content',
                    'content',
                    array(
                        'class' => 'col-sm-2 control-label'
                    )
                );
                ?>
                <div class="col-sm-7">
                 <!--   <?php
                    if (empty($text)) {
                    ?>
                    <textarea id="content" name="content"><?php echo set_value('text'); ?></textarea>
                    <?php
                    } else {
                        $option = array('id' => 'content');
                        echo form_textarea('content', $text, $option);
                    }
                    echo form_error('content', '<div class="alert alert-danger">', '</div>');
                    ?>-->
                        <?php
                    if (empty($content)) {
                    ?>
                    <textarea id="content" name="content"><?php echo set_value('content'); ?></textarea>
                    <?php
                    } else {
                        $option = array('id' => 'content');
                        echo form_textarea('content', $content, $option);
                    }
                    echo form_error('content', '<div class="alert alert-danger">', '</div>');
                    ?>
                </div>
            </div>
            <div class="form-group">
                <?php
                if (!empty($fileupload)) {
                    echo '<div class="col-sm-5 col-sm-offset-2">';
                    echo anchor('assets/images/filedata/'.$fileupload,$fileupload,array('target' => '_blank'));
                    echo '</div>';
                    echo '<div class="col-sm-5 col-sm-offset-2">';
                    echo '<i class="fa fa-trash fa-2x delfile" title="Delete File" data-id="'.$id.'"></i>';
                    echo '</div>';
                } else {
                    echo form_label(
                        'File',
                        'file',
                        array(
                            'class' => 'col-sm-2 control-label'
                        )
                    );
                ?>
                <div class="col-sm-5">
                    <input type="file" name="fileupload">
                </div>
                <?php
                }  // end else
                ?>
            </div>    
            <div class="form-group">
                <div class="col-sm-5 col-sm-offset-2">
                    <?php
                    echo form_submit(
                        'submit',
                        'Save',
                        array(
                            'class'=>'btn btn-primary'
                        )
                    );
                    ?>
                </div>
        
            <?php
            echo form_close();
            ?>
        </div>
    </div>
</div>
