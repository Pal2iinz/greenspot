<?php
if (isset($intro)) {
    foreach ($intro as $key => $in) {
        $id = $in->id;
        $image = $in->image;
        $url = $in->url;
        $status = $in->status;
    } // end foreach
} else {
    $id = null;
    $image = null;
    $url = null;
    $status = null;
}
?>
<!-- Begin page heading -->
<?php echo heading('Form Footer Banner',1,array('class' => 'page-heading'));?>
<!-- End page heading -->
<ol class="breadcrumb default square rsaquo sm">
   <li><a href="index.html"><i class="fa fa-home"></i></a></li>
   <li><a href="#fakelink">Intro</a></li>
   <li class="active">Form</li>
</ol>
<div class='the-box'>
    <?php
        if ($this->session->flashdata('msg-success')) {
    ?>
    <div class="alert alert-success alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert">x</button>
        <?php echo $this->session->flashdata('msg-success'); ?>
    </div>
    <?php
    } // end if msg
    if ($this->session->flashdata('msg-danger')) {
    ?>
    <div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert">x</button>
        <?php echo $this->session->flashdata('msg-danger'); ?>
    </div>
    <?php
    } // end if msg
    if ($this->session->flashdata('msg-update')) {
    ?>
    <div class="alert alert-info alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert">x</button>
        <?php echo $this->session->flashdata('msg-update'); ?>
    </div>
    <?php
    }// end if msg
    ?>
    <div class="row">
        <div class="col-md-12">
            <?php
            echo form_open_multipart(
                'backend/intro/save',
                array(
                    'class'=>'form-horizontal'
                )
            );
            echo form_hidden('id', $id);
            ?>
            <div class="form-group">
                <?php
                if (!empty($image)) {
                    echo '<div class="col-sm-5 col-sm-offset-2">';
                    echo image_asset_att(
                        'intro/'.$image,
                        array(
                            'class' => 'img-thumbnail img-responsive',
                            'style' => 'width: 150px;'
                        )
                    );
                    echo '</div>';
                    echo '<div class="col-sm-5 col-sm-offset-2">';
                    echo '<i class="fa fa-trash fa-2x delimagehero" title="Delete Image" data-id="'.$id.'"></i>';
                    echo '</div>';
                } else {
                    echo form_label(
                        'Intro Image',
                        'intro',
                        array(
                            'class' => 'col-sm-2 control-label'
                        )
                    );
                ?>
                <div class="col-sm-5">
                    <input type="file" name="image">
                </div>
                <?php
                }  // end else
                ?>
            </div>
            <div class="form-group">
                <?php
                echo form_label(
                    'Url',
                    'url',
                    array(
                        'class' => 'col-sm-2 control-label'
                    )
                );
                ?>
                <div class="col-sm-5">
                    <?php
                    echo form_input('url', $url);
                    ?>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-5 col-sm-offset-2">
                    <?php
                    echo form_submit(
                        'submit',
                        'Upload',
                        array(
                            'class'=>'btn btn-primary'
                        )
                    );
                    ?>
                </div>;
            <?php
            echo form_close();
            ?>
        </div>
    </div>
