<!-- Begin page heading -->
<?php echo heading('Jobs Management',1,array('class' => 'page-heading'));?>
<!-- End page heading -->
<ol class="breadcrumb default square rsaquo sm">
   <li><a href="index.html"><i class="fa fa-home"></i></a></li>
   <li><a href="#fakelink">Jobs</a></li>
   <li class="active">Index</li>
</ol>
<?php
    if ($this->session->flashdata('msg-success')) {
?>
    <div class="alert alert-success" role="alert">
        <?php echo $this->session->flashdata('msg-success'); ?>
    </div>
<?php
} // end if msg
    if ($this->session->flashdata('msg-danger')) {
?>
    <div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert">x</button>
        <?php echo $this->session->flashdata('msg-danger'); ?>
    </div>
<?php
} // end if msg
?>
<div class='the-box'>
    <div class="row">
        <div class="col-md-12">
            <h4>Jobs Management</h4>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 table-responsive">
            <table class='table datatable'>
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Position</th>
                        <th>Department</th>
                        <th>Number</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        if (isset($jobs)) {
                            $i = 1;
                            foreach ($jobs as $key => $jo) {
                        ?>
                        <tr>
                            <td><?php echo $i; ?></td>
                            <td><?php echo $jo->position; ?></td>
                             <td><?php echo $jo->department; ?></td>
                            <td><?php echo $jo->number; ?></td>
                            <td>
                                <?php
                                if ($jo->status == 1) {
                                    echo '<span class="label label-danger">Open</span>';
                                } else {
                                    echo '<span class="label label-default">Close</span>';
                                }
                                ?>
                            </td>
                            <td>
                                <div class="dropdown">
                                    <button class="btn btn-primary dropdown-toggle"
                                    type="button" id="dropdown" data-toggle="dropdown">
                                        <i class="fa fa-cog"></i>
                                    </button>
                                    <ul class="dropdown-menu" aria-labelledby="dropdown">
                                        <li>
                                            <?php
                                            $cl = array('class' => 'edit');
                                            echo anchor(
                                                'backend/jobs/form/'.$jo->id,
                                                'Edit'
                                            );
                                            ?>
                                        </li>
                                        <li>
                                            <?php
                                            if ($jo->status == 1) {
                                            ?>
                                            <a class="closejobs"
                                            data-id="<?php echo $jo->id;?>">Close</a>
                                            <?php
                                            } else {
                                            ?>
                                            <a class="open"
                                            data-id="<?php echo $jo->id;?>">Open</a>
                                            <?php
                                            } // end if
                                            ?>
                                        </li>
                                        <li>
                                            <a class="delete"
                                            data-id="<?php echo $jo->id;?>">
                                                Delete
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                        <?php
                            $i++;
                            }
                        }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?php
                echo anchor('backend/jobs/form',
                    'Add Jobs',
                    array(
                        'class' => 'btn btn-primary'
                    )
                )
            ?>
        </div>
    </div>
