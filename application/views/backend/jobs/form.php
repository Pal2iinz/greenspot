<?php
if (isset($jobs)) {
    foreach ($jobs as $key => $jo) {
        $id = $jo->id;
        $position = $jo->position;
        $department = $jo->department;
        $workplace = $jo->workplace;
        $number = $jo->number;
        $content = $jo->content;
    } // end foreach
} else {
    $id = null;
    $position = null;
    $department = null;
    $workplace = null;
    $number = null;
    $content = null;
}
?>
<!-- Begin page heading -->
<?php echo heading('Form Jobs',1,array('class' => 'page-heading'));?>
<!-- End page heading -->
<ol class="breadcrumb default square rsaquo sm">
   <li><a href="index.html"><i class="fa fa-home"></i></a></li>
   <li><a href="#fakelink">Jobs</a></li>
   <li class="active">Form</li>
</ol>
<div class='the-box'>
    <?php
        if ($this->session->flashdata('msg-success')) {
    ?>
    <div class="alert alert-success alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert">x</button>
        <?php echo $this->session->flashdata('msg-success'); ?>
    </div>
    <?php
    } // end if msg
    if ($this->session->flashdata('msg-danger')) {
    ?>
    <div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert">x</button>
        <?php echo $this->session->flashdata('msg-danger'); ?>
    </div>
    <?php
    } // end if msg
    if ($this->session->flashdata('msg-update')) {
    ?>
    <div class="alert alert-info alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert">x</button>
        <?php echo $this->session->flashdata('msg-update'); ?>
    </div>
    <?php
    }// end if msg
    ?>
    <div class="row">
        <div class="col-md-12">
            <?php
            echo form_open_multipart(
                'backend/jobs/save',
                array(
                    'class'=>'form-horizontal'
                )
            );
            echo form_hidden('id', $id);
            ?>
            <div class="form-group">
                 <?php
                echo form_label(
                    'Position',
                    'position',
                    array(
                        'class' => 'col-sm-2 control-label'
                    )
                );
                ?>
                <div class="col-sm-5">
                    <?php
                    echo form_input('position', $position);
                    ?>
                </div>
            </div>
            <div class="form-group">
                <?php
                echo form_label(
                    'Department',
                    'department',
                    array(
                        'class' => 'col-sm-2 control-label'
                    )
                );
                ?>
                <div class="col-sm-5">
                    <?php
                    echo form_input('department', $department);
                    ?>
                </div>
            </div>
            <div class="form-group">
                <?php
                echo form_label(
                    'Workplace',
                    'workplace',
                    array(
                        'class' => 'col-sm-2 control-label'
                    )
                );
                ?>
                <div class="col-sm-5">
                    <?php
                    echo form_input('workplace', $workplace);
                    ?>
                </div>
            </div>
            <div class="form-group">
                <?php
                echo form_label(
                    'Number',
                    'number',
                    array(
                        'class' => 'col-sm-2 control-label'
                    )
                );
                ?>
                <div class="col-sm-5">
                    <?php
                    echo form_input('number', $number);
                    ?>
                </div>
            </div>
            <div class="form-group">
             <?php
                echo form_label(
                    'Description',
                    'description',
                    array(
                        'class' => 'col-sm-2 control-label'
                    )
                );
                ?>
              <div class="col-sm-8">
                <?php
                    if (empty($content)) {
                    ?>
                    <textarea id="content" name="content"><?php echo set_value('content'); ?></textarea>
                    <?php
                    } else {
                        $option = array('id' => 'content');
                        echo form_textarea('content', $content, $option);
                    }
                    echo form_error('content', '<div class="alert alert-danger">', '</div>');
                    ?>
             	</div>
            </div>
            <div class="form-group">
                <div class="col-sm-5 col-sm-offset-2">
                    <?php
                    echo form_submit(
                        'submit',
                        'Save',
                        array(
                            'class'=>'btn btn-primary'
                        )
                    );
                    ?>
                </div>;
            <?php
            echo form_close();
            ?>
        </div>
    </div>
