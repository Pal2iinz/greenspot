<!-- Begin page heading -->
<?php echo heading('Event Management (Vitamilk Club)',1,array('class' => 'page-heading'));?>
<!-- End page heading -->
<ol class="breadcrumb default square rsaquo sm">
   <li><a href="index.html"><i class="fa fa-home"></i></a></li>
   <li><a href="#fakelink">Event</a></li>
   <li class="active">Vitamilk Club</li>
</ol>
<?php
    if ($this->session->flashdata('msg-success')) {
?>
    <div class="alert alert-success" role="alert">
        <?php echo $this->session->flashdata('msg-success'); ?>
    </div>
<?php
} // end if msg
    if ($this->session->flashdata('msg-danger')) {
?>
    <div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert">x</button>
        <?php echo $this->session->flashdata('msg-danger'); ?>
    </div>
<?php
} // end if msg
?>
<div class='the-box'>
    <div class="row">
        <div class="col-md-12">
            <h4>Vitamilk Club</h4>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 table-responsive">
            <table class='table datatable'>
                <thead>
                    <tr>
                        <th>ลำดับ</th>
                        <th>ชื่อ-นามสกุล</th>
                        <th>วันเกิด</th>
                        <th>Facebook</th>
                        <th>เบอร์โทรศัพท์</th>
                        <th>ชื่อ-นามสกุล(เพื่อน)</th>
                        <th>Size เสื้อ</th>
                        <th>Attach</th>
                    </tr>
                </thead>
                <tbody>
                	<?php
                		$iNum = 1;
                		if(isset($register)){
                			foreach ($register as $key => $reg) {
                	?>
                  <tr>
                    <td><?php echo $iNum;?></td>
                    <td><?php echo $reg->fullname;?></td>
                    <td><?php echo $reg->birthdate;?></td>
                    <td><?php echo $reg->facebook;?></td>
                    <td><?php echo $reg->phone;?></td>
                    <td><?php echo $reg->fullname2;?></td>
                    <td>
                  		<?php
                  			switch ($reg->size) {
                  		 	case '1':
                  		 		echo "S";
                  		 		break;
                  		 	case '2':
                  		 		echo "M";
                  		 		break;
                		 		case '3':
                		 			echo "L";
                		 		break;
                		 		case '4':
                		 			echo "XL";
                		 		break;
                		 		case '5':
                		 			echo "2XL";
                		 		break;
                		 		case '6':
                		 			echo "3XL";
                		 		break;
                  		 	default:
                  		 		echo "-";
                  		 	break;
                  		 } 
                    		?> / 
                    	<?php 
                    		switch ($reg->size2) {
                  		 	case '1':
                  		 		echo "S";
                  		 		break;
                  		 	case '2':
                  		 		echo "M";
                  		 		break;
                		 		case '3':
                		 			echo "L";
                		 		break;
                		 		case '4':
                		 			echo "XL";
                		 		break;
                		 		case '5':
                		 			echo "2XL";
                		 		break;
                		 		case '6':
                		 			echo "3XL";
                		 		break;
                  		 	default:
                  		 		echo "-";
                  		 	break;
                  		 } 
                  		;?>
                    </td>
                    <td>
                    	<?php
                    		$id = $reg->id;
                    		$attach = $this->Event_model->GetAttachByRegId($id);
                    		foreach ($attach as $key => $value) {
                    			$filedata = $value['attach'];
                    			echo "<a href='../../../assets/images/events/vitamilkclub/".$id."/".$filedata."' target='_blank'>";
                    			echo $filedata;
                    			echo "</a>";
                    			echo '</br>';
                    		}
    										
                    	?>
                    </td>
                  </tr>
                  <?php
                  	$iNum++; 
                			} 
              			}
              		?>
                </tbody>
            </table>
        </div>
    </div>
