<!-- Begin page heading -->
<?php echo heading('Hero Management',1,array('class' => 'page-heading'));?>
<!-- End page heading -->
<ol class="breadcrumb default square rsaquo sm">
   <li><a href="index.html"><i class="fa fa-home"></i></a></li>
   <li><a href="#fakelink">Hero</a></li>
   <li class="active">Index</li>
</ol>
<?php
    if ($this->session->flashdata('msg-success')) {
?>
    <div class="alert alert-success" role="alert">
        <?php echo $this->session->flashdata('msg-success'); ?>
    </div>
<?php
} // end if msg
    if ($this->session->flashdata('msg-danger')) {
?>
    <div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert">x</button>
        <?php echo $this->session->flashdata('msg-danger'); ?>
    </div>
<?php
} // end if msg
?>

<div class='the-box'>
    <div class="row">
        <div class="col-md-12">
            <h4>Hero Banner</h4>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 table-responsive">
            <table class='table datatable'>
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Hero</th>
                        <th>Title</th>
                        <th>Page</th>
                        <th>Position</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        if (isset($hero)) {
                            $i = 1;
                            foreach ($hero as $key => $h) {
                        ?>
                        <tr>
                            <td><?php echo $i; ?></td>
                            <td>
                                <?php
                                echo image_asset_att(
                                    'hero/'.$h->hero,
                                    array(
                                        'class' => 'img-thumbnail img-responsive',
                                        'style' => 'width: 150px;'
                                    )
                                );
                                ?>
                            </td>
                            <td><?php echo $h->title; ?></td>
                            <td>
                                <?php
                                switch ($h->page) {
                                    case '1':
                                        echo 'homepage';
                                        break;
                                    case '2':
                                        echo 'about';
                                        break;
                                    case '3':
                                        echo 'vitamilk';
                                        break;
                                    case '4':
                                        echo 'vsoy';
                                        break;
                                    case '5':
                                        echo 'greenspot';
                                        break;
                                    case '6':
                                        echo 'healthy';
                                        break;
                                    case '7':
                                        echo 'healthy-menu';
                                        break;
                                    case '8':
                                        echo 'job career';
                                        break;
                                    case '9':
                                        echo 'Pro-Vita';
                                        break;
                                    default:
                                        echo 'ไมได้ตั้งค่า';
                                        break;
                                }
                                ?>
                            </td>
                            <td><?php echo $h->position; ?></td>
                            <td>
                                <?php
                                if ($h->status == 1) {
                                    echo '<span class="label label-danger">Open</span>';
                                } else {
                                    echo '<span class="label label-default">Close</span>';
                                }
                                ?>
                            </td>
                            <td>
                                <div class="dropdown">
                                    <button class="btn btn-primary dropdown-toggle"
                                    type="button" id="dropdown" data-toggle="dropdown">
                                        <i class="fa fa-cog"></i>
                                    </button>
                                    <ul class="dropdown-menu" aria-labelledby="dropdown">
                                        <li>
                                            <?php
                                            $cl = array('class' => 'edit');
                                            echo anchor(
                                                'backend/hero/form/'.$h->id,
                                                'Edit'
                                            );
                                            ?>
                                        </li>
                                        <li>
                                            <?php
                                            if ($h->status == 1) {
                                            ?>
                                            <a class="closehero"
                                            data-id="<?php echo $h->id;?>">Close</a>
                                            <?php
                                            } else {
                                            ?>
                                            <a class="openhero"
                                            data-id="<?php echo $h->id;?>">Open</a>
                                            <?php
                                            } // end if
                                            ?>
                                        </li>
                                        <li>
                                            <a class="delete"
                                            data-id="<?php echo $h->id;?>">
                                                Delete
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                        <?php
                            $i++;
                            }
                        }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?php
                echo anchor('backend/hero/form',
                    'Add Hero',
                    array(
                        'class' => 'btn btn-primary'
                    )
                )
            ?>
        </div>
    </div>
</div>
