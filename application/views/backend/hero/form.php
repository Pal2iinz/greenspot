<?php
if (isset($hero)) {
    foreach ($hero as $key => $h) {
        $id = $h->id;
        $title = $h->title;
        $image = $h->hero;
        $position = $h->position;
        $status = $h->status;
    } // end foreach
} else {
    $id = null;
    $title = null;
    $image = null;
    $position= null;
    $status = null;
}
?>
<!-- Begin page heading -->
<?php echo heading('Form Hero Banner',1,array('class' => 'page-heading'));?>
<!-- End page heading -->
<ol class="breadcrumb default square rsaquo sm">
   <li><a href="index.html"><i class="fa fa-home"></i></a></li>
   <li><a href="#fakelink">Hero Banner</a></li>
   <li class="active">Form</li>
</ol>
<div class='the-box'>
    <?php
        if ($this->session->flashdata('msg-success')) {
    ?>
    <div class="alert alert-success alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert">x</button>
        <?php echo $this->session->flashdata('msg-success'); ?>
    </div>
    <?php
    } // end if msg
    if ($this->session->flashdata('msg-danger')) {
    ?>
    <div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert">x</button>
        <?php echo $this->session->flashdata('msg-danger'); ?>
    </div>
    <?php
    } // end if msg
    if ($this->session->flashdata('msg-update')) {
    ?>
    <div class="alert alert-info alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert">x</button>
        <?php echo $this->session->flashdata('msg-update'); ?>
    </div>
    <?php
    }// end if msg
    ?>
    <div class="row">
        <div class="col-md-12">
            <?php
            $opt = array('class' => 'form-horizontal');
            echo form_open_multipart('backend/hero/save',$opt);
            echo form_hidden('id', $id);
            ?>
            <div class="form-group">
                <?php
                echo form_label(
                    'Title',
                    'title',
                    array(
                        'class' => 'col-sm-2 control-label'
                    )
                );
                ?>
                <div class="col-sm-5">
                    <?php
                    echo form_input('title', $title);
                    echo form_error('title', '<div class="alert alert-danger">', '</div>');
                    ?>
                </div>
            </div>
            <div class="form-group">
                <?php
                echo form_label(
                    'Pages',
                    'pages',
                    array(
                        'class' => 'col-sm-2 control-label'
                    )
                );
                ?>
                <div class="col-sm-5">
                    <select name="page">
                        <option value="1">Homepage</option>
                        <option value="2">About</option>
                        <option value="3">Vitamilk</option>
                        <option value="4">Vsoy</option>
                        <option value="5">Greenspot</option>
                        <option value="6">Healthy</option>
                        <option value="7">Healthy-Menu</option>
                        <option value="8">Job Career</option>
                        <option value="9">Pro-Vita</option>   
                    </select>
                </div>
            </div>
            <div class="form-group">
                <?php
                echo form_label(
                    'Position',
                    'position',
                    array(
                        'class' => 'col-sm-2 control-label'
                    )
                );
                ?>
                <div class="col-sm-5">
                    <select name="position">
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <?php
                if (!empty($image)) {
                    echo '<div class="col-sm-5 col-sm-offset-2">';
                    echo image_asset_att(
                        'hero/'.$image,
                        array(
                            'class' => 'img-thumbnail img-responsive',
                            'style' => 'width: 150px;'
                        )
                    );
                    echo '</div>';
                    echo '<div class="col-sm-5 col-sm-offset-2">';
                    echo '<i class="fa fa-trash fa-2x delimage" title="Delete Image" data-id="'.$id.'"></i>';
                    echo '</div>';
                } else {
                    echo form_label(
                        'Hero Image',
                        'hero',
                        array(
                            'class' => 'col-sm-2 control-label'
                        )
                    );
                ?>
                <div class="col-sm-5">
                    <input type="file" name="hero">
                </div>
                <?php
                }  // end else
                ?>
            </div>
            <div class="form-group">
                <div class="col-sm-5 col-sm-offset-2">
                    <?php
                    echo form_submit(
                        'submit',
                        'submit',
                        array(
                            'class'=>'btn btn-primary'
                        )
                    );
                    ?>
                </div>
            <?php
            echo form_close();
            ?>
        </div>
    </div>
