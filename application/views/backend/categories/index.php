<!-- Begin page heading -->
<?php echo heading('Categories Management',1,array('class' => 'page-heading'));?>
<!-- End page heading -->
<ol class="breadcrumb default square rsaquo sm">
   <li><a href="index.html"><i class="fa fa-home"></i></a></li>
   <li><a href="#fakelink">Categories</a></li>
   <li class="active">Index</li>
</ol>
<?php
    if ($this->session->flashdata('msg-success')) {
?>
    <div class="alert alert-success" role="alert">
        <?php echo $this->session->flashdata('msg-success'); ?>
    </div>
<?php
} // end if msg
    if ($this->session->flashdata('msg-danger')) {
?>
    <div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert">x</button>
        <?php echo $this->session->flashdata('msg-danger'); ?>
    </div>
<?php
} // end if msg
?>
<div class='the-box'>
    <div class="row">
        <div class="col-md-12 table-responsive">
            <table class='table datatable'>
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Categories</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        if (isset($categories)) {
                            $i = 1;
                            foreach ($categories as $key => $c) {
                    ?>
                    <tr>
                        <td><?php echo $i; ?></td>
                        <td><?php echo $c->categories; ?></td>
                        <td>
                            <div class="dropdown">
                                <button class="btn btn-primary dropdown-toggle"
                                type="button" id="dropdown" data-toggle="dropdown">
                                    <i class="fa fa-cog"></i>
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="dropdown">
                                    <li>
                                        <a class="edit"
                                            data-id="<?php echo $c->id;?>"
                                            data-c="<?php
                                            echo $c->categories;
                                            ?>" >
                                                Edit
                                        </a>
                                    </li>
                                    <li>
                                        <a class="delete"
                                        data-id="<?php echo $c->id;?>">
                                            Delete
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                    <?php
                        $i++;
                        } // end foreach
                    } // end if isset
                    ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <a class="btn btn-primary" id="addNew">Add Categories</a>
        </div>
    </div>

<!-- Modal -->
<div class="modal fade" id="modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Add Categories</h4>
            </div>
            <?php
            echo form_open('backend/product/categories/save');
            ?>
            <div class="modal-body">
                <input type="hidden" name="id" id="id" value="">
                <?php
                echo form_label(
                    'Categories',
                    'Categories',
                    array(
                        'class' => 'form-group control-label'
                    )
                );
                echo form_input(
                    array(
                        'name' => 'categories',
                        'id' => 'categories',
                        'class' => 'form-group form-control'
                    )
                );
                ?>
            </div><!-- /.modal-body -->
            <div class="modal-footer">
                <?php
                echo form_submit('submit','submit',array('class'=>'btn btn-primary'));
                ?>
            </div><!--/.modal-footer-->
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
