<!-- Begin page heading -->
<?php echo heading('Blog Management',1,array('class' => 'page-heading'));?>
<!-- End page heading -->
<ol class="breadcrumb default square rsaquo sm">
   <li><a href="index.html"><i class="fa fa-home"></i></a></li>
   <li><a href="#fakelink">Blog</a></li>
   <li class="active">Index</li>
</ol>
<?php
    if ($this->session->flashdata('msg-success')) {
?>
    <div class="alert alert-success" role="alert">
        <?php echo $this->session->flashdata('msg-success'); ?>
    </div>
<?php
} // end if msg
    if ($this->session->flashdata('msg-danger')) {
?>
    <div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert">x</button>
        <?php echo $this->session->flashdata('msg-danger'); ?>
    </div>
<?php
} // end if msg
?>
<div class='the-box'>
    <div class="row">
        <div class="col-md-12">
            <h4>Blog</h4>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 table-responsive">
            <table class='table datatable'>
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Image</th>
                        <th>Title</th>
                        <th>Categories</th>
                        <th>Sub Categories</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        if (isset($blog)) {
                            $i = 1;
                            foreach ($blog as $key => $b) {
                        ?>
                        <tr>
                            <td><?php echo $i; ?></td>
                            <td>
                                <?php
                                if($b->thumbnail !== ''){
                                echo image_asset_att(
                                    'blog/'.$b->thumbnail,
                                    array(
                                        'class' => 'img-thumbnail img-responsive',
                                        'style' => 'width: 150px;'
                                    )
                                );
                            }else{
                                 echo img(
                                    'http://fakeimg.pl/150x157/',
                                    array(
                                        'class' => 'img-thumbnail img-responsive',
                                        'style' => 'width: 150px;'
                                    )
                                );
                            }
                               ?>
                            </td>
                            <td><?php echo $b->title; ?></td>
                            <td>
                                <?php
                                switch ($b->categories) {
                                    case '1':
                                        echo 'สาระสุขภาพ';
                                        break;
                                    case '2':
                                        echo 'เมนูสุขภาพ';
                                        break;
                                    default:
                                        echo 'ไมได้ตั้งค่า';
                                        break;
                                }
                                ?>
                            </td>
                            <td>
                                <?php
                                if (isset($categories)) {
                                    foreach ($categories as $key => $c) {
                                        if ($b->subcat == $c->id) {
                                            echo $c->categories;
                                        } // end if
                                    } // end foreach
                                } // end if
                                ?>
                            </td>
                            <td>
                                <div class="dropdown">
                                    <button class="btn btn-primary dropdown-toggle"
                                    type="button" id="dropdown" data-toggle="dropdown">
                                        <i class="fa fa-cog"></i>
                                    </button>
                                    <ul class="dropdown-menu" aria-labelledby="dropdown">
                                        <li>
                                            <?php
                                            $cl = array('class' => 'edit');
                                            echo anchor(
                                                'backend/blog/form/'.$b->id,
                                                'Edit'
                                            );
                                            ?>
                                        </li>
                                        <li>
                                            <a class="delete"
                                            data-id="<?php echo $b->id;?>">
                                                Delete
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                        <?php
                            $i++;
                            }
                        }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?php
                echo anchor('backend/blog/form',
                    'Add Blog',
                    array(
                        'class' => 'btn btn-primary'
                    )
                )
            ?>
        </div>
    </div>
