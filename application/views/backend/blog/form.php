<?php
if (isset($blog)) {
    foreach ($blog as $key => $b) {
        $id = $b->id;
        $title = $b->title;
        $image = $b->image;
        $categories = $b->categories;
        $subcat = $b->subcat;
        $post = $b->post;
        $thumbnail = $b->thumbnail;
    } // end foreach
} else {
    $id = null;
    $title = null;
    $image = null;
    $categories = null;
    $subcat = null;
    $post = null;
    $thumbnail = null;
}
?>
<!-- Begin page heading -->
<?php echo heading('Form Blog',1,array('class' => 'page-heading'));?>
<!-- End page heading -->
<ol class="breadcrumb default square rsaquo sm">
   <li><a href="index.html"><i class="fa fa-home"></i></a></li>
   <li><a href="#fakelink">Blog</a></li>
   <li class="active">Form</li>
</ol>
<div class='the-box'>
    <?php
        if ($this->session->flashdata('msg-success')) {
    ?>
    <div class="alert alert-success alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert">x</button>
        <?php echo $this->session->flashdata('msg-success'); ?>
    </div>
    <?php
    } // end if msg
    if ($this->session->flashdata('msg-danger')) {
    ?>
    <div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert">x</button>
        <?php echo $this->session->flashdata('msg-danger'); ?>
    </div>
    <?php
    } // end if msg
    if ($this->session->flashdata('msg-update')) {
    ?>
    <div class="alert alert-info alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert">x</button>
        <?php echo $this->session->flashdata('msg-update'); ?>
    </div>
    <?php
    }// end if msg
    ?>
    <div class="row">
        <div class="col-md-12">
            <?php
            echo form_open_multipart(
                'backend/blog/save',
                array(
                    'class'=>'form-horizontal'
                )
            );
            echo form_hidden('id', $id);
            ?>
            <div class="form-group">
                <?php
                echo form_label(
                    'Title',
                    'title',
                    array(
                        'class' => 'col-sm-2 control-label'
                    )
                );
                ?>
                <div class="col-sm-7">
                    <?php
                    echo form_input('title', $title);
                    ?>
                </div>
            </div>
            <div class="form-group">
                <?php
                echo form_label(
                    'Categories',
                    'categories',
                    array(
                        'class' => 'col-sm-2 control-label'
                    )
                );
                ?>
                <div class="col-sm-7">
                    <select name="categories">
                        <option value="1">สาระสุขภาพ</option>
                        <option value="2">เมนสุขภาพ</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <?php
                echo form_label(
                    'subcat',
                    'subcat',
                    array(
                        'class' => 'col-sm-2 control-label'
                    )
                );
                ?>
                <div class="col-sm-7">
                    <?php
                    $options = array();
                    foreach ($cat as $key => $c) {
                        $options[$c->id] = $c->categories;
                    }
                    echo form_dropdown('subcat', $options, $categories);
                    ?>
                </div>
            </div>
            <div class="form-group">
                <?php
                echo form_label(
                    'Post',
                    'post',
                    array(
                        'class' => 'col-sm-2 control-label'
                    )
                );
                ?>
                <div class="col-sm-7">
                    <?php
                    if (empty($post)) {
                    ?>
                    <textarea id="post" name="post"><?php echo set_value('post'); ?></textarea>
                    <?php
                    } else {
                        $option = array('id' => 'post');
                        echo form_textarea('post', $post, $option);
                    }
                    echo form_error('post', '<div class="alert alert-danger">', '</div>');
                    ?>
                </div>
            </div>
            <div class="form-group">
                <?php
                if (!empty($thumbnail)) {
                    echo '<div class="col-sm-7 col-sm-offset-2">';
                    echo image_asset_att(
                        'blog/'.$thumbnail,
                        array(
                            'class' => 'img-thumbnail img-responsive',
                            'style' => 'width: 300px;'
                        )
                    );
                    echo '</div>';
                    echo '<div class="col-sm-5 col-sm-offset-2">';
                    echo '<i class="fa fa-trash fa-2x delimage" title="Delete Image" data-id="'.$id.'" data-position="1"></i>';
                    echo '</div>';
                    echo '<input type="hidden" name="thumbnail" value='.$thumbnail.'>';
                } else {
                    echo form_label(
                        'Thumbnail',
                        'image',
                        array(
                            'class' => 'col-sm-2 control-label'
                        )
                    );
                ?>
                <div class="col-sm-7">
                    <input type="file" name="thumbnail">
                    <span id="help" class="help-block">
                         รูปภาพขนาด 310 x 190 pixel
                     </span>
                </div>
                <?php
                }  // end else
                ?>
            </div>

            <div class="form-group">
                <?php
                if (!empty($image)) {
                    echo '<div class="col-sm-7 col-sm-offset-2">';
                    echo image_asset_att(
                        'blog/'.$image,
                        array(
                            'class' => 'img-thumbnail img-responsive',
                            'style' => 'width: 300px;'
                        )
                    );
                    echo '</div>';
                    echo '<div class="col-sm-5 col-sm-offset-2">';
                    echo '<i class="fa fa-trash fa-2x delimage" title="Delete Image" data-id="'.$id.'" data-position="2"></i>';
                    echo '</div>';
                    echo '<input type="hidden" name="image" value='.$image.'>';
                } else {
                    echo form_label(
                        'Image',
                        'image',
                        array(
                            'class' => 'col-sm-2 control-label'
                        )
                    );
                ?>
                <div class="col-sm-7">
                    <input type="file" name="image">
                    <span id="help" class="help-block">
                         รูปภาพขนาด 900 x 340 pixel
                     </span>
                </div>
                <?php
                }  // end else
                ?>
            </div>
            <div class="form-group">
                <div class="col-sm-5 col-sm-offset-2">
                    <?php
                    echo form_submit(
                        'submit',
                        'Save',
                        array(
                            'class'=>'btn btn-primary'
                        )
                    );
                    ?>
                </div>;
            <?php
            echo form_close();
            ?>
        </div>
    </div>
