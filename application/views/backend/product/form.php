<?php
/*
 echo "<pre>";
 var_dump($product);
 echo "</pre>";
 die();
 */
if (isset($product)) {
    foreach ($product as $key => $p) {
        $id = $p->id;
        $title = $p->title;
        $formula = $p->formula;
        $benefit = $p->benefit;
        $nutrition = $p->nutrition;
        $product_img = $p->product_img;
        $product_thumb = $p->product_thumb;
        $category = $p->category;
        $note = $p->note;
        $status = $p->status;
    } // end foreach
} else {
    $id = null;
    $title = null;
    $formula = null;
    $benefit = null;
    $nutrition = null;
    $product_img = null;
    $$product_thumb = null;
    $category = null;
    $note = null;
    $status = null;
}
?>
<!-- Begin page heading -->
<?php echo heading('PRODUCT MANAGEMENT',1,array('class' => 'page-heading'));?>
<!-- End page heading -->
<ol class="breadcrumb default square rsaquo sm">
   <li><a href="index.html"><i class="fa fa-home"></i></a></li>
   <li><a href="#fakelink">Product</a></li>
   <li class="active">Form</li>
</ol>
<div class='the-box'>
    <?php
        if ($this->session->flashdata('msg-success')) {
    ?>
    <div class="alert alert-success alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert">x</button>
        <?php echo $this->session->flashdata('msg-success'); ?>
    </div>
    <?php
    } // end if msg
    if ($this->session->flashdata('msg-danger')) {
    ?>
    <div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert">x</button>
        <?php echo $this->session->flashdata('msg-danger'); ?>
    </div>
    <?php
    } // end if msg
    if ($this->session->flashdata('msg-update')) {
    ?>
    <div class="alert alert-info alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert">x</button>
        <?php echo $this->session->flashdata('msg-update'); ?>
    </div>
    <?php
    }// end if msg
    ?>
    <div class="row">
        <div class="col-md-12">
            <?php
            echo form_open_multipart(
                'backend/product/save',
                array(
                    'class'=>'form-horizontal'
                )
            );
            echo form_hidden('id',$id);
            ?>
            <div class="form-group">
                <?php
                echo form_label(
                    'Title',
                    'title',
                    array(
                        'class' => 'col-sm-2 control-label'
                    )
                );
                ?>
                <div class="col-sm-7">
                    <?php
                    if (empty($title)) {
                    ?>
                    <input type="text" name="title" value="<?php echo set_value('title'); ?>">
                    <?php
                    } else {
                        echo form_input('title', $title);
                    }
                    echo form_error('title', '<div class="alert alert-danger">', '</div>');
                    ?>
                </div>
            </div>
            <div class="form-group">
                <?php
                echo form_label(
                    'Formula',
                    'formula',
                    array(
                        'class' => 'col-sm-2 control-label'
                    )
                );
                ?>
                <div class="col-sm-7">
                    <?php
                    if (empty($formula)) {
                    ?>
                    <input type="text" name="formula" value="<?php echo set_value('formula'); ?>">
                    <?php
                    } else {
                        echo form_input('formula', $formula);
                    }
                    echo form_error('formula', '<div class="alert alert-danger">', '</div>');
                    ?>
                </div>
            </div>
            <div class="form-group">
                <?php
                echo form_label(
                    'Benefit',
                    'benefit',
                    array(
                        'class' => 'col-sm-2 control-label'
                    )
                );
                ?>
                <div class="col-sm-7">
                    <?php
                    if (empty($benefit)) {
                    ?>
                    <textarea id="benefit" name="benefit"><?php echo set_value('benefit'); ?></textarea>
                    <?php
                    } else {
                        echo form_textarea('benefit', $benefit);
                    }
                    echo form_error('benefit', '<div class="alert alert-danger">', '</div>');
                    ?>
                </div>
            </div>
            <div class="form-group">
                <?php
                echo form_label(
                    'Categories',
                    'categories',
                    array(
                        'class' => 'col-sm-2 control-label'
                    )
                );
                ?>
                <div class="col-sm-7">
                    <?php
                    $options = array();
                    foreach ($categories as $key => $c) {
                        $options[$c->id] = $c->categories;
                    }
                    echo form_dropdown('categories', $options, $category);
                    ?>
                </div>
            </div>
            <div class="form-group">
                <?php
                echo form_label(
                    'Note',
                    'Note',
                    array(
                        'class' => 'col-sm-2 control-label'
                    )
                );
                ?>
                <div class="col-sm-7">
                    <?php
                    echo form_input('note', $note);
                    echo form_error('note', '<div class="alert alert-danger">', '</div>');
                    ?>
                </div>
            </div>
            <div class="form-group">
                <?php
                // var_dump($nutrition);
                if (!empty($nutrition)) {
                    echo '<div class="col-sm-7 col-sm-offset-2">';
                    echo image_asset_att(
                        'product/'.$nutrition,
                        array(
                            'class' => 'img-thumbnail img-responsive',
                            'style' => 'width: 150px;'
                        )
                    );
                    echo '</div>';
                    echo '<div class="col-sm-7 col-sm-offset-2">';
                    echo '<span class="btn btn-danger delimage" title="Delete Image" data-position="1" data-id="'.$id.'">Delete</sapn>';
                    echo '</div>';
                    echo '<input type="hidden" name="nutrition" value="'.$nutrition.'">';
                } else {
                    echo form_label(
                        'Nutrition',
                        'nutrition',
                        array(
                            'class' => 'col-sm-2 control-label'
                        )
                    );
                ?>
                <div class="col-sm-7">
                    <input type="file" name="nutrition">
                </div>
                <?php
                }  // end else
                ?>
            </div>
            <div class="form-group">
                <?php
                if (!empty($product_thumb)) {
                    // var_dump($product_img);
                    echo '<div class="col-sm-7 col-sm-offset-2">';
                    echo image_asset_att(
                        'product/'.$product_thumb,
                        array(
                            'class' => 'img-thumbnail img-responsive',
                            'style' => 'width: 150px;'
                        )
                    );
                    echo '</div>';
                    echo '<div class="col-sm-7 col-sm-offset-2">';
                    echo '<span class="btn btn-danger delimage" title="Delete Image" data-position="2" data-id="'.$id.'">Delete</sapn>';
                    echo '</div>';
                    echo '<input type="hidden" name="product_thumb" value="'.$product_thumb.'">';
                } else {
                    // var_dump("Not Have");
                    echo form_label(
                        'Product thumb',
                        'Product thumb',
                        array(
                            'class' => 'col-sm-2 control-label'
                        )
                    );
                ?>
                <div class="col-sm-7">
                    <input type="file" name="product_thumb">
                </div>
                <?php
                } // end if
                ?>
            </div>
            <div class="form-group">
                <?php
                if (!empty($product_img)) {
                    // var_dump($product_img);
                    echo '<div class="col-sm-7 col-sm-offset-2">';
                    echo image_asset_att(
                        'product/'.$product_img,
                        array(
                            'class' => 'img-thumbnail img-responsive',
                            'style' => 'width: 150px;'
                        )
                    );
                    echo '</div>';
                    echo '<div class="col-sm-7 col-sm-offset-2">';
                    echo '<span class="btn btn-danger delimage" title="Delete Image" data-position="3" data-id="'.$id.'">Delete</sapn>';
                    echo '</div>';
                    echo '<input type="hidden" name="product_img" value="'.$product_img.'">';
                } else {
                    // var_dump("Not Have");
                    echo form_label(
                        'Product img',
                        'Product img',
                        array(
                            'class' => 'col-sm-2 control-label'
                        )
                    );
                ?>
                <div class="col-sm-7">
                    <input type="file" name="product_img">
                </div>
                <?php
                } // end if
                ?>
            </div>
            <div class="form-group">
                <div class="col-sm-7 col-sm-offset-2">
                    <br><br>
                    <?php
                    echo form_submit(
                        'submit',
                        'submit',
                        array(
                            'class' => 'btn btn-primary'
                        )
                    );
                    ?>
                </div>
            </div>
            <?php
            echo form_close();
            ?>
        </div>
    </div>
