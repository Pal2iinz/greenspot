<!-- Begin page heading -->
<?php echo heading('Product Management',1,array('class' => 'page-heading'));?>
<!-- End page heading -->
<ol class="breadcrumb default square rsaquo sm">
   <li><a href="index.html"><i class="fa fa-home"></i></a></li>
   <li><a href="#fakelink">Product</a></li>
   <li class="active">Index</li>
</ol>
<?php
    if ($this->session->flashdata('msg-success')) {
?>
    <div class="alert alert-success" role="alert">
        <?php echo $this->session->flashdata('msg-success'); ?>
    </div>
<?php
} // end if msg
    if ($this->session->flashdata('msg-danger')) {
?>
    <div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert">x</button>
        <?php echo $this->session->flashdata('msg-danger'); ?>
    </div>
<?php
} // end if msg
?>

<div class='the-box'>
    <div class="row">
        <div class="col-md-12 table-responsive">
            <table class='table datatable'>
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Thumb</th>
                        <th>Product</th>
                        <th>Nutrition</th>
                        <th>Title</th>
                        <th>Formula</th>
                        <th>Categories</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if (isset($product)) {
                        $i = 1;
                        foreach ($product as $key => $p) {
                        ?>
                        <tr>
                            <td><?php echo $i; ?></td>
                            <td>
                                <?php
                                echo image_asset_att(
                                    'product/'.$p->product_thumb,
                                    array(
                                        'class' => 'img-thumbnail img-responsive',
                                        'style' => 'width: 100px;'
                                    )
                                );
                                ?>
                            </td>
                            <td>
                                <?php
                                echo image_asset_att(
                                    'product/'.$p->product_img,
                                    array(
                                        'class' => 'img-thumbnail img-responsive',
                                        'style' => 'width: 150px;'
                                    )
                                );
                                ?>
                            </td>
                            <td>
                                <?php
                                echo image_asset_att(
                                    'product/'.$p->nutrition,
                                    array(
                                        'class' => 'img-thumbnail img-responsive',
                                        'style' => 'width: 120px;'
                                    )
                                );
                                ?>
                            </td>
                            <td><?php echo $p->title; ?></td>
                            <td><?php echo $p->formula; ?></td>
                            <td>
                                <?php
                                if (isset($categories)) {
                                    foreach ($categories as $key => $c) {
                                        if ($p->category == $c->id) {
                                            echo $c->categories;
                                        } // end if
                                    } // end foreach
                                } // end if
                                ?>
                            </td>
                            <td>
                                <?php
                                if ($p->status == 1) {
                                    echo '<span class="label label-danger">Open</span>';
                                } else {
                                    echo '<span class="label label-default">Close</span>';
                                }
                                ?>
                            </td>
                            <td>
                                <div class="dropdown">
                                    <button class="btn btn-primary dropdown-toggle"
                                    type="button" id="dropdown" data-toggle="dropdown">
                                        <i class="fa fa-cog"></i>
                                    </button>
                                    <ul class="dropdown-menu" aria-labelledby="dropdown">
                                        <li>
                                            <?php
                                            $cl = array('class' => 'edit');
                                            echo anchor(
                                                'backend/product/form/'.$p->id,
                                                'Edit'
                                            );
                                            ?>
                                        </li>
                                        <li>
                                            <?php
                                            if ($p->status == 1) {
                                            ?>
                                            <a class="ac_close"
                                            data-id="<?php echo $p->id;?>">Close</a>
                                            <?php
                                            } else {
                                            ?>
                                            <a class="ac_open"
                                            data-id="<?php echo $p->id;?>">Open</a>
                                            <?php
                                            } // end if
                                            ?>
                                        </li>
                                        <li>
                                            <a class="delete"
                                            data-id="<?php echo $p->id;?>">
                                                Delete
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                        <?php
                        $i++;
                        }
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?php
                echo anchor('backend/product/form',
                    'Add Product',
                    array(
                        'class' => 'btn btn-primary'
                    )
                )
            ?>
        </div>
    </div>
