<?php
/**
* Parses and verifies the doc comments for file.
*
* PHP version 5.6
*
* @category  PHP
* @package   Digiday
* @author    Teerapuch Kassakul <teerapuch@hotmail.com>
* @copyright 2016 Digiday
* @license   http://opensource.org/licenses/mit-license.php MIT License
* @link      http://teerapuch.com
*/
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* Parses and verifies the doc comments for file.
*
* PHP version 5.6
*
* @category  PHP
* @package   Digiday
* @author    Teerapuch Kassakul <teerapuch@hotmail.com>
* @copyright 2016 Digiday
* @license   http://opensource.org/licenses/mit-license.php MIT License
* @link      http://teerapuch.com
*/
if (!function_exists('is_logged_in_admin')) {

    function is_logged_in_admin()
    {
        // Get current CodeIgniter instance
        $CI =& get_instance();
        // We need to use $CI->session instead of $this->session
        $isLoggedIn = $CI->session->userdata('logged_in_admin');
        if (!isset($isLoggedIn)) {
            redirect("/","refresh");
            return false;
        } else {
            return true;
        }
    }

}

if ( ! function_exists('is_logged_in_member')) {
    function is_logged_in_member()
    {
        // Get current CodeIgniter instance
        $CI =& get_instance();
        // We need to use $CI->session instead of $this->session
        $isLoggedIn = $CI->session->userdata('logged_in_member');
        if (!isset($isLoggedIn)) {
            return false;
        } else {
            return true;
        }
    }
}
/**
 *
 * @param $department int id of the department
 *
 * @return boolean
*/
if ( ! function_exists('checkDep')) {
    function checkDep($department)
    {
        $oCI =& get_instance();
        $dep = $oCI->session->userdata['logged_in_admin']['department'];
        if($dep != $department) {
            redirect("logout", "refresh");
        } // end if
    }
}

?>
