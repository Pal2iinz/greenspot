-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 21, 2017 at 07:09 PM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `greenspot`
--

-- --------------------------------------------------------

--
-- Table structure for table `access_tbl`
--

CREATE TABLE IF NOT EXISTS `access_tbl` (
  `id` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `perm` int(11) NOT NULL,
  `access` int(11) NOT NULL,
  `updatedate` datetime NOT NULL,
  `createdate` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `banner_tbl`
--

CREATE TABLE IF NOT EXISTS `banner_tbl` (
  `id` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `page` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `updatedate` datetime NOT NULL,
  `createdate` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `banner_tbl`
--

INSERT INTO `banner_tbl` (`id`, `image`, `url`, `page`, `status`, `updatedate`, `createdate`, `deleted`) VALUES
(1, 'banner_img_21052017061618.png', 'https://teerapuch.com', 3, 1, '2017-05-21 14:52:40', '2017-05-21 06:16:18', 0),
(2, 'banner_img_21052017130943.png', 'test cone', 1, 2, '2017-05-21 13:46:49', '2017-05-21 13:09:43', 0),
(4, 'banner_img_21052017134604.jpg', 'bbbbb', 2, 1, '2017-05-21 13:46:29', '0000-00-00 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `blog_tbl`
--

CREATE TABLE IF NOT EXISTS `blog_tbl` (
  `id` int(11) NOT NULL,
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `thumbnail` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `categories` int(11) NOT NULL,
  `subcat` int(1) NOT NULL,
  `post` blob NOT NULL,
  `updatedate` datetime NOT NULL,
  `createdate` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blog_tbl`
--

INSERT INTO `blog_tbl` (`id`, `title`, `image`, `thumbnail`, `categories`, `subcat`, `post`, `updatedate`, `createdate`, `deleted`) VALUES
(1, 'รู้จักกับ “ข้าวสีนิล”', 'blog_21052017185917.jpg', 'thumbnail_21052017185917.jpg', 1, 1, 0x3c70207873733d72656d6f7665643ee2809ce0b882e0b989e0b8b2e0b8a7e0b8aae0b8b5e0b899e0b8b4e0b8a5e2809d20e0b882e0b989e0b8b2e0b8a7e0b980e0b8a1e0b8a5e0b987e0b894e0b8aae0b8b5e0b8a1e0b988e0b8a7e0b887e0b894e0b8b3e0b8aae0b8a7e0b8a2e0b981e0b89be0b8a5e0b881e0b895e0b8b220e0b8a3e0b8aae0b8aae0b8b1e0b8a1e0b89ce0b8b1e0b8aae0b8abe0b899e0b8b6e0b89ae0b8abe0b899e0b8b1e0b89a20e0b983e0b884e0b8a3e0b888e0b8b0e0b8a3e0b8b9e0b989e0b8abe0b8a3e0b8b7e0b8ade0b984e0b8a1e0b988e0b8a7e0b988e0b8b2e0b980e0b89be0b987e0b899e0b8ade0b8b2e0b8abe0b8b2e0b8a3e0b8ade0b8b1e0b899e0b897e0b8a3e0b887e0b884e0b8b8e0b893e0b884e0b988e0b8b220e0b8a1e0b8b2e0b881e0b894e0b989e0b8a7e0b8a2e0b884e0b8b8e0b893e0b89be0b8a3e0b8b0e0b982e0b8a2e0b88ae0b899e0b98c20e0b981e0b896e0b8a1e0b8a2e0b8b1e0b887e0b981e0b89be0b8a3e0b8a3e0b8b9e0b89be0b984e0b894e0b989e0b8abe0b8a5e0b8b2e0b881e0b8abe0b8a5e0b8b2e0b8a220e0b980e0b8a3e0b8b2e0b8a1e0b8b2e0b8a3e0b8b9e0b989e0b888e0b8b1e0b881e0b980e0b888e0b989e0b8b2e0b882e0b8b2e0b8a7e0b8aae0b8b5e0b899e0b8b4e0b8a5e0b899e0b8b5e0b989e0b983e0b8abe0b989e0b8a1e0b8b2e0b881e0b881e0b8a7e0b988e0b8b2e0b980e0b894e0b8b4e0b8a1e0b881e0b8b1e0b899e0b884e0b988e0b8b03c2f703e3c70207873733d72656d6f7665643ee0b882e0b989e0b8b2e0b8a7e0b8aae0b8b5e0b899e0b8b4e0b8a5e0b8a5e0b8b1e0b881e0b8a9e0b893e0b8b0e0b980e0b894e0b988e0b89920e0b884e0b8b7e0b8ad20e0b8a1e0b8b5e0b8aae0b8b5e0b8a1e0b988e0b8a7e0b887e0b980e0b882e0b989e0b8a1e0b882e0b989e0b8b2e0b8a7e0b8aae0b8b5e0b8a1e0b988e0b8a7e0b887e0b980e0b882e0b989e0b8a1e0b888e0b899e0b980e0b881e0b8b7e0b8ade0b89ae0b894e0b8b320e0b88be0b8b6e0b988e0b887e0b980e0b89be0b987e0b899e0b8aae0b8b5e0b882e0b8ade0b887e0b8aae0b8b2e0b8a3e0b8a3e0b887e0b884e0b8a7e0b8b1e0b895e0b896e0b8b820e0b897e0b8b5e0b988e0b8ade0b8b8e0b894e0b8a1e0b984e0b89be0b894e0b989e0b8a7e0b8a220e2809ce0b981e0b8ade0b899e0b982e0b897e0b984e0b88be0b899e0b8b2e0b899e0b8b4e0b899e2809d20e0b981e0b8a5e0b8b020e2809ce0b982e0b89be0b8a3e0b981e0b8ade0b899e0b982e0b897e0b984e0b88be0b8a2e0b8b2e0b899e0b8b4e0b899e2809d20e0b897e0b8b5e0b988e0b8ade0b8a2e0b8b9e0b988e0b983e0b899e0b8aae0b988e0b8a7e0b899e0b882e0b8ade0b887e0b89be0b8a5e0b8b2e0b8a2e0b888e0b8a1e0b8b9e0b881e0b882e0b989e0b8b2e0b8a720e0b897e0b8b3e0b887e0b8b2e0b899e0b8a3e0b988e0b8a7e0b8a1e0b881e0b8b1e0b899e0b895e0b989e0b8b2e0b899e0b8ade0b899e0b8b8e0b8a1e0b8b9e0b8a5e0b8ade0b8b4e0b8aae0b8a3e0b8b020e0b982e0b894e0b8a2e0b89be0b881e0b895e0b8b420e2809ce0b981e0b8ade0b899e0b982e0b897e0b984e0b88be0b899e0b8b2e0b899e0b8b4e0b899e2809d20e0b981e0b8a5e0b8b020e2809ce0b982e0b89be0b8a3e0b981e0b8ade0b899e0b982e0b897e0b984e0b88be0b8a2e0b8b2e0b899e0b8b4e0b899e2809d20e0b888e0b8b0e0b89ee0b89ae0b8a1e0b8b2e0b881e0b983e0b899e0b89ee0b8b7e0b88ae0b8abe0b8a3e0b8b7e0b8ade0b89ce0b8a5e0b984e0b8a1e0b989e0b897e0b8b5e0b988e0b8a1e0b8b5e0b8aae0b8b5e0b8a1e0b988e0b8a7e0b887e0b8a2e0b8b4e0b988e0b887e0b8aae0b8b5e0b980e0b882e0b989e0b8a1e0b8a1e0b8b2e0b881e0b980e0b897e0b988e0b8b2e0b984e0b8abe0b8a3e0b988e0b881e0b987e0b888e0b8b0e0b8a2e0b8b4e0b988e0b887e0b8a1e0b8b5e0b8a4e0b897e0b898e0b8b4e0b98ce0b983e0b899e0b881e0b8b2e0b8a3e0b895e0b989e0b8b2e0b899e0b8ade0b899e0b8b8e0b8a1e0b8b9e0b8a5e0b8ade0b8b4e0b8aae0b8a3e0b8b0e0b984e0b894e0b989e0b894e0b8b5e0b882e0b8b6e0b989e0b899e0b895e0b8b2e0b8a1e0b8a5e0b8b3e0b894e0b8b1e0b89a20e0b981e0b895e0b988e0b888e0b8b2e0b881e0b8a7e0b8b4e0b888e0b8b1e0b8a2e0b89ee0b89ae0b8a7e0b988e0b8b220e0b983e0b899e0b882e0b989e0b8b2e0b8a7e0b8aae0b8b5e0b899e0b8b4e0b8a520e0b8a1e0b8b5e0b89be0b8a3e0b8b0e0b8aae0b8b4e0b897e0b898e0b8b4e0b8a0e0b8b2e0b89ee0b983e0b899e0b881e0b8b2e0b8a3e0b895e0b989e0b8b2e0b899e0b8ade0b899e0b8b8e0b8a1e0b8b9e0b8a5e0b8ade0b8b4e0b8aae0b8a3e0b8b0e0b984e0b894e0b989e0b894e0b8b5e0b881e0b8a7e0b988e0b8b2e0b89ce0b8a5e0b984e0b8a1e0b989e0b983e0b899e0b895e0b8a3e0b8b0e0b881e0b8b9e0b8a5e0b980e0b89ae0b8ade0b8a3e0b8b5e0b988e0b896e0b8b6e0b887203320e0b980e0b897e0b988e0b8b23c2f703e3c70207873733d72656d6f7665643ee0b981e0b8ade0b899e0b982e0b897e0b984e0b88be0b8a2e0b8b2e0b899e0b8b4e0b89920e0b897e0b8b3e0b887e0b8b2e0b899e0b984e0b894e0b989e0b894e0b8b5e0b881e0b8a7e0b988e0b8b220e0b8a7e0b8b4e0b895e0b8b2e0b8a1e0b8b4e0b899204520e0b896e0b8b6e0b887203520e0b980e0b897e0b988e0b8b220e0b88ae0b988e0b8a7e0b8a2e0b8a2e0b8b1e0b89ae0b8a2e0b8b1e0b989e0b887e0b8a3e0b8b4e0b989e0b8a7e0b8a3e0b8ade0b8a2e0b8ade0b8b1e0b899e0b980e0b881e0b8b4e0b894e0b881e0b988e0b8ade0b899e0b8a7e0b8b1e0b8a220e0b8aae0b8b2e0b8a3e0b8aae0b881e0b8b1e0b894e0b981e0b8ade0b899e0b982e0b897e0b984e0b88be0b8a2e0b8b2e0b899e0b8b4e0b899e0b980e0b89be0b987e0b899e0b8aae0b8b2e0b8a3e0b895e0b989e0b8b2e0b899e0b8ade0b899e0b8b8e0b8a1e0b8b9e0b8a5e0b8ade0b8b4e0b8aae0b8a3e0b8b02028416e74696f786964616e742920e0b88ae0b988e0b8a7e0b8a2e0b88ae0b8b0e0b8a5e0b8ade0b884e0b8a7e0b8b2e0b8a1e0b980e0b8aae0b8b7e0b988e0b8ade0b8a1e0b882e0b8ade0b887e0b980e0b88be0b8a5e0b8a5e0b98c20e0b8a3e0b8b2e0b8a2e0b887e0b8b2e0b899e0b881e0b8b2e0b8a3e0b8a7e0b8b4e0b888e0b8b1e0b8a2e0b983e0b899e0b89be0b8a3e0b8b0e0b980e0b897e0b8a8e0b88de0b8b5e0b988e0b89be0b8b8e0b988e0b899e0b89ee0b89ae0b8a7e0b988e0b8b2e0b983e0b899e0b8aae0b8b1e0b895e0b8a7e0b98ce0b897e0b894e0b8a5e0b8ade0b88720e0b981e0b8ade0b899e0b982e0b897e0b984e0b88be0b8a2e0b8b2e0b899e0b8b4e0b899e0b8aae0b8b2e0b8a1e0b8b2e0b8a3e0b896e0b881e0b8a3e0b8b0e0b895e0b8b8e0b989e0b899e0b983e0b8abe0b989e0b882e0b899e0b887e0b8ade0b881e0b881e0b8a5e0b8b1e0b89ae0b884e0b8b7e0b899e0b8a1e0b8b2e0b980e0b8a3e0b987e0b8a7e0b881e0b8a7e0b988e0b8b2e0b895e0b8b1e0b8a7e0b980e0b89be0b8a3e0b8b5e0b8a2e0b89ae0b980e0b897e0b8b5e0b8a2e0b89ae0b897e0b8b5e0b988e0b984e0b8a1e0b988e0b984e0b894e0b989e0b983e0b88ae0b989e0b8aae0b8b2e0b8a3e0b896e0b8b6e0b887e0b896e0b8b6e0b887e0b980e0b897e0b988e0b8b2e0b895e0b8b1e0b8a7e0b980e0b8a5e0b8a2e0b897e0b8b5e0b980e0b894e0b8b5e0b8a2e0b8a720e0b881e0b8b2e0b8a3e0b8a8e0b8b6e0b881e0b8a9e0b8b2e0b983e0b899e0b8abe0b8a5e0b8ade0b894e0b897e0b894e0b8a5e0b8ade0b887e0b8a2e0b8b7e0b899e0b8a2e0b8b1e0b899e0b8a7e0b988e0b8b220e0b8aae0b8b2e0b8a3e0b981e0b8ade0b899e0b982e0b897e0b984e0b88be0b8a2e0b8b2e0b899e0b8b4e0b89920e0b881e0b8a3e0b8b0e0b895e0b8b8e0b989e0b899e0b983e0b8abe0b989e0b980e0b88be0b8a5e0b8a5e0b98ce0b8a3e0b8b2e0b881e0b89ce0b8a1e0b8aae0b8a3e0b989e0b8b2e0b887e0b89ce0b8a1e0b8a1e0b8b2e0b881e0b882e0b8b6e0b989e0b899e0b896e0b8b6e0b887203320e0b980e0b897e0b988e0b8b220e0b899e0b8ade0b881e0b888e0b8b2e0b881e0b888e0b8b0e0b88ae0b988e0b8a7e0b8a2e0b881e0b8a3e0b8b0e0b895e0b8b8e0b989e0b899e0b983e0b8abe0b989e0b89ce0b8a1e0b8a1e0b8b5e0b8aae0b8b8e0b882e0b8a0e0b8b2e0b89ee0b894e0b8b5e0b981e0b8a5e0b989e0b8a720e0b8a2e0b8b1e0b887e0b88ae0b988e0b8a7e0b8a2e0b983e0b8abe0b989e0b89ce0b8b4e0b8a7e0b8abe0b899e0b8b1e0b887e0b894e0b8b9e0b8ade0b988e0b8ade0b899e0b881e0b8a7e0b988e0b8b2e0b8a7e0b8b1e0b8a2e0b981e0b8a5e0b8b0e0b8a5e0b894e0b884e0b8a7e0b8b2e0b8a1e0b980e0b8aae0b8b5e0b8a2e0b8abe0b8b2e0b8a2e0b882e0b8ade0b887e0b89ce0b8b4e0b8a7e0b8abe0b899e0b8b1e0b887e0b897e0b8b5e0b988e0b980e0b881e0b8b4e0b894e0b888e0b8b2e0b881e0b8ade0b899e0b8b8e0b8a1e0b8b9e0b8a5e0b8ade0b8b4e0b8aae0b8a3e0b8b020e0b88be0b8b6e0b988e0b887e0b8aae0b8b2e0b980e0b8abe0b895e0b8b8e0b8abe0b8a5e0b8b1e0b881e0b8a1e0b8b2e0b888e0b8b2e0b881e0b981e0b8aae0b887e0b8ade0b8b1e0b8a5e0b895e0b8a3e0b989e0b8b2e0b984e0b8a7e0b982e0b8ade0b980e0b8a5e0b8972028e0b981e0b8aae0b887e0b981e0b894e0b894e0b981e0b8a5e0b8b0e0b984e0b89fe0b899e0b8b4e0b8ade0b8ade0b899293c2f703e3c70207873733d72656d6f7665643ee0b982e0b894e0b8a2e0b983e0b899e0b881e0b8b2e0b8a3e0b897e0b894e0b8a5e0b8ade0b887e0b980e0b894e0b8b5e0b8a2e0b8a7e0b881e0b8b1e0b899e0b89ee0b89ae0b8a7e0b988e0b8b220e0b8aae0b8b2e0b8a3e0b981e0b8ade0b899e0b982e0b897e0b984e0b88be0b8a2e0b8b2e0b899e0b8b4e0b899e0b980e0b8a1e0b8b7e0b988e0b8ade0b983e0b88ae0b989e0b8a3e0b988e0b8a7e0b8a1e0b881e0b8b1e0b89ae0b8a7e0b8b4e0b895e0b8b2e0b8a1e0b8b4e0b899e0b8ade0b8b5e0b888e0b8b0e0b88ae0b988e0b8a7e0b8a2e0b983e0b8abe0b989e0b881e0b8b2e0b8a3e0b897e0b8b3e0b887e0b8b2e0b899e0b882e0b8ade0b887e0b8a1e0b8b1e0b899e0b894e0b8b5e0b882e0b8b6e0b989e0b899e0b980e0b89ee0b8a3e0b8b2e0b8b020e0b8aae0b8b2e0b8a320416e74686f6379616e696e20e0b888e0b8b0e0b88ae0b988e0b8a7e0b8a2e0b89be0b989e0b8ade0b887e0b881e0b8b1e0b899e0b984e0b8a1e0b988e0b983e0b8abe0b989e0b89ce0b8b4e0b8a7e0b8abe0b899e0b8b1e0b887e0b984e0b8a1e0b988e0b983e0b8abe0b989e0b89be0b8a5e0b894e0b89be0b8a5e0b988e0b8ade0b8a2e0b8ade0b8b4e0b8a5e0b8b2e0b8aae0b895e0b8b4e0b899e0b980e0b89be0b987e0b899e0b89ce0b8a5e0b983e0b8abe0b989e0b89ce0b8b4e0b8a7e0b8abe0b899e0b8b1e0b887e0b984e0b8a1e0b988e0b8aae0b8b9e0b88de0b980e0b8aae0b8b5e0b8a2e0b884e0b8a7e0b8b2e0b8a1e0b8a2e0b8b7e0b894e0b8abe0b8a2e0b8b8e0b988e0b89920e0b894e0b8b1e0b887e0b899e0b8b1e0b989e0b89920e0b980e0b8a3e0b8b2e0b888e0b8b0e0b884e0b887e0b884e0b8a7e0b8b2e0b8a1e0b8abe0b899e0b8b8e0b988e0b8a12de0b8aae0b8b2e0b8a7e0b8ade0b988e0b8ade0b899e0b881e0b8a7e0b988e0b8b2e0b8a7e0b8b1e0b8a220e0b894e0b8b9e0b894e0b8b520e0b980e0b8ade0b8b2e0b984e0b8a7e0b989e0b984e0b894e0b989e0b8ade0b8a2e0b988e0b8b2e0b887e0b8a2e0b8b2e0b8a7e0b899e0b8b2e0b899e0b8a2e0b8b4e0b988e0b887e0b882e0b8b6e0b989e0b899e0b899e0b8b1e0b988e0b899e0b980e0b8ade0b8873c2f703e3c70207873733d72656d6f7665643ee0b8a3e0b8b9e0b989e0b8ade0b8a2e0b988e0b8b2e0b887e0b899e0b8b5e0b989e0b981e0b8a5e0b989e0b8a7e0b884e0b887e0b895e0b989e0b8ade0b887e0b8a3e0b8b5e0b89ae0b984e0b89be0b8a5e0b8ade0b887e0b8abe0b8b2e0b8a1e0b8b2e0b8a3e0b8b1e0b89ae0b89be0b8a3e0b8b0e0b897e0b8b2e0b899e0b894e0b8b9e0b981e0b8a5e0b989e0b8a720e0b888e0b8b0e0b984e0b894e0b989e0b8abe0b899e0b989e0b8b2e0b983e0b8aa20e0b89ce0b8b4e0b8a7e0b980e0b894e0b989e0b887e0b881e0b988e0b8ade0b899e0b983e0b884e0b8a320e0b981e0b895e0b988e0b896e0b989e0b8b2e0b984e0b8a1e0b988e0b8a3e0b8b9e0b989e0b888e0b8b0e0b984e0b89be0b8abe0b8b2e0b897e0b8b2e0b899e0b888e0b8b2e0b881e0b984e0b8abe0b89920e0b881e0b987e0b8aae0b8b2e0b8a1e0b8b2e0b8a3e0b896e0b8abe0b8b2e0b980e0b884e0b8a3e0b8b7e0b988e0b8ade0b887e0b894e0b8b7e0b988e0b8a1e0b897e0b8b5e0b988e0b8a1e0b8b5e0b8aae0b988e0b8a7e0b899e0b89ce0b8aae0b8a1e0b882e0b8ade0b887e0b887e0b8b2e0b894e0b8b3e0b981e0b8a5e0b8b0e0b882e0b989e0b8b2e0b8a7e0b8aae0b8b5e0b899e0b8b4e0b8a5e0b881e0b987e0b8ade0b8a3e0b988e0b8ade0b8a2e0b981e0b8a5e0b8b0e0b884e0b8b8e0b893e0b884e0b988e0b8b2e0b897e0b8b2e0b887e0b8aae0b8b2e0b8a3e0b8ade0b8b2e0b8abe0b8b2e0b8a3e0b984e0b8a1e0b988e0b895e0b988e0b8b2e0b887e0b881e0b8b1e0b899e0b980e0b8a5e0b8a2e0b884e0b988e0b8b03c2f703e3c70207873733d72656d6f7665643ee0b882e0b8ade0b89ae0b884e0b8b8e0b893e0b882e0b989e0b8ade0b8a1e0b8b9e0b8a5e0b888e0b8b2e0b88120e0b89ce0b8a82ee0b894e0b8a32ee0b89ee0b8b4e0b8a1e0b89ee0b98ce0b980e0b89ee0b987e0b88d20e0b89ee0b8a3e0b980e0b889e0b8a5e0b8b4e0b8a1e0b89ee0b887e0b8a8e0b98c2c20e0b8a8e0b8b2e0b8aae0b895e0b8a3e0b8b2e0b888e0b8b2e0b8a3e0b8a2e0b98ce0b980e0b881e0b8b5e0b8a2e0b8a3e0b895e0b8b4e0b884e0b8b8e0b8932c20e0b894e0b8a32ee0b899e0b8b4e0b898e0b8b4e0b8a2e0b8b220e0b8a3e0b8b1e0b895e0b899e0b8b2e0b89be0b899e0b899e0b897e0b98c2c2073696e696e726963652e636f6d3c2f703e, '2017-05-21 18:59:17', '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `categories_tbl`
--

CREATE TABLE IF NOT EXISTS `categories_tbl` (
  `id` int(11) NOT NULL,
  `categories` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `updatedate` datetime NOT NULL,
  `createdate` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories_tbl`
--

INSERT INTO `categories_tbl` (`id`, `categories`, `updatedate`, `createdate`, `deleted`) VALUES
(1, 'vitamilk', '2017-05-09 18:53:44', '2017-05-09 18:53:44', 0),
(2, 'vsoy', '2017-05-09 19:11:25', '2017-05-09 19:11:25', 0),
(3, 'greenspot', '2017-05-09 20:42:52', '2017-05-09 19:15:27', 0);

-- --------------------------------------------------------

--
-- Table structure for table `content_tbl`
--

CREATE TABLE IF NOT EXISTS `content_tbl` (
  `id` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `page` int(11) NOT NULL,
  `updatedate` datetime NOT NULL,
  `createdate` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `content_tbl`
--

INSERT INTO `content_tbl` (`id`, `image`, `content`, `page`, `updatedate`, `createdate`, `deleted`) VALUES
(1, 'content_img_21052017143610.png', '<h3 xss="removed">น้ำส้มกรีนสปอตเป็นเครื่องดื่มให้ความสดชื่น ที่มีคุณภาพ ทำจากน้ำส้มแท้ ไม่อัดลม เหมาะสำหรับดับกระหาย ให้ความสดชื่น</h3><p xss="removed">สำหรับผลิตภัณฑ์ของกรีนสปอต เมื่อนำมาผสมให้เป็น Cocktail หรือ Mocktail นั้นจะให้รสชาติที่อร่อย แปลกใหม่ ไม่จำเจ</p>', 1, '2017-05-21 14:38:33', '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `department_tbl`
--

CREATE TABLE IF NOT EXISTS `department_tbl` (
  `id` int(11) NOT NULL,
  `department` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `updatedate` datetime NOT NULL,
  `createdate` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `department_tbl`
--

INSERT INTO `department_tbl` (`id`, `department`, `updatedate`, `createdate`, `deleted`) VALUES
(1, 'IT', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(2, 'Marketing', '2017-05-06 17:28:33', '2017-05-06 17:28:33', 0);

-- --------------------------------------------------------

--
-- Table structure for table `hero_tbl`
--

CREATE TABLE IF NOT EXISTS `hero_tbl` (
  `id` int(11) NOT NULL,
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hero` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `page` int(11) NOT NULL,
  `position` tinyint(2) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `updatedate` datetime NOT NULL,
  `createdate` datetime NOT NULL,
  `deleted` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `hero_tbl`
--

INSERT INTO `hero_tbl` (`id`, `title`, `hero`, `page`, `position`, `status`, `updatedate`, `createdate`, `deleted`) VALUES
(1, 'Test', 'hero_19052017181255.png', 1, 1, 1, '2017-05-19 18:49:41', '2017-05-19 18:12:55', 0),
(2, 'Second Hero', 'hero_19052017213502.png', 4, 1, 2, '2017-05-21 14:44:24', '2017-05-19 18:41:04', 0);

-- --------------------------------------------------------

--
-- Table structure for table `permission_tbl`
--

CREATE TABLE IF NOT EXISTS `permission_tbl` (
  `id` int(11) NOT NULL,
  `department` int(11) NOT NULL,
  `permission` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `updatedate` datetime NOT NULL,
  `createdate` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_tbl`
--

INSERT INTO `permission_tbl` (`id`, `department`, `permission`, `updatedate`, `createdate`, `deleted`) VALUES
(1, 1, 'add user', '2017-05-06 00:00:00', '2017-05-06 00:00:00', 0),
(2, 1, 'edit user', '2017-05-06 17:27:11', '2017-05-06 17:27:11', 0),
(3, 1, 'delete user', '2017-05-06 17:27:19', '2017-05-06 17:27:19', 0);

-- --------------------------------------------------------

--
-- Table structure for table `product_tbl`
--

CREATE TABLE IF NOT EXISTS `product_tbl` (
  `id` int(11) NOT NULL,
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `formula` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `benefit` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_thumb` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_img` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nutrition` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` int(11) NOT NULL,
  `note` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(2) NOT NULL,
  `updatedate` datetime NOT NULL,
  `createdate` datetime NOT NULL,
  `deleted` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_tbl`
--

INSERT INTO `product_tbl` (`id`, `title`, `formula`, `benefit`, `product_thumb`, `product_img`, `nutrition`, `category`, `note`, `status`, `updatedate`, `createdate`, `deleted`) VALUES
(1, 'ไวตามิ้ลค์', 'สูตรออริจินัล', '<p>มีโอเมก้า 3,6,9 และวิตามินบี12 ที่มีส่วนช่วยในระบบประสาทและสมอง</p><p>โปรตีนจากถั่วเหลืองทั้งเมล็ด เป็นแหล่งโปรตีนธรรมชาติ</p>', 'product_thumb_20052017172420.png', 'product_img_20052017172420.png', 'nutrition_20052017172420.png', 1, '', 1, '2017-05-20 17:24:20', '2017-05-20 17:24:20', 0);

-- --------------------------------------------------------

--
-- Table structure for table `promotion_tbl`
--

CREATE TABLE IF NOT EXISTS `promotion_tbl` (
  `id` int(11) NOT NULL,
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` tinyint(1) NOT NULL,
  `status` tinyint(1) NOT NULL COMMENT 'สถานะเปิดปิด',
  `updatedate` datetime NOT NULL,
  `createdate` datetime NOT NULL,
  `deleted` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `promotion_tbl`
--

INSERT INTO `promotion_tbl` (`id`, `title`, `image`, `url`, `type`, `status`, `updatedate`, `createdate`, `deleted`) VALUES
(1, 'Title Promotion 1', 'promotion_19052017212335.jpg', '#', 1, 1, '2017-05-19 21:24:21', '2017-05-14 07:50:35', 0),
(2, 'ขวดใหม่ ต้องลอง55', 'promotion_14052017143422.jpg', 'https://www.youtube.com/watch?v=hNVKo9Kb2mM', 1, 2, '2017-05-21 13:48:45', '2017-05-14 14:34:22', 0);

-- --------------------------------------------------------

--
-- Table structure for table `role_tbl`
--

CREATE TABLE IF NOT EXISTS `role_tbl` (
  `id` int(11) NOT NULL,
  `role` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `updatedate` datetime NOT NULL,
  `createdate` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_tbl`
--

INSERT INTO `role_tbl` (`id`, `role`, `updatedate`, `createdate`, `deleted`) VALUES
(1, 'Manager', '2017-05-06 17:26:32', '2017-05-06 17:26:16', 0),
(2, 'Operator', '2017-05-06 17:26:48', '2017-05-06 17:26:48', 0);

-- --------------------------------------------------------

--
-- Table structure for table `social_tbl`
--

CREATE TABLE IF NOT EXISTS `social_tbl` (
  `id` int(11) NOT NULL,
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(2) NOT NULL,
  `updatedate` datetime NOT NULL,
  `createdate` datetime NOT NULL,
  `deleted` tinyint(4) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `social_tbl`
--

INSERT INTO `social_tbl` (`id`, `title`, `details`, `image`, `url`, `status`, `updatedate`, `createdate`, `deleted`) VALUES
(1, 'สดชื่นราคาสบาย', 'พ้นช่วงกลางเดือนมาต้องบริหารเรื่องเงินกันซะหน่อย ใครมีเคล็บลับที่ว่าเวิร์ค! มาแชร์กัน #แต่ถ้าอยากสดชื่นราคาสบายกระเป๋าต้องไวตามิ้ลค์นะคร้าบ', 'social_19052017200241.jpg', 'https://www.facebook.com/VitamilkThailand/photos/a.104365162422.107180.93893262422/10154570308502423/?type=3', 1, '2017-05-19 20:13:59', '2017-05-19 20:02:41', 0),
(2, 'เงยหน้าแล้วไปต่อ', 'เพราะสิ้นเดือนนั้นยังอีกไกล แต่ยังไงก็ต้อง… #เงยหน้าแล้วไปต่อ', 'social_19052017210225.jpg', 'https://www.facebook.com/VitamilkThailand/photos/a.104365162422.107180.93893262422/10154570305387423/?type=3&theater', 1, '2017-05-19 21:12:35', '2017-05-19 20:11:37', 0),
(3, 'บิงโก', 'ไหนมีใครบิงโกบ้าง รายงานตัวหน่อยยย หรือถ้าใครดับเบิ้ลบิงโก ก็โชว์มาใต้คอมเม้นท์นี้เลย! #วันนี้ใครยังไม่บิงโกลองเติมความสดชื่นด้วยไวตามิ้ลค์สิ', 'social_19052017210415.jpg', 'https://www.facebook.com/VitamilkThailand/photos/a.104365162422.107180.93893262422/10154563042342423/?type=3&theater', 1, '2017-05-19 21:08:36', '2017-05-19 21:04:15', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_tbl`
--

CREATE TABLE IF NOT EXISTS `user_tbl` (
  `id` int(11) NOT NULL,
  `firstname` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `department` int(2) NOT NULL,
  `role` int(2) NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ipaddress` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `updatedate` datetime NOT NULL,
  `createdate` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_tbl`
--

INSERT INTO `user_tbl` (`id`, `firstname`, `lastname`, `email`, `password`, `department`, `role`, `avatar`, `ipaddress`, `updatedate`, `createdate`, `deleted`) VALUES
(1, 'teerapuch', 'kassakul', 'admin@mail.com', '$2y$12$PXBfhI7.fPuIRb4lCMYSmObEupd4XK1a9wz6DP5WRI/RRvVsHn7jO', 1, 1, 'avatar_14052017074630.jpg', '::1', '2017-05-14 07:46:30', '2017-05-06 00:00:00', 0),
(2, 'Mana', 'Pual', 'teerapuch@mail.com', '$2y$12$i1IPBx7n7NkowdGJI8SUPOb7C6xrI.sd4L4NMHSFMFu/P0zTefY1y', 1, 2, '', '', '2017-05-14 07:09:36', '2017-05-14 07:09:36', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `access_tbl`
--
ALTER TABLE `access_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banner_tbl`
--
ALTER TABLE `banner_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog_tbl`
--
ALTER TABLE `blog_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories_tbl`
--
ALTER TABLE `categories_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `content_tbl`
--
ALTER TABLE `content_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `department_tbl`
--
ALTER TABLE `department_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hero_tbl`
--
ALTER TABLE `hero_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permission_tbl`
--
ALTER TABLE `permission_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_tbl`
--
ALTER TABLE `product_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `promotion_tbl`
--
ALTER TABLE `promotion_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_tbl`
--
ALTER TABLE `role_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `social_tbl`
--
ALTER TABLE `social_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_tbl`
--
ALTER TABLE `user_tbl`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `access_tbl`
--
ALTER TABLE `access_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `banner_tbl`
--
ALTER TABLE `banner_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `blog_tbl`
--
ALTER TABLE `blog_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `categories_tbl`
--
ALTER TABLE `categories_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `content_tbl`
--
ALTER TABLE `content_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `department_tbl`
--
ALTER TABLE `department_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `hero_tbl`
--
ALTER TABLE `hero_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `permission_tbl`
--
ALTER TABLE `permission_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `product_tbl`
--
ALTER TABLE `product_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `promotion_tbl`
--
ALTER TABLE `promotion_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `role_tbl`
--
ALTER TABLE `role_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `social_tbl`
--
ALTER TABLE `social_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `user_tbl`
--
ALTER TABLE `user_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
