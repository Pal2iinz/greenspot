$(document).ready(function(){
  $( ".career img" ).addClass('img-responsive ');
  $( ".career img" ).each(function(){
    var $this = $(this); 
    $this.wrap('<a href="' + $this.attr('src') + '" data-lightbox="' + $this.attr('src') + '" ></a>');
	});
	$('.detail').on('click',function(){
		var id = $(this).attr('data-id');
		var aData = getDataJob(id);
		aData.done(function( data ) {
				$("#id").val(data[0].id);
				$("#content").html(data[0].content);
		});
	});  

		function getDataJob(id) {
          var request = $.ajax({
            url: "jobcareer/getdatajob/"+id,
            type: "POST",
            data: { id  : id},
            dataType: "json"
          });
            return request;
        }
});