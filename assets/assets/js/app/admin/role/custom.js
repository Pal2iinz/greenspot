$('document').ready(function(){
    $('.datatable').dataTable();
});

// add new case
$("#addNew").click(function(){
	$("#role").val('');
    $("#modal").modal('show');
});

$('.edit').click(function(){
	// get data from edit btn
    var id = $(this).attr('data-id');
    var role = $(this).attr('data-r');
    // set value to modal
    $("#id").val(id);
    $("#role").val(role);
	// show modal
    $("#modal").modal('show');
});

// delete case
$('.delete').click(function(){
    // get value from table
    var id   = $(this).attr("data-id");
    // create form json
    var formData = {id:id};
    // check user confirm
    var cf = confirm("คุณต้องการลบรายการนี้ออกจากระบบ");
    if(cf == true){
        // ajax post
        var request = $.ajax({
                    url: "role/delete",
                    type: "POST",
                    data: formData,
                    success: function( data, textStatus, jQxhr ){
                        location.reload();
                    },
                    error: function( jqXhr, textStatus, errorThrown ){
                        console.log( errorThrown );
                    }
                }); // end ajax
    } else {
        console.log('cancle');
    } // end else
}); // end delete
