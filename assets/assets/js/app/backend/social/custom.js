$('document').ready(function(){
  $('.datatable').dataTable();
});
// open case
$('.open').click(function(){
  // get value from table
  var id   = $(this).attr("data-id");
  // create form json
  var formData = {id:id};
  // ajax post
  var request = $.ajax({
    url: "open",
    type: "POST",
    data: formData,
    success: function( data, textStatus, jQxhr ){
      location.reload();
    },
    error: function( jqXhr, textStatus, errorThrown ){
      console.log( errorThrown );
      }
    }); // end ajax
}); // end open
// close case
$('.closed').click(function(){
  // get value from table
  var id   = $(this).attr("data-id");
  // create form json
  var formData = {id:id};
  // ajax post
  var request = $.ajax({
    url: "close",
    type: "POST",
    data: formData,
    success: function( data, textStatus, jQxhr ){
      location.reload();
      },
      error: function( jqXhr, textStatus, errorThrown ){
        console.log( errorThrown );
      }
    }); // end ajax
}); // end close
// delete case
$('.delete').click(function(){
  // get value from table
  var id   = $(this).attr("data-id");
	// create form json
  var formData = {id:id};
  // check user confirm
  var cf = confirm("คุณต้องการลบรายการนี้ออกจากระบบ");
  if(cf == true){
    // ajax post
    var request = $.ajax({
        url: "delete",
        type: "POST",
        data: formData,
        success: function( data, textStatus, jQxhr ){
          location.reload();
        },
        error: function( jqXhr, textStatus, errorThrown ){
          console.log( errorThrown );
        }
    }); // end ajax
  } else {
    console.log('cancle');
  } // end else
}); // end delete
