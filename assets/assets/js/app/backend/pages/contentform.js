$('document').ready(function(){
  $('input, textarea, select')
  .not('input[type=checkbox]')
  .not('input[type=submit]')
  .not('input[type=radio]')
  .addClass('form-control');
  $('#content').summernote({
      height: 200,
  });
});

// delete Photo case
$('.delimage').click(function(){
  // get value from table
  var id   = $(this).attr("data-id");
  // create form json
  var formData = {id:id};
  console.log(formData);
  // check user confirm
  var cf = confirm("คุณต้องการลบรูปภาพนี้ออกจากระบบ");
  if(cf == true){
    // ajax post
    var request = $.ajax({
        url: "../deleteimage",
        type: "POST",
        data: formData,
        success: function( data, textStatus, jQxhr ){
          location.reload();
        },
        error: function( jqXhr, textStatus, errorThrown ){
          console.log( errorThrown );
        }
    }); // end ajax
  } else {
    console.log('cancle');
  } // end else
}); // end delete




// delete Photo case
$('.delfile').click(function(){
  // get value from table
  var id   = $(this).attr("data-id");
  // create form json
  var formData = {id:id};
  // check user confirm
  var cf = confirm("คุณต้องการลบไฟล์นี้ออกจากระบบ");
  if(cf == true){
    // ajax post
    var request = $.ajax({
        url: "../delfile",
        type: "POST",
        data: formData,
        success: function( data, textStatus, jQxhr ){
          location.reload();
        },
        error: function( jqXhr, textStatus, errorThrown ){
          console.log( errorThrown );
        }
    }); // end ajax
  } else {
    console.log('cancle');
  } // end else
}); // end delete
