$('document').ready(function(){
  $('input, textarea, select')
  .not('input[type=checkbox]')
  .not('input[type=submit]')
  .not('input[type=radio]')
  .addClass('form-control');
});

// open hero case
$('.openhero').click(function(){
  // get value from table
  var id   = $(this).attr("data-id");
  // create form json
  var formData = {id:id};
  // ajax post
  var request = $.ajax({
    url: "../backend/hero/open",
    type: "POST",
    data: formData,
    success: function( data, textStatus, jQxhr ){
      location.reload();
    },
    error: function( jqXhr, textStatus, errorThrown ){
      console.log( errorThrown );
      }
    }); // end ajax
}); // end open

// close hero case
$('.closehero').click(function(){
  // get value from table
  var id  = $(this).attr("data-id");
  // create form json
  var formData = {id:id};
  // ajax post
  var request = $.ajax({
    url: "../backend/hero/close",
    type: "POST",
    data: formData,
    success: function( data, textStatus, jQxhr ){
      location.reload();
      },
      error: function( jqXhr, textStatus, errorThrown ){
        console.log( errorThrown );
      }
    }); // end ajax
}); // end close

// delete hero case
$('.delete').click(function(){
  // get value from table
  var id   = $(this).attr("data-id");
	// create form json
  var formData = {id:id};
  // check user confirm
  var cf = confirm("คุณต้องการลบรายการนี้ออกจากระบบ");
  if(cf == true){
    // ajax post
    var request = $.ajax({
        url: "delete",
        type: "POST",
        data: formData,
        success: function( data, textStatus, jQxhr ){
          location.reload();
        },
        error: function( jqXhr, textStatus, errorThrown ){
          console.log( errorThrown );
        }
    }); // end ajax
  } else {
    console.log('cancle');
  } // end else
}); // end delete


// delete Photo case
$('.delimage').click(function(){
  // get value from table
  var id   = $(this).attr("data-id");
	// create form json
  var formData = {id:id};
  // check user confirm
  var cf = confirm("คุณต้องการลบรูปภาพนี้ออกจากระบบ");
  if(cf == true){
    // ajax post
    var request = $.ajax({
        url: "../delimage",
        type: "POST",
        data: formData,
        success: function( data, textStatus, jQxhr ){
          location.reload();
        },
        error: function( jqXhr, textStatus, errorThrown ){
          console.log( errorThrown );
        }
    }); // end ajax
  } else {
    console.log('cancle');
  } // end else
}); // end delete
