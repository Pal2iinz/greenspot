var _gsScope = "undefined" != typeof module && module.exports && "undefined" != typeof global ? global : this || window;
(_gsScope._gsQueue || (_gsScope._gsQueue = [])).push(function() {
        "use strict";
        _gsScope._gsDefine("TweenMax", ["core.Animation", "core.SimpleTimeline", "TweenLite"], function(t, e, i) {
                var n = function(t) {
                        var e, i = [],
                            n = t.length;
                        for (e = 0; e !== n; i.push(t[e++]));
                        return i
                    },
                    s = function(t, e, n) {
                        i.call(this, t, e, n), this._cycle = 0, this._yoyo = this.vars.yoyo === !0, this._repeat = this.vars.repeat || 0, this._repeatDelay = this.vars.repeatDelay || 0, this._dirty = !0, this.render = s.prototype.render
                    },
                    r = 1e-10,
                    o = i._internals,
                    a = o.isSelector,
                    l = o.isArray,
                    h = s.prototype = i.to({}, .1, {}),
                    u = [];
                s.version = "1.14.2", h.constructor = s, h.kill()._gc = !1, s.killTweensOf = s.killDelayedCallsTo = i.killTweensOf, s.getTweensOf = i.getTweensOf, s.lagSmoothing = i.lagSmoothing, s.ticker = i.ticker, s.render = i.render, h.invalidate = function() {
                    return this._yoyo = this.vars.yoyo === !0, this._repeat = this.vars.repeat || 0, this._repeatDelay = this.vars.repeatDelay || 0, this._uncache(!0), i.prototype.invalidate.call(this)
                }, h.updateTo = function(t, e) {
                    var n, s = this.ratio,
                        r = this.vars.immediateRender || t.immediateRender;
                    e && this._startTime < this._timeline._time && (this._startTime = this._timeline._time, this._uncache(!1), this._gc ? this._enabled(!0, !1) : this._timeline.insert(this, this._startTime - this._delay));
                    for (n in t) this.vars[n] = t[n];
                    if (this._initted || r)
                        if (e) this._initted = !1;
                        else if (this._gc && this._enabled(!0, !1), this._notifyPluginsOfEnabled && this._firstPT && i._onPluginEvent("_onDisable", this), this._time / this._duration > .998) {
                        var o = this._time;
                        this.render(0, !0, !1), this._initted = !1, this.render(o, !0, !1)
                    } else if (this._time > 0 || r) {
                        this._initted = !1, this._init();
                        for (var a, l = 1 / (1 - s), h = this._firstPT; h;) a = h.s + h.c, h.c *= l, h.s = a - h.c, h = h._next
                    }
                    return this
                }, h.render = function(t, e, i) {
                    this._initted || 0 === this._duration && this.vars.repeat && this.invalidate();
                    var n, s, a, l, h, c, p, d, f = this._dirty ? this.totalDuration() : this._totalDuration,
                        m = this._time,
                        _ = this._totalTime,
                        g = this._cycle,
                        v = this._duration,
                        y = this._rawPrevTime;
                    if (t >= f ? (this._totalTime = f, this._cycle = this._repeat, this._yoyo && 0 !== (1 & this._cycle) ? (this._time = 0, this.ratio = this._ease._calcEnd ? this._ease.getRatio(0) : 0) : (this._time = v, this.ratio = this._ease._calcEnd ? this._ease.getRatio(1) : 1), this._reversed || (n = !0, s = "onComplete"), 0 === v && (this._initted || !this.vars.lazy || i) && (this._startTime === this._timeline._duration && (t = 0), (0 === t || 0 > y || y === r) && y !== t && (i = !0, y > r && (s = "onReverseComplete")), this._rawPrevTime = d = !e || t || y === t ? t : r)) : 1e-7 > t ? (this._totalTime = this._time = this._cycle = 0, this.ratio = this._ease._calcEnd ? this._ease.getRatio(0) : 0, (0 !== _ || 0 === v && y > 0 && y !== r) && (s = "onReverseComplete", n = this._reversed), 0 > t && (this._active = !1, 0 === v && (this._initted || !this.vars.lazy || i) && (y >= 0 && (i = !0), this._rawPrevTime = d = !e || t || y === t ? t : r)), this._initted || (i = !0)) : (this._totalTime = this._time = t, 0 !== this._repeat && (l = v + this._repeatDelay, this._cycle = this._totalTime / l >> 0, 0 !== this._cycle && this._cycle === this._totalTime / l && this._cycle--, this._time = this._totalTime - this._cycle * l, this._yoyo && 0 !== (1 & this._cycle) && (this._time = v - this._time), this._time > v ? this._time = v : 0 > this._time && (this._time = 0)), this._easeType ? (h = this._time / v, c = this._easeType, p = this._easePower, (1 === c || 3 === c && h >= .5) && (h = 1 - h), 3 === c && (h *= 2), 1 === p ? h *= h : 2 === p ? h *= h * h : 3 === p ? h *= h * h * h : 4 === p && (h *= h * h * h * h), this.ratio = 1 === c ? 1 - h : 2 === c ? h : .5 > this._time / v ? h / 2 : 1 - h / 2) : this.ratio = this._ease.getRatio(this._time / v)), m === this._time && !i && g === this._cycle) return void(_ !== this._totalTime && this._onUpdate && (e || this._onUpdate.apply(this.vars.onUpdateScope || this, this.vars.onUpdateParams || u)));
                    if (!this._initted) {
                        if (this._init(), !this._initted || this._gc) return;
                        if (!i && this._firstPT && (this.vars.lazy !== !1 && this._duration || this.vars.lazy && !this._duration)) return this._time = m, this._totalTime = _, this._rawPrevTime = y, this._cycle = g, o.lazyTweens.push(this), void(this._lazy = [t, e]);
                        this._time && !n ? this.ratio = this._ease.getRatio(this._time / v) : n && this._ease._calcEnd && (this.ratio = this._ease.getRatio(0 === this._time ? 0 : 1))
                    }
                    for (this._lazy !== !1 && (this._lazy = !1), this._active || !this._paused && this._time !== m && t >= 0 && (this._active = !0), 0 === _ && (2 === this._initted && t > 0 && this._init(), this._startAt && (t >= 0 ? this._startAt.render(t, e, i) : s || (s = "_dummyGS")), this.vars.onStart && (0 !== this._totalTime || 0 === v) && (e || this.vars.onStart.apply(this.vars.onStartScope || this, this.vars.onStartParams || u))), a = this._firstPT; a;) a.f ? a.t[a.p](a.c * this.ratio + a.s) : a.t[a.p] = a.c * this.ratio + a.s, a = a._next;
                    this._onUpdate && (0 > t && this._startAt && this._startTime && this._startAt.render(t, e, i), e || (this._totalTime !== _ || n) && this._onUpdate.apply(this.vars.onUpdateScope || this, this.vars.onUpdateParams || u)), this._cycle !== g && (e || this._gc || this.vars.onRepeat && this.vars.onRepeat.apply(this.vars.onRepeatScope || this, this.vars.onRepeatParams || u)), s && (!this._gc || i) && (0 > t && this._startAt && !this._onUpdate && this._startTime && this._startAt.render(t, e, i), n && (this._timeline.autoRemoveChildren && this._enabled(!1, !1), this._active = !1), !e && this.vars[s] && this.vars[s].apply(this.vars[s + "Scope"] || this, this.vars[s + "Params"] || u), 0 === v && this._rawPrevTime === r && d !== r && (this._rawPrevTime = 0))
                }, s.to = function(t, e, i) {
                    return new s(t, e, i)
                }, s.from = function(t, e, i) {
                    return i.runBackwards = !0, i.immediateRender = 0 != i.immediateRender, new s(t, e, i)
                }, s.fromTo = function(t, e, i, n) {
                    return n.startAt = i, n.immediateRender = 0 != n.immediateRender && 0 != i.immediateRender, new s(t, e, n)
                }, s.staggerTo = s.allTo = function(t, e, r, o, h, c, p) {
                    o = o || 0;
                    var d, f, m, _, g = r.delay || 0,
                        v = [],
                        y = function() {
                            r.onComplete && r.onComplete.apply(r.onCompleteScope || this, arguments), h.apply(p || this, c || u)
                        };
                    for (l(t) || ("string" == typeof t && (t = i.selector(t) || t), a(t) && (t = n(t))), t = t || [], 0 > o && (t = n(t), t.reverse(), o *= -1), d = t.length - 1, m = 0; d >= m; m++) {
                        f = {};
                        for (_ in r) f[_] = r[_];
                        f.delay = g, m === d && h && (f.onComplete = y), v[m] = new s(t[m], e, f), g += o
                    }
                    return v
                }, s.staggerFrom = s.allFrom = function(t, e, i, n, r, o, a) {
                    return i.runBackwards = !0, i.immediateRender = 0 != i.immediateRender, s.staggerTo(t, e, i, n, r, o, a)
                }, s.staggerFromTo = s.allFromTo = function(t, e, i, n, r, o, a, l) {
                    return n.startAt = i, n.immediateRender = 0 != n.immediateRender && 0 != i.immediateRender, s.staggerTo(t, e, n, r, o, a, l)
                }, s.delayedCall = function(t, e, i, n, r) {
                    return new s(e, 0, {
                        delay: t,
                        onComplete: e,
                        onCompleteParams: i,
                        onCompleteScope: n,
                        onReverseComplete: e,
                        onReverseCompleteParams: i,
                        onReverseCompleteScope: n,
                        immediateRender: !1,
                        useFrames: r,
                        overwrite: 0
                    })
                }, s.set = function(t, e) {
                    return new s(t, 0, e)
                }, s.isTweening = function(t) {
                    return i.getTweensOf(t, !0).length > 0
                };
                var c = function(t, e) {
                        for (var n = [], s = 0, r = t._first; r;) r instanceof i ? n[s++] = r : (e && (n[s++] = r), n = n.concat(c(r, e)), s = n.length), r = r._next;
                        return n
                    },
                    p = s.getAllTweens = function(e) {
                        return c(t._rootTimeline, e).concat(c(t._rootFramesTimeline, e))
                    };
                s.killAll = function(t, i, n, s) {
                    null == i && (i = !0), null == n && (n = !0);
                    var r, o, a, l = p(0 != s),
                        h = l.length,
                        u = i && n && s;
                    for (a = 0; h > a; a++) o = l[a], (u || o instanceof e || (r = o.target === o.vars.onComplete) && n || i && !r) && (t ? o.totalTime(o._reversed ? 0 : o.totalDuration()) : o._enabled(!1, !1))
                }, s.killChildTweensOf = function(t, e) {
                    if (null != t) {
                        var r, h, u, c, p, d = o.tweenLookup;
                        if ("string" == typeof t && (t = i.selector(t) || t), a(t) && (t = n(t)), l(t))
                            for (c = t.length; --c > -1;) s.killChildTweensOf(t[c], e);
                        else {
                            r = [];
                            for (u in d)
                                for (h = d[u].target.parentNode; h;) h === t && (r = r.concat(d[u].tweens)), h = h.parentNode;
                            for (p = r.length, c = 0; p > c; c++) e && r[c].totalTime(r[c].totalDuration()), r[c]._enabled(!1, !1)
                        }
                    }
                };
                var d = function(t, i, n, s) {
                    i = i !== !1, n = n !== !1, s = s !== !1;
                    for (var r, o, a = p(s), l = i && n && s, h = a.length; --h > -1;) o = a[h], (l || o instanceof e || (r = o.target === o.vars.onComplete) && n || i && !r) && o.paused(t)
                };
                return s.pauseAll = function(t, e, i) {
                    d(!0, t, e, i)
                }, s.resumeAll = function(t, e, i) {
                    d(!1, t, e, i)
                }, s.globalTimeScale = function(e) {
                    var n = t._rootTimeline,
                        s = i.ticker.time;
                    return arguments.length ? (e = e || r, n._startTime = s - (s - n._startTime) * n._timeScale / e, n = t._rootFramesTimeline, s = i.ticker.frame, n._startTime = s - (s - n._startTime) * n._timeScale / e, n._timeScale = t._rootTimeline._timeScale = e, e) : n._timeScale
                }, h.progress = function(t) {
                    return arguments.length ? this.totalTime(this.duration() * (this._yoyo && 0 !== (1 & this._cycle) ? 1 - t : t) + this._cycle * (this._duration + this._repeatDelay), !1) : this._time / this.duration()
                }, h.totalProgress = function(t) {
                    return arguments.length ? this.totalTime(this.totalDuration() * t, !1) : this._totalTime / this.totalDuration()
                }, h.time = function(t, e) {
                    return arguments.length ? (this._dirty && this.totalDuration(), t > this._duration && (t = this._duration), this._yoyo && 0 !== (1 & this._cycle) ? t = this._duration - t + this._cycle * (this._duration + this._repeatDelay) : 0 !== this._repeat && (t += this._cycle * (this._duration + this._repeatDelay)), this.totalTime(t, e)) : this._time
                }, h.duration = function(e) {
                    return arguments.length ? t.prototype.duration.call(this, e) : this._duration
                }, h.totalDuration = function(t) {
                    return arguments.length ? -1 === this._repeat ? this : this.duration((t - this._repeat * this._repeatDelay) / (this._repeat + 1)) : (this._dirty && (this._totalDuration = -1 === this._repeat ? 999999999999 : this._duration * (this._repeat + 1) + this._repeatDelay * this._repeat, this._dirty = !1), this._totalDuration)
                }, h.repeat = function(t) {
                    return arguments.length ? (this._repeat = t, this._uncache(!0)) : this._repeat
                }, h.repeatDelay = function(t) {
                    return arguments.length ? (this._repeatDelay = t, this._uncache(!0)) : this._repeatDelay
                }, h.yoyo = function(t) {
                    return arguments.length ? (this._yoyo = t, this) : this._yoyo
                }, s
            }, !0), _gsScope._gsDefine("TimelineLite", ["core.Animation", "core.SimpleTimeline", "TweenLite"], function(t, e, i) {
                var n = function(t) {
                        e.call(this, t), this._labels = {}, this.autoRemoveChildren = this.vars.autoRemoveChildren === !0, this.smoothChildTiming = this.vars.smoothChildTiming === !0, this._sortChildren = !0, this._onUpdate = this.vars.onUpdate;
                        var i, n, s = this.vars;
                        for (n in s) i = s[n], a(i) && -1 !== i.join("").indexOf("{self}") && (s[n] = this._swapSelfInParams(i));
                        a(s.tweens) && this.add(s.tweens, 0, s.align, s.stagger)
                    },
                    s = 1e-10,
                    r = i._internals,
                    o = r.isSelector,
                    a = r.isArray,
                    l = r.lazyTweens,
                    h = r.lazyRender,
                    u = [],
                    c = _gsScope._gsDefine.globals,
                    p = function(t) {
                        var e, i = {};
                        for (e in t) i[e] = t[e];
                        return i
                    },
                    d = function(t, e, i, n) {
                        var s = t._timeline._totalTime;
                        (e || !this._forcingPlayhead) && (t._timeline.pause(t._startTime), e && e.apply(n || t._timeline, i || u), this._forcingPlayhead && t._timeline.seek(s))
                    },
                    f = function(t) {
                        var e, i = [],
                            n = t.length;
                        for (e = 0; e !== n; i.push(t[e++]));
                        return i
                    },
                    m = n.prototype = new e;
                return n.version = "1.14.2", m.constructor = n, m.kill()._gc = m._forcingPlayhead = !1, m.to = function(t, e, n, s) {
                    var r = n.repeat && c.TweenMax || i;
                    return e ? this.add(new r(t, e, n), s) : this.set(t, n, s)
                }, m.from = function(t, e, n, s) {
                    return this.add((n.repeat && c.TweenMax || i).from(t, e, n), s)
                }, m.fromTo = function(t, e, n, s, r) {
                    var o = s.repeat && c.TweenMax || i;
                    return e ? this.add(o.fromTo(t, e, n, s), r) : this.set(t, s, r)
                }, m.staggerTo = function(t, e, s, r, a, l, h, u) {
                    var c, d = new n({
                        onComplete: l,
                        onCompleteParams: h,
                        onCompleteScope: u,
                        smoothChildTiming: this.smoothChildTiming
                    });
                    for ("string" == typeof t && (t = i.selector(t) || t), t = t || [], o(t) && (t = f(t)), r = r || 0, 0 > r && (t = f(t), t.reverse(), r *= -1), c = 0; t.length > c; c++) s.startAt && (s.startAt = p(s.startAt)), d.to(t[c], e, p(s), c * r);
                    return this.add(d, a)
                }, m.staggerFrom = function(t, e, i, n, s, r, o, a) {
                    return i.immediateRender = 0 != i.immediateRender, i.runBackwards = !0, this.staggerTo(t, e, i, n, s, r, o, a)
                }, m.staggerFromTo = function(t, e, i, n, s, r, o, a, l) {
                    return n.startAt = i, n.immediateRender = 0 != n.immediateRender && 0 != i.immediateRender, this.staggerTo(t, e, n, s, r, o, a, l)
                }, m.call = function(t, e, n, s) {
                    return this.add(i.delayedCall(0, t, e, n), s)
                }, m.set = function(t, e, n) {
                    return n = this._parseTimeOrLabel(n, 0, !0), null == e.immediateRender && (e.immediateRender = n === this._time && !this._paused), this.add(new i(t, 0, e), n)
                }, n.exportRoot = function(t, e) {
                    t = t || {}, null == t.smoothChildTiming && (t.smoothChildTiming = !0);
                    var s, r, o = new n(t),
                        a = o._timeline;
                    for (null == e && (e = !0), a._remove(o, !0), o._startTime = 0, o._rawPrevTime = o._time = o._totalTime = a._time, s = a._first; s;) r = s._next, e && s instanceof i && s.target === s.vars.onComplete || o.add(s, s._startTime - s._delay), s = r;
                    return a.add(o, 0), o
                }, m.add = function(s, r, o, l) {
                    var h, u, c, p, d, f;
                    if ("number" != typeof r && (r = this._parseTimeOrLabel(r, 0, !0, s)), !(s instanceof t)) {
                        if (s instanceof Array || s && s.push && a(s)) {
                            for (o = o || "normal", l = l || 0, h = r, u = s.length, c = 0; u > c; c++) a(p = s[c]) && (p = new n({
                                tweens: p
                            })), this.add(p, h), "string" != typeof p && "function" != typeof p && ("sequence" === o ? h = p._startTime + p.totalDuration() / p._timeScale : "start" === o && (p._startTime -= p.delay())), h += l;
                            return this._uncache(!0)
                        }
                        if ("string" == typeof s) return this.addLabel(s, r);
                        if ("function" != typeof s) throw "Cannot add " + s + " into the timeline; it is not a tween, timeline, function, or string.";
                        s = i.delayedCall(0, s)
                    }
                    if (e.prototype.add.call(this, s, r), (this._gc || this._time === this._duration) && !this._paused && this._duration < this.duration())
                        for (d = this, f = d.rawTime() > s._startTime; d._timeline;) f && d._timeline.smoothChildTiming ? d.totalTime(d._totalTime, !0) : d._gc && d._enabled(!0, !1), d = d._timeline;
                    return this
                }, m.remove = function(e) {
                    if (e instanceof t) return this._remove(e, !1);
                    if (e instanceof Array || e && e.push && a(e)) {
                        for (var i = e.length; --i > -1;) this.remove(e[i]);
                        return this
                    }
                    return "string" == typeof e ? this.removeLabel(e) : this.kill(null, e)
                }, m._remove = function(t, i) {
                    e.prototype._remove.call(this, t, i);
                    var n = this._last;
                    return n ? this._time > n._startTime + n._totalDuration / n._timeScale && (this._time = this.duration(), this._totalTime = this._totalDuration) : this._time = this._totalTime = this._duration = this._totalDuration = 0, this
                }, m.append = function(t, e) {
                    return this.add(t, this._parseTimeOrLabel(null, e, !0, t))
                }, m.insert = m.insertMultiple = function(t, e, i, n) {
                    return this.add(t, e || 0, i, n)
                }, m.appendMultiple = function(t, e, i, n) {
                    return this.add(t, this._parseTimeOrLabel(null, e, !0, t), i, n)
                }, m.addLabel = function(t, e) {
                    return this._labels[t] = this._parseTimeOrLabel(e), this
                }, m.addPause = function(t, e, i, n) {
                    return this.call(d, ["{self}", e, i, n], this, t)
                }, m.removeLabel = function(t) {
                    return delete this._labels[t], this
                }, m.getLabelTime = function(t) {
                    return null != this._labels[t] ? this._labels[t] : -1
                }, m._parseTimeOrLabel = function(e, i, n, s) {
                    var r;
                    if (s instanceof t && s.timeline === this) this.remove(s);
                    else if (s && (s instanceof Array || s.push && a(s)))
                        for (r = s.length; --r > -1;) s[r] instanceof t && s[r].timeline === this && this.remove(s[r]);
                    if ("string" == typeof i) return this._parseTimeOrLabel(i, n && "number" == typeof e && null == this._labels[i] ? e - this.duration() : 0, n);
                    if (i = i || 0, "string" != typeof e || !isNaN(e) && null == this._labels[e]) null == e && (e = this.duration());
                    else {
                        if (r = e.indexOf("="), -1 === r) return null == this._labels[e] ? n ? this._labels[e] = this.duration() + i : i : this._labels[e] + i;
                        i = parseInt(e.charAt(r - 1) + "1", 10) * Number(e.substr(r + 1)), e = r > 1 ? this._parseTimeOrLabel(e.substr(0, r - 1), 0, n) : this.duration()
                    }
                    return Number(e) + i
                }, m.seek = function(t, e) {
                    return this.totalTime("number" == typeof t ? t : this._parseTimeOrLabel(t), e !== !1)
                }, m.stop = function() {
                    return this.paused(!0)
                }, m.gotoAndPlay = function(t, e) {
                    return this.play(t, e)
                }, m.gotoAndStop = function(t, e) {
                    return this.pause(t, e)
                }, m.render = function(t, e, i) {
                    this._gc && this._enabled(!0, !1);
                    var n, r, o, a, c, p = this._dirty ? this.totalDuration() : this._totalDuration,
                        d = this._time,
                        f = this._startTime,
                        m = this._timeScale,
                        _ = this._paused;
                    if (t >= p ? (this._totalTime = this._time = p, this._reversed || this._hasPausedChild() || (r = !0, a = "onComplete", 0 === this._duration && (0 === t || 0 > this._rawPrevTime || this._rawPrevTime === s) && this._rawPrevTime !== t && this._first && (c = !0, this._rawPrevTime > s && (a = "onReverseComplete"))), this._rawPrevTime = this._duration || !e || t || this._rawPrevTime === t ? t : s, t = p + 1e-4) : 1e-7 > t ? (this._totalTime = this._time = 0, (0 !== d || 0 === this._duration && this._rawPrevTime !== s && (this._rawPrevTime > 0 || 0 > t && this._rawPrevTime >= 0)) && (a = "onReverseComplete", r = this._reversed), 0 > t ? (this._active = !1, this._rawPrevTime >= 0 && this._first && (c = !0), this._rawPrevTime = t) : (this._rawPrevTime = this._duration || !e || t || this._rawPrevTime === t ? t : s, t = 0, this._initted || (c = !0))) : this._totalTime = this._time = this._rawPrevTime = t, this._time !== d && this._first || i || c) {
                        if (this._initted || (this._initted = !0), this._active || !this._paused && this._time !== d && t > 0 && (this._active = !0), 0 === d && this.vars.onStart && 0 !== this._time && (e || this.vars.onStart.apply(this.vars.onStartScope || this, this.vars.onStartParams || u)), this._time >= d)
                            for (n = this._first; n && (o = n._next, !this._paused || _);)(n._active || n._startTime <= this._time && !n._paused && !n._gc) && (n._reversed ? n.render((n._dirty ? n.totalDuration() : n._totalDuration) - (t - n._startTime) * n._timeScale, e, i) : n.render((t - n._startTime) * n._timeScale, e, i)), n = o;
                        else
                            for (n = this._last; n && (o = n._prev, !this._paused || _);)(n._active || d >= n._startTime && !n._paused && !n._gc) && (n._reversed ? n.render((n._dirty ? n.totalDuration() : n._totalDuration) - (t - n._startTime) * n._timeScale, e, i) : n.render((t - n._startTime) * n._timeScale, e, i)), n = o;
                        this._onUpdate && (e || (l.length && h(), this._onUpdate.apply(this.vars.onUpdateScope || this, this.vars.onUpdateParams || u))), a && (this._gc || (f === this._startTime || m !== this._timeScale) && (0 === this._time || p >= this.totalDuration()) && (r && (l.length && h(), this._timeline.autoRemoveChildren && this._enabled(!1, !1), this._active = !1), !e && this.vars[a] && this.vars[a].apply(this.vars[a + "Scope"] || this, this.vars[a + "Params"] || u)))
                    }
                }, m._hasPausedChild = function() {
                    for (var t = this._first; t;) {
                        if (t._paused || t instanceof n && t._hasPausedChild()) return !0;
                        t = t._next
                    }
                    return !1
                }, m.getChildren = function(t, e, n, s) {
                    s = s || -9999999999;
                    for (var r = [], o = this._first, a = 0; o;) s > o._startTime || (o instanceof i ? e !== !1 && (r[a++] = o) : (n !== !1 && (r[a++] = o), t !== !1 && (r = r.concat(o.getChildren(!0, e, n)), a = r.length))), o = o._next;
                    return r
                }, m.getTweensOf = function(t, e) {
                    var n, s, r = this._gc,
                        o = [],
                        a = 0;
                    for (r && this._enabled(!0, !0), n = i.getTweensOf(t), s = n.length; --s > -1;)(n[s].timeline === this || e && this._contains(n[s])) && (o[a++] = n[s]);
                    return r && this._enabled(!1, !0), o
                }, m.recent = function() {
                    return this._recent
                }, m._contains = function(t) {
                    for (var e = t.timeline; e;) {
                        if (e === this) return !0;
                        e = e.timeline
                    }
                    return !1
                }, m.shiftChildren = function(t, e, i) {
                    i = i || 0;
                    for (var n, s = this._first, r = this._labels; s;) s._startTime >= i && (s._startTime += t), s = s._next;
                    if (e)
                        for (n in r) r[n] >= i && (r[n] += t);
                    return this._uncache(!0)
                }, m._kill = function(t, e) {
                    if (!t && !e) return this._enabled(!1, !1);
                    for (var i = e ? this.getTweensOf(e) : this.getChildren(!0, !0, !1), n = i.length, s = !1; --n > -1;) i[n]._kill(t, e) && (s = !0);
                    return s
                }, m.clear = function(t) {
                    var e = this.getChildren(!1, !0, !0),
                        i = e.length;
                    for (this._time = this._totalTime = 0; --i > -1;) e[i]._enabled(!1, !1);
                    return t !== !1 && (this._labels = {}), this._uncache(!0)
                }, m.invalidate = function() {
                    for (var e = this._first; e;) e.invalidate(), e = e._next;
                    return t.prototype.invalidate.call(this)
                }, m._enabled = function(t, i) {
                    if (t === this._gc)
                        for (var n = this._first; n;) n._enabled(t, !0), n = n._next;
                    return e.prototype._enabled.call(this, t, i)
                }, m.totalTime = function() {
                    this._forcingPlayhead = !0;
                    var e = t.prototype.totalTime.apply(this, arguments);
                    return this._forcingPlayhead = !1, e
                }, m.duration = function(t) {
                    return arguments.length ? (0 !== this.duration() && 0 !== t && this.timeScale(this._duration / t), this) : (this._dirty && this.totalDuration(), this._duration)
                }, m.totalDuration = function(t) {
                    if (!arguments.length) {
                        if (this._dirty) {
                            for (var e, i, n = 0, s = this._last, r = 999999999999; s;) e = s._prev, s._dirty && s.totalDuration(), s._startTime > r && this._sortChildren && !s._paused ? this.add(s, s._startTime - s._delay) : r = s._startTime, 0 > s._startTime && !s._paused && (n -= s._startTime, this._timeline.smoothChildTiming && (this._startTime += s._startTime / this._timeScale), this.shiftChildren(-s._startTime, !1, -9999999999), r = 0), i = s._startTime + s._totalDuration / s._timeScale, i > n && (n = i), s = e;
                            this._duration = this._totalDuration = n, this._dirty = !1
                        }
                        return this._totalDuration
                    }
                    return 0 !== this.totalDuration() && 0 !== t && this.timeScale(this._totalDuration / t), this
                }, m.usesFrames = function() {
                    for (var e = this._timeline; e._timeline;) e = e._timeline;
                    return e === t._rootFramesTimeline
                }, m.rawTime = function() {
                    return this._paused ? this._totalTime : (this._timeline.rawTime() - this._startTime) * this._timeScale
                }, n
            }, !0), _gsScope._gsDefine("TimelineMax", ["TimelineLite", "TweenLite", "easing.Ease"], function(t, e, i) {
                var n = function(e) {
                        t.call(this, e), this._repeat = this.vars.repeat || 0, this._repeatDelay = this.vars.repeatDelay || 0, this._cycle = 0, this._yoyo = this.vars.yoyo === !0, this._dirty = !0
                    },
                    s = 1e-10,
                    r = [],
                    o = e._internals,
                    a = o.lazyTweens,
                    l = o.lazyRender,
                    h = new i(null, null, 1, 0),
                    u = n.prototype = new t;
                return u.constructor = n, u.kill()._gc = !1, n.version = "1.14.2", u.invalidate = function() {
                    return this._yoyo = this.vars.yoyo === !0, this._repeat = this.vars.repeat || 0, this._repeatDelay = this.vars.repeatDelay || 0, this._uncache(!0), t.prototype.invalidate.call(this)
                }, u.addCallback = function(t, i, n, s) {
                    return this.add(e.delayedCall(0, t, n, s), i)
                }, u.removeCallback = function(t, e) {
                    if (t)
                        if (null == e) this._kill(null, t);
                        else
                            for (var i = this.getTweensOf(t, !1), n = i.length, s = this._parseTimeOrLabel(e); --n > -1;) i[n]._startTime === s && i[n]._enabled(!1, !1);
                    return this
                }, u.tweenTo = function(t, i) {
                    i = i || {};
                    var n, s, o, a = {
                        ease: h,
                        overwrite: i.delay ? 2 : 1,
                        useFrames: this.usesFrames(),
                        immediateRender: !1
                    };
                    for (s in i) a[s] = i[s];
                    return a.time = this._parseTimeOrLabel(t), n = Math.abs(Number(a.time) - this._time) / this._timeScale || .001, o = new e(this, n, a), a.onStart = function() {
                        o.target.paused(!0), o.vars.time !== o.target.time() && n === o.duration() && o.duration(Math.abs(o.vars.time - o.target.time()) / o.target._timeScale), i.onStart && i.onStart.apply(i.onStartScope || o, i.onStartParams || r)
                    }, o
                }, u.tweenFromTo = function(t, e, i) {
                    i = i || {}, t = this._parseTimeOrLabel(t), i.startAt = {
                        onComplete: this.seek,
                        onCompleteParams: [t],
                        onCompleteScope: this
                    }, i.immediateRender = i.immediateRender !== !1;
                    var n = this.tweenTo(e, i);
                    return n.duration(Math.abs(n.vars.time - t) / this._timeScale || .001)
                }, u.render = function(t, e, i) {
                    this._gc && this._enabled(!0, !1);
                    var n, o, h, u, c, p, d = this._dirty ? this.totalDuration() : this._totalDuration,
                        f = this._duration,
                        m = this._time,
                        _ = this._totalTime,
                        g = this._startTime,
                        v = this._timeScale,
                        y = this._rawPrevTime,
                        x = this._paused,
                        T = this._cycle;
                    if (t >= d ? (this._locked || (this._totalTime = d, this._cycle = this._repeat), this._reversed || this._hasPausedChild() || (o = !0, u = "onComplete", 0 === this._duration && (0 === t || 0 > y || y === s) && y !== t && this._first && (c = !0, y > s && (u = "onReverseComplete"))), this._rawPrevTime = this._duration || !e || t || this._rawPrevTime === t ? t : s, this._yoyo && 0 !== (1 & this._cycle) ? this._time = t = 0 : (this._time = f, t = f + 1e-4)) : 1e-7 > t ? (this._locked || (this._totalTime = this._cycle = 0), this._time = 0, (0 !== m || 0 === f && y !== s && (y > 0 || 0 > t && y >= 0) && !this._locked) && (u = "onReverseComplete", o = this._reversed), 0 > t ? (this._active = !1, y >= 0 && this._first && (c = !0), this._rawPrevTime = t) : (this._rawPrevTime = f || !e || t || this._rawPrevTime === t ? t : s, t = 0, this._initted || (c = !0))) : (0 === f && 0 > y && (c = !0), this._time = this._rawPrevTime = t, this._locked || (this._totalTime = t, 0 !== this._repeat && (p = f + this._repeatDelay, this._cycle = this._totalTime / p >> 0, 0 !== this._cycle && this._cycle === this._totalTime / p && this._cycle--, this._time = this._totalTime - this._cycle * p, this._yoyo && 0 !== (1 & this._cycle) && (this._time = f - this._time), this._time > f ? (this._time = f, t = f + 1e-4) : 0 > this._time ? this._time = t = 0 : t = this._time))), this._cycle !== T && !this._locked) {
                        var b = this._yoyo && 0 !== (1 & T),
                            w = b === (this._yoyo && 0 !== (1 & this._cycle)),
                            S = this._totalTime,
                            P = this._cycle,
                            C = this._rawPrevTime,
                            I = this._time;
                        if (this._totalTime = T * f, T > this._cycle ? b = !b : this._totalTime += f, this._time = m, this._rawPrevTime = 0 === f ? y - 1e-4 : y, this._cycle = T, this._locked = !0, m = b ? 0 : f, this.render(m, e, 0 === f), e || this._gc || this.vars.onRepeat && this.vars.onRepeat.apply(this.vars.onRepeatScope || this, this.vars.onRepeatParams || r), w && (m = b ? f + 1e-4 : -1e-4, this.render(m, !0, !1)), this._locked = !1, this._paused && !x) return;
                        this._time = I, this._totalTime = S, this._cycle = P, this._rawPrevTime = C
                    }
                    if (!(this._time !== m && this._first || i || c)) return void(_ !== this._totalTime && this._onUpdate && (e || this._onUpdate.apply(this.vars.onUpdateScope || this, this.vars.onUpdateParams || r)));
                    if (this._initted || (this._initted = !0), this._active || !this._paused && this._totalTime !== _ && t > 0 && (this._active = !0), 0 === _ && this.vars.onStart && 0 !== this._totalTime && (e || this.vars.onStart.apply(this.vars.onStartScope || this, this.vars.onStartParams || r)), this._time >= m)
                        for (n = this._first; n && (h = n._next, !this._paused || x);)(n._active || n._startTime <= this._time && !n._paused && !n._gc) && (n._reversed ? n.render((n._dirty ? n.totalDuration() : n._totalDuration) - (t - n._startTime) * n._timeScale, e, i) : n.render((t - n._startTime) * n._timeScale, e, i)), n = h;
                    else
                        for (n = this._last; n && (h = n._prev, !this._paused || x);)(n._active || m >= n._startTime && !n._paused && !n._gc) && (n._reversed ? n.render((n._dirty ? n.totalDuration() : n._totalDuration) - (t - n._startTime) * n._timeScale, e, i) : n.render((t - n._startTime) * n._timeScale, e, i)), n = h;
                    this._onUpdate && (e || (a.length && l(), this._onUpdate.apply(this.vars.onUpdateScope || this, this.vars.onUpdateParams || r))), u && (this._locked || this._gc || (g === this._startTime || v !== this._timeScale) && (0 === this._time || d >= this.totalDuration()) && (o && (a.length && l(), this._timeline.autoRemoveChildren && this._enabled(!1, !1), this._active = !1), !e && this.vars[u] && this.vars[u].apply(this.vars[u + "Scope"] || this, this.vars[u + "Params"] || r)))
                }, u.getActive = function(t, e, i) {
                    null == t && (t = !0), null == e && (e = !0), null == i && (i = !1);
                    var n, s, r = [],
                        o = this.getChildren(t, e, i),
                        a = 0,
                        l = o.length;
                    for (n = 0; l > n; n++) s = o[n], s.isActive() && (r[a++] = s);
                    return r
                }, u.getLabelAfter = function(t) {
                    t || 0 !== t && (t = this._time);
                    var e, i = this.getLabelsArray(),
                        n = i.length;
                    for (e = 0; n > e; e++)
                        if (i[e].time > t) return i[e].name;
                    return null
                }, u.getLabelBefore = function(t) {
                    null == t && (t = this._time);
                    for (var e = this.getLabelsArray(), i = e.length; --i > -1;)
                        if (t > e[i].time) return e[i].name;
                    return null
                }, u.getLabelsArray = function() {
                    var t, e = [],
                        i = 0;
                    for (t in this._labels) e[i++] = {
                        time: this._labels[t],
                        name: t
                    };
                    return e.sort(function(t, e) {
                        return t.time - e.time
                    }), e
                }, u.progress = function(t, e) {
                    return arguments.length ? this.totalTime(this.duration() * (this._yoyo && 0 !== (1 & this._cycle) ? 1 - t : t) + this._cycle * (this._duration + this._repeatDelay), e) : this._time / this.duration()
                }, u.totalProgress = function(t, e) {
                    return arguments.length ? this.totalTime(this.totalDuration() * t, e) : this._totalTime / this.totalDuration()
                }, u.totalDuration = function(e) {
                    return arguments.length ? -1 === this._repeat ? this : this.duration((e - this._repeat * this._repeatDelay) / (this._repeat + 1)) : (this._dirty && (t.prototype.totalDuration.call(this), this._totalDuration = -1 === this._repeat ? 999999999999 : this._duration * (this._repeat + 1) + this._repeatDelay * this._repeat), this._totalDuration)
                }, u.time = function(t, e) {
                    return arguments.length ? (this._dirty && this.totalDuration(), t > this._duration && (t = this._duration), this._yoyo && 0 !== (1 & this._cycle) ? t = this._duration - t + this._cycle * (this._duration + this._repeatDelay) : 0 !== this._repeat && (t += this._cycle * (this._duration + this._repeatDelay)), this.totalTime(t, e)) : this._time
                }, u.repeat = function(t) {
                    return arguments.length ? (this._repeat = t, this._uncache(!0)) : this._repeat
                }, u.repeatDelay = function(t) {
                    return arguments.length ? (this._repeatDelay = t, this._uncache(!0)) : this._repeatDelay
                }, u.yoyo = function(t) {
                    return arguments.length ? (this._yoyo = t, this) : this._yoyo
                }, u.currentLabel = function(t) {
                    return arguments.length ? this.seek(t, !0) : this.getLabelBefore(this._time + 1e-8)
                }, n
            }, !0),
            function() {
                var t = 180 / Math.PI,
                    e = [],
                    i = [],
                    n = [],
                    s = {},
                    r = function(t, e, i, n) {
                        this.a = t, this.b = e, this.c = i, this.d = n, this.da = n - t, this.ca = i - t, this.ba = e - t
                    },
                    o = ",x,y,z,left,top,right,bottom,marginTop,marginLeft,marginRight,marginBottom,paddingLeft,paddingTop,paddingRight,paddingBottom,backgroundPosition,backgroundPosition_y,",
                    a = function(t, e, i, n) {
                        var s = {
                                a: t
                            },
                            r = {},
                            o = {},
                            a = {
                                c: n
                            },
                            l = (t + e) / 2,
                            h = (e + i) / 2,
                            u = (i + n) / 2,
                            c = (l + h) / 2,
                            p = (h + u) / 2,
                            d = (p - c) / 8;
                        return s.b = l + (t - l) / 4, r.b = c + d, s.c = r.a = (s.b + r.b) / 2, r.c = o.a = (c + p) / 2, o.b = p - d, a.b = u + (n - u) / 4, o.c = a.a = (o.b + a.b) / 2, [s, r, o, a]
                    },
                    l = function(t, s, r, o, l) {
                        var h, u, c, p, d, f, m, _, g, v, y, x, T, b = t.length - 1,
                            w = 0,
                            S = t[0].a;
                        for (h = 0; b > h; h++) d = t[w], u = d.a, c = d.d, p = t[w + 1].d, l ? (y = e[h], x = i[h], T = .25 * (x + y) * s / (o ? .5 : n[h] || .5), f = c - (c - u) * (o ? .5 * s : 0 !== y ? T / y : 0), m = c + (p - c) * (o ? .5 * s : 0 !== x ? T / x : 0), _ = c - (f + ((m - f) * (3 * y / (y + x) + .5) / 4 || 0))) : (f = c - .5 * (c - u) * s, m = c + .5 * (p - c) * s, _ = c - (f + m) / 2), f += _, m += _, d.c = g = f, d.b = 0 !== h ? S : S = d.a + .6 * (d.c - d.a), d.da = c - u, d.ca = g - u, d.ba = S - u, r ? (v = a(u, S, g, c), t.splice(w, 1, v[0], v[1], v[2], v[3]), w += 4) : w++, S = m;
                        d = t[w], d.b = S, d.c = S + .4 * (d.d - S), d.da = d.d - d.a, d.ca = d.c - d.a, d.ba = S - d.a, r && (v = a(d.a, S, d.c, d.d), t.splice(w, 1, v[0], v[1], v[2], v[3]))
                    },
                    h = function(t, n, s, o) {
                        var a, l, h, u, c, p, d = [];
                        if (o)
                            for (t = [o].concat(t), l = t.length; --l > -1;) "string" == typeof(p = t[l][n]) && "=" === p.charAt(1) && (t[l][n] = o[n] + Number(p.charAt(0) + p.substr(2)));
                        if (a = t.length - 2, 0 > a) return d[0] = new r(t[0][n], 0, 0, t[-1 > a ? 0 : 1][n]), d;
                        for (l = 0; a > l; l++) h = t[l][n], u = t[l + 1][n], d[l] = new r(h, 0, 0, u), s && (c = t[l + 2][n], e[l] = (e[l] || 0) + (u - h) * (u - h), i[l] = (i[l] || 0) + (c - u) * (c - u));
                        return d[l] = new r(t[l][n], 0, 0, t[l + 1][n]), d
                    },
                    u = function(t, r, a, u, c, p) {
                        var d, f, m, _, g, v, y, x, T = {},
                            b = [],
                            w = p || t[0];
                        c = "string" == typeof c ? "," + c + "," : o, null == r && (r = 1);
                        for (f in t[0]) b.push(f);
                        if (t.length > 1) {
                            for (x = t[t.length - 1], y = !0, d = b.length; --d > -1;)
                                if (f = b[d], Math.abs(w[f] - x[f]) > .05) {
                                    y = !1;
                                    break
                                }
                            y && (t = t.concat(), p && t.unshift(p), t.push(t[1]), p = t[t.length - 3])
                        }
                        for (e.length = i.length = n.length = 0, d = b.length; --d > -1;) f = b[d], s[f] = -1 !== c.indexOf("," + f + ","), T[f] = h(t, f, s[f], p);
                        for (d = e.length; --d > -1;) e[d] = Math.sqrt(e[d]), i[d] = Math.sqrt(i[d]);
                        if (!u) {
                            for (d = b.length; --d > -1;)
                                if (s[f])
                                    for (m = T[b[d]], v = m.length - 1, _ = 0; v > _; _++) g = m[_ + 1].da / i[_] + m[_].da / e[_], n[_] = (n[_] || 0) + g * g;
                            for (d = n.length; --d > -1;) n[d] = Math.sqrt(n[d])
                        }
                        for (d = b.length, _ = a ? 4 : 1; --d > -1;) f = b[d], m = T[f], l(m, r, a, u, s[f]), y && (m.splice(0, _), m.splice(m.length - _, _));
                        return T
                    },
                    c = function(t, e, i) {
                        e = e || "soft";
                        var n, s, o, a, l, h, u, c, p, d, f, m = {},
                            _ = "cubic" === e ? 3 : 2,
                            g = "soft" === e,
                            v = [];
                        if (g && i && (t = [i].concat(t)), null == t || _ + 1 > t.length) throw "invalid Bezier data";
                        for (p in t[0]) v.push(p);
                        for (h = v.length; --h > -1;) {
                            for (p = v[h], m[p] = l = [], d = 0, c = t.length, u = 0; c > u; u++) n = null == i ? t[u][p] : "string" == typeof(f = t[u][p]) && "=" === f.charAt(1) ? i[p] + Number(f.charAt(0) + f.substr(2)) : Number(f), g && u > 1 && c - 1 > u && (l[d++] = (n + l[d - 2]) / 2), l[d++] = n;
                            for (c = d - _ + 1, d = 0, u = 0; c > u; u += _) n = l[u], s = l[u + 1], o = l[u + 2], a = 2 === _ ? 0 : l[u + 3], l[d++] = f = 3 === _ ? new r(n, s, o, a) : new r(n, (2 * s + n) / 3, (2 * s + o) / 3, o);
                            l.length = d
                        }
                        return m
                    },
                    p = function(t, e, i) {
                        for (var n, s, r, o, a, l, h, u, c, p, d, f = 1 / i, m = t.length; --m > -1;)
                            for (p = t[m], r = p.a, o = p.d - r, a = p.c - r, l = p.b - r, n = s = 0, u = 1; i >= u; u++) h = f * u, c = 1 - h, n = s - (s = (h * h * o + 3 * c * (h * a + c * l)) * h), d = m * i + u - 1, e[d] = (e[d] || 0) + n * n
                    },
                    d = function(t, e) {
                        e = e >> 0 || 6;
                        var i, n, s, r, o = [],
                            a = [],
                            l = 0,
                            h = 0,
                            u = e - 1,
                            c = [],
                            d = [];
                        for (i in t) p(t[i], o, e);
                        for (s = o.length, n = 0; s > n; n++) l += Math.sqrt(o[n]), r = n % e, d[r] = l, r === u && (h += l, r = n / e >> 0, c[r] = d, a[r] = h, l = 0, d = []);
                        return {
                            length: h,
                            lengths: a,
                            segments: c
                        }
                    },
                    f = _gsScope._gsDefine.plugin({
                        propName: "bezier",
                        priority: -1,
                        version: "1.3.3",
                        API: 2,
                        global: !0,
                        init: function(t, e, i) {
                            this._target = t, e instanceof Array && (e = {
                                values: e
                            }), this._func = {}, this._round = {}, this._props = [], this._timeRes = null == e.timeResolution ? 6 : parseInt(e.timeResolution, 10);
                            var n, s, r, o, a, l = e.values || [],
                                h = {},
                                p = l[0],
                                f = e.autoRotate || i.vars.orientToBezier;
                            this._autoRotate = f ? f instanceof Array ? f : [
                                ["x", "y", "rotation", f === !0 ? 0 : Number(f) || 0]
                            ] : null;
                            for (n in p) this._props.push(n);
                            for (r = this._props.length; --r > -1;) n = this._props[r], this._overwriteProps.push(n), s = this._func[n] = "function" == typeof t[n], h[n] = s ? t[n.indexOf("set") || "function" != typeof t["get" + n.substr(3)] ? n : "get" + n.substr(3)]() : parseFloat(t[n]), a || h[n] !== l[0][n] && (a = h);
                            if (this._beziers = "cubic" !== e.type && "quadratic" !== e.type && "soft" !== e.type ? u(l, isNaN(e.curviness) ? 1 : e.curviness, !1, "thruBasic" === e.type, e.correlate, a) : c(l, e.type, h), this._segCount = this._beziers[n].length, this._timeRes) {
                                var m = d(this._beziers, this._timeRes);
                                this._length = m.length, this._lengths = m.lengths, this._segments = m.segments, this._l1 = this._li = this._s1 = this._si = 0, this._l2 = this._lengths[0], this._curSeg = this._segments[0], this._s2 = this._curSeg[0], this._prec = 1 / this._curSeg.length
                            }
                            if (f = this._autoRotate)
                                for (this._initialRotations = [], f[0] instanceof Array || (this._autoRotate = f = [f]), r = f.length; --r > -1;) {
                                    for (o = 0; 3 > o; o++) n = f[r][o], this._func[n] = "function" == typeof t[n] ? t[n.indexOf("set") || "function" != typeof t["get" + n.substr(3)] ? n : "get" + n.substr(3)] : !1;
                                    n = f[r][2], this._initialRotations[r] = this._func[n] ? this._func[n].call(this._target) : this._target[n]
                                }
                            return this._startRatio = i.vars.runBackwards ? 1 : 0, !0
                        },
                        set: function(e) {
                            var i, n, s, r, o, a, l, h, u, c, p = this._segCount,
                                d = this._func,
                                f = this._target,
                                m = e !== this._startRatio;
                            if (this._timeRes) {
                                if (u = this._lengths, c = this._curSeg, e *= this._length, s = this._li, e > this._l2 && p - 1 > s) {
                                    for (h = p - 1; h > s && e >= (this._l2 = u[++s]););
                                    this._l1 = u[s - 1], this._li = s, this._curSeg = c = this._segments[s], this._s2 = c[this._s1 = this._si = 0]
                                } else if (this._l1 > e && s > 0) {
                                    for (; s > 0 && (this._l1 = u[--s]) >= e;);
                                    0 === s && this._l1 > e ? this._l1 = 0 : s++, this._l2 = u[s], this._li = s, this._curSeg = c = this._segments[s], this._s1 = c[(this._si = c.length - 1) - 1] || 0, this._s2 = c[this._si]
                                }
                                if (i = s, e -= this._l1, s = this._si, e > this._s2 && c.length - 1 > s) {
                                    for (h = c.length - 1; h > s && e >= (this._s2 = c[++s]););
                                    this._s1 = c[s - 1], this._si = s
                                } else if (this._s1 > e && s > 0) {
                                    for (; s > 0 && (this._s1 = c[--s]) >= e;);
                                    0 === s && this._s1 > e ? this._s1 = 0 : s++, this._s2 = c[s], this._si = s
                                }
                                a = (s + (e - this._s1) / (this._s2 - this._s1)) * this._prec
                            } else i = 0 > e ? 0 : e >= 1 ? p - 1 : p * e >> 0, a = (e - i * (1 / p)) * p;
                            for (n = 1 - a, s = this._props.length; --s > -1;) r = this._props[s], o = this._beziers[r][i], l = (a * a * o.da + 3 * n * (a * o.ca + n * o.ba)) * a + o.a, this._round[r] && (l = Math.round(l)), d[r] ? f[r](l) : f[r] = l;
                            if (this._autoRotate) {
                                var _, g, v, y, x, T, b, w = this._autoRotate;
                                for (s = w.length; --s > -1;) r = w[s][2], T = w[s][3] || 0, b = w[s][4] === !0 ? 1 : t, o = this._beziers[w[s][0]], _ = this._beziers[w[s][1]], o && _ && (o = o[i], _ = _[i], g = o.a + (o.b - o.a) * a, y = o.b + (o.c - o.b) * a, g += (y - g) * a, y += (o.c + (o.d - o.c) * a - y) * a, v = _.a + (_.b - _.a) * a, x = _.b + (_.c - _.b) * a, v += (x - v) * a, x += (_.c + (_.d - _.c) * a - x) * a, l = m ? Math.atan2(x - v, y - g) * b + T : this._initialRotations[s], d[r] ? f[r](l) : f[r] = l)
                            }
                        }
                    }),
                    m = f.prototype;
                f.bezierThrough = u, f.cubicToQuadratic = a, f._autoCSS = !0, f.quadraticToCubic = function(t, e, i) {
                    return new r(t, (2 * e + t) / 3, (2 * e + i) / 3, i)
                }, f._cssRegister = function() {
                    var t = _gsScope._gsDefine.globals.CSSPlugin;
                    if (t) {
                        var e = t._internals,
                            i = e._parseToProxy,
                            n = e._setPluginRatio,
                            s = e.CSSPropTween;
                        e._registerComplexSpecialProp("bezier", {
                            parser: function(t, e, r, o, a, l) {
                                e instanceof Array && (e = {
                                    values: e
                                }), l = new f;
                                var h, u, c, p = e.values,
                                    d = p.length - 1,
                                    m = [],
                                    _ = {};
                                if (0 > d) return a;
                                for (h = 0; d >= h; h++) c = i(t, p[h], o, a, l, d !== h), m[h] = c.end;
                                for (u in e) _[u] = e[u];
                                return _.values = m, a = new s(t, "bezier", 0, 0, c.pt, 2), a.data = c, a.plugin = l, a.setRatio = n, 0 === _.autoRotate && (_.autoRotate = !0), !_.autoRotate || _.autoRotate instanceof Array || (h = _.autoRotate === !0 ? 0 : Number(_.autoRotate), _.autoRotate = null != c.end.left ? [
                                    ["left", "top", "rotation", h, !1]
                                ] : null != c.end.x ? [
                                    ["x", "y", "rotation", h, !1]
                                ] : !1), _.autoRotate && (o._transform || o._enableTransforms(!1), c.autoRotate = o._target._gsTransform), l._onInitTween(c.proxy, _, o._tween), a
                            }
                        })
                    }
                }, m._roundProps = function(t, e) {
                    for (var i = this._overwriteProps, n = i.length; --n > -1;)(t[i[n]] || t.bezier || t.bezierThrough) && (this._round[i[n]] = e)
                }, m._kill = function(t) {
                    var e, i, n = this._props;
                    for (e in this._beziers)
                        if (e in t)
                            for (delete this._beziers[e], delete this._func[e], i = n.length; --i > -1;) n[i] === e && n.splice(i, 1);
                    return this._super._kill.call(this, t)
                }
            }(), _gsScope._gsDefine("plugins.CSSPlugin", ["plugins.TweenPlugin", "TweenLite"], function(t, e) {
                var i, n, s, r, o = function() {
                        t.call(this, "css"), this._overwriteProps.length = 0, this.setRatio = o.prototype.setRatio
                    },
                    a = {},
                    l = o.prototype = new t("css");
                l.constructor = o, o.version = "1.14.2", o.API = 2, o.defaultTransformPerspective = 0, o.defaultSkewType = "compensated", l = "px", o.suffixMap = {
                    top: l,
                    right: l,
                    bottom: l,
                    left: l,
                    width: l,
                    height: l,
                    fontSize: l,
                    padding: l,
                    margin: l,
                    perspective: l,
                    lineHeight: ""
                };
                var h, u, c, p, d, f, m = /(?:\d|\-\d|\.\d|\-\.\d)+/g,
                    _ = /(?:\d|\-\d|\.\d|\-\.\d|\+=\d|\-=\d|\+=.\d|\-=\.\d)+/g,
                    g = /(?:\+=|\-=|\-|\b)[\d\-\.]+[a-zA-Z0-9]*(?:%|\b)/gi,
                    v = /(?![+-]?\d*\.?\d+|e[+-]\d+)[^0-9]/g,
                    y = /(?:\d|\-|\+|=|#|\.)*/g,
                    x = /opacity *= *([^)]*)/i,
                    T = /opacity:([^;]*)/i,
                    b = /alpha\(opacity *=.+?\)/i,
                    w = /^(rgb|hsl)/,
                    S = /([A-Z])/g,
                    P = /-([a-z])/gi,
                    C = /(^(?:url\(\"|url\())|(?:(\"\))$|\)$)/gi,
                    I = function(t, e) {
                        return e.toUpperCase()
                    },
                    z = /(?:Left|Right|Width)/i,
                    O = /(M11|M12|M21|M22)=[\d\-\.e]+/gi,
                    R = /progid\:DXImageTransform\.Microsoft\.Matrix\(.+?\)/i,
                    M = /,(?=[^\)]*(?:\(|$))/gi,
                    E = Math.PI / 180,
                    k = 180 / Math.PI,
                    A = {},
                    L = document,
                    D = L.createElement("div"),
                    j = L.createElement("img"),
                    F = o._internals = {
                        _specialProps: a
                    },
                    N = navigator.userAgent,
                    W = function() {
                        var t, e = N.indexOf("Android"),
                            i = L.createElement("div");
                        return c = -1 !== N.indexOf("Safari") && -1 === N.indexOf("Chrome") && (-1 === e || Number(N.substr(e + 8, 1)) > 3), d = c && 6 > Number(N.substr(N.indexOf("Version/") + 8, 1)), p = -1 !== N.indexOf("Firefox"), (/MSIE ([0-9]{1,}[\.0-9]{0,})/.exec(N) || /Trident\/.*rv:([0-9]{1,}[\.0-9]{0,})/.exec(N)) && (f = parseFloat(RegExp.$1)), i.innerHTML = "<a style='top:1px;opacity:.55;'>a</a>", t = i.getElementsByTagName("a")[0], t ? /^0.55/.test(t.style.opacity) : !1
                    }(),
                    q = function(t) {
                        return x.test("string" == typeof t ? t : (t.currentStyle ? t.currentStyle.filter : t.style.filter) || "") ? parseFloat(RegExp.$1) / 100 : 1
                    },
                    X = function(t) {
                        window.console && console.log(t)
                    },
                    B = "",
                    Y = "",
                    U = function(t, e) {
                        e = e || D;
                        var i, n, s = e.style;
                        if (void 0 !== s[t]) return t;
                        for (t = t.charAt(0).toUpperCase() + t.substr(1), i = ["O", "Moz", "ms", "Ms", "Webkit"], n = 5; --n > -1 && void 0 === s[i[n] + t];);
                        return n >= 0 ? (Y = 3 === n ? "ms" : i[n], B = "-" + Y.toLowerCase() + "-", Y + t) : null
                    },
                    H = L.defaultView ? L.defaultView.getComputedStyle : function() {},
                    Q = o.getStyle = function(t, e, i, n, s) {
                        var r;
                        return W || "opacity" !== e ? (!n && t.style[e] ? r = t.style[e] : (i = i || H(t)) ? r = i[e] || i.getPropertyValue(e) || i.getPropertyValue(e.replace(S, "-$1").toLowerCase()) : t.currentStyle && (r = t.currentStyle[e]), null == s || r && "none" !== r && "auto" !== r && "auto auto" !== r ? r : s) : q(t)
                    },
                    V = F.convertToPixels = function(t, i, n, s, r) {
                        if ("px" === s || !s) return n;
                        if ("auto" === s || !n) return 0;
                        var a, l, h, u = z.test(i),
                            c = t,
                            p = D.style,
                            d = 0 > n;
                        if (d && (n = -n), "%" === s && -1 !== i.indexOf("border")) a = n / 100 * (u ? t.clientWidth : t.clientHeight);
                        else {
                            if (p.cssText = "border:0 solid red;position:" + Q(t, "position") + ";line-height:0;", "%" !== s && c.appendChild) p[u ? "borderLeftWidth" : "borderTopWidth"] = n + s;
                            else {
                                if (c = t.parentNode || L.body, l = c._gsCache, h = e.ticker.frame, l && u && l.time === h) return l.width * n / 100;
                                p[u ? "width" : "height"] = n + s
                            }
                            c.appendChild(D), a = parseFloat(D[u ? "offsetWidth" : "offsetHeight"]), c.removeChild(D), u && "%" === s && o.cacheWidths !== !1 && (l = c._gsCache = c._gsCache || {}, l.time = h, l.width = 100 * (a / n)), 0 !== a || r || (a = V(t, i, n, s, !0))
                        }
                        return d ? -a : a
                    },
                    G = F.calculateOffset = function(t, e, i) {
                        if ("absolute" !== Q(t, "position", i)) return 0;
                        var n = "left" === e ? "Left" : "Top",
                            s = Q(t, "margin" + n, i);
                        return t["offset" + n] - (V(t, e, parseFloat(s), s.replace(y, "")) || 0)
                    },
                    Z = function(t, e) {
                        var i, n, s = {};
                        if (e = e || H(t, null))
                            if (i = e.length)
                                for (; --i > -1;) s[e[i].replace(P, I)] = e.getPropertyValue(e[i]);
                            else
                                for (i in e) s[i] = e[i];
                        else if (e = t.currentStyle || t.style)
                            for (i in e) "string" == typeof i && void 0 === s[i] && (s[i.replace(P, I)] = e[i]);
                        return W || (s.opacity = q(t)), n = Re(t, e, !1), s.rotation = n.rotation, s.skewX = n.skewX, s.scaleX = n.scaleX, s.scaleY = n.scaleY, s.x = n.x, s.y = n.y, we && (s.z = n.z, s.rotationX = n.rotationX, s.rotationY = n.rotationY, s.scaleZ = n.scaleZ), s.filters && delete s.filters, s
                    },
                    $ = function(t, e, i, n, s) {
                        var r, o, a, l = {},
                            h = t.style;
                        for (o in i) "cssText" !== o && "length" !== o && isNaN(o) && (e[o] !== (r = i[o]) || s && s[o]) && -1 === o.indexOf("Origin") && ("number" == typeof r || "string" == typeof r) && (l[o] = "auto" !== r || "left" !== o && "top" !== o ? "" !== r && "auto" !== r && "none" !== r || "string" != typeof e[o] || "" === e[o].replace(v, "") ? r : 0 : G(t, o), void 0 !== h[o] && (a = new ce(h, o, h[o], a)));
                        if (n)
                            for (o in n) "className" !== o && (l[o] = n[o]);
                        return {
                            difs: l,
                            firstMPT: a
                        }
                    },
                    J = {
                        width: ["Left", "Right"],
                        height: ["Top", "Bottom"]
                    },
                    K = ["marginLeft", "marginRight", "marginTop", "marginBottom"],
                    te = function(t, e, i) {
                        var n = parseFloat("width" === e ? t.offsetWidth : t.offsetHeight),
                            s = J[e],
                            r = s.length;
                        for (i = i || H(t, null); --r > -1;) n -= parseFloat(Q(t, "padding" + s[r], i, !0)) || 0, n -= parseFloat(Q(t, "border" + s[r] + "Width", i, !0)) || 0;
                        return n
                    },
                    ee = function(t, e) {
                        (null == t || "" === t || "auto" === t || "auto auto" === t) && (t = "0 0");
                        var i = t.split(" "),
                            n = -1 !== t.indexOf("left") ? "0%" : -1 !== t.indexOf("right") ? "100%" : i[0],
                            s = -1 !== t.indexOf("top") ? "0%" : -1 !== t.indexOf("bottom") ? "100%" : i[1];
                        return null == s ? s = "0" : "center" === s && (s = "50%"), ("center" === n || isNaN(parseFloat(n)) && -1 === (n + "").indexOf("=")) && (n = "50%"), e && (e.oxp = -1 !== n.indexOf("%"), e.oyp = -1 !== s.indexOf("%"), e.oxr = "=" === n.charAt(1), e.oyr = "=" === s.charAt(1), e.ox = parseFloat(n.replace(v, "")), e.oy = parseFloat(s.replace(v, ""))), n + " " + s + (i.length > 2 ? " " + i[2] : "")
                    },
                    ie = function(t, e) {
                        return "string" == typeof t && "=" === t.charAt(1) ? parseInt(t.charAt(0) + "1", 10) * parseFloat(t.substr(2)) : parseFloat(t) - parseFloat(e)
                    },
                    ne = function(t, e) {
                        return null == t ? e : "string" == typeof t && "=" === t.charAt(1) ? parseInt(t.charAt(0) + "1", 10) * parseFloat(t.substr(2)) + e : parseFloat(t)
                    },
                    se = function(t, e, i, n) {
                        var s, r, o, a, l = 1e-6;
                        return null == t ? a = e : "number" == typeof t ? a = t : (s = 360, r = t.split("_"), o = Number(r[0].replace(v, "")) * (-1 === t.indexOf("rad") ? 1 : k) - ("=" === t.charAt(1) ? 0 : e), r.length && (n && (n[i] = e + o), -1 !== t.indexOf("short") && (o %= s, o !== o % (s / 2) && (o = 0 > o ? o + s : o - s)), -1 !== t.indexOf("_cw") && 0 > o ? o = (o + 9999999999 * s) % s - (0 | o / s) * s : -1 !== t.indexOf("ccw") && o > 0 && (o = (o - 9999999999 * s) % s - (0 | o / s) * s)), a = e + o), l > a && a > -l && (a = 0), a
                    },
                    re = {
                        aqua: [0, 255, 255],
                        lime: [0, 255, 0],
                        silver: [192, 192, 192],
                        black: [0, 0, 0],
                        maroon: [128, 0, 0],
                        teal: [0, 128, 128],
                        blue: [0, 0, 255],
                        navy: [0, 0, 128],
                        white: [255, 255, 255],
                        fuchsia: [255, 0, 255],
                        olive: [128, 128, 0],
                        yellow: [255, 255, 0],
                        orange: [255, 165, 0],
                        gray: [128, 128, 128],
                        purple: [128, 0, 128],
                        green: [0, 128, 0],
                        red: [255, 0, 0],
                        pink: [255, 192, 203],
                        cyan: [0, 255, 255],
                        transparent: [255, 255, 255, 0]
                    },
                    oe = function(t, e, i) {
                        return t = 0 > t ? t + 1 : t > 1 ? t - 1 : t, 0 | 255 * (1 > 6 * t ? e + 6 * (i - e) * t : .5 > t ? i : 2 > 3 * t ? e + 6 * (i - e) * (2 / 3 - t) : e) + .5
                    },
                    ae = o.parseColor = function(t) {
                        var e, i, n, s, r, o;
                        return t && "" !== t ? "number" == typeof t ? [t >> 16, 255 & t >> 8, 255 & t] : ("," === t.charAt(t.length - 1) && (t = t.substr(0, t.length - 1)), re[t] ? re[t] : "#" === t.charAt(0) ? (4 === t.length && (e = t.charAt(1), i = t.charAt(2), n = t.charAt(3), t = "#" + e + e + i + i + n + n), t = parseInt(t.substr(1), 16), [t >> 16, 255 & t >> 8, 255 & t]) : "hsl" === t.substr(0, 3) ? (t = t.match(m), s = Number(t[0]) % 360 / 360, r = Number(t[1]) / 100, o = Number(t[2]) / 100, i = .5 >= o ? o * (r + 1) : o + r - o * r, e = 2 * o - i, t.length > 3 && (t[3] = Number(t[3])), t[0] = oe(s + 1 / 3, e, i), t[1] = oe(s, e, i), t[2] = oe(s - 1 / 3, e, i), t) : (t = t.match(m) || re.transparent, t[0] = Number(t[0]), t[1] = Number(t[1]), t[2] = Number(t[2]), t.length > 3 && (t[3] = Number(t[3])), t)) : re.black
                    },
                    le = "(?:\\b(?:(?:rgb|rgba|hsl|hsla)\\(.+?\\))|\\B#.+?\\b";
                for (l in re) le += "|" + l + "\\b";
                le = RegExp(le + ")", "gi");
                var he = function(t, e, i, n) {
                        if (null == t) return function(t) {
                            return t
                        };
                        var s, r = e ? (t.match(le) || [""])[0] : "",
                            o = t.split(r).join("").match(g) || [],
                            a = t.substr(0, t.indexOf(o[0])),
                            l = ")" === t.charAt(t.length - 1) ? ")" : "",
                            h = -1 !== t.indexOf(" ") ? " " : ",",
                            u = o.length,
                            c = u > 0 ? o[0].replace(m, "") : "";
                        return u ? s = e ? function(t) {
                            var e, p, d, f;
                            if ("number" == typeof t) t += c;
                            else if (n && M.test(t)) {
                                for (f = t.replace(M, "|").split("|"), d = 0; f.length > d; d++) f[d] = s(f[d]);
                                return f.join(",")
                            }
                            if (e = (t.match(le) || [r])[0], p = t.split(e).join("").match(g) || [], d = p.length, u > d--)
                                for (; u > ++d;) p[d] = i ? p[0 | (d - 1) / 2] : o[d];
                            return a + p.join(h) + h + e + l + (-1 !== t.indexOf("inset") ? " inset" : "")
                        } : function(t) {
                            var e, r, p;
                            if ("number" == typeof t) t += c;
                            else if (n && M.test(t)) {
                                for (r = t.replace(M, "|").split("|"), p = 0; r.length > p; p++) r[p] = s(r[p]);
                                return r.join(",")
                            }
                            if (e = t.match(g) || [], p = e.length, u > p--)
                                for (; u > ++p;) e[p] = i ? e[0 | (p - 1) / 2] : o[p];
                            return a + e.join(h) + l
                        } : function(t) {
                            return t
                        }
                    },
                    ue = function(t) {
                        return t = t.split(","),
                            function(e, i, n, s, r, o, a) {
                                var l, h = (i + "").split(" ");
                                for (a = {}, l = 0; 4 > l; l++) a[t[l]] = h[l] = h[l] || h[(l - 1) / 2 >> 0];
                                return s.parse(e, a, r, o)
                            }
                    },
                    ce = (F._setPluginRatio = function(t) {
                        this.plugin.setRatio(t);
                        for (var e, i, n, s, r = this.data, o = r.proxy, a = r.firstMPT, l = 1e-6; a;) e = o[a.v], a.r ? e = Math.round(e) : l > e && e > -l && (e = 0), a.t[a.p] = e, a = a._next;
                        if (r.autoRotate && (r.autoRotate.rotation = o.rotation), 1 === t)
                            for (a = r.firstMPT; a;) {
                                if (i = a.t, i.type) {
                                    if (1 === i.type) {
                                        for (s = i.xs0 + i.s + i.xs1, n = 1; i.l > n; n++) s += i["xn" + n] + i["xs" + (n + 1)];
                                        i.e = s
                                    }
                                } else i.e = i.s + i.xs0;
                                a = a._next
                            }
                    }, function(t, e, i, n, s) {
                        this.t = t, this.p = e, this.v = i, this.r = s, n && (n._prev = this, this._next = n)
                    }),
                    pe = (F._parseToProxy = function(t, e, i, n, s, r) {
                        var o, a, l, h, u, c = n,
                            p = {},
                            d = {},
                            f = i._transform,
                            m = A;
                        for (i._transform = null, A = e, n = u = i.parse(t, e, n, s), A = m, r && (i._transform = f, c && (c._prev = null, c._prev && (c._prev._next = null))); n && n !== c;) {
                            if (1 >= n.type && (a = n.p, d[a] = n.s + n.c, p[a] = n.s, r || (h = new ce(n, "s", a, h, n.r), n.c = 0), 1 === n.type))
                                for (o = n.l; --o > 0;) l = "xn" + o, a = n.p + "_" + l, d[a] = n.data[l], p[a] = n[l], r || (h = new ce(n, l, a, h, n.rxp[l]));
                            n = n._next
                        }
                        return {
                            proxy: p,
                            end: d,
                            firstMPT: h,
                            pt: u
                        }
                    }, F.CSSPropTween = function(t, e, n, s, o, a, l, h, u, c, p) {
                        this.t = t, this.p = e, this.s = n, this.c = s, this.n = l || e, t instanceof pe || r.push(this.n), this.r = h, this.type = a || 0, u && (this.pr = u, i = !0), this.b = void 0 === c ? n : c, this.e = void 0 === p ? n + s : p, o && (this._next = o, o._prev = this)
                    }),
                    de = o.parseComplex = function(t, e, i, n, s, r, o, a, l, u) {
                        i = i || r || "", o = new pe(t, e, 0, 0, o, u ? 2 : 1, null, !1, a, i, n), n += "";
                        var c, p, d, f, g, v, y, x, T, b, S, P, C = i.split(", ").join(",").split(" "),
                            I = n.split(", ").join(",").split(" "),
                            z = C.length,
                            O = h !== !1;
                        for ((-1 !== n.indexOf(",") || -1 !== i.indexOf(",")) && (C = C.join(" ").replace(M, ", ").split(" "), I = I.join(" ").replace(M, ", ").split(" "), z = C.length), z !== I.length && (C = (r || "").split(" "), z = C.length), o.plugin = l, o.setRatio = u, c = 0; z > c; c++)
                            if (f = C[c], g = I[c], x = parseFloat(f), x || 0 === x) o.appendXtra("", x, ie(g, x), g.replace(_, ""), O && -1 !== g.indexOf("px"), !0);
                            else if (s && ("#" === f.charAt(0) || re[f] || w.test(f))) P = "," === g.charAt(g.length - 1) ? ")," : ")", f = ae(f), g = ae(g), T = f.length + g.length > 6, T && !W && 0 === g[3] ? (o["xs" + o.l] += o.l ? " transparent" : "transparent", o.e = o.e.split(I[c]).join("transparent")) : (W || (T = !1), o.appendXtra(T ? "rgba(" : "rgb(", f[0], g[0] - f[0], ",", !0, !0).appendXtra("", f[1], g[1] - f[1], ",", !0).appendXtra("", f[2], g[2] - f[2], T ? "," : P, !0), T && (f = 4 > f.length ? 1 : f[3], o.appendXtra("", f, (4 > g.length ? 1 : g[3]) - f, P, !1)));
                        else if (v = f.match(m)) {
                            if (y = g.match(_), !y || y.length !== v.length) return o;
                            for (d = 0, p = 0; v.length > p; p++) S = v[p], b = f.indexOf(S, d), o.appendXtra(f.substr(d, b - d), Number(S), ie(y[p], S), "", O && "px" === f.substr(b + S.length, 2), 0 === p), d = b + S.length;
                            o["xs" + o.l] += f.substr(d)
                        } else o["xs" + o.l] += o.l ? " " + f : f;
                        if (-1 !== n.indexOf("=") && o.data) {
                            for (P = o.xs0 + o.data.s, c = 1; o.l > c; c++) P += o["xs" + c] + o.data["xn" + c];
                            o.e = P + o["xs" + c]
                        }
                        return o.l || (o.type = -1, o.xs0 = o.e), o.xfirst || o
                    },
                    fe = 9;
                for (l = pe.prototype, l.l = l.pr = 0; --fe > 0;) l["xn" + fe] = 0, l["xs" + fe] = "";
                l.xs0 = "", l._next = l._prev = l.xfirst = l.data = l.plugin = l.setRatio = l.rxp = null, l.appendXtra = function(t, e, i, n, s, r) {
                    var o = this,
                        a = o.l;
                    return o["xs" + a] += r && a ? " " + t : t || "", i || 0 === a || o.plugin ? (o.l++, o.type = o.setRatio ? 2 : 1, o["xs" + o.l] = n || "", a > 0 ? (o.data["xn" + a] = e + i, o.rxp["xn" + a] = s, o["xn" + a] = e, o.plugin || (o.xfirst = new pe(o, "xn" + a, e, i, o.xfirst || o, 0, o.n, s, o.pr), o.xfirst.xs0 = 0), o) : (o.data = {
                        s: e + i
                    }, o.rxp = {}, o.s = e, o.c = i, o.r = s, o)) : (o["xs" + a] += e + (n || ""), o)
                };
                var me = function(t, e) {
                        e = e || {}, this.p = e.prefix ? U(t) || t : t, a[t] = a[this.p] = this, this.format = e.formatter || he(e.defaultValue, e.color, e.collapsible, e.multi), e.parser && (this.parse = e.parser), this.clrs = e.color, this.multi = e.multi, this.keyword = e.keyword, this.dflt = e.defaultValue, this.pr = e.priority || 0
                    },
                    _e = F._registerComplexSpecialProp = function(t, e, i) {
                        "object" != typeof e && (e = {
                            parser: i
                        });
                        var n, s, r = t.split(","),
                            o = e.defaultValue;
                        for (i = i || [o], n = 0; r.length > n; n++) e.prefix = 0 === n && e.prefix, e.defaultValue = i[n] || o, s = new me(r[n], e)
                    },
                    ge = function(t) {
                        if (!a[t]) {
                            var e = t.charAt(0).toUpperCase() + t.substr(1) + "Plugin";
                            _e(t, {
                                parser: function(t, i, n, s, r, o, l) {
                                    var h = (_gsScope.GreenSockGlobals || _gsScope).com.greensock.plugins[e];
                                    return h ? (h._cssRegister(), a[n].parse(t, i, n, s, r, o, l)) : (X("Error: " + e + " js file not loaded."), r)
                                }
                            })
                        }
                    };
                l = me.prototype, l.parseComplex = function(t, e, i, n, s, r) {
                    var o, a, l, h, u, c, p = this.keyword;
                    if (this.multi && (M.test(i) || M.test(e) ? (a = e.replace(M, "|").split("|"), l = i.replace(M, "|").split("|")) : p && (a = [e], l = [i])), l) {
                        for (h = l.length > a.length ? l.length : a.length, o = 0; h > o; o++) e = a[o] = a[o] || this.dflt, i = l[o] = l[o] || this.dflt, p && (u = e.indexOf(p), c = i.indexOf(p), u !== c && (i = -1 === c ? l : a, i[o] += " " + p));
                        e = a.join(", "), i = l.join(", ")
                    }
                    return de(t, this.p, e, i, this.clrs, this.dflt, n, this.pr, s, r)
                }, l.parse = function(t, e, i, n, r, o) {
                    return this.parseComplex(t.style, this.format(Q(t, this.p, s, !1, this.dflt)), this.format(e), r, o)
                }, o.registerSpecialProp = function(t, e, i) {
                    _e(t, {
                        parser: function(t, n, s, r, o, a) {
                            var l = new pe(t, s, 0, 0, o, 2, s, !1, i);
                            return l.plugin = a, l.setRatio = e(t, n, r._tween, s), l
                        },
                        priority: i
                    })
                };
                var ve, ye = "scaleX,scaleY,scaleZ,x,y,z,skewX,skewY,rotation,rotationX,rotationY,perspective,xPercent,yPercent".split(","),
                    xe = U("transform"),
                    Te = B + "transform",
                    be = U("transformOrigin"),
                    we = null !== U("perspective"),
                    Se = F.Transform = function() {
                        this.skewY = 0
                    },
                    Pe = window.SVGElement,
                    Ce = function(t, e, i) {
                        var n, s = L.createElementNS("http://www.w3.org/2000/svg", t),
                            r = /([a-z])([A-Z])/g;
                        for (n in i) s.setAttributeNS(null, n.replace(r, "$1-$2").toLowerCase(), i[n]);
                        return e.appendChild(s), s
                    },
                    Ie = document.documentElement,
                    ze = function() {
                        var t, e, i, n = f || /Android/i.test(N) && !window.chrome;
                        return L.createElementNS && !n && (t = Ce("svg", Ie), e = Ce("rect", t, {
                            width: 100,
                            height: 50,
                            x: 100
                        }), i = e.getBoundingClientRect().left, e.style[be] = "50% 50%", e.style[xe] = "scale(0.5,0.5)", n = i === e.getBoundingClientRect().left, Ie.removeChild(t)), n
                    }(),
                    Oe = function(t, e, i) {
                        var n = t.getBBox();
                        e = ee(e).split(" "), i.xOrigin = (-1 !== e[0].indexOf("%") ? parseFloat(e[0]) / 100 * n.width : parseFloat(e[0])) + n.x, i.yOrigin = (-1 !== e[1].indexOf("%") ? parseFloat(e[1]) / 100 * n.height : parseFloat(e[1])) + n.y
                    },
                    Re = F.getTransform = function(t, e, i, n) {
                        if (t._gsTransform && i && !n) return t._gsTransform;
                        var r, a, l, h, u, c, p, d, f, m, _, g, v, y = i ? t._gsTransform || new Se : new Se,
                            x = 0 > y.scaleX,
                            T = 2e-5,
                            b = 1e5,
                            w = 179.99,
                            S = w * E,
                            P = we ? parseFloat(Q(t, be, e, !1, "0 0 0").split(" ")[2]) || y.zOrigin || 0 : 0,
                            C = parseFloat(o.defaultTransformPerspective) || 0;
                        if (xe ? r = Q(t, Te, e, !0) : t.currentStyle && (r = t.currentStyle.filter.match(O), r = r && 4 === r.length ? [r[0].substr(4), Number(r[2].substr(4)), Number(r[1].substr(4)), r[3].substr(4), y.x || 0, y.y || 0].join(",") : ""), r && "none" !== r && "matrix(1, 0, 0, 1, 0, 0)" !== r) {
                            for (a = (r || "").match(/(?:\-|\b)[\d\-\.e]+\b/gi) || [], l = a.length; --l > -1;) h = Number(a[l]), a[l] = (u = h - (h |= 0)) ? (0 | u * b + (0 > u ? -.5 : .5)) / b + h : h;
                            if (16 === a.length) {
                                var I = a[8],
                                    z = a[9],
                                    R = a[10],
                                    M = a[12],
                                    A = a[13],
                                    L = a[14];
                                if (y.zOrigin && (L = -y.zOrigin, M = I * L - a[12], A = z * L - a[13], L = R * L + y.zOrigin - a[14]), !i || n || null == y.rotationX) {
                                    var D, j, F, N, W, q, X, B = a[0],
                                        Y = a[1],
                                        U = a[2],
                                        H = a[3],
                                        V = a[4],
                                        G = a[5],
                                        Z = a[6],
                                        $ = a[7],
                                        J = a[11],
                                        K = Math.atan2(Z, R),
                                        te = -S > K || K > S;
                                    y.rotationX = K * k, K && (N = Math.cos(-K), W = Math.sin(-K), D = V * N + I * W, j = G * N + z * W, F = Z * N + R * W, I = V * -W + I * N, z = G * -W + z * N, R = Z * -W + R * N, J = $ * -W + J * N, V = D, G = j, Z = F), K = Math.atan2(I, B), y.rotationY = K * k, K && (q = -S > K || K > S, N = Math.cos(-K), W = Math.sin(-K), D = B * N - I * W, j = Y * N - z * W, F = U * N - R * W, z = Y * W + z * N, R = U * W + R * N, J = H * W + J * N, B = D, Y = j, U = F), K = Math.atan2(Y, G), y.rotation = K * k, K && (X = -S > K || K > S, N = Math.cos(-K), W = Math.sin(-K), B = B * N + V * W, j = Y * N + G * W, G = Y * -W + G * N, Z = U * -W + Z * N, Y = j), X && te ? y.rotation = y.rotationX = 0 : X && q ? y.rotation = y.rotationY = 0 : q && te && (y.rotationY = y.rotationX = 0), y.scaleX = (0 | Math.sqrt(B * B + Y * Y) * b + .5) / b, y.scaleY = (0 | Math.sqrt(G * G + z * z) * b + .5) / b, y.scaleZ = (0 | Math.sqrt(Z * Z + R * R) * b + .5) / b, y.skewX = 0, y.perspective = J ? 1 / (0 > J ? -J : J) : 0, y.x = M, y.y = A, y.z = L
                                }
                            } else if (!(we && !n && a.length && y.x === a[4] && y.y === a[5] && (y.rotationX || y.rotationY) || void 0 !== y.x && "none" === Q(t, "display", e))) {
                                var ee = a.length >= 6,
                                    ie = ee ? a[0] : 1,
                                    ne = a[1] || 0,
                                    se = a[2] || 0,
                                    re = ee ? a[3] : 1;
                                y.x = a[4] || 0, y.y = a[5] || 0, c = Math.sqrt(ie * ie + ne * ne), p = Math.sqrt(re * re + se * se), d = ie || ne ? Math.atan2(ne, ie) * k : y.rotation || 0, f = se || re ? Math.atan2(se, re) * k + d : y.skewX || 0, m = c - Math.abs(y.scaleX || 0), _ = p - Math.abs(y.scaleY || 0), Math.abs(f) > 90 && 270 > Math.abs(f) && (x ? (c *= -1, f += 0 >= d ? 180 : -180, d += 0 >= d ? 180 : -180) : (p *= -1, f += 0 >= f ? 180 : -180)), g = (d - y.rotation) % 180, v = (f - y.skewX) % 180, (void 0 === y.skewX || m > T || -T > m || _ > T || -T > _ || g > -w && w > g && !1 | g * b || v > -w && w > v && !1 | v * b) && (y.scaleX = c, y.scaleY = p, y.rotation = d, y.skewX = f), we && (y.rotationX = y.rotationY = y.z = 0, y.perspective = C, y.scaleZ = 1)
                            }
                            y.zOrigin = P;
                            for (l in y) T > y[l] && y[l] > -T && (y[l] = 0)
                        } else y = {
                            x: 0,
                            y: 0,
                            z: 0,
                            scaleX: 1,
                            scaleY: 1,
                            scaleZ: 1,
                            skewX: 0,
                            skewY: 0,
                            perspective: C,
                            rotation: 0,
                            rotationX: 0,
                            rotationY: 0,
                            zOrigin: 0
                        };
                        return i && (t._gsTransform = y), y.svg = Pe && t instanceof Pe && t.parentNode instanceof Pe, y.svg && (Oe(t, Q(t, be, s, !1, "50% 50%") + "", y), ve = o.useSVGTransformAttr || ze), y.xPercent = y.yPercent = 0, y
                    },
                    Me = function(t) {
                        var e, i, n = this.data,
                            s = -n.rotation * E,
                            r = s + n.skewX * E,
                            o = 1e5,
                            a = (0 | Math.cos(s) * n.scaleX * o) / o,
                            l = (0 | Math.sin(s) * n.scaleX * o) / o,
                            h = (0 | Math.sin(r) * -n.scaleY * o) / o,
                            u = (0 | Math.cos(r) * n.scaleY * o) / o,
                            c = this.t.style,
                            p = this.t.currentStyle;
                        if (p) {
                            i = l, l = -h, h = -i, e = p.filter, c.filter = "";
                            var d, m, _ = this.t.offsetWidth,
                                g = this.t.offsetHeight,
                                v = "absolute" !== p.position,
                                T = "progid:DXImageTransform.Microsoft.Matrix(M11=" + a + ", M12=" + l + ", M21=" + h + ", M22=" + u,
                                b = n.x + _ * n.xPercent / 100,
                                w = n.y + g * n.yPercent / 100;
                            if (null != n.ox && (d = (n.oxp ? .01 * _ * n.ox : n.ox) - _ / 2, m = (n.oyp ? .01 * g * n.oy : n.oy) - g / 2, b += d - (d * a + m * l), w += m - (d * h + m * u)), v ? (d = _ / 2, m = g / 2, T += ", Dx=" + (d - (d * a + m * l) + b) + ", Dy=" + (m - (d * h + m * u) + w) + ")") : T += ", sizingMethod='auto expand')", c.filter = -1 !== e.indexOf("DXImageTransform.Microsoft.Matrix(") ? e.replace(R, T) : T + " " + e, (0 === t || 1 === t) && 1 === a && 0 === l && 0 === h && 1 === u && (v && -1 === T.indexOf("Dx=0, Dy=0") || x.test(e) && 100 !== parseFloat(RegExp.$1) || -1 === e.indexOf("gradient(" && e.indexOf("Alpha")) && c.removeAttribute("filter")), !v) {
                                var S, P, C, I = 8 > f ? 1 : -1;
                                for (d = n.ieOffsetX || 0, m = n.ieOffsetY || 0, n.ieOffsetX = Math.round((_ - ((0 > a ? -a : a) * _ + (0 > l ? -l : l) * g)) / 2 + b), n.ieOffsetY = Math.round((g - ((0 > u ? -u : u) * g + (0 > h ? -h : h) * _)) / 2 + w), fe = 0; 4 > fe; fe++) P = K[fe], S = p[P], i = -1 !== S.indexOf("px") ? parseFloat(S) : V(this.t, P, parseFloat(S), S.replace(y, "")) || 0, C = i !== n[P] ? 2 > fe ? -n.ieOffsetX : -n.ieOffsetY : 2 > fe ? d - n.ieOffsetX : m - n.ieOffsetY, c[P] = (n[P] = Math.round(i - C * (0 === fe || 2 === fe ? 1 : I))) + "px"
                            }
                        }
                    },
                    Ee = F.set3DTransformRatio = function(t) {
                        var e, i, n, s, r, o, a, l, h, u, c, d, f, m, _, g, v, y, x, T, b, w, S, P = this.data,
                            C = this.t.style,
                            I = P.rotation * E,
                            z = P.scaleX,
                            O = P.scaleY,
                            R = P.scaleZ,
                            M = P.x,
                            k = P.y,
                            A = P.z,
                            L = P.perspective;
                        if (!(1 !== t && 0 !== t || "auto" !== P.force3D || P.rotationY || P.rotationX || 1 !== R || L || A)) return void ke.call(this, t);
                        if (p) {
                            var D = 1e-4;
                            D > z && z > -D && (z = R = 2e-5), D > O && O > -D && (O = R = 2e-5), !L || P.z || P.rotationX || P.rotationY || (L = 0)
                        }
                        if (I || P.skewX) y = Math.cos(I), x = Math.sin(I), e = y, r = x, P.skewX && (I -= P.skewX * E, y = Math.cos(I), x = Math.sin(I), "simple" === P.skewType && (T = Math.tan(P.skewX * E), T = Math.sqrt(1 + T * T), y *= T, x *= T)), i = -x, o = y;
                        else {
                            if (!(P.rotationY || P.rotationX || 1 !== R || L || P.svg)) return void(C[xe] = (P.xPercent || P.yPercent ? "translate(" + P.xPercent + "%," + P.yPercent + "%) translate3d(" : "translate3d(") + M + "px," + k + "px," + A + "px)" + (1 !== z || 1 !== O ? " scale(" + z + "," + O + ")" : ""));
                            e = o = 1, i = r = 0
                        }
                        c = 1, n = s = a = l = h = u = d = f = m = 0, _ = L ? -1 / L : 0, g = P.zOrigin, v = 1e5, I = P.rotationY * E, I && (y = Math.cos(I), x = Math.sin(I), h = c * -x, f = _ * -x, n = e * x, a = r * x, c *= y, _ *= y, e *= y, r *= y), I = P.rotationX * E, I && (y = Math.cos(I), x = Math.sin(I), T = i * y + n * x, b = o * y + a * x, w = u * y + c * x, S = m * y + _ * x, n = i * -x + n * y, a = o * -x + a * y, c = u * -x + c * y, _ = m * -x + _ * y, i = T, o = b, u = w, m = S), 1 !== R && (n *= R, a *= R, c *= R, _ *= R), 1 !== O && (i *= O, o *= O, u *= O, m *= O), 1 !== z && (e *= z, r *= z, h *= z, f *= z), g && (d -= g, s = n * d, l = a * d, d = c * d + g), P.svg && (s += P.xOrigin - (P.xOrigin * e + P.yOrigin * i), l += P.yOrigin - (P.xOrigin * r + P.yOrigin * o)), s = (T = (s += M) - (s |= 0)) ? (0 | T * v + (0 > T ? -.5 : .5)) / v + s : s, l = (T = (l += k) - (l |= 0)) ? (0 | T * v + (0 > T ? -.5 : .5)) / v + l : l, d = (T = (d += A) - (d |= 0)) ? (0 | T * v + (0 > T ? -.5 : .5)) / v + d : d, C[xe] = (P.xPercent || P.yPercent ? "translate(" + P.xPercent + "%," + P.yPercent + "%) matrix3d(" : "matrix3d(") + [(0 | e * v) / v, (0 | r * v) / v, (0 | h * v) / v, (0 | f * v) / v, (0 | i * v) / v, (0 | o * v) / v, (0 | u * v) / v, (0 | m * v) / v, (0 | n * v) / v, (0 | a * v) / v, (0 | c * v) / v, (0 | _ * v) / v, s, l, d, L ? 1 + -d / L : 1].join(",") + ")"
                    },
                    ke = F.set2DTransformRatio = function(t) {
                        var e, i, n, s, r, o, a, l, h, u, c, p = this.data,
                            d = this.t,
                            f = d.style,
                            m = p.x,
                            _ = p.y;
                        return !(p.rotationX || p.rotationY || p.z || p.force3D === !0 || "auto" === p.force3D && 1 !== t && 0 !== t) || p.svg && ve || !we ? (s = p.scaleX, r = p.scaleY, void(p.rotation || p.skewX || p.svg ? (e = p.rotation * E, i = e - p.skewX * E, n = 1e5, o = Math.cos(e) * s, a = Math.sin(e) * s, l = Math.sin(i) * -r, h = Math.cos(i) * r, p.svg && (m += p.xOrigin - (p.xOrigin * o + p.yOrigin * l), _ += p.yOrigin - (p.xOrigin * a + p.yOrigin * h), c = 1e-6, c > m && m > -c && (m = 0), c > _ && _ > -c && (_ = 0)), u = (0 | o * n) / n + "," + (0 | a * n) / n + "," + (0 | l * n) / n + "," + (0 | h * n) / n + "," + m + "," + _ + ")", p.svg && ve ? d.setAttribute("transform", "matrix(" + u) : f[xe] = (p.xPercent || p.yPercent ? "translate(" + p.xPercent + "%," + p.yPercent + "%) matrix(" : "matrix(") + u) : f[xe] = (p.xPercent || p.yPercent ? "translate(" + p.xPercent + "%," + p.yPercent + "%) matrix(" : "matrix(") + s + ",0,0," + r + "," + m + "," + _ + ")")) : (this.setRatio = Ee, void Ee.call(this, t))
                    };
                _e("transform,scale,scaleX,scaleY,scaleZ,x,y,z,rotation,rotationX,rotationY,rotationZ,skewX,skewY,shortRotation,shortRotationX,shortRotationY,shortRotationZ,transformOrigin,transformPerspective,directionalRotation,parseTransform,force3D,skewType,xPercent,yPercent", {
                    parser: function(t, e, i, n, r, a, l) {
                        if (n._transform) return r;
                        var h, u, c, p, d, f, m, _ = n._transform = Re(t, s, !0, l.parseTransform),
                            g = t.style,
                            v = 1e-6,
                            y = ye.length,
                            x = l,
                            T = {};
                        if ("string" == typeof x.transform && xe) c = D.style, c[xe] = x.transform, c.display = "block", c.position = "absolute", L.body.appendChild(D), h = Re(D, null, !1), L.body.removeChild(D);
                        else if ("object" == typeof x) {
                            if (h = {
                                    scaleX: ne(null != x.scaleX ? x.scaleX : x.scale, _.scaleX),
                                    scaleY: ne(null != x.scaleY ? x.scaleY : x.scale, _.scaleY),
                                    scaleZ: ne(x.scaleZ, _.scaleZ),
                                    x: ne(x.x, _.x),
                                    y: ne(x.y, _.y),
                                    z: ne(x.z, _.z),
                                    xPercent: ne(x.xPercent, _.xPercent),
                                    yPercent: ne(x.yPercent, _.yPercent),
                                    perspective: ne(x.transformPerspective, _.perspective)
                                }, m = x.directionalRotation, null != m)
                                if ("object" == typeof m)
                                    for (c in m) x[c] = m[c];
                                else x.rotation = m;
                            "string" == typeof x.x && -1 !== x.x.indexOf("%") && (h.x = 0, h.xPercent = ne(x.x, _.xPercent)), "string" == typeof x.y && -1 !== x.y.indexOf("%") && (h.y = 0, h.yPercent = ne(x.y, _.yPercent)), h.rotation = se("rotation" in x ? x.rotation : "shortRotation" in x ? x.shortRotation + "_short" : "rotationZ" in x ? x.rotationZ : _.rotation, _.rotation, "rotation", T), we && (h.rotationX = se("rotationX" in x ? x.rotationX : "shortRotationX" in x ? x.shortRotationX + "_short" : _.rotationX || 0, _.rotationX, "rotationX", T), h.rotationY = se("rotationY" in x ? x.rotationY : "shortRotationY" in x ? x.shortRotationY + "_short" : _.rotationY || 0, _.rotationY, "rotationY", T)), h.skewX = null == x.skewX ? _.skewX : se(x.skewX, _.skewX), h.skewY = null == x.skewY ? _.skewY : se(x.skewY, _.skewY), (u = h.skewY - _.skewY) && (h.skewX += u, h.rotation += u)
                        }
                        for (we && null != x.force3D && (_.force3D = x.force3D, f = !0), _.skewType = x.skewType || _.skewType || o.defaultSkewType, d = _.force3D || _.z || _.rotationX || _.rotationY || h.z || h.rotationX || h.rotationY || h.perspective, d || null == x.scale || (h.scaleZ = 1); --y > -1;) i = ye[y], p = h[i] - _[i], (p > v || -v > p || null != x[i] || null != A[i]) && (f = !0, r = new pe(_, i, _[i], p, r), i in T && (r.e = T[i]), r.xs0 = 0, r.plugin = a, n._overwriteProps.push(r.n));
                        return p = x.transformOrigin, p && _.svg && (Oe(t, p, h), r = new pe(_, "xOrigin", _.xOrigin, h.xOrigin - _.xOrigin, r, -1, "transformOrigin"), r.b = _.xOrigin, r.e = r.xs0 = h.xOrigin, r = new pe(_, "yOrigin", _.yOrigin, h.yOrigin - _.yOrigin, r, -1, "transformOrigin"), r.b = _.yOrigin, r.e = r.xs0 = h.yOrigin, p = "0px 0px"), (p || we && d && _.zOrigin) && (xe ? (f = !0, i = be, p = (p || Q(t, i, s, !1, "50% 50%")) + "", r = new pe(g, i, 0, 0, r, -1, "transformOrigin"), r.b = g[i], r.plugin = a, we ? (c = _.zOrigin, p = p.split(" "), _.zOrigin = (p.length > 2 && (0 === c || "0px" !== p[2]) ? parseFloat(p[2]) : c) || 0, r.xs0 = r.e = p[0] + " " + (p[1] || "50%") + " 0px", r = new pe(_, "zOrigin", 0, 0, r, -1, r.n), r.b = c, r.xs0 = r.e = _.zOrigin) : r.xs0 = r.e = p) : ee(p + "", _)), f && (n._transformType = _.svg && ve || !d && 3 !== this._transformType ? 2 : 3), r
                    },
                    prefix: !0
                }), _e("boxShadow", {
                    defaultValue: "0px 0px 0px 0px #999",
                    prefix: !0,
                    color: !0,
                    multi: !0,
                    keyword: "inset"
                }), _e("borderRadius", {
                    defaultValue: "0px",
                    parser: function(t, e, i, r, o) {
                        e = this.format(e);
                        var a, l, h, u, c, p, d, f, m, _, g, v, y, x, T, b, w = ["borderTopLeftRadius", "borderTopRightRadius", "borderBottomRightRadius", "borderBottomLeftRadius"],
                            S = t.style;
                        for (m = parseFloat(t.offsetWidth), _ = parseFloat(t.offsetHeight), a = e.split(" "), l = 0; w.length > l; l++) this.p.indexOf("border") && (w[l] = U(w[l])), c = u = Q(t, w[l], s, !1, "0px"), -1 !== c.indexOf(" ") && (u = c.split(" "), c = u[0], u = u[1]), p = h = a[l], d = parseFloat(c), v = c.substr((d + "").length), y = "=" === p.charAt(1), y ? (f = parseInt(p.charAt(0) + "1", 10), p = p.substr(2), f *= parseFloat(p), g = p.substr((f + "").length - (0 > f ? 1 : 0)) || "") : (f = parseFloat(p), g = p.substr((f + "").length)), "" === g && (g = n[i] || v), g !== v && (x = V(t, "borderLeft", d, v), T = V(t, "borderTop", d, v), "%" === g ? (c = 100 * (x / m) + "%", u = 100 * (T / _) + "%") : "em" === g ? (b = V(t, "borderLeft", 1, "em"), c = x / b + "em", u = T / b + "em") : (c = x + "px", u = T + "px"), y && (p = parseFloat(c) + f + g, h = parseFloat(u) + f + g)), o = de(S, w[l], c + " " + u, p + " " + h, !1, "0px", o);
                        return o
                    },
                    prefix: !0,
                    formatter: he("0px 0px 0px 0px", !1, !0)
                }), _e("backgroundPosition", {
                    defaultValue: "0 0",
                    parser: function(t, e, i, n, r, o) {
                        var a, l, h, u, c, p, d = "background-position",
                            m = s || H(t, null),
                            _ = this.format((m ? f ? m.getPropertyValue(d + "-x") + " " + m.getPropertyValue(d + "-y") : m.getPropertyValue(d) : t.currentStyle.backgroundPositionX + " " + t.currentStyle.backgroundPositionY) || "0 0"),
                            g = this.format(e);
                        if (-1 !== _.indexOf("%") != (-1 !== g.indexOf("%")) && (p = Q(t, "backgroundImage").replace(C, ""), p && "none" !== p)) {
                            for (a = _.split(" "), l = g.split(" "), j.setAttribute("src", p), h = 2; --h > -1;) _ = a[h], u = -1 !== _.indexOf("%"), u !== (-1 !== l[h].indexOf("%")) && (c = 0 === h ? t.offsetWidth - j.width : t.offsetHeight - j.height, a[h] = u ? parseFloat(_) / 100 * c + "px" : 100 * (parseFloat(_) / c) + "%");
                            _ = a.join(" ")
                        }
                        return this.parseComplex(t.style, _, g, r, o)
                    },
                    formatter: ee
                }), _e("backgroundSize", {
                    defaultValue: "0 0",
                    formatter: ee
                }), _e("perspective", {
                    defaultValue: "0px",
                    prefix: !0
                }), _e("perspectiveOrigin", {
                    defaultValue: "50% 50%",
                    prefix: !0
                }), _e("transformStyle", {
                    prefix: !0
                }), _e("backfaceVisibility", {
                    prefix: !0
                }), _e("userSelect", {
                    prefix: !0
                }), _e("margin", {
                    parser: ue("marginTop,marginRight,marginBottom,marginLeft")
                }), _e("padding", {
                    parser: ue("paddingTop,paddingRight,paddingBottom,paddingLeft")
                }), _e("clip", {
                    defaultValue: "rect(0px,0px,0px,0px)",
                    parser: function(t, e, i, n, r, o) {
                        var a, l, h;
                        return 9 > f ? (l = t.currentStyle, h = 8 > f ? " " : ",", a = "rect(" + l.clipTop + h + l.clipRight + h + l.clipBottom + h + l.clipLeft + ")", e = this.format(e).split(",").join(h)) : (a = this.format(Q(t, this.p, s, !1, this.dflt)), e = this.format(e)), this.parseComplex(t.style, a, e, r, o)
                    }
                }), _e("textShadow", {
                    defaultValue: "0px 0px 0px #999",
                    color: !0,
                    multi: !0
                }), _e("autoRound,strictUnits", {
                    parser: function(t, e, i, n, s) {
                        return s
                    }
                }), _e("border", {
                    defaultValue: "0px solid #000",
                    parser: function(t, e, i, n, r, o) {
                        return this.parseComplex(t.style, this.format(Q(t, "borderTopWidth", s, !1, "0px") + " " + Q(t, "borderTopStyle", s, !1, "solid") + " " + Q(t, "borderTopColor", s, !1, "#000")), this.format(e), r, o)
                    },
                    color: !0,
                    formatter: function(t) {
                        var e = t.split(" ");
                        return e[0] + " " + (e[1] || "solid") + " " + (t.match(le) || ["#000"])[0]
                    }
                }), _e("borderWidth", {
                    parser: ue("borderTopWidth,borderRightWidth,borderBottomWidth,borderLeftWidth")
                }), _e("float,cssFloat,styleFloat", {
                    parser: function(t, e, i, n, s) {
                        var r = t.style,
                            o = "cssFloat" in r ? "cssFloat" : "styleFloat";
                        return new pe(r, o, 0, 0, s, -1, i, !1, 0, r[o], e)
                    }
                });
                var Ae = function(t) {
                    var e, i = this.t,
                        n = i.filter || Q(this.data, "filter") || "",
                        s = 0 | this.s + this.c * t;
                    100 === s && (-1 === n.indexOf("atrix(") && -1 === n.indexOf("radient(") && -1 === n.indexOf("oader(") ? (i.removeAttribute("filter"), e = !Q(this.data, "filter")) : (i.filter = n.replace(b, ""), e = !0)), e || (this.xn1 && (i.filter = n = n || "alpha(opacity=" + s + ")"), -1 === n.indexOf("pacity") ? 0 === s && this.xn1 || (i.filter = n + " alpha(opacity=" + s + ")") : i.filter = n.replace(x, "opacity=" + s))
                };
                _e("opacity,alpha,autoAlpha", {
                    defaultValue: "1",
                    parser: function(t, e, i, n, r, o) {
                        var a = parseFloat(Q(t, "opacity", s, !1, "1")),
                            l = t.style,
                            h = "autoAlpha" === i;
                        return "string" == typeof e && "=" === e.charAt(1) && (e = ("-" === e.charAt(0) ? -1 : 1) * parseFloat(e.substr(2)) + a), h && 1 === a && "hidden" === Q(t, "visibility", s) && 0 !== e && (a = 0), W ? r = new pe(l, "opacity", a, e - a, r) : (r = new pe(l, "opacity", 100 * a, 100 * (e - a), r), r.xn1 = h ? 1 : 0, l.zoom = 1, r.type = 2, r.b = "alpha(opacity=" + r.s + ")", r.e = "alpha(opacity=" + (r.s + r.c) + ")", r.data = t, r.plugin = o, r.setRatio = Ae), h && (r = new pe(l, "visibility", 0, 0, r, -1, null, !1, 0, 0 !== a ? "inherit" : "hidden", 0 === e ? "hidden" : "inherit"), r.xs0 = "inherit", n._overwriteProps.push(r.n), n._overwriteProps.push(i)), r
                    }
                });
                var Le = function(t, e) {
                        e && (t.removeProperty ? ("ms" === e.substr(0, 2) && (e = "M" + e.substr(1)), t.removeProperty(e.replace(S, "-$1").toLowerCase())) : t.removeAttribute(e))
                    },
                    De = function(t) {
                        if (this.t._gsClassPT = this, 1 === t || 0 === t) {
                            this.t.setAttribute("class", 0 === t ? this.b : this.e);
                            for (var e = this.data, i = this.t.style; e;) e.v ? i[e.p] = e.v : Le(i, e.p), e = e._next;
                            1 === t && this.t._gsClassPT === this && (this.t._gsClassPT = null)
                        } else this.t.getAttribute("class") !== this.e && this.t.setAttribute("class", this.e)
                    };
                _e("className", {
                    parser: function(t, e, n, r, o, a, l) {
                        var h, u, c, p, d, f = t.getAttribute("class") || "",
                            m = t.style.cssText;
                        if (o = r._classNamePT = new pe(t, n, 0, 0, o, 2), o.setRatio = De, o.pr = -11, i = !0, o.b = f, u = Z(t, s), c = t._gsClassPT) {
                            for (p = {}, d = c.data; d;) p[d.p] = 1, d = d._next;
                            c.setRatio(1)
                        }
                        return t._gsClassPT = o, o.e = "=" !== e.charAt(1) ? e : f.replace(RegExp("\\s*\\b" + e.substr(2) + "\\b"), "") + ("+" === e.charAt(0) ? " " + e.substr(2) : ""), r._tween._duration && (t.setAttribute("class", o.e), h = $(t, u, Z(t), l, p), t.setAttribute("class", f), o.data = h.firstMPT, t.style.cssText = m, o = o.xfirst = r.parse(t, h.difs, o, a)), o
                    }
                });
                var je = function(t) {
                    if ((1 === t || 0 === t) && this.data._totalTime === this.data._totalDuration && "isFromStart" !== this.data.data) {
                        var e, i, n, s, r = this.t.style,
                            o = a.transform.parse;
                        if ("all" === this.e) r.cssText = "", s = !0;
                        else
                            for (e = this.e.split(" ").join("").split(","), n = e.length; --n > -1;) i = e[n], a[i] && (a[i].parse === o ? s = !0 : i = "transformOrigin" === i ? be : a[i].p), Le(r, i);
                        s && (Le(r, xe), this.t._gsTransform && delete this.t._gsTransform)
                    }
                };
                for (_e("clearProps", {
                        parser: function(t, e, n, s, r) {
                            return r = new pe(t, n, 0, 0, r, 2), r.setRatio = je, r.e = e, r.pr = -10, r.data = s._tween, i = !0, r
                        }
                    }), l = "bezier,throwProps,physicsProps,physics2D".split(","), fe = l.length; fe--;) ge(l[fe]);
                l = o.prototype, l._firstPT = null, l._onInitTween = function(t, e, a) {
                    if (!t.nodeType) return !1;
                    this._target = t, this._tween = a, this._vars = e, h = e.autoRound, i = !1, n = e.suffixMap || o.suffixMap, s = H(t, ""), r = this._overwriteProps;
                    var l, p, f, m, _, g, v, y, x, b = t.style;
                    if (u && "" === b.zIndex && (l = Q(t, "zIndex", s), ("auto" === l || "" === l) && this._addLazySet(b, "zIndex", 0)), "string" == typeof e && (m = b.cssText, l = Z(t, s), b.cssText = m + ";" + e, l = $(t, l, Z(t)).difs, !W && T.test(e) && (l.opacity = parseFloat(RegExp.$1)), e = l, b.cssText = m), this._firstPT = p = this.parse(t, e, null), this._transformType) {
                        for (x = 3 === this._transformType, xe ? c && (u = !0, "" === b.zIndex && (v = Q(t, "zIndex", s), ("auto" === v || "" === v) && this._addLazySet(b, "zIndex", 0)), d && this._addLazySet(b, "WebkitBackfaceVisibility", this._vars.WebkitBackfaceVisibility || (x ? "visible" : "hidden"))) : b.zoom = 1, f = p; f && f._next;) f = f._next;
                        y = new pe(t, "transform", 0, 0, null, 2), this._linkCSSP(y, null, f), y.setRatio = x && we ? Ee : xe ? ke : Me, y.data = this._transform || Re(t, s, !0), r.pop()
                    }
                    if (i) {
                        for (; p;) {
                            for (g = p._next, f = m; f && f.pr > p.pr;) f = f._next;
                            (p._prev = f ? f._prev : _) ? p._prev._next = p: m = p, (p._next = f) ? f._prev = p : _ = p, p = g
                        }
                        this._firstPT = m
                    }
                    return !0
                }, l.parse = function(t, e, i, r) {
                    var o, l, u, c, p, d, f, m, _, g, v = t.style;
                    for (o in e) d = e[o], l = a[o], l ? i = l.parse(t, d, o, this, i, r, e) : (p = Q(t, o, s) + "", _ = "string" == typeof d, "color" === o || "fill" === o || "stroke" === o || -1 !== o.indexOf("Color") || _ && w.test(d) ? (_ || (d = ae(d), d = (d.length > 3 ? "rgba(" : "rgb(") + d.join(",") + ")"), i = de(v, o, p, d, !0, "transparent", i, 0, r)) : !_ || -1 === d.indexOf(" ") && -1 === d.indexOf(",") ? (u = parseFloat(p), f = u || 0 === u ? p.substr((u + "").length) : "", ("" === p || "auto" === p) && ("width" === o || "height" === o ? (u = te(t, o, s), f = "px") : "left" === o || "top" === o ? (u = G(t, o, s), f = "px") : (u = "opacity" !== o ? 0 : 1, f = "")), g = _ && "=" === d.charAt(1), g ? (c = parseInt(d.charAt(0) + "1", 10), d = d.substr(2), c *= parseFloat(d), m = d.replace(y, "")) : (c = parseFloat(d), m = _ ? d.substr((c + "").length) || "" : ""), "" === m && (m = o in n ? n[o] : f), d = c || 0 === c ? (g ? c + u : c) + m : e[o], f !== m && "" !== m && (c || 0 === c) && u && (u = V(t, o, u, f), "%" === m ? (u /= V(t, o, 100, "%") / 100, e.strictUnits !== !0 && (p = u + "%")) : "em" === m ? u /= V(t, o, 1, "em") : "px" !== m && (c = V(t, o, c, m), m = "px"), g && (c || 0 === c) && (d = c + u + m)), g && (c += u), !u && 0 !== u || !c && 0 !== c ? void 0 !== v[o] && (d || "NaN" != d + "" && null != d) ? (i = new pe(v, o, c || u || 0, 0, i, -1, o, !1, 0, p, d), i.xs0 = "none" !== d || "display" !== o && -1 === o.indexOf("Style") ? d : p) : X("invalid " + o + " tween value: " + e[o]) : (i = new pe(v, o, u, c - u, i, 0, o, h !== !1 && ("px" === m || "zIndex" === o), 0, p, d), i.xs0 = m)) : i = de(v, o, p, d, !0, null, i, 0, r)), r && i && !i.plugin && (i.plugin = r);
                    return i
                }, l.setRatio = function(t) {
                    var e, i, n, s = this._firstPT,
                        r = 1e-6;
                    if (1 !== t || this._tween._time !== this._tween._duration && 0 !== this._tween._time)
                        if (t || this._tween._time !== this._tween._duration && 0 !== this._tween._time || this._tween._rawPrevTime === -1e-6)
                            for (; s;) {
                                if (e = s.c * t + s.s, s.r ? e = Math.round(e) : r > e && e > -r && (e = 0), s.type)
                                    if (1 === s.type)
                                        if (n = s.l, 2 === n) s.t[s.p] = s.xs0 + e + s.xs1 + s.xn1 + s.xs2;
                                        else if (3 === n) s.t[s.p] = s.xs0 + e + s.xs1 + s.xn1 + s.xs2 + s.xn2 + s.xs3;
                                else if (4 === n) s.t[s.p] = s.xs0 + e + s.xs1 + s.xn1 + s.xs2 + s.xn2 + s.xs3 + s.xn3 + s.xs4;
                                else if (5 === n) s.t[s.p] = s.xs0 + e + s.xs1 + s.xn1 + s.xs2 + s.xn2 + s.xs3 + s.xn3 + s.xs4 + s.xn4 + s.xs5;
                                else {
                                    for (i = s.xs0 + e + s.xs1, n = 1; s.l > n; n++) i += s["xn" + n] + s["xs" + (n + 1)];
                                    s.t[s.p] = i
                                } else -1 === s.type ? s.t[s.p] = s.xs0 : s.setRatio && s.setRatio(t);
                                else s.t[s.p] = e + s.xs0;
                                s = s._next
                            } else
                                for (; s;) 2 !== s.type ? s.t[s.p] = s.b : s.setRatio(t), s = s._next;
                        else
                            for (; s;) 2 !== s.type ? s.t[s.p] = s.e : s.setRatio(t), s = s._next
                }, l._enableTransforms = function(t) {
                    this._transform = this._transform || Re(this._target, s, !0), this._transformType = this._transform.svg && ve || !t && 3 !== this._transformType ? 2 : 3
                };
                var Fe = function() {
                    this.t[this.p] = this.e, this.data._linkCSSP(this, this._next, null, !0)
                };
                l._addLazySet = function(t, e, i) {
                    var n = this._firstPT = new pe(t, e, 0, 0, this._firstPT, 2);
                    n.e = i, n.setRatio = Fe, n.data = this
                }, l._linkCSSP = function(t, e, i, n) {
                    return t && (e && (e._prev = t), t._next && (t._next._prev = t._prev), t._prev ? t._prev._next = t._next : this._firstPT === t && (this._firstPT = t._next, n = !0), i ? i._next = t : n || null !== this._firstPT || (this._firstPT = t), t._next = e, t._prev = i), t
                }, l._kill = function(e) {
                    var i, n, s, r = e;
                    if (e.autoAlpha || e.alpha) {
                        r = {};
                        for (n in e) r[n] = e[n];
                        r.opacity = 1, r.autoAlpha && (r.visibility = 1)
                    }
                    return e.className && (i = this._classNamePT) && (s = i.xfirst, s && s._prev ? this._linkCSSP(s._prev, i._next, s._prev._prev) : s === this._firstPT && (this._firstPT = i._next), i._next && this._linkCSSP(i._next, i._next._next, s._prev), this._classNamePT = null), t.prototype._kill.call(this, r)
                };
                var Ne = function(t, e, i) {
                    var n, s, r, o;
                    if (t.slice)
                        for (s = t.length; --s > -1;) Ne(t[s], e, i);
                    else
                        for (n = t.childNodes, s = n.length; --s > -1;) r = n[s], o = r.type, r.style && (e.push(Z(r)), i && i.push(r)), 1 !== o && 9 !== o && 11 !== o || !r.childNodes.length || Ne(r, e, i)
                };
                return o.cascadeTo = function(t, i, n) {
                    var s, r, o, a = e.to(t, i, n),
                        l = [a],
                        h = [],
                        u = [],
                        c = [],
                        p = e._internals.reservedProps;
                    for (t = a._targets || a.target, Ne(t, h, c), a.render(i, !0), Ne(t, u), a.render(0, !0), a._enabled(!0), s = c.length; --s > -1;)
                        if (r = $(c[s], h[s], u[s]), r.firstMPT) {
                            r = r.difs;
                            for (o in n) p[o] && (r[o] = n[o]);
                            l.push(e.to(c[s], i, r))
                        }
                    return l
                }, t.activate([o]), o
            }, !0),
            function() {
                var t = _gsScope._gsDefine.plugin({
                        propName: "roundProps",
                        priority: -1,
                        API: 2,
                        init: function(t, e, i) {
                            return this._tween = i, !0
                        }
                    }),
                    e = t.prototype;
                e._onInitAllProps = function() {
                    for (var t, e, i, n = this._tween, s = n.vars.roundProps instanceof Array ? n.vars.roundProps : n.vars.roundProps.split(","), r = s.length, o = {}, a = n._propLookup.roundProps; --r > -1;) o[s[r]] = 1;
                    for (r = s.length; --r > -1;)
                        for (t = s[r], e = n._firstPT; e;) i = e._next, e.pg ? e.t._roundProps(o, !0) : e.n === t && (this._add(e.t, t, e.s, e.c), i && (i._prev = e._prev), e._prev ? e._prev._next = i : n._firstPT === e && (n._firstPT = i), e._next = e._prev = null, n._propLookup[t] = a), e = i;
                    return !1
                }, e._add = function(t, e, i, n) {
                    this._addTween(t, e, i, i + n, e, !0), this._overwriteProps.push(e)
                }
            }(), _gsScope._gsDefine.plugin({
                propName: "attr",
                API: 2,
                version: "0.3.3",
                init: function(t, e) {
                    var i, n, s;
                    if ("function" != typeof t.setAttribute) return !1;
                    this._target = t, this._proxy = {}, this._start = {}, this._end = {};
                    for (i in e) this._start[i] = this._proxy[i] = n = t.getAttribute(i), s = this._addTween(this._proxy, i, parseFloat(n), e[i], i), this._end[i] = s ? s.s + s.c : e[i], this._overwriteProps.push(i);
                    return !0
                },
                set: function(t) {
                    this._super.setRatio.call(this, t);
                    for (var e, i = this._overwriteProps, n = i.length, s = 1 === t ? this._end : t ? this._proxy : this._start; --n > -1;) e = i[n], this._target.setAttribute(e, s[e] + "")
                }
            }), _gsScope._gsDefine.plugin({
                propName: "directionalRotation",
                version: "0.2.1",
                API: 2,
                init: function(t, e) {
                    "object" != typeof e && (e = {
                        rotation: e
                    }), this.finals = {};
                    var i, n, s, r, o, a, l = e.useRadians === !0 ? 2 * Math.PI : 360,
                        h = 1e-6;
                    for (i in e) "useRadians" !== i && (a = (e[i] + "").split("_"), n = a[0], s = parseFloat("function" != typeof t[i] ? t[i] : t[i.indexOf("set") || "function" != typeof t["get" + i.substr(3)] ? i : "get" + i.substr(3)]()), r = this.finals[i] = "string" == typeof n && "=" === n.charAt(1) ? s + parseInt(n.charAt(0) + "1", 10) * Number(n.substr(2)) : Number(n) || 0, o = r - s, a.length && (n = a.join("_"), -1 !== n.indexOf("short") && (o %= l, o !== o % (l / 2) && (o = 0 > o ? o + l : o - l)), -1 !== n.indexOf("_cw") && 0 > o ? o = (o + 9999999999 * l) % l - (0 | o / l) * l : -1 !== n.indexOf("ccw") && o > 0 && (o = (o - 9999999999 * l) % l - (0 | o / l) * l)), (o > h || -h > o) && (this._addTween(t, i, s, s + o, i), this._overwriteProps.push(i)));
                    return !0
                },
                set: function(t) {
                    var e;
                    if (1 !== t) this._super.setRatio.call(this, t);
                    else
                        for (e = this._firstPT; e;) e.f ? e.t[e.p](this.finals[e.p]) : e.t[e.p] = this.finals[e.p], e = e._next
                }
            })._autoCSS = !0, _gsScope._gsDefine("easing.Back", ["easing.Ease"], function(t) {
                var e, i, n, s = _gsScope.GreenSockGlobals || _gsScope,
                    r = s.com.greensock,
                    o = 2 * Math.PI,
                    a = Math.PI / 2,
                    l = r._class,
                    h = function(e, i) {
                        var n = l("easing." + e, function() {}, !0),
                            s = n.prototype = new t;
                        return s.constructor = n, s.getRatio = i, n
                    },
                    u = t.register || function() {},
                    c = function(t, e, i, n) {
                        var s = l("easing." + t, {
                            easeOut: new e,
                            easeIn: new i,
                            easeInOut: new n
                        }, !0);
                        return u(s, t), s
                    },
                    p = function(t, e, i) {
                        this.t = t, this.v = e, i && (this.next = i, i.prev = this, this.c = i.v - e, this.gap = i.t - t)
                    },
                    d = function(e, i) {
                        var n = l("easing." + e, function(t) {
                                this._p1 = t || 0 === t ? t : 1.70158, this._p2 = 1.525 * this._p1
                            }, !0),
                            s = n.prototype = new t;
                        return s.constructor = n, s.getRatio = i, s.config = function(t) {
                            return new n(t)
                        }, n
                    },
                    f = c("Back", d("BackOut", function(t) {
                        return (t -= 1) * t * ((this._p1 + 1) * t + this._p1) + 1
                    }), d("BackIn", function(t) {
                        return t * t * ((this._p1 + 1) * t - this._p1)
                    }), d("BackInOut", function(t) {
                        return 1 > (t *= 2) ? .5 * t * t * ((this._p2 + 1) * t - this._p2) : .5 * ((t -= 2) * t * ((this._p2 + 1) * t + this._p2) + 2)
                    })),
                    m = l("easing.SlowMo", function(t, e, i) {
                        e = e || 0 === e ? e : .7, null == t ? t = .7 : t > 1 && (t = 1), this._p = 1 !== t ? e : 0, this._p1 = (1 - t) / 2, this._p2 = t, this._p3 = this._p1 + this._p2, this._calcEnd = i === !0
                    }, !0),
                    _ = m.prototype = new t;
                return _.constructor = m, _.getRatio = function(t) {
                    var e = t + (.5 - t) * this._p;
                    return this._p1 > t ? this._calcEnd ? 1 - (t = 1 - t / this._p1) * t : e - (t = 1 - t / this._p1) * t * t * t * e : t > this._p3 ? this._calcEnd ? 1 - (t = (t - this._p3) / this._p1) * t : e + (t - e) * (t = (t - this._p3) / this._p1) * t * t * t : this._calcEnd ? 1 : e
                }, m.ease = new m(.7, .7), _.config = m.config = function(t, e, i) {
                    return new m(t, e, i)
                }, e = l("easing.SteppedEase", function(t) {
                    t = t || 1, this._p1 = 1 / t, this._p2 = t + 1
                }, !0), _ = e.prototype = new t, _.constructor = e, _.getRatio = function(t) {
                    return 0 > t ? t = 0 : t >= 1 && (t = .999999999), (this._p2 * t >> 0) * this._p1
                }, _.config = e.config = function(t) {
                    return new e(t)
                }, i = l("easing.RoughEase", function(e) {
                    e = e || {};
                    for (var i, n, s, r, o, a, l = e.taper || "none", h = [], u = 0, c = 0 | (e.points || 20), d = c, f = e.randomize !== !1, m = e.clamp === !0, _ = e.template instanceof t ? e.template : null, g = "number" == typeof e.strength ? .4 * e.strength : .4; --d > -1;) i = f ? Math.random() : 1 / c * d, n = _ ? _.getRatio(i) : i, "none" === l ? s = g : "out" === l ? (r = 1 - i, s = r * r * g) : "in" === l ? s = i * i * g : .5 > i ? (r = 2 * i, s = .5 * r * r * g) : (r = 2 * (1 - i), s = .5 * r * r * g), f ? n += Math.random() * s - .5 * s : d % 2 ? n += .5 * s : n -= .5 * s, m && (n > 1 ? n = 1 : 0 > n && (n = 0)), h[u++] = {
                        x: i,
                        y: n
                    };
                    for (h.sort(function(t, e) {
                            return t.x - e.x
                        }), a = new p(1, 1, null), d = c; --d > -1;) o = h[d], a = new p(o.x, o.y, a);
                    this._prev = new p(0, 0, 0 !== a.t ? a : a.next)
                }, !0), _ = i.prototype = new t, _.constructor = i, _.getRatio = function(t) {
                    var e = this._prev;
                    if (t > e.t) {
                        for (; e.next && t >= e.t;) e = e.next;
                        e = e.prev
                    } else
                        for (; e.prev && e.t >= t;) e = e.prev;
                    return this._prev = e, e.v + (t - e.t) / e.gap * e.c
                }, _.config = function(t) {
                    return new i(t)
                }, i.ease = new i, c("Bounce", h("BounceOut", function(t) {
                    return 1 / 2.75 > t ? 7.5625 * t * t : 2 / 2.75 > t ? 7.5625 * (t -= 1.5 / 2.75) * t + .75 : 2.5 / 2.75 > t ? 7.5625 * (t -= 2.25 / 2.75) * t + .9375 : 7.5625 * (t -= 2.625 / 2.75) * t + .984375
                }), h("BounceIn", function(t) {
                    return 1 / 2.75 > (t = 1 - t) ? 1 - 7.5625 * t * t : 2 / 2.75 > t ? 1 - (7.5625 * (t -= 1.5 / 2.75) * t + .75) : 2.5 / 2.75 > t ? 1 - (7.5625 * (t -= 2.25 / 2.75) * t + .9375) : 1 - (7.5625 * (t -= 2.625 / 2.75) * t + .984375)
                }), h("BounceInOut", function(t) {
                    var e = .5 > t;
                    return t = e ? 1 - 2 * t : 2 * t - 1, t = 1 / 2.75 > t ? 7.5625 * t * t : 2 / 2.75 > t ? 7.5625 * (t -= 1.5 / 2.75) * t + .75 : 2.5 / 2.75 > t ? 7.5625 * (t -= 2.25 / 2.75) * t + .9375 : 7.5625 * (t -= 2.625 / 2.75) * t + .984375, e ? .5 * (1 - t) : .5 * t + .5
                })), c("Circ", h("CircOut", function(t) {
                    return Math.sqrt(1 - (t -= 1) * t)
                }), h("CircIn", function(t) {
                    return -(Math.sqrt(1 - t * t) - 1)
                }), h("CircInOut", function(t) {
                    return 1 > (t *= 2) ? -.5 * (Math.sqrt(1 - t * t) - 1) : .5 * (Math.sqrt(1 - (t -= 2) * t) + 1)
                })), n = function(e, i, n) {
                    var s = l("easing." + e, function(t, e) {
                            this._p1 = t || 1, this._p2 = e || n, this._p3 = this._p2 / o * (Math.asin(1 / this._p1) || 0)
                        }, !0),
                        r = s.prototype = new t;
                    return r.constructor = s, r.getRatio = i, r.config = function(t, e) {
                        return new s(t, e)
                    }, s
                }, c("Elastic", n("ElasticOut", function(t) {
                    return this._p1 * Math.pow(2, -10 * t) * Math.sin((t - this._p3) * o / this._p2) + 1
                }, .3), n("ElasticIn", function(t) {
                    return -(this._p1 * Math.pow(2, 10 * (t -= 1)) * Math.sin((t - this._p3) * o / this._p2))
                }, .3), n("ElasticInOut", function(t) {
                    return 1 > (t *= 2) ? -.5 * this._p1 * Math.pow(2, 10 * (t -= 1)) * Math.sin((t - this._p3) * o / this._p2) : .5 * this._p1 * Math.pow(2, -10 * (t -= 1)) * Math.sin((t - this._p3) * o / this._p2) + 1
                }, .45)), c("Expo", h("ExpoOut", function(t) {
                    return 1 - Math.pow(2, -10 * t)
                }), h("ExpoIn", function(t) {
                    return Math.pow(2, 10 * (t - 1)) - .001
                }), h("ExpoInOut", function(t) {
                    return 1 > (t *= 2) ? .5 * Math.pow(2, 10 * (t - 1)) : .5 * (2 - Math.pow(2, -10 * (t - 1)))
                })), c("Sine", h("SineOut", function(t) {
                    return Math.sin(t * a)
                }), h("SineIn", function(t) {
                    return -Math.cos(t * a) + 1
                }), h("SineInOut", function(t) {
                    return -.5 * (Math.cos(Math.PI * t) - 1)
                })), l("easing.EaseLookup", {
                    find: function(e) {
                        return t.map[e]
                    }
                }, !0), u(s.SlowMo, "SlowMo", "ease,"), u(i, "RoughEase", "ease,"), u(e, "SteppedEase", "ease,"), f
            }, !0)
    }), _gsScope._gsDefine && _gsScope._gsQueue.pop()(),
    function(t, e) {
        "use strict";
        var i = t.GreenSockGlobals = t.GreenSockGlobals || t;
        if (!i.TweenLite) {
            var n, s, r, o, a, l = function(t) {
                    var e, n = t.split("."),
                        s = i;
                    for (e = 0; n.length > e; e++) s[n[e]] = s = s[n[e]] || {};
                    return s
                },
                h = l("com.greensock"),
                u = 1e-10,
                c = function(t) {
                    var e, i = [],
                        n = t.length;
                    for (e = 0; e !== n; i.push(t[e++]));
                    return i
                },
                p = function() {},
                d = function() {
                    var t = Object.prototype.toString,
                        e = t.call([]);
                    return function(i) {
                        return null != i && (i instanceof Array || "object" == typeof i && !!i.push && t.call(i) === e)
                    }
                }(),
                f = {},
                m = function(n, s, r, o) {
                    this.sc = f[n] ? f[n].sc : [], f[n] = this, this.gsClass = null, this.func = r;
                    var a = [];
                    this.check = function(h) {
                        for (var u, c, p, d, _ = s.length, g = _; --_ > -1;)(u = f[s[_]] || new m(s[_], [])).gsClass ? (a[_] = u.gsClass, g--) : h && u.sc.push(this);
                        if (0 === g && r)
                            for (c = ("com.greensock." + n).split("."), p = c.pop(), d = l(c.join("."))[p] = this.gsClass = r.apply(r, a), o && (i[p] = d, "function" == typeof define && define.amd ? define((t.GreenSockAMDPath ? t.GreenSockAMDPath + "/" : "") + n.split(".").pop(), [], function() {
                                    return d
                                }) : n === e && "undefined" != typeof module && module.exports && (module.exports = d)), _ = 0; this.sc.length > _; _++) this.sc[_].check()
                    }, this.check(!0)
                },
                _ = t._gsDefine = function(t, e, i, n) {
                    return new m(t, e, i, n)
                },
                g = h._class = function(t, e, i) {
                    return e = e || function() {}, _(t, [], function() {
                        return e
                    }, i), e
                };
            _.globals = i;
            var v = [0, 0, 1, 1],
                y = [],
                x = g("easing.Ease", function(t, e, i, n) {
                    this._func = t, this._type = i || 0, this._power = n || 0, this._params = e ? v.concat(e) : v
                }, !0),
                T = x.map = {},
                b = x.register = function(t, e, i, n) {
                    for (var s, r, o, a, l = e.split(","), u = l.length, c = (i || "easeIn,easeOut,easeInOut").split(","); --u > -1;)
                        for (r = l[u], s = n ? g("easing." + r, null, !0) : h.easing[r] || {}, o = c.length; --o > -1;) a = c[o], T[r + "." + a] = T[a + r] = s[a] = t.getRatio ? t : t[a] || new t
                };
            for (r = x.prototype, r._calcEnd = !1, r.getRatio = function(t) {
                    if (this._func) return this._params[0] = t, this._func.apply(null, this._params);
                    var e = this._type,
                        i = this._power,
                        n = 1 === e ? 1 - t : 2 === e ? t : .5 > t ? 2 * t : 2 * (1 - t);
                    return 1 === i ? n *= n : 2 === i ? n *= n * n : 3 === i ? n *= n * n * n : 4 === i && (n *= n * n * n * n), 1 === e ? 1 - n : 2 === e ? n : .5 > t ? n / 2 : 1 - n / 2
                }, n = ["Linear", "Quad", "Cubic", "Quart", "Quint,Strong"], s = n.length; --s > -1;) r = n[s] + ",Power" + s, b(new x(null, null, 1, s), r, "easeOut", !0), b(new x(null, null, 2, s), r, "easeIn" + (0 === s ? ",easeNone" : "")), b(new x(null, null, 3, s), r, "easeInOut");
            T.linear = h.easing.Linear.easeIn, T.swing = h.easing.Quad.easeInOut;
            var w = g("events.EventDispatcher", function(t) {
                this._listeners = {}, this._eventTarget = t || this
            });
            r = w.prototype, r.addEventListener = function(t, e, i, n, s) {
                s = s || 0;
                var r, l, h = this._listeners[t],
                    u = 0;
                for (null == h && (this._listeners[t] = h = []), l = h.length; --l > -1;) r = h[l], r.c === e && r.s === i ? h.splice(l, 1) : 0 === u && s > r.pr && (u = l + 1);
                h.splice(u, 0, {
                    c: e,
                    s: i,
                    up: n,
                    pr: s
                }), this !== o || a || o.wake()
            }, r.removeEventListener = function(t, e) {
                var i, n = this._listeners[t];
                if (n)
                    for (i = n.length; --i > -1;)
                        if (n[i].c === e) return void n.splice(i, 1)
            }, r.dispatchEvent = function(t) {
                var e, i, n, s = this._listeners[t];
                if (s)
                    for (e = s.length, i = this._eventTarget; --e > -1;) n = s[e], n && (n.up ? n.c.call(n.s || i, {
                        type: t,
                        target: i
                    }) : n.c.call(n.s || i))
            };
            var S = t.requestAnimationFrame,
                P = t.cancelAnimationFrame,
                C = Date.now || function() {
                    return (new Date).getTime()
                },
                I = C();
            for (n = ["ms", "moz", "webkit", "o"], s = n.length; --s > -1 && !S;) S = t[n[s] + "RequestAnimationFrame"], P = t[n[s] + "CancelAnimationFrame"] || t[n[s] + "CancelRequestAnimationFrame"];
            g("Ticker", function(t, e) {
                var i, n, s, r, l, h = this,
                    c = C(),
                    d = e !== !1 && S,
                    f = 500,
                    m = 33,
                    _ = function(t) {
                        var e, o, a = C() - I;
                        a > f && (c += a - m), I += a, h.time = (I - c) / 1e3, e = h.time - l, (!i || e > 0 || t === !0) && (h.frame++, l += e + (e >= r ? .004 : r - e), o = !0), t !== !0 && (s = n(_)), o && h.dispatchEvent("tick")
                    };
                w.call(h), h.time = h.frame = 0, h.tick = function() {
                    _(!0)
                }, h.lagSmoothing = function(t, e) {
                    f = t || 1 / u, m = Math.min(e, f, 0)
                }, h.sleep = function() {
                    null != s && (d && P ? P(s) : clearTimeout(s), n = p, s = null, h === o && (a = !1))
                }, h.wake = function() {
                    null !== s ? h.sleep() : h.frame > 10 && (I = C() - f + 5), n = 0 === i ? p : d && S ? S : function(t) {
                        return setTimeout(t, 0 | 1e3 * (l - h.time) + 1)
                    }, h === o && (a = !0), _(2)
                }, h.fps = function(t) {
                    return arguments.length ? (i = t, r = 1 / (i || 60), l = this.time + r, void h.wake()) : i
                }, h.useRAF = function(t) {
                    return arguments.length ? (h.sleep(), d = t, void h.fps(i)) : d
                }, h.fps(t), setTimeout(function() {
                    d && (!s || 5 > h.frame) && h.useRAF(!1)
                }, 1500)
            }), r = h.Ticker.prototype = new h.events.EventDispatcher, r.constructor = h.Ticker;
            var z = g("core.Animation", function(t, e) {
                if (this.vars = e = e || {}, this._duration = this._totalDuration = t || 0, this._delay = Number(e.delay) || 0, this._timeScale = 1, this._active = e.immediateRender === !0, this.data = e.data, this._reversed = e.reversed === !0, B) {
                    a || o.wake();
                    var i = this.vars.useFrames ? X : B;
                    i.add(this, i._time), this.vars.paused && this.paused(!0)
                }
            });
            o = z.ticker = new h.Ticker, r = z.prototype, r._dirty = r._gc = r._initted = r._paused = !1, r._totalTime = r._time = 0, r._rawPrevTime = -1, r._next = r._last = r._onUpdate = r._timeline = r.timeline = null, r._paused = !1;
            var O = function() {
                a && C() - I > 2e3 && o.wake(), setTimeout(O, 2e3)
            };
            O(), r.play = function(t, e) {
                return null != t && this.seek(t, e), this.reversed(!1).paused(!1)
            }, r.pause = function(t, e) {
                return null != t && this.seek(t, e), this.paused(!0)
            }, r.resume = function(t, e) {
                return null != t && this.seek(t, e), this.paused(!1)
            }, r.seek = function(t, e) {
                return this.totalTime(Number(t), e !== !1)
            }, r.restart = function(t, e) {
                return this.reversed(!1).paused(!1).totalTime(t ? -this._delay : 0, e !== !1, !0)
            }, r.reverse = function(t, e) {
                return null != t && this.seek(t || this.totalDuration(), e), this.reversed(!0).paused(!1)
            }, r.render = function() {}, r.invalidate = function() {
                return this._time = this._totalTime = 0, this._initted = this._gc = !1, this._rawPrevTime = -1, (this._gc || !this.timeline) && this._enabled(!0), this
            }, r.isActive = function() {
                var t, e = this._timeline,
                    i = this._startTime;
                return !e || !this._gc && !this._paused && e.isActive() && (t = e.rawTime()) >= i && i + this.totalDuration() / this._timeScale > t
            }, r._enabled = function(t, e) {
                return a || o.wake(), this._gc = !t, this._active = this.isActive(), e !== !0 && (t && !this.timeline ? this._timeline.add(this, this._startTime - this._delay) : !t && this.timeline && this._timeline._remove(this, !0)), !1
            }, r._kill = function() {
                return this._enabled(!1, !1)
            }, r.kill = function(t, e) {
                return this._kill(t, e), this
            }, r._uncache = function(t) {
                for (var e = t ? this : this.timeline; e;) e._dirty = !0, e = e.timeline;
                return this
            }, r._swapSelfInParams = function(t) {
                for (var e = t.length, i = t.concat(); --e > -1;) "{self}" === t[e] && (i[e] = this);
                return i
            }, r.eventCallback = function(t, e, i, n) {
                if ("on" === (t || "").substr(0, 2)) {
                    var s = this.vars;
                    if (1 === arguments.length) return s[t];
                    null == e ? delete s[t] : (s[t] = e, s[t + "Params"] = d(i) && -1 !== i.join("").indexOf("{self}") ? this._swapSelfInParams(i) : i, s[t + "Scope"] = n), "onUpdate" === t && (this._onUpdate = e)
                }
                return this
            }, r.delay = function(t) {
                return arguments.length ? (this._timeline.smoothChildTiming && this.startTime(this._startTime + t - this._delay), this._delay = t, this) : this._delay
            }, r.duration = function(t) {
                return arguments.length ? (this._duration = this._totalDuration = t, this._uncache(!0), this._timeline.smoothChildTiming && this._time > 0 && this._time < this._duration && 0 !== t && this.totalTime(this._totalTime * (t / this._duration), !0), this) : (this._dirty = !1, this._duration)
            }, r.totalDuration = function(t) {
                return this._dirty = !1, arguments.length ? this.duration(t) : this._totalDuration
            }, r.time = function(t, e) {
                return arguments.length ? (this._dirty && this.totalDuration(), this.totalTime(t > this._duration ? this._duration : t, e)) : this._time
            }, r.totalTime = function(t, e, i) {
                if (a || o.wake(), !arguments.length) return this._totalTime;
                if (this._timeline) {
                    if (0 > t && !i && (t += this.totalDuration()), this._timeline.smoothChildTiming) {
                        this._dirty && this.totalDuration();
                        var n = this._totalDuration,
                            s = this._timeline;
                        if (t > n && !i && (t = n), this._startTime = (this._paused ? this._pauseTime : s._time) - (this._reversed ? n - t : t) / this._timeScale, s._dirty || this._uncache(!1), s._timeline)
                            for (; s._timeline;) s._timeline._time !== (s._startTime + s._totalTime) / s._timeScale && s.totalTime(s._totalTime, !0), s = s._timeline
                    }
                    this._gc && this._enabled(!0, !1), (this._totalTime !== t || 0 === this._duration) && (this.render(t, e, !1), A.length && Y())
                }
                return this
            }, r.progress = r.totalProgress = function(t, e) {
                return arguments.length ? this.totalTime(this.duration() * t, e) : this._time / this.duration()
            }, r.startTime = function(t) {
                return arguments.length ? (t !== this._startTime && (this._startTime = t, this.timeline && this.timeline._sortChildren && this.timeline.add(this, t - this._delay)), this) : this._startTime
            }, r.endTime = function(t) {
                return this._startTime + (0 != t ? this.totalDuration() : this.duration()) / this._timeScale
            }, r.timeScale = function(t) {
                if (!arguments.length) return this._timeScale;
                if (t = t || u, this._timeline && this._timeline.smoothChildTiming) {
                    var e = this._pauseTime,
                        i = e || 0 === e ? e : this._timeline.totalTime();
                    this._startTime = i - (i - this._startTime) * this._timeScale / t
                }
                return this._timeScale = t, this._uncache(!1)
            }, r.reversed = function(t) {
                return arguments.length ? (t != this._reversed && (this._reversed = t, this.totalTime(this._timeline && !this._timeline.smoothChildTiming ? this.totalDuration() - this._totalTime : this._totalTime, !0)), this) : this._reversed
            }, r.paused = function(t) {
                if (!arguments.length) return this._paused;
                if (t != this._paused && this._timeline) {
                    a || t || o.wake();
                    var e = this._timeline,
                        i = e.rawTime(),
                        n = i - this._pauseTime;
                    !t && e.smoothChildTiming && (this._startTime += n, this._uncache(!1)), this._pauseTime = t ? i : null, this._paused = t, this._active = this.isActive(), !t && 0 !== n && this._initted && this.duration() && this.render(e.smoothChildTiming ? this._totalTime : (i - this._startTime) / this._timeScale, !0, !0)
                }
                return this._gc && !t && this._enabled(!0, !1), this
            };
            var R = g("core.SimpleTimeline", function(t) {
                z.call(this, 0, t), this.autoRemoveChildren = this.smoothChildTiming = !0
            });
            r = R.prototype = new z, r.constructor = R, r.kill()._gc = !1, r._first = r._last = r._recent = null, r._sortChildren = !1, r.add = r.insert = function(t, e) {
                var i, n;
                if (t._startTime = Number(e || 0) + t._delay, t._paused && this !== t._timeline && (t._pauseTime = t._startTime + (this.rawTime() - t._startTime) / t._timeScale), t.timeline && t.timeline._remove(t, !0), t.timeline = t._timeline = this, t._gc && t._enabled(!0, !0), i = this._last, this._sortChildren)
                    for (n = t._startTime; i && i._startTime > n;) i = i._prev;
                return i ? (t._next = i._next, i._next = t) : (t._next = this._first, this._first = t), t._next ? t._next._prev = t : this._last = t, t._prev = i, this._recent = t, this._timeline && this._uncache(!0), this
            }, r._remove = function(t, e) {
                return t.timeline === this && (e || t._enabled(!1, !0), t._prev ? t._prev._next = t._next : this._first === t && (this._first = t._next), t._next ? t._next._prev = t._prev : this._last === t && (this._last = t._prev), t._next = t._prev = t.timeline = null, t === this._recent && (this._recent = this._last), this._timeline && this._uncache(!0)), this
            }, r.render = function(t, e, i) {
                var n, s = this._first;
                for (this._totalTime = this._time = this._rawPrevTime = t; s;) n = s._next, (s._active || t >= s._startTime && !s._paused) && (s._reversed ? s.render((s._dirty ? s.totalDuration() : s._totalDuration) - (t - s._startTime) * s._timeScale, e, i) : s.render((t - s._startTime) * s._timeScale, e, i)), s = n
            }, r.rawTime = function() {
                return a || o.wake(), this._totalTime
            };
            var M = g("TweenLite", function(e, i, n) {
                    if (z.call(this, i, n), this.render = M.prototype.render, null == e) throw "Cannot tween a null target.";
                    this.target = e = "string" != typeof e ? e : M.selector(e) || e;
                    var s, r, o, a = e.jquery || e.length && e !== t && e[0] && (e[0] === t || e[0].nodeType && e[0].style && !e.nodeType),
                        l = this.vars.overwrite;
                    if (this._overwrite = l = null == l ? q[M.defaultOverwrite] : "number" == typeof l ? l >> 0 : q[l], (a || e instanceof Array || e.push && d(e)) && "number" != typeof e[0])
                        for (this._targets = o = c(e), this._propLookup = [], this._siblings = [], s = 0; o.length > s; s++) r = o[s], r ? "string" != typeof r ? r.length && r !== t && r[0] && (r[0] === t || r[0].nodeType && r[0].style && !r.nodeType) ? (o.splice(s--, 1), this._targets = o = o.concat(c(r))) : (this._siblings[s] = U(r, this, !1), 1 === l && this._siblings[s].length > 1 && Q(r, this, null, 1, this._siblings[s])) : (r = o[s--] = M.selector(r), "string" == typeof r && o.splice(s + 1, 1)) : o.splice(s--, 1);
                    else this._propLookup = {}, this._siblings = U(e, this, !1), 1 === l && this._siblings.length > 1 && Q(e, this, null, 1, this._siblings);
                    (this.vars.immediateRender || 0 === i && 0 === this._delay && this.vars.immediateRender !== !1) && (this._time = -u, this.render(-this._delay))
                }, !0),
                E = function(e) {
                    return e && e.length && e !== t && e[0] && (e[0] === t || e[0].nodeType && e[0].style && !e.nodeType)
                },
                k = function(t, e) {
                    var i, n = {};
                    for (i in t) W[i] || i in e && "transform" !== i && "x" !== i && "y" !== i && "width" !== i && "height" !== i && "className" !== i && "border" !== i || !(!j[i] || j[i] && j[i]._autoCSS) || (n[i] = t[i], delete t[i]);
                    t.css = n
                };
            r = M.prototype = new z, r.constructor = M, r.kill()._gc = !1, r.ratio = 0, r._firstPT = r._targets = r._overwrittenProps = r._startAt = null, r._notifyPluginsOfEnabled = r._lazy = !1, M.version = "1.14.2", M.defaultEase = r._ease = new x(null, null, 1, 1), M.defaultOverwrite = "auto", M.ticker = o, M.autoSleep = !0, M.lagSmoothing = function(t, e) {
                o.lagSmoothing(t, e)
            }, M.selector = t.$ || t.jQuery || function(e) {
                var i = t.$ || t.jQuery;
                return i ? (M.selector = i, i(e)) : "undefined" == typeof document ? e : document.querySelectorAll ? document.querySelectorAll(e) : document.getElementById("#" === e.charAt(0) ? e.substr(1) : e)
            };
            var A = [],
                L = {},
                D = M._internals = {
                    isArray: d,
                    isSelector: E,
                    lazyTweens: A
                },
                j = M._plugins = {},
                F = D.tweenLookup = {},
                N = 0,
                W = D.reservedProps = {
                    ease: 1,
                    delay: 1,
                    overwrite: 1,
                    onComplete: 1,
                    onCompleteParams: 1,
                    onCompleteScope: 1,
                    useFrames: 1,
                    runBackwards: 1,
                    startAt: 1,
                    onUpdate: 1,
                    onUpdateParams: 1,
                    onUpdateScope: 1,
                    onStart: 1,
                    onStartParams: 1,
                    onStartScope: 1,
                    onReverseComplete: 1,
                    onReverseCompleteParams: 1,
                    onReverseCompleteScope: 1,
                    onRepeat: 1,
                    onRepeatParams: 1,
                    onRepeatScope: 1,
                    easeParams: 1,
                    yoyo: 1,
                    immediateRender: 1,
                    repeat: 1,
                    repeatDelay: 1,
                    data: 1,
                    paused: 1,
                    reversed: 1,
                    autoCSS: 1,
                    lazy: 1,
                    onOverwrite: 1
                },
                q = {
                    none: 0,
                    all: 1,
                    auto: 2,
                    concurrent: 3,
                    allOnStart: 4,
                    preexisting: 5,
                    "true": 1,
                    "false": 0
                },
                X = z._rootFramesTimeline = new R,
                B = z._rootTimeline = new R,
                Y = D.lazyRender = function() {
                    var t, e = A.length;
                    for (L = {}; --e > -1;) t = A[e], t && t._lazy !== !1 && (t.render(t._lazy[0], t._lazy[1], !0), t._lazy = !1);
                    A.length = 0
                };
            B._startTime = o.time, X._startTime = o.frame, B._active = X._active = !0, setTimeout(Y, 1), z._updateRoot = M.render = function() {
                var t, e, i;
                if (A.length && Y(), B.render((o.time - B._startTime) * B._timeScale, !1, !1), X.render((o.frame - X._startTime) * X._timeScale, !1, !1), A.length && Y(), !(o.frame % 120)) {
                    for (i in F) {
                        for (e = F[i].tweens, t = e.length; --t > -1;) e[t]._gc && e.splice(t, 1);
                        0 === e.length && delete F[i]
                    }
                    if (i = B._first, (!i || i._paused) && M.autoSleep && !X._first && 1 === o._listeners.tick.length) {
                        for (; i && i._paused;) i = i._next;
                        i || o.sleep()
                    }
                }
            }, o.addEventListener("tick", z._updateRoot);
            var U = function(t, e, i) {
                    var n, s, r = t._gsTweenID;
                    if (F[r || (t._gsTweenID = r = "t" + N++)] || (F[r] = {
                            target: t,
                            tweens: []
                        }), e && (n = F[r].tweens, n[s = n.length] = e, i))
                        for (; --s > -1;) n[s] === e && n.splice(s, 1);
                    return F[r].tweens
                },
                H = function(t, e, i, n) {
                    var s, r, o = t.vars.onOverwrite;
                    return o && (s = o(t, e, i, n)), o = M.onOverwrite, o && (r = o(t, e, i, n)), s !== !1 && r !== !1
                },
                Q = function(t, e, i, n, s) {
                    var r, o, a, l;
                    if (1 === n || n >= 4) {
                        for (l = s.length, r = 0; l > r; r++)
                            if ((a = s[r]) !== e) a._gc || H(a, e) && a._enabled(!1, !1) && (o = !0);
                            else if (5 === n) break;
                        return o
                    }
                    var h, c = e._startTime + u,
                        p = [],
                        d = 0,
                        f = 0 === e._duration;
                    for (r = s.length; --r > -1;)(a = s[r]) === e || a._gc || a._paused || (a._timeline !== e._timeline ? (h = h || V(e, 0, f), 0 === V(a, h, f) && (p[d++] = a)) : c >= a._startTime && a._startTime + a.totalDuration() / a._timeScale > c && ((f || !a._initted) && 2e-10 >= c - a._startTime || (p[d++] = a)));
                    for (r = d; --r > -1;)
                        if (a = p[r], 2 === n && a._kill(i, t, e) && (o = !0), 2 !== n || !a._firstPT && a._initted) {
                            if (2 !== n && !H(a, e)) continue;
                            a._enabled(!1, !1) && (o = !0)
                        }
                    return o
                },
                V = function(t, e, i) {
                    for (var n = t._timeline, s = n._timeScale, r = t._startTime; n._timeline;) {
                        if (r += n._startTime, s *= n._timeScale, n._paused) return -100;
                        n = n._timeline
                    }
                    return r /= s, r > e ? r - e : i && r === e || !t._initted && 2 * u > r - e ? u : (r += t.totalDuration() / t._timeScale / s) > e + u ? 0 : r - e - u
                };
            r._init = function() {
                var t, e, i, n, s, r = this.vars,
                    o = this._overwrittenProps,
                    a = this._duration,
                    l = !!r.immediateRender,
                    h = r.ease;
                if (r.startAt) {
                    this._startAt && (this._startAt.render(-1, !0), this._startAt.kill()), s = {};
                    for (n in r.startAt) s[n] = r.startAt[n];
                    if (s.overwrite = !1, s.immediateRender = !0, s.lazy = l && r.lazy !== !1, s.startAt = s.delay = null, this._startAt = M.to(this.target, 0, s), l)
                        if (this._time > 0) this._startAt = null;
                        else if (0 !== a) return
                } else if (r.runBackwards && 0 !== a)
                    if (this._startAt) this._startAt.render(-1, !0), this._startAt.kill(), this._startAt = null;
                    else {
                        0 !== this._time && (l = !1), i = {};
                        for (n in r) W[n] && "autoCSS" !== n || (i[n] = r[n]);
                        if (i.overwrite = 0, i.data = "isFromStart", i.lazy = l && r.lazy !== !1, i.immediateRender = l, this._startAt = M.to(this.target, 0, i), l) {
                            if (0 === this._time) return
                        } else this._startAt._init(), this._startAt._enabled(!1), this.vars.immediateRender && (this._startAt = null)
                    }
                if (this._ease = h = h ? h instanceof x ? h : "function" == typeof h ? new x(h, r.easeParams) : T[h] || M.defaultEase : M.defaultEase, r.easeParams instanceof Array && h.config && (this._ease = h.config.apply(h, r.easeParams)), this._easeType = this._ease._type, this._easePower = this._ease._power, this._firstPT = null, this._targets)
                    for (t = this._targets.length; --t > -1;) this._initProps(this._targets[t], this._propLookup[t] = {}, this._siblings[t], o ? o[t] : null) && (e = !0);
                else e = this._initProps(this.target, this._propLookup, this._siblings, o);
                if (e && M._onPluginEvent("_onInitAllProps", this), o && (this._firstPT || "function" != typeof this.target && this._enabled(!1, !1)), r.runBackwards)
                    for (i = this._firstPT; i;) i.s += i.c, i.c = -i.c, i = i._next;
                this._onUpdate = r.onUpdate, this._initted = !0
            }, r._initProps = function(e, i, n, s) {
                var r, o, a, l, h, u;
                if (null == e) return !1;
                L[e._gsTweenID] && Y(), this.vars.css || e.style && e !== t && e.nodeType && j.css && this.vars.autoCSS !== !1 && k(this.vars, e);
                for (r in this.vars) {
                    if (u = this.vars[r], W[r]) u && (u instanceof Array || u.push && d(u)) && -1 !== u.join("").indexOf("{self}") && (this.vars[r] = u = this._swapSelfInParams(u, this));
                    else if (j[r] && (l = new j[r])._onInitTween(e, this.vars[r], this)) {
                        for (this._firstPT = h = {
                                _next: this._firstPT,
                                t: l,
                                p: "setRatio",
                                s: 0,
                                c: 1,
                                f: !0,
                                n: r,
                                pg: !0,
                                pr: l._priority
                            }, o = l._overwriteProps.length; --o > -1;) i[l._overwriteProps[o]] = this._firstPT;
                        (l._priority || l._onInitAllProps) && (a = !0), (l._onDisable || l._onEnable) && (this._notifyPluginsOfEnabled = !0)
                    } else this._firstPT = i[r] = h = {
                        _next: this._firstPT,
                        t: e,
                        p: r,
                        f: "function" == typeof e[r],
                        n: r,
                        pg: !1,
                        pr: 0
                    }, h.s = h.f ? e[r.indexOf("set") || "function" != typeof e["get" + r.substr(3)] ? r : "get" + r.substr(3)]() : parseFloat(e[r]), h.c = "string" == typeof u && "=" === u.charAt(1) ? parseInt(u.charAt(0) + "1", 10) * Number(u.substr(2)) : Number(u) - h.s || 0;
                    h && h._next && (h._next._prev = h)
                }
                return s && this._kill(s, e) ? this._initProps(e, i, n, s) : this._overwrite > 1 && this._firstPT && n.length > 1 && Q(e, this, i, this._overwrite, n) ? (this._kill(i, e), this._initProps(e, i, n, s)) : (this._firstPT && (this.vars.lazy !== !1 && this._duration || this.vars.lazy && !this._duration) && (L[e._gsTweenID] = !0), a)
            }, r.render = function(t, e, i) {
                var n, s, r, o, a = this._time,
                    l = this._duration,
                    h = this._rawPrevTime;
                if (t >= l) this._totalTime = this._time = l, this.ratio = this._ease._calcEnd ? this._ease.getRatio(1) : 1, this._reversed || (n = !0, s = "onComplete"), 0 === l && (this._initted || !this.vars.lazy || i) && (this._startTime === this._timeline._duration && (t = 0), (0 === t || 0 > h || h === u) && h !== t && (i = !0, h > u && (s = "onReverseComplete")), this._rawPrevTime = o = !e || t || h === t ? t : u);
                else if (1e-7 > t) this._totalTime = this._time = 0, this.ratio = this._ease._calcEnd ? this._ease.getRatio(0) : 0, (0 !== a || 0 === l && h > 0 && h !== u) && (s = "onReverseComplete", n = this._reversed), 0 > t && (this._active = !1, 0 === l && (this._initted || !this.vars.lazy || i) && (h >= 0 && (i = !0), this._rawPrevTime = o = !e || t || h === t ? t : u)), this._initted || (i = !0);
                else if (this._totalTime = this._time = t, this._easeType) {
                    var c = t / l,
                        p = this._easeType,
                        d = this._easePower;
                    (1 === p || 3 === p && c >= .5) && (c = 1 - c), 3 === p && (c *= 2), 1 === d ? c *= c : 2 === d ? c *= c * c : 3 === d ? c *= c * c * c : 4 === d && (c *= c * c * c * c), this.ratio = 1 === p ? 1 - c : 2 === p ? c : .5 > t / l ? c / 2 : 1 - c / 2
                } else this.ratio = this._ease.getRatio(t / l);
                if (this._time !== a || i) {
                    if (!this._initted) {
                        if (this._init(), !this._initted || this._gc) return;
                        if (!i && this._firstPT && (this.vars.lazy !== !1 && this._duration || this.vars.lazy && !this._duration)) return this._time = this._totalTime = a, this._rawPrevTime = h, A.push(this), void(this._lazy = [t, e]);
                        this._time && !n ? this.ratio = this._ease.getRatio(this._time / l) : n && this._ease._calcEnd && (this.ratio = this._ease.getRatio(0 === this._time ? 0 : 1))
                    }
                    for (this._lazy !== !1 && (this._lazy = !1), this._active || !this._paused && this._time !== a && t >= 0 && (this._active = !0), 0 === a && (this._startAt && (t >= 0 ? this._startAt.render(t, e, i) : s || (s = "_dummyGS")), this.vars.onStart && (0 !== this._time || 0 === l) && (e || this.vars.onStart.apply(this.vars.onStartScope || this, this.vars.onStartParams || y))), r = this._firstPT; r;) r.f ? r.t[r.p](r.c * this.ratio + r.s) : r.t[r.p] = r.c * this.ratio + r.s, r = r._next;
                    this._onUpdate && (0 > t && this._startAt && t !== -1e-4 && this._startAt.render(t, e, i), e || (this._time !== a || n) && this._onUpdate.apply(this.vars.onUpdateScope || this, this.vars.onUpdateParams || y)), s && (!this._gc || i) && (0 > t && this._startAt && !this._onUpdate && t !== -1e-4 && this._startAt.render(t, e, i), n && (this._timeline.autoRemoveChildren && this._enabled(!1, !1), this._active = !1), !e && this.vars[s] && this.vars[s].apply(this.vars[s + "Scope"] || this, this.vars[s + "Params"] || y), 0 === l && this._rawPrevTime === u && o !== u && (this._rawPrevTime = 0))
                }
            }, r._kill = function(t, e, i) {
                if ("all" === t && (t = null), null == t && (null == e || e === this.target)) return this._lazy = !1, this._enabled(!1, !1);
                e = "string" != typeof e ? e || this._targets || this.target : M.selector(e) || e;
                var n, s, r, o, a, l, h, u, c;
                if ((d(e) || E(e)) && "number" != typeof e[0])
                    for (n = e.length; --n > -1;) this._kill(t, e[n]) && (l = !0);
                else {
                    if (this._targets) {
                        for (n = this._targets.length; --n > -1;)
                            if (e === this._targets[n]) {
                                a = this._propLookup[n] || {}, this._overwrittenProps = this._overwrittenProps || [], s = this._overwrittenProps[n] = t ? this._overwrittenProps[n] || {} : "all";
                                break
                            }
                    } else {
                        if (e !== this.target) return !1;
                        a = this._propLookup, s = this._overwrittenProps = t ? this._overwrittenProps || {} : "all"
                    }
                    if (a) {
                        if (h = t || a, u = t !== s && "all" !== s && t !== a && ("object" != typeof t || !t._tempKill), i && (M.onOverwrite || this.vars.onOverwrite)) {
                            for (r in h) a[r] && (c || (c = []), c.push(r));
                            if (!H(this, i, e, c)) return !1
                        }
                        for (r in h)(o = a[r]) && (o.pg && o.t._kill(h) && (l = !0), o.pg && 0 !== o.t._overwriteProps.length || (o._prev ? o._prev._next = o._next : o === this._firstPT && (this._firstPT = o._next), o._next && (o._next._prev = o._prev), o._next = o._prev = null), delete a[r]), u && (s[r] = 1);
                        !this._firstPT && this._initted && this._enabled(!1, !1)
                    }
                }
                return l
            }, r.invalidate = function() {
                return this._notifyPluginsOfEnabled && M._onPluginEvent("_onDisable", this), this._firstPT = this._overwrittenProps = this._startAt = this._onUpdate = null, this._notifyPluginsOfEnabled = this._active = this._lazy = !1, this._propLookup = this._targets ? {} : [], z.prototype.invalidate.call(this), this.vars.immediateRender && (this._time = -u, this.render(-this._delay)), this
            }, r._enabled = function(t, e) {
                if (a || o.wake(), t && this._gc) {
                    var i, n = this._targets;
                    if (n)
                        for (i = n.length; --i > -1;) this._siblings[i] = U(n[i], this, !0);
                    else this._siblings = U(this.target, this, !0)
                }
                return z.prototype._enabled.call(this, t, e), this._notifyPluginsOfEnabled && this._firstPT ? M._onPluginEvent(t ? "_onEnable" : "_onDisable", this) : !1
            }, M.to = function(t, e, i) {
                return new M(t, e, i)
            }, M.from = function(t, e, i) {
                return i.runBackwards = !0, i.immediateRender = 0 != i.immediateRender, new M(t, e, i)
            }, M.fromTo = function(t, e, i, n) {
                return n.startAt = i, n.immediateRender = 0 != n.immediateRender && 0 != i.immediateRender, new M(t, e, n)
            }, M.delayedCall = function(t, e, i, n, s) {
                return new M(e, 0, {
                    delay: t,
                    onComplete: e,
                    onCompleteParams: i,
                    onCompleteScope: n,
                    onReverseComplete: e,
                    onReverseCompleteParams: i,
                    onReverseCompleteScope: n,
                    immediateRender: !1,
                    useFrames: s,
                    overwrite: 0
                })
            }, M.set = function(t, e) {
                return new M(t, 0, e)
            }, M.getTweensOf = function(t, e) {
                if (null == t) return [];
                t = "string" != typeof t ? t : M.selector(t) || t;
                var i, n, s, r;
                if ((d(t) || E(t)) && "number" != typeof t[0]) {
                    for (i = t.length, n = []; --i > -1;) n = n.concat(M.getTweensOf(t[i], e));
                    for (i = n.length; --i > -1;)
                        for (r = n[i], s = i; --s > -1;) r === n[s] && n.splice(i, 1)
                } else
                    for (n = U(t).concat(), i = n.length; --i > -1;)(n[i]._gc || e && !n[i].isActive()) && n.splice(i, 1);
                return n
            }, M.killTweensOf = M.killDelayedCallsTo = function(t, e, i) {
                "object" == typeof e && (i = e, e = !1);
                for (var n = M.getTweensOf(t, e), s = n.length; --s > -1;) n[s]._kill(i, t)
            };
            var G = g("plugins.TweenPlugin", function(t, e) {
                this._overwriteProps = (t || "").split(","), this._propName = this._overwriteProps[0], this._priority = e || 0, this._super = G.prototype
            }, !0);
            if (r = G.prototype, G.version = "1.10.1", G.API = 2, r._firstPT = null, r._addTween = function(t, e, i, n, s, r) {
                    var o, a;
                    return null != n && (o = "number" == typeof n || "=" !== n.charAt(1) ? Number(n) - i : parseInt(n.charAt(0) + "1", 10) * Number(n.substr(2))) ? (this._firstPT = a = {
                        _next: this._firstPT,
                        t: t,
                        p: e,
                        s: i,
                        c: o,
                        f: "function" == typeof t[e],
                        n: s || e,
                        r: r
                    }, a._next && (a._next._prev = a), a) : void 0
                }, r.setRatio = function(t) {
                    for (var e, i = this._firstPT, n = 1e-6; i;) e = i.c * t + i.s, i.r ? e = Math.round(e) : n > e && e > -n && (e = 0), i.f ? i.t[i.p](e) : i.t[i.p] = e, i = i._next
                }, r._kill = function(t) {
                    var e, i = this._overwriteProps,
                        n = this._firstPT;
                    if (null != t[this._propName]) this._overwriteProps = [];
                    else
                        for (e = i.length; --e > -1;) null != t[i[e]] && i.splice(e, 1);
                    for (; n;) null != t[n.n] && (n._next && (n._next._prev = n._prev), n._prev ? (n._prev._next = n._next, n._prev = null) : this._firstPT === n && (this._firstPT = n._next)), n = n._next;
                    return !1
                }, r._roundProps = function(t, e) {
                    for (var i = this._firstPT; i;)(t[this._propName] || null != i.n && t[i.n.split(this._propName + "_").join("")]) && (i.r = e), i = i._next
                }, M._onPluginEvent = function(t, e) {
                    var i, n, s, r, o, a = e._firstPT;
                    if ("_onInitAllProps" === t) {
                        for (; a;) {
                            for (o = a._next, n = s; n && n.pr > a.pr;) n = n._next;
                            (a._prev = n ? n._prev : r) ? a._prev._next = a: s = a, (a._next = n) ? n._prev = a : r = a, a = o
                        }
                        a = e._firstPT = s
                    }
                    for (; a;) a.pg && "function" == typeof a.t[t] && a.t[t]() && (i = !0), a = a._next;
                    return i
                }, G.activate = function(t) {
                    for (var e = t.length; --e > -1;) t[e].API === G.API && (j[(new t[e])._propName] = t[e]);
                    return !0
                }, _.plugin = function(t) {
                    if (!(t && t.propName && t.init && t.API)) throw "illegal plugin definition.";
                    var e, i = t.propName,
                        n = t.priority || 0,
                        s = t.overwriteProps,
                        r = {
                            init: "_onInitTween",
                            set: "setRatio",
                            kill: "_kill",
                            round: "_roundProps",
                            initAll: "_onInitAllProps"
                        },
                        o = g("plugins." + i.charAt(0).toUpperCase() + i.substr(1) + "Plugin", function() {
                            G.call(this, i, n), this._overwriteProps = s || []
                        }, t.global === !0),
                        a = o.prototype = new G(i);
                    a.constructor = o, o.API = t.API;
                    for (e in r) "function" == typeof t[e] && (a[r[e]] = t[e]);
                    return o.version = t.version, G.activate([o]), o
                }, n = t._gsQueue) {
                for (s = 0; n.length > s; s++) n[s]();
                for (r in f) f[r].func || t.console.log("GSAP encountered missing dependency: com.greensock." + r)
            }
            a = !1
        }
    }("undefined" != typeof module && module.exports && "undefined" != typeof global ? global : this || window, "TweenMax"),
    function(t) {
        function e() {}

        function i(t) {
            function i(e) {
                e.prototype.option || (e.prototype.option = function(e) {
                    t.isPlainObject(e) && (this.options = t.extend(!0, this.options, e))
                })
            }

            function s(e, i) {
                t.fn[e] = function(s) {
                    if ("string" == typeof s) {
                        for (var o = n.call(arguments, 1), a = 0, l = this.length; l > a; a++) {
                            var h = this[a],
                                u = t.data(h, e);
                            if (u)
                                if (t.isFunction(u[s]) && "_" !== s.charAt(0)) {
                                    var c = u[s].apply(u, o);
                                    if (void 0 !== c) return c
                                } else r("no such method '" + s + "' for " + e + " instance");
                            else r("cannot call methods on " + e + " prior to initialization; attempted to call '" + s + "'")
                        }
                        return this
                    }
                    return this.each(function() {
                        var n = t.data(this, e);
                        n ? (n.option(s), n._init()) : (n = new i(this, s), t.data(this, e, n))
                    })
                }
            }
            if (t) {
                var r = "undefined" == typeof console ? e : function(t) {
                    console.error(t)
                };
                return t.bridget = function(t, e) {
                    i(e), s(t, e)
                }, t.bridget
            }
        }
        var n = Array.prototype.slice;
        "function" == typeof define && define.amd ? define("jquery-bridget/jquery.bridget", ["jquery"], i) : i("object" == typeof exports ? require("jquery") : t.jQuery)
    }(window),
    function(t) {
        function e(e) {
            var i = t.event;
            return i.target = i.target || i.srcElement || e, i
        }
        var i = document.documentElement,
            n = function() {};
        i.addEventListener ? n = function(t, e, i) {
            t.addEventListener(e, i, !1)
        } : i.attachEvent && (n = function(t, i, n) {
            t[i + n] = n.handleEvent ? function() {
                var i = e(t);
                n.handleEvent.call(n, i)
            } : function() {
                var i = e(t);
                n.call(t, i)
            }, t.attachEvent("on" + i, t[i + n])
        });
        var s = function() {};
        i.removeEventListener ? s = function(t, e, i) {
            t.removeEventListener(e, i, !1)
        } : i.detachEvent && (s = function(t, e, i) {
            t.detachEvent("on" + e, t[e + i]);
            try {
                delete t[e + i]
            } catch (n) {
                t[e + i] = void 0
            }
        });
        var r = {
            bind: n,
            unbind: s
        };
        "function" == typeof define && define.amd ? define("eventie/eventie", r) : "object" == typeof exports ? module.exports = r : t.eventie = r
    }(window),
    function() {
        function t() {}

        function e(t, e) {
            for (var i = t.length; i--;)
                if (t[i].listener === e) return i;
            return -1
        }

        function i(t) {
            return function() {
                return this[t].apply(this, arguments)
            }
        }
        var n = t.prototype,
            s = this,
            r = s.EventEmitter;
        n.getListeners = function(t) {
            var e, i, n = this._getEvents();
            if (t instanceof RegExp) {
                e = {};
                for (i in n) n.hasOwnProperty(i) && t.test(i) && (e[i] = n[i])
            } else e = n[t] || (n[t] = []);
            return e
        }, n.flattenListeners = function(t) {
            var e, i = [];
            for (e = 0; t.length > e; e += 1) i.push(t[e].listener);
            return i
        }, n.getListenersAsObject = function(t) {
            var e, i = this.getListeners(t);
            return i instanceof Array && (e = {}, e[t] = i), e || i
        }, n.addListener = function(t, i) {
            var n, s = this.getListenersAsObject(t),
                r = "object" == typeof i;
            for (n in s) s.hasOwnProperty(n) && -1 === e(s[n], i) && s[n].push(r ? i : {
                listener: i,
                once: !1
            });
            return this
        }, n.on = i("addListener"), n.addOnceListener = function(t, e) {
            return this.addListener(t, {
                listener: e,
                once: !0
            })
        }, n.once = i("addOnceListener"), n.defineEvent = function(t) {
            return this.getListeners(t), this
        }, n.defineEvents = function(t) {
            for (var e = 0; t.length > e; e += 1) this.defineEvent(t[e]);
            return this
        }, n.removeListener = function(t, i) {
            var n, s, r = this.getListenersAsObject(t);
            for (s in r) r.hasOwnProperty(s) && (n = e(r[s], i), -1 !== n && r[s].splice(n, 1));
            return this
        }, n.off = i("removeListener"), n.addListeners = function(t, e) {
            return this.manipulateListeners(!1, t, e)
        }, n.removeListeners = function(t, e) {
            return this.manipulateListeners(!0, t, e)
        }, n.manipulateListeners = function(t, e, i) {
            var n, s, r = t ? this.removeListener : this.addListener,
                o = t ? this.removeListeners : this.addListeners;
            if ("object" != typeof e || e instanceof RegExp)
                for (n = i.length; n--;) r.call(this, e, i[n]);
            else
                for (n in e) e.hasOwnProperty(n) && (s = e[n]) && ("function" == typeof s ? r.call(this, n, s) : o.call(this, n, s));
            return this
        }, n.removeEvent = function(t) {
            var e, i = typeof t,
                n = this._getEvents();
            if ("string" === i) delete n[t];
            else if (t instanceof RegExp)
                for (e in n) n.hasOwnProperty(e) && t.test(e) && delete n[e];
            else delete this._events;
            return this
        }, n.removeAllListeners = i("removeEvent"), n.emitEvent = function(t, e) {
            var i, n, s, r, o = this.getListenersAsObject(t);
            for (s in o)
                if (o.hasOwnProperty(s))
                    for (n = o[s].length; n--;) i = o[s][n], i.once === !0 && this.removeListener(t, i.listener), r = i.listener.apply(this, e || []), r === this._getOnceReturnValue() && this.removeListener(t, i.listener);
            return this
        }, n.trigger = i("emitEvent"), n.emit = function(t) {
            var e = Array.prototype.slice.call(arguments, 1);
            return this.emitEvent(t, e)
        }, n.setOnceReturnValue = function(t) {
            return this._onceReturnValue = t, this
        }, n._getOnceReturnValue = function() {
            return this.hasOwnProperty("_onceReturnValue") ? this._onceReturnValue : !0
        }, n._getEvents = function() {
            return this._events || (this._events = {})
        }, t.noConflict = function() {
            return s.EventEmitter = r, t
        }, "function" == typeof define && define.amd ? define("eventEmitter/EventEmitter", [], function() {
            return t
        }) : "object" == typeof module && module.exports ? module.exports = t : s.EventEmitter = t
    }.call(this),
    function(t) {
        function e(t) {
            if (t) {
                if ("string" == typeof n[t]) return t;
                t = t.charAt(0).toUpperCase() + t.slice(1);
                for (var e, s = 0, r = i.length; r > s; s++)
                    if (e = i[s] + t, "string" == typeof n[e]) return e
            }
        }
        var i = "Webkit Moz ms Ms O".split(" "),
            n = document.documentElement.style;
        "function" == typeof define && define.amd ? define("get-style-property/get-style-property", [], function() {
            return e
        }) : "object" == typeof exports ? module.exports = e : t.getStyleProperty = e
    }(window),
    function(t) {
        function e(t) {
            var e = parseFloat(t),
                i = -1 === t.indexOf("%") && !isNaN(e);
            return i && e
        }

        function i() {}

        function n() {
            for (var t = {
                    width: 0,
                    height: 0,
                    innerWidth: 0,
                    innerHeight: 0,
                    outerWidth: 0,
                    outerHeight: 0
                }, e = 0, i = o.length; i > e; e++) {
                var n = o[e];
                t[n] = 0
            }
            return t
        }

        function s(i) {
            function s() {
                if (!p) {
                    p = !0;
                    var n = t.getComputedStyle;
                    if (h = function() {
                            var t = n ? function(t) {
                                return n(t, null)
                            } : function(t) {
                                return t.currentStyle
                            };
                            return function(e) {
                                var i = t(e);
                                return i || r("Style returned " + i + ". Are you running this code in a hidden iframe on Firefox? See http://bit.ly/getsizebug1"), i
                            }
                        }(), u = i("boxSizing")) {
                        var s = document.createElement("div");
                        s.style.width = "200px", s.style.padding = "1px 2px 3px 4px", s.style.borderStyle = "solid", s.style.borderWidth = "1px 2px 3px 4px", s.style[u] = "border-box";
                        var o = document.body || document.documentElement;
                        o.appendChild(s);
                        var a = h(s);
                        c = 200 === e(a.width), o.removeChild(s)
                    }
                }
            }

            function a(t) {
                if (s(), "string" == typeof t && (t = document.querySelector(t)), t && "object" == typeof t && t.nodeType) {
                    var i = h(t);
                    if ("none" === i.display) return n();
                    var r = {};
                    r.width = t.offsetWidth, r.height = t.offsetHeight;
                    for (var a = r.isBorderBox = !(!u || !i[u] || "border-box" !== i[u]), p = 0, d = o.length; d > p; p++) {
                        var f = o[p],
                            m = i[f];
                        m = l(t, m);
                        var _ = parseFloat(m);
                        r[f] = isNaN(_) ? 0 : _
                    }
                    var g = r.paddingLeft + r.paddingRight,
                        v = r.paddingTop + r.paddingBottom,
                        y = r.marginLeft + r.marginRight,
                        x = r.marginTop + r.marginBottom,
                        T = r.borderLeftWidth + r.borderRightWidth,
                        b = r.borderTopWidth + r.borderBottomWidth,
                        w = a && c,
                        S = e(i.width);
                    S !== !1 && (r.width = S + (w ? 0 : g + T));
                    var P = e(i.height);
                    return P !== !1 && (r.height = P + (w ? 0 : v + b)), r.innerWidth = r.width - (g + T), r.innerHeight = r.height - (v + b), r.outerWidth = r.width + y, r.outerHeight = r.height + x, r
                }
            }

            function l(e, i) {
                if (t.getComputedStyle || -1 === i.indexOf("%")) return i;
                var n = e.style,
                    s = n.left,
                    r = e.runtimeStyle,
                    o = r && r.left;
                return o && (r.left = e.currentStyle.left), n.left = i, i = n.pixelLeft, n.left = s, o && (r.left = o), i
            }
            var h, u, c, p = !1;
            return a
        }
        var r = "undefined" == typeof console ? i : function(t) {
                console.error(t)
            },
            o = ["paddingLeft", "paddingRight", "paddingTop", "paddingBottom", "marginLeft", "marginRight", "marginTop", "marginBottom", "borderLeftWidth", "borderRightWidth", "borderTopWidth", "borderBottomWidth"];
        "function" == typeof define && define.amd ? define("get-size/get-size", ["get-style-property/get-style-property"], s) : "object" == typeof exports ? module.exports = s(require("desandro-get-style-property")) : t.getSize = s(t.getStyleProperty)
    }(window),
    function(t) {
        function e(t) {
            "function" == typeof t && (e.isReady ? t() : o.push(t))
        }

        function i(t) {
            var i = "readystatechange" === t.type && "complete" !== r.readyState;
            e.isReady || i || n()
        }

        function n() {
            e.isReady = !0;
            for (var t = 0, i = o.length; i > t; t++) {
                var n = o[t];
                n()
            }
        }

        function s(s) {
            return "complete" === r.readyState ? n() : (s.bind(r, "DOMContentLoaded", i), s.bind(r, "readystatechange", i), s.bind(t, "load", i)), e
        }
        var r = t.document,
            o = [];
        e.isReady = !1, "function" == typeof define && define.amd ? define("doc-ready/doc-ready", ["eventie/eventie"], s) : "object" == typeof exports ? module.exports = s(require("eventie")) : t.docReady = s(t.eventie)
    }(window),
    function(t) {
        function e(t, e) {
            return t[o](e)
        }

        function i(t) {
            if (!t.parentNode) {
                var e = document.createDocumentFragment();
                e.appendChild(t)
            }
        }

        function n(t, e) {
            i(t);
            for (var n = t.parentNode.querySelectorAll(e), s = 0, r = n.length; r > s; s++)
                if (n[s] === t) return !0;
            return !1
        }

        function s(t, n) {
            return i(t), e(t, n)
        }
        var r, o = function() {
            if (t.matches) return "matches";
            if (t.matchesSelector) return "matchesSelector";
            for (var e = ["webkit", "moz", "ms", "o"], i = 0, n = e.length; n > i; i++) {
                var s = e[i],
                    r = s + "MatchesSelector";
                if (t[r]) return r
            }
        }();
        if (o) {
            var a = document.createElement("div"),
                l = e(a, "div");
            r = l ? e : s
        } else r = n;
        "function" == typeof define && define.amd ? define("matches-selector/matches-selector", [], function() {
            return r
        }) : "object" == typeof exports ? module.exports = r : window.matchesSelector = r
    }(Element.prototype),
    function(t, e) {
        "function" == typeof define && define.amd ? define("fizzy-ui-utils/utils", ["doc-ready/doc-ready", "matches-selector/matches-selector"], function(i, n) {
            return e(t, i, n)
        }) : "object" == typeof exports ? module.exports = e(t, require("doc-ready"), require("desandro-matches-selector")) : t.fizzyUIUtils = e(t, t.docReady, t.matchesSelector)
    }(window, function(t, e, i) {
        var n = {};
        n.extend = function(t, e) {
            for (var i in e) t[i] = e[i];
            return t
        }, n.modulo = function(t, e) {
            return (t % e + e) % e
        };
        var s = Object.prototype.toString;
        n.isArray = function(t) {
            return "[object Array]" == s.call(t)
        }, n.makeArray = function(t) {
            var e = [];
            if (n.isArray(t)) e = t;
            else if (t && "number" == typeof t.length)
                for (var i = 0, s = t.length; s > i; i++) e.push(t[i]);
            else e.push(t);
            return e
        }, n.indexOf = Array.prototype.indexOf ? function(t, e) {
            return t.indexOf(e)
        } : function(t, e) {
            for (var i = 0, n = t.length; n > i; i++)
                if (t[i] === e) return i;
            return -1
        }, n.removeFrom = function(t, e) {
            var i = n.indexOf(t, e); - 1 != i && t.splice(i, 1)
        }, n.isElement = "function" == typeof HTMLElement || "object" == typeof HTMLElement ? function(t) {
            return t instanceof HTMLElement
        } : function(t) {
            return t && "object" == typeof t && 1 == t.nodeType && "string" == typeof t.nodeName
        }, n.setText = function() {
            function t(t, i) {
                e = e || (void 0 !== document.documentElement.textContent ? "textContent" : "innerText"), t[e] = i
            }
            var e;
            return t
        }(), n.getParent = function(t, e) {
            for (; t != document.body;)
                if (t = t.parentNode, i(t, e)) return t
        }, n.getQueryElement = function(t) {
            return "string" == typeof t ? document.querySelector(t) : t
        }, n.handleEvent = function(t) {
            var e = "on" + t.type;
            this[e] && this[e](t)
        }, n.filterFindElements = function(t, e) {
            t = n.makeArray(t);
            for (var s = [], r = 0, o = t.length; o > r; r++) {
                var a = t[r];
                if (n.isElement(a))
                    if (e) {
                        i(a, e) && s.push(a);
                        for (var l = a.querySelectorAll(e), h = 0, u = l.length; u > h; h++) s.push(l[h])
                    } else s.push(a)
            }
            return s
        }, n.debounceMethod = function(t, e, i) {
            var n = t.prototype[e],
                s = e + "Timeout";
            t.prototype[e] = function() {
                var t = this[s];
                t && clearTimeout(t);
                var e = arguments,
                    r = this;
                this[s] = setTimeout(function() {
                    n.apply(r, e), delete r[s]
                }, i || 100)
            }
        }, n.toDashed = function(t) {
            return t.replace(/(.)([A-Z])/g, function(t, e, i) {
                return e + "-" + i
            }).toLowerCase()
        };
        var r = t.console;
        return n.htmlInit = function(i, s) {
            e(function() {
                for (var e = n.toDashed(s), o = document.querySelectorAll(".js-" + e), a = "data-" + e + "-options", l = 0, h = o.length; h > l; l++) {
                    var u, c = o[l],
                        p = c.getAttribute(a);
                    try {
                        u = p && JSON.parse(p)
                    } catch (d) {
                        r && r.error("Error parsing " + a + " on " + c.nodeName.toLowerCase() + (c.id ? "#" + c.id : "") + ": " + d);
                        continue
                    }
                    var f = new i(c, u),
                        m = t.jQuery;
                    m && m.data(c, s, f)
                }
            })
        }, n
    }),
    function(t, e) {
        "function" == typeof define && define.amd ? define("outlayer/item", ["eventEmitter/EventEmitter", "get-size/get-size", "get-style-property/get-style-property", "fizzy-ui-utils/utils"], function(i, n, s, r) {
            return e(t, i, n, s, r)
        }) : "object" == typeof exports ? module.exports = e(t, require("wolfy87-eventemitter"), require("get-size"), require("desandro-get-style-property"), require("fizzy-ui-utils")) : (t.Outlayer = {}, t.Outlayer.Item = e(t, t.EventEmitter, t.getSize, t.getStyleProperty, t.fizzyUIUtils))
    }(window, function(t, e, i, n, s) {
        function r(t) {
            for (var e in t) return !1;
            return e = null, !0
        }

        function o(t, e) {
            t && (this.element = t, this.layout = e, this.position = {
                x: 0,
                y: 0
            }, this._create())
        }
        var a = t.getComputedStyle,
            l = a ? function(t) {
                return a(t, null)
            } : function(t) {
                return t.currentStyle
            },
            h = n("transition"),
            u = n("transform"),
            c = h && u,
            p = !!n("perspective"),
            d = {
                WebkitTransition: "webkitTransitionEnd",
                MozTransition: "transitionend",
                OTransition: "otransitionend",
                transition: "transitionend"
            }[h],
            f = ["transform", "transition", "transitionDuration", "transitionProperty"],
            m = function() {
                for (var t = {}, e = 0, i = f.length; i > e; e++) {
                    var s = f[e],
                        r = n(s);
                    r && r !== s && (t[s] = r)
                }
                return t
            }();
        s.extend(o.prototype, e.prototype), o.prototype._create = function() {
            this._transn = {
                ingProperties: {},
                clean: {},
                onEnd: {}
            }, this.css({
                position: "absolute"
            })
        }, o.prototype.handleEvent = function(t) {
            var e = "on" + t.type;
            this[e] && this[e](t)
        }, o.prototype.getSize = function() {
            this.size = i(this.element)
        }, o.prototype.css = function(t) {
            var e = this.element.style;
            for (var i in t) {
                var n = m[i] || i;
                e[n] = t[i]
            }
        }, o.prototype.getPosition = function() {
            var t = l(this.element),
                e = this.layout.options,
                i = e.isOriginLeft,
                n = e.isOriginTop,
                s = parseInt(t[i ? "left" : "right"], 10),
                r = parseInt(t[n ? "top" : "bottom"], 10);
            s = isNaN(s) ? 0 : s, r = isNaN(r) ? 0 : r;
            var o = this.layout.size;
            s -= i ? o.paddingLeft : o.paddingRight, r -= n ? o.paddingTop : o.paddingBottom, this.position.x = s, this.position.y = r
        }, o.prototype.layoutPosition = function() {
            var t = this.layout.size,
                e = this.layout.options,
                i = {},
                n = e.isOriginLeft ? "paddingLeft" : "paddingRight",
                s = e.isOriginLeft ? "left" : "right",
                r = e.isOriginLeft ? "right" : "left",
                o = this.position.x + t[n];
            o = e.percentPosition && !e.isHorizontal ? 100 * (o / t.width) + "%" : o + "px", i[s] = o, i[r] = "";
            var a = e.isOriginTop ? "paddingTop" : "paddingBottom",
                l = e.isOriginTop ? "top" : "bottom",
                h = e.isOriginTop ? "bottom" : "top",
                u = this.position.y + t[a];
            u = e.percentPosition && e.isHorizontal ? 100 * (u / t.height) + "%" : u + "px", i[l] = u, i[h] = "", this.css(i), this.emitEvent("layout", [this])
        };
        var _ = p ? function(t, e) {
            return "translate3d(" + t + "px, " + e + "px, 0)"
        } : function(t, e) {
            return "translate(" + t + "px, " + e + "px)"
        };
        o.prototype._transitionTo = function(t, e) {
            this.getPosition();
            var i = this.position.x,
                n = this.position.y,
                s = parseInt(t, 10),
                r = parseInt(e, 10),
                o = s === this.position.x && r === this.position.y;
            if (this.setPosition(t, e), o && !this.isTransitioning) return void this.layoutPosition();
            var a = t - i,
                l = e - n,
                h = {},
                u = this.layout.options;
            a = u.isOriginLeft ? a : -a, l = u.isOriginTop ? l : -l, h.transform = _(a, l), this.transition({
                to: h,
                onTransitionEnd: {
                    transform: this.layoutPosition
                },
                isCleaning: !0
            })
        }, o.prototype.goTo = function(t, e) {
            this.setPosition(t, e), this.layoutPosition()
        }, o.prototype.moveTo = c ? o.prototype._transitionTo : o.prototype.goTo, o.prototype.setPosition = function(t, e) {
            this.position.x = parseInt(t, 10), this.position.y = parseInt(e, 10)
        }, o.prototype._nonTransition = function(t) {
            this.css(t.to), t.isCleaning && this._removeStyles(t.to);
            for (var e in t.onTransitionEnd) t.onTransitionEnd[e].call(this)
        }, o.prototype._transition = function(t) {
            if (!parseFloat(this.layout.options.transitionDuration)) return void this._nonTransition(t);
            var e = this._transn;
            for (var i in t.onTransitionEnd) e.onEnd[i] = t.onTransitionEnd[i];
            for (i in t.to) e.ingProperties[i] = !0, t.isCleaning && (e.clean[i] = !0);
            if (t.from) {
                this.css(t.from);
                var n = this.element.offsetHeight;
                n = null
            }
            this.enableTransition(t.to), this.css(t.to), this.isTransitioning = !0
        };
        var g = u && s.toDashed(u) + ",opacity";
        o.prototype.enableTransition = function() {
            this.isTransitioning || (this.css({
                transitionProperty: g,
                transitionDuration: this.layout.options.transitionDuration
            }), this.element.addEventListener(d, this, !1))
        }, o.prototype.transition = o.prototype[h ? "_transition" : "_nonTransition"], o.prototype.onwebkitTransitionEnd = function(t) {
            this.ontransitionend(t)
        }, o.prototype.onotransitionend = function(t) {
            this.ontransitionend(t)
        };
        var v = {
            "-webkit-transform": "transform",
            "-moz-transform": "transform",
            "-o-transform": "transform"
        };
        o.prototype.ontransitionend = function(t) {
            if (t.target === this.element) {
                var e = this._transn,
                    i = v[t.propertyName] || t.propertyName;
                if (delete e.ingProperties[i], r(e.ingProperties) && this.disableTransition(), i in e.clean && (this.element.style[t.propertyName] = "", delete e.clean[i]), i in e.onEnd) {
                    var n = e.onEnd[i];
                    n.call(this), delete e.onEnd[i]
                }
                this.emitEvent("transitionEnd", [this])
            }
        }, o.prototype.disableTransition = function() {
            this.removeTransitionStyles(), this.element.removeEventListener(d, this, !1), this.isTransitioning = !1
        }, o.prototype._removeStyles = function(t) {
            var e = {};
            for (var i in t) e[i] = "";
            this.css(e)
        };
        var y = {
            transitionProperty: "",
            transitionDuration: ""
        };
        return o.prototype.removeTransitionStyles = function() {
            this.css(y)
        }, o.prototype.removeElem = function() {
            this.element.parentNode.removeChild(this.element), this.css({
                display: ""
            }), this.emitEvent("remove", [this])
        }, o.prototype.remove = function() {
            if (!h || !parseFloat(this.layout.options.transitionDuration)) return void this.removeElem();
            var t = this;
            this.once("transitionEnd", function() {
                t.removeElem()
            }), this.hide()
        }, o.prototype.reveal = function() {
            delete this.isHidden, this.css({
                display: ""
            });
            var t = this.layout.options,
                e = {},
                i = this.getHideRevealTransitionEndProperty("visibleStyle");
            e[i] = this.onRevealTransitionEnd, this.transition({
                from: t.hiddenStyle,
                to: t.visibleStyle,
                isCleaning: !0,
                onTransitionEnd: e
            })
        }, o.prototype.onRevealTransitionEnd = function() {
            this.isHidden || this.emitEvent("reveal")
        }, o.prototype.getHideRevealTransitionEndProperty = function(t) {
            var e = this.layout.options[t];
            if (e.opacity) return "opacity";
            for (var i in e) return i
        }, o.prototype.hide = function() {
            this.isHidden = !0, this.css({
                display: ""
            });
            var t = this.layout.options,
                e = {},
                i = this.getHideRevealTransitionEndProperty("hiddenStyle");
            e[i] = this.onHideTransitionEnd, this.transition({
                from: t.visibleStyle,
                to: t.hiddenStyle,
                isCleaning: !0,
                onTransitionEnd: e
            })
        }, o.prototype.onHideTransitionEnd = function() {
            this.isHidden && (this.css({
                display: "none"
            }), this.emitEvent("hide"))
        }, o.prototype.destroy = function() {
            this.css({
                position: "",
                left: "",
                right: "",
                top: "",
                bottom: "",
                transition: "",
                transform: ""
            })
        }, o
    }),
    function(t, e) {
        "function" == typeof define && define.amd ? define("outlayer/outlayer", ["eventie/eventie", "eventEmitter/EventEmitter", "get-size/get-size", "fizzy-ui-utils/utils", "./item"], function(i, n, s, r, o) {
            return e(t, i, n, s, r, o)
        }) : "object" == typeof exports ? module.exports = e(t, require("eventie"), require("wolfy87-eventemitter"), require("get-size"), require("fizzy-ui-utils"), require("./item")) : t.Outlayer = e(t, t.eventie, t.EventEmitter, t.getSize, t.fizzyUIUtils, t.Outlayer.Item)
    }(window, function(t, e, i, n, s, r) {
        function o(t, e) {
            var i = s.getQueryElement(t);
            if (!i) return void(a && a.error("Bad element for " + this.constructor.namespace + ": " + (i || t)));
            this.element = i, l && (this.$element = l(this.element)), this.options = s.extend({}, this.constructor.defaults), this.option(e);
            var n = ++u;
            this.element.outlayerGUID = n, c[n] = this, this._create(), this.options.isInitLayout && this.layout()
        }
        var a = t.console,
            l = t.jQuery,
            h = function() {},
            u = 0,
            c = {};
        return o.namespace = "outlayer", o.Item = r, o.defaults = {
            containerStyle: {
                position: "relative"
            },
            isInitLayout: !0,
            isOriginLeft: !0,
            isOriginTop: !0,
            isResizeBound: !0,
            isResizingContainer: !0,
            transitionDuration: "0.4s",
            hiddenStyle: {
                opacity: 0,
                transform: "scale(0.001)"
            },
            visibleStyle: {
                opacity: 1,
                transform: "scale(1)"
            }
        }, s.extend(o.prototype, i.prototype), o.prototype.option = function(t) {
            s.extend(this.options, t)
        }, o.prototype._create = function() {
            this.reloadItems(), this.stamps = [], this.stamp(this.options.stamp), s.extend(this.element.style, this.options.containerStyle), this.options.isResizeBound && this.bindResize()
        }, o.prototype.reloadItems = function() {
            this.items = this._itemize(this.element.children)
        }, o.prototype._itemize = function(t) {
            for (var e = this._filterFindItemElements(t), i = this.constructor.Item, n = [], s = 0, r = e.length; r > s; s++) {
                var o = e[s],
                    a = new i(o, this);
                n.push(a)
            }
            return n
        }, o.prototype._filterFindItemElements = function(t) {
            return s.filterFindElements(t, this.options.itemSelector)
        }, o.prototype.getItemElements = function() {
            for (var t = [], e = 0, i = this.items.length; i > e; e++) t.push(this.items[e].element);
            return t
        }, o.prototype.layout = function() {
            this._resetLayout(), this._manageStamps();
            var t = void 0 !== this.options.isLayoutInstant ? this.options.isLayoutInstant : !this._isLayoutInited;
            this.layoutItems(this.items, t), this._isLayoutInited = !0
        }, o.prototype._init = o.prototype.layout, o.prototype._resetLayout = function() {
            this.getSize()
        }, o.prototype.getSize = function() {
            this.size = n(this.element)
        }, o.prototype._getMeasurement = function(t, e) {
            var i, r = this.options[t];
            r ? ("string" == typeof r ? i = this.element.querySelector(r) : s.isElement(r) && (i = r), this[t] = i ? n(i)[e] : r) : this[t] = 0
        }, o.prototype.layoutItems = function(t, e) {
            t = this._getItemsForLayout(t), this._layoutItems(t, e), this._postLayout()
        }, o.prototype._getItemsForLayout = function(t) {
            for (var e = [], i = 0, n = t.length; n > i; i++) {
                var s = t[i];
                s.isIgnored || e.push(s)
            }
            return e
        }, o.prototype._layoutItems = function(t, e) {
            if (this._emitCompleteOnItems("layout", t), t && t.length) {
                for (var i = [], n = 0, s = t.length; s > n; n++) {
                    var r = t[n],
                        o = this._getItemLayoutPosition(r);
                    o.item = r, o.isInstant = e || r.isLayoutInstant, i.push(o)
                }
                this._processLayoutQueue(i)
            }
        }, o.prototype._getItemLayoutPosition = function() {
            return {
                x: 0,
                y: 0
            }
        }, o.prototype._processLayoutQueue = function(t) {
            for (var e = 0, i = t.length; i > e; e++) {
                var n = t[e];
                this._positionItem(n.item, n.x, n.y, n.isInstant)
            }
        }, o.prototype._positionItem = function(t, e, i, n) {
            n ? t.goTo(e, i) : t.moveTo(e, i)
        }, o.prototype._postLayout = function() {
            this.resizeContainer()
        }, o.prototype.resizeContainer = function() {
            if (this.options.isResizingContainer) {
                var t = this._getContainerSize();
                t && (this._setContainerMeasure(t.width, !0), this._setContainerMeasure(t.height, !1))
            }
        }, o.prototype._getContainerSize = h, o.prototype._setContainerMeasure = function(t, e) {
            if (void 0 !== t) {
                var i = this.size;
                i.isBorderBox && (t += e ? i.paddingLeft + i.paddingRight + i.borderLeftWidth + i.borderRightWidth : i.paddingBottom + i.paddingTop + i.borderTopWidth + i.borderBottomWidth), t = Math.max(t, 0), this.element.style[e ? "width" : "height"] = t + "px"
            }
        }, o.prototype._emitCompleteOnItems = function(t, e) {
            function i() {
                s.emitEvent(t + "Complete", [e])
            }

            function n() {
                o++, o === r && i()
            }
            var s = this,
                r = e.length;
            if (!e || !r) return void i();
            for (var o = 0, a = 0, l = e.length; l > a; a++) {
                var h = e[a];
                h.once(t, n)
            }
        }, o.prototype.ignore = function(t) {
            var e = this.getItem(t);
            e && (e.isIgnored = !0)
        }, o.prototype.unignore = function(t) {
            var e = this.getItem(t);
            e && delete e.isIgnored
        }, o.prototype.stamp = function(t) {
            if (t = this._find(t)) {
                this.stamps = this.stamps.concat(t);
                for (var e = 0, i = t.length; i > e; e++) {
                    var n = t[e];
                    this.ignore(n)
                }
            }
        }, o.prototype.unstamp = function(t) {
            if (t = this._find(t))
                for (var e = 0, i = t.length; i > e; e++) {
                    var n = t[e];
                    s.removeFrom(this.stamps, n), this.unignore(n)
                }
        }, o.prototype._find = function(t) {
            return t ? ("string" == typeof t && (t = this.element.querySelectorAll(t)), t = s.makeArray(t)) : void 0
        }, o.prototype._manageStamps = function() {
            if (this.stamps && this.stamps.length) {
                this._getBoundingRect();
                for (var t = 0, e = this.stamps.length; e > t; t++) {
                    var i = this.stamps[t];
                    this._manageStamp(i)
                }
            }
        }, o.prototype._getBoundingRect = function() {
            var t = this.element.getBoundingClientRect(),
                e = this.size;
            this._boundingRect = {
                left: t.left + e.paddingLeft + e.borderLeftWidth,
                top: t.top + e.paddingTop + e.borderTopWidth,
                right: t.right - (e.paddingRight + e.borderRightWidth),
                bottom: t.bottom - (e.paddingBottom + e.borderBottomWidth)
            }
        }, o.prototype._manageStamp = h, o.prototype._getElementOffset = function(t) {
            var e = t.getBoundingClientRect(),
                i = this._boundingRect,
                s = n(t),
                r = {
                    left: e.left - i.left - s.marginLeft,
                    top: e.top - i.top - s.marginTop,
                    right: i.right - e.right - s.marginRight,
                    bottom: i.bottom - e.bottom - s.marginBottom
                };
            return r
        }, o.prototype.handleEvent = function(t) {
            var e = "on" + t.type;
            this[e] && this[e](t)
        }, o.prototype.bindResize = function() {
            this.isResizeBound || (e.bind(t, "resize", this), this.isResizeBound = !0)
        }, o.prototype.unbindResize = function() {
            this.isResizeBound && e.unbind(t, "resize", this), this.isResizeBound = !1
        }, o.prototype.onresize = function() {
            function t() {
                e.resize(), delete e.resizeTimeout
            }
            this.resizeTimeout && clearTimeout(this.resizeTimeout);
            var e = this;
            this.resizeTimeout = setTimeout(t, 100)
        }, o.prototype.resize = function() {
            this.isResizeBound && this.needsResizeLayout() && this.layout()
        }, o.prototype.needsResizeLayout = function() {
            var t = n(this.element),
                e = this.size && t;
            return e && t.innerWidth !== this.size.innerWidth
        }, o.prototype.addItems = function(t) {
            var e = this._itemize(t);
            return e.length && (this.items = this.items.concat(e)), e
        }, o.prototype.appended = function(t) {
            var e = this.addItems(t);
            e.length && (this.layoutItems(e, !0), this.reveal(e))
        }, o.prototype.prepended = function(t) {
            var e = this._itemize(t);
            if (e.length) {
                var i = this.items.slice(0);
                this.items = e.concat(i), this._resetLayout(), this._manageStamps(), this.layoutItems(e, !0), this.reveal(e), this.layoutItems(i)
            }
        }, o.prototype.reveal = function(t) {
            this._emitCompleteOnItems("reveal", t);
            for (var e = t && t.length, i = 0; e && e > i; i++) {
                var n = t[i];
                n.reveal()
            }
        }, o.prototype.hide = function(t) {
            this._emitCompleteOnItems("hide", t);
            for (var e = t && t.length, i = 0; e && e > i; i++) {
                var n = t[i];
                n.hide()
            }
        }, o.prototype.revealItemElements = function(t) {
            var e = this.getItems(t);
            this.reveal(e)
        }, o.prototype.hideItemElements = function(t) {
            var e = this.getItems(t);
            this.hide(e)
        }, o.prototype.getItem = function(t) {
            for (var e = 0, i = this.items.length; i > e; e++) {
                var n = this.items[e];
                if (n.element === t) return n
            }
        }, o.prototype.getItems = function(t) {
            t = s.makeArray(t);
            for (var e = [], i = 0, n = t.length; n > i; i++) {
                var r = t[i],
                    o = this.getItem(r);
                o && e.push(o)
            }
            return e
        }, o.prototype.remove = function(t) {
            var e = this.getItems(t);
            if (this._emitCompleteOnItems("remove", e), e && e.length)
                for (var i = 0, n = e.length; n > i; i++) {
                    var r = e[i];
                    r.remove(), s.removeFrom(this.items, r)
                }
        }, o.prototype.destroy = function() {
            var t = this.element.style;
            t.height = "", t.position = "", t.width = "";
            for (var e = 0, i = this.items.length; i > e; e++) {
                var n = this.items[e];
                n.destroy()
            }
            this.unbindResize();
            var s = this.element.outlayerGUID;
            delete c[s], delete this.element.outlayerGUID, l && l.removeData(this.element, this.constructor.namespace)
        }, o.data = function(t) {
            t = s.getQueryElement(t);
            var e = t && t.outlayerGUID;
            return e && c[e]
        }, o.create = function(t, e) {
            function i() {
                o.apply(this, arguments)
            }
            return Object.create ? i.prototype = Object.create(o.prototype) : s.extend(i.prototype, o.prototype), i.prototype.constructor = i, i.defaults = s.extend({}, o.defaults), s.extend(i.defaults, e), i.prototype.settings = {}, i.namespace = t, i.data = o.data, i.Item = function() {
                r.apply(this, arguments)
            }, i.Item.prototype = new r, s.htmlInit(i, t), l && l.bridget && l.bridget(t, i), i
        }, o.Item = r, o
    }),
    function(t, e) {
        "function" == typeof define && define.amd ? define("isotope/js/item", ["outlayer/outlayer"], e) : "object" == typeof exports ? module.exports = e(require("outlayer")) : (t.Isotope = t.Isotope || {}, t.Isotope.Item = e(t.Outlayer))
    }(window, function(t) {
        function e() {
            t.Item.apply(this, arguments)
        }
        e.prototype = new t.Item, e.prototype._create = function() {
            this.id = this.layout.itemGUID++, t.Item.prototype._create.call(this), this.sortData = {}
        }, e.prototype.updateSortData = function() {
            if (!this.isIgnored) {
                this.sortData.id = this.id, this.sortData["original-order"] = this.id, this.sortData.random = Math.random();
                var t = this.layout.options.getSortData,
                    e = this.layout._sorters;
                for (var i in t) {
                    var n = e[i];
                    this.sortData[i] = n(this.element, this)
                }
            }
        };
        var i = e.prototype.destroy;
        return e.prototype.destroy = function() {
            i.apply(this, arguments), this.css({
                display: ""
            })
        }, e
    }),
    function(t, e) {
        "function" == typeof define && define.amd ? define("isotope/js/layout-mode", ["get-size/get-size", "outlayer/outlayer"], e) : "object" == typeof exports ? module.exports = e(require("get-size"), require("outlayer")) : (t.Isotope = t.Isotope || {}, t.Isotope.LayoutMode = e(t.getSize, t.Outlayer))
    }(window, function(t, e) {
        function i(t) {
            this.isotope = t, t && (this.options = t.options[this.namespace], this.element = t.element, this.items = t.filteredItems, this.size = t.size)
        }
        return function() {
            function t(t) {
                return function() {
                    return e.prototype[t].apply(this.isotope, arguments)
                }
            }
            for (var n = ["_resetLayout", "_getItemLayoutPosition", "_manageStamp", "_getContainerSize", "_getElementOffset", "needsResizeLayout"], s = 0, r = n.length; r > s; s++) {
                var o = n[s];
                i.prototype[o] = t(o)
            }
        }(), i.prototype.needsVerticalResizeLayout = function() {
            var e = t(this.isotope.element),
                i = this.isotope.size && e;
            return i && e.innerHeight != this.isotope.size.innerHeight
        }, i.prototype._getMeasurement = function() {
            this.isotope._getMeasurement.apply(this, arguments)
        }, i.prototype.getColumnWidth = function() {
            this.getSegmentSize("column", "Width")
        }, i.prototype.getRowHeight = function() {
            this.getSegmentSize("row", "Height")
        }, i.prototype.getSegmentSize = function(t, e) {
            var i = t + e,
                n = "outer" + e;
            if (this._getMeasurement(i, n), !this[i]) {
                var s = this.getFirstItemSize();
                this[i] = s && s[n] || this.isotope.size["inner" + e]
            }
        }, i.prototype.getFirstItemSize = function() {
            var e = this.isotope.filteredItems[0];
            return e && e.element && t(e.element)
        }, i.prototype.layout = function() {
            this.isotope.layout.apply(this.isotope, arguments)
        }, i.prototype.getSize = function() {
            this.isotope.getSize(), this.size = this.isotope.size
        }, i.modes = {}, i.create = function(t, e) {
            function n() {
                i.apply(this, arguments)
            }
            return n.prototype = new i, e && (n.options = e), n.prototype.namespace = t, i.modes[t] = n, n
        }, i
    }),
    function(t, e) {
        "function" == typeof define && define.amd ? define("masonry/masonry", ["outlayer/outlayer", "get-size/get-size", "fizzy-ui-utils/utils"], e) : "object" == typeof exports ? module.exports = e(require("outlayer"), require("get-size"), require("fizzy-ui-utils")) : t.Masonry = e(t.Outlayer, t.getSize, t.fizzyUIUtils)
    }(window, function(t, e, i) {
        var n = t.create("masonry");
        return n.prototype._resetLayout = function() {
            this.getSize(), this._getMeasurement("columnWidth", "outerWidth"), this._getMeasurement("gutter", "outerWidth"), this.measureColumns();
            var t = this.cols;
            for (this.colYs = []; t--;) this.colYs.push(0);
            this.maxY = 0
        }, n.prototype.measureColumns = function() {
            if (this.getContainerWidth(), !this.columnWidth) {
                var t = this.items[0],
                    i = t && t.element;
                this.columnWidth = i && e(i).outerWidth || this.containerWidth
            }
            var n = this.columnWidth += this.gutter,
                s = this.containerWidth + this.gutter,
                r = s / n,
                o = n - s % n,
                a = o && 1 > o ? "round" : "floor";
            r = Math[a](r), this.cols = Math.max(r, 1)
        }, n.prototype.getContainerWidth = function() {
            var t = this.options.isFitWidth ? this.element.parentNode : this.element,
                i = e(t);
            this.containerWidth = i && i.innerWidth
        }, n.prototype._getItemLayoutPosition = function(t) {
            t.getSize();
            var e = t.size.outerWidth % this.columnWidth,
                n = e && 1 > e ? "round" : "ceil",
                s = Math[n](t.size.outerWidth / this.columnWidth);
            s = Math.min(s, this.cols);
            for (var r = this._getColGroup(s), o = Math.min.apply(Math, r), a = i.indexOf(r, o), l = {
                    x: this.columnWidth * a,
                    y: o
                }, h = o + t.size.outerHeight, u = this.cols + 1 - r.length, c = 0; u > c; c++) this.colYs[a + c] = h;
            return l
        }, n.prototype._getColGroup = function(t) {
            if (2 > t) return this.colYs;
            for (var e = [], i = this.cols + 1 - t, n = 0; i > n; n++) {
                var s = this.colYs.slice(n, n + t);
                e[n] = Math.max.apply(Math, s)
            }
            return e
        }, n.prototype._manageStamp = function(t) {
            var i = e(t),
                n = this._getElementOffset(t),
                s = this.options.isOriginLeft ? n.left : n.right,
                r = s + i.outerWidth,
                o = Math.floor(s / this.columnWidth);
            o = Math.max(0, o);
            var a = Math.floor(r / this.columnWidth);
            a -= r % this.columnWidth ? 0 : 1, a = Math.min(this.cols - 1, a);
            for (var l = (this.options.isOriginTop ? n.top : n.bottom) + i.outerHeight, h = o; a >= h; h++) this.colYs[h] = Math.max(l, this.colYs[h])
        }, n.prototype._getContainerSize = function() {
            this.maxY = Math.max.apply(Math, this.colYs);
            var t = {
                height: this.maxY
            };
            return this.options.isFitWidth && (t.width = this._getContainerFitWidth()), t
        }, n.prototype._getContainerFitWidth = function() {
            for (var t = 0, e = this.cols; --e && 0 === this.colYs[e];) t++;
            return (this.cols - t) * this.columnWidth - this.gutter
        }, n.prototype.needsResizeLayout = function() {
            var t = this.containerWidth;
            return this.getContainerWidth(), t !== this.containerWidth
        }, n
    }),
    function(t, e) {
        "function" == typeof define && define.amd ? define("isotope/js/layout-modes/masonry", ["../layout-mode", "masonry/masonry"], e) : "object" == typeof exports ? module.exports = e(require("../layout-mode"), require("masonry-layout")) : e(t.Isotope.LayoutMode, t.Masonry)
    }(window, function(t, e) {
        function i(t, e) {
            for (var i in e) t[i] = e[i];
            return t
        }
        var n = t.create("masonry"),
            s = n.prototype._getElementOffset,
            r = n.prototype.layout,
            o = n.prototype._getMeasurement;
        i(n.prototype, e.prototype), n.prototype._getElementOffset = s, n.prototype.layout = r, n.prototype._getMeasurement = o;
        var a = n.prototype.measureColumns;
        n.prototype.measureColumns = function() {
            this.items = this.isotope.filteredItems, a.call(this)
        };
        var l = n.prototype._manageStamp;
        return n.prototype._manageStamp = function() {
            this.options.isOriginLeft = this.isotope.options.isOriginLeft, this.options.isOriginTop = this.isotope.options.isOriginTop, l.apply(this, arguments)
        }, n
    }),
    function(t, e) {
        "function" == typeof define && define.amd ? define("isotope/js/layout-modes/fit-rows", ["../layout-mode"], e) : "object" == typeof exports ? module.exports = e(require("../layout-mode")) : e(t.Isotope.LayoutMode)
    }(window, function(t) {
        var e = t.create("fitRows");
        return e.prototype._resetLayout = function() {
            this.x = 0, this.y = 0, this.maxY = 0, this._getMeasurement("gutter", "outerWidth")
        }, e.prototype._getItemLayoutPosition = function(t) {
            t.getSize();
            var e = t.size.outerWidth + this.gutter,
                i = this.isotope.size.innerWidth + this.gutter;
            0 !== this.x && e + this.x > i && (this.x = 0, this.y = this.maxY);
            var n = {
                x: this.x,
                y: this.y
            };
            return this.maxY = Math.max(this.maxY, this.y + t.size.outerHeight), this.x += e, n
        }, e.prototype._getContainerSize = function() {
            return {
                height: this.maxY
            }
        }, e
    }),
    function(t, e) {
        "function" == typeof define && define.amd ? define("isotope/js/layout-modes/vertical", ["../layout-mode"], e) : "object" == typeof exports ? module.exports = e(require("../layout-mode")) : e(t.Isotope.LayoutMode)
    }(window, function(t) {
        var e = t.create("vertical", {
            horizontalAlignment: 0
        });
        return e.prototype._resetLayout = function() {
            this.y = 0
        }, e.prototype._getItemLayoutPosition = function(t) {
            t.getSize();
            var e = (this.isotope.size.innerWidth - t.size.outerWidth) * this.options.horizontalAlignment,
                i = this.y;
            return this.y += t.size.outerHeight, {
                x: e,
                y: i
            }
        }, e.prototype._getContainerSize = function() {
            return {
                height: this.y
            }
        }, e
    }),
    function(t, e) {
        "function" == typeof define && define.amd ? define(["outlayer/outlayer", "get-size/get-size", "matches-selector/matches-selector", "fizzy-ui-utils/utils", "isotope/js/item", "isotope/js/layout-mode", "isotope/js/layout-modes/masonry", "isotope/js/layout-modes/fit-rows", "isotope/js/layout-modes/vertical"], function(i, n, s, r, o, a) {
            return e(t, i, n, s, r, o, a)
        }) : "object" == typeof exports ? module.exports = e(t, require("outlayer"), require("get-size"), require("desandro-matches-selector"), require("fizzy-ui-utils"), require("./item"), require("./layout-mode"), require("./layout-modes/masonry"), require("./layout-modes/fit-rows"), require("./layout-modes/vertical")) : t.Isotope = e(t, t.Outlayer, t.getSize, t.matchesSelector, t.fizzyUIUtils, t.Isotope.Item, t.Isotope.LayoutMode)
    }(window, function(t, e, i, n, s, r, o) {
        function a(t, e) {
            return function(i, n) {
                for (var s = 0, r = t.length; r > s; s++) {
                    var o = t[s],
                        a = i.sortData[o],
                        l = n.sortData[o];
                    if (a > l || l > a) {
                        var h = void 0 !== e[o] ? e[o] : e,
                            u = h ? 1 : -1;
                        return (a > l ? 1 : -1) * u
                    }
                }
                return 0
            }
        }
        var l = t.jQuery,
            h = String.prototype.trim ? function(t) {
                return t.trim()
            } : function(t) {
                return t.replace(/^\s+|\s+$/g, "")
            },
            u = document.documentElement,
            c = u.textContent ? function(t) {
                return t.textContent
            } : function(t) {
                return t.innerText
            },
            p = e.create("isotope", {
                layoutMode: "masonry",
                isJQueryFiltering: !0,
                sortAscending: !0
            });
        p.Item = r, p.LayoutMode = o, p.prototype._create = function() {
            this.itemGUID = 0, this._sorters = {}, this._getSorters(), e.prototype._create.call(this), this.modes = {}, this.filteredItems = this.items, this.sortHistory = ["original-order"];
            for (var t in o.modes) this._initLayoutMode(t)
        }, p.prototype.reloadItems = function() {
            this.itemGUID = 0, e.prototype.reloadItems.call(this)
        }, p.prototype._itemize = function() {
            for (var t = e.prototype._itemize.apply(this, arguments), i = 0, n = t.length; n > i; i++) {
                var s = t[i];
                s.id = this.itemGUID++
            }
            return this._updateItemsSortData(t), t
        }, p.prototype._initLayoutMode = function(t) {
            var e = o.modes[t],
                i = this.options[t] || {};
            this.options[t] = e.options ? s.extend(e.options, i) : i, this.modes[t] = new e(this)
        }, p.prototype.layout = function() {
            return !this._isLayoutInited && this.options.isInitLayout ? void this.arrange() : void this._layout()
        }, p.prototype._layout = function() {
            var t = this._getIsInstant();
            this._resetLayout(), this._manageStamps(), this.layoutItems(this.filteredItems, t), this._isLayoutInited = !0
        }, p.prototype.arrange = function(t) {
            function e() {
                n.reveal(i.needReveal), n.hide(i.needHide)
            }
            this.option(t), this._getIsInstant();
            var i = this._filter(this.items);
            this.filteredItems = i.matches;
            var n = this;
            this._bindArrangeComplete(), this._isInstant ? this._noTransition(e) : e(), this._sort(), this._layout()
        }, p.prototype._init = p.prototype.arrange, p.prototype._getIsInstant = function() {
            var t = void 0 !== this.options.isLayoutInstant ? this.options.isLayoutInstant : !this._isLayoutInited;
            return this._isInstant = t, t
        }, p.prototype._bindArrangeComplete = function() {
            function t() {
                e && i && n && s.emitEvent("arrangeComplete", [s.filteredItems])
            }
            var e, i, n, s = this;
            this.once("layoutComplete", function() {
                e = !0, t()
            }), this.once("hideComplete", function() {
                i = !0, t()
            }), this.once("revealComplete", function() {
                n = !0, t()
            })
        }, p.prototype._filter = function(t) {
            var e = this.options.filter;
            e = e || "*";
            for (var i = [], n = [], s = [], r = this._getFilterTest(e), o = 0, a = t.length; a > o; o++) {
                var l = t[o];
                if (!l.isIgnored) {
                    var h = r(l);
                    h && i.push(l), h && l.isHidden ? n.push(l) : h || l.isHidden || s.push(l)
                }
            }
            return {
                matches: i,
                needReveal: n,
                needHide: s
            }
        }, p.prototype._getFilterTest = function(t) {
            return l && this.options.isJQueryFiltering ? function(e) {
                return l(e.element).is(t)
            } : "function" == typeof t ? function(e) {
                return t(e.element)
            } : function(e) {
                return n(e.element, t)
            }
        }, p.prototype.updateSortData = function(t) {
            var e;
            t ? (t = s.makeArray(t), e = this.getItems(t)) : e = this.items, this._getSorters(), this._updateItemsSortData(e)
        }, p.prototype._getSorters = function() {
            var t = this.options.getSortData;
            for (var e in t) {
                var i = t[e];
                this._sorters[e] = d(i)
            }
        }, p.prototype._updateItemsSortData = function(t) {
            for (var e = t && t.length, i = 0; e && e > i; i++) {
                var n = t[i];
                n.updateSortData()
            }
        };
        var d = function() {
            function t(t) {
                if ("string" != typeof t) return t;
                var i = h(t).split(" "),
                    n = i[0],
                    s = n.match(/^\[(.+)\]$/),
                    r = s && s[1],
                    o = e(r, n),
                    a = p.sortDataParsers[i[1]];
                return t = a ? function(t) {
                    return t && a(o(t))
                } : function(t) {
                    return t && o(t)
                }
            }

            function e(t, e) {
                var i;
                return i = t ? function(e) {
                    return e.getAttribute(t)
                } : function(t) {
                    var i = t.querySelector(e);
                    return i && c(i)
                }
            }
            return t
        }();
        p.sortDataParsers = {
            parseInt: function(t) {
                return parseInt(t, 10)
            },
            parseFloat: function(t) {
                return parseFloat(t)
            }
        }, p.prototype._sort = function() {
            var t = this.options.sortBy;
            if (t) {
                var e = [].concat.apply(t, this.sortHistory),
                    i = a(e, this.options.sortAscending);
                this.filteredItems.sort(i), t != this.sortHistory[0] && this.sortHistory.unshift(t)
            }
        }, p.prototype._mode = function() {
            var t = this.options.layoutMode,
                e = this.modes[t];
            if (!e) throw Error("No layout mode: " + t);
            return e.options = this.options[t], e
        }, p.prototype._resetLayout = function() {
            e.prototype._resetLayout.call(this), this._mode()._resetLayout()
        }, p.prototype._getItemLayoutPosition = function(t) {
            return this._mode()._getItemLayoutPosition(t)
        }, p.prototype._manageStamp = function(t) {
            this._mode()._manageStamp(t)
        }, p.prototype._getContainerSize = function() {
            return this._mode()._getContainerSize()
        }, p.prototype.needsResizeLayout = function() {
            return this._mode().needsResizeLayout()
        }, p.prototype.appended = function(t) {
            var e = this.addItems(t);
            if (e.length) {
                var i = this._filterRevealAdded(e);
                this.filteredItems = this.filteredItems.concat(i)
            }
        }, p.prototype.prepended = function(t) {
            var e = this._itemize(t);
            if (e.length) {
                this._resetLayout(), this._manageStamps();
                var i = this._filterRevealAdded(e);
                this.layoutItems(this.filteredItems), this.filteredItems = i.concat(this.filteredItems), this.items = e.concat(this.items)
            }
        }, p.prototype._filterRevealAdded = function(t) {
            var e = this._filter(t);
            return this.hide(e.needHide), this.reveal(e.matches), this.layoutItems(e.matches, !0), e.matches
        }, p.prototype.insert = function(t) {
            var e = this.addItems(t);
            if (e.length) {
                var i, n, s = e.length;
                for (i = 0; s > i; i++) n = e[i], this.element.appendChild(n.element);
                var r = this._filter(e).matches;
                for (i = 0; s > i; i++) e[i].isLayoutInstant = !0;
                for (this.arrange(), i = 0; s > i; i++) delete e[i].isLayoutInstant;
                this.reveal(r)
            }
        };
        var f = p.prototype.remove;
        return p.prototype.remove = function(t) {
            t = s.makeArray(t);
            var e = this.getItems(t);
            f.call(this, t);
            var i = e && e.length;
            if (i)
                for (var n = 0; i > n; n++) {
                    var r = e[n];
                    s.removeFrom(this.filteredItems, r)
                }
        }, p.prototype.shuffle = function() {
            for (var t = 0, e = this.items.length; e > t; t++) {
                var i = this.items[t];
                i.sortData.random = Math.random()
            }
            this.options.sortBy = "random", this._sort(), this._layout()
        }, p.prototype._noTransition = function(t) {
            var e = this.options.transitionDuration;
            this.options.transitionDuration = 0;
            var i = t.call(this);
            return this.options.transitionDuration = e, i
        }, p.prototype.getFilteredItemElements = function() {
            for (var t = [], e = 0, i = this.filteredItems.length; i > e; e++) t.push(this.filteredItems[e].element);
            return t
        }, p
    }),
    function(t) {
        function e(e, i) {
            function n() {
                return u.update(), r(), u
            }

            function s() {
                var t = y.toLowerCase();
                _.obj.css(v, x / f.ratio), d.obj.css(v, -x), b.start = _.obj.offset()[v], f.obj.css(t, m[i.axis]), m.obj.css(t, m[i.axis]), _.obj.css(t, _[i.axis])
            }

            function r() {
                w ? p.obj[0].ontouchstart = function(t) {
                    1 === t.touches.length && (o(t.touches[0]), t.stopPropagation())
                } : (_.obj.bind("mousedown", o), m.obj.bind("mouseup", l)), i.scroll && window.addEventListener ? (c[0].addEventListener("DOMMouseScroll", a, !1), c[0].addEventListener("mousewheel", a, !1)) : i.scroll && (c[0].onmousewheel = a)
            }

            function o(e) {
                t("body").addClass("noSelect");
                var i = parseInt(_.obj.css(v), 10);
                b.start = g ? e.pageX : e.pageY, T.start = "auto" == i ? 0 : i, w ? (document.ontouchmove = function(t) {
                    t.preventDefault(), l(t.touches[0])
                }, document.ontouchend = h) : (t(document).bind("mousemove", l), t(document).bind("mouseup", h), _.obj.bind("mouseup", h))
            }

            function a(e) {
                if (d.ratio < 1) {
                    var n = e || window.event,
                        s = n.wheelDelta ? n.wheelDelta / 120 : -n.detail / 3;
                    x -= s * i.wheel, x = Math.min(d[i.axis] - p[i.axis], Math.max(0, x)), _.obj.css(v, x / f.ratio), d.obj.css(v, -x), (i.lockscroll || x !== d[i.axis] - p[i.axis] && 0 !== x) && (n = t.event.fix(n), n.preventDefault())
                }
            }

            function l(t) {
                d.ratio < 1 && (T.now = i.invertscroll && w ? Math.min(m[i.axis] - _[i.axis], Math.max(0, T.start + (b.start - (g ? t.pageX : t.pageY)))) : Math.min(m[i.axis] - _[i.axis], Math.max(0, T.start + ((g ? t.pageX : t.pageY) - b.start))), x = T.now * f.ratio, d.obj.css(v, -x), _.obj.css(v, T.now))
            }

            function h() {
                t("body").removeClass("noSelect"), t(document).unbind("mousemove", l), t(document).unbind("mouseup", h), _.obj.unbind("mouseup", h), document.ontouchmove = document.ontouchend = null
            }
            var u = this,
                c = e,
                p = {
                    obj: t(".viewport", e)
                },
                d = {
                    obj: t(".overview", e)
                },
                f = {
                    obj: t(".scrollbar", e)
                },
                m = {
                    obj: t(".track", f.obj)
                },
                _ = {
                    obj: t(".thumb", f.obj)
                },
                g = "x" === i.axis,
                v = g ? "left" : "top",
                y = g ? "Width" : "Height",
                x = 0,
                T = {
                    start: 0,
                    now: 0
                },
                b = {},
                w = "ontouchstart" in document.documentElement ? !0 : !1;
            return this.update = function(t) {
                p[i.axis] = p.obj[0]["offset" + y], d[i.axis] = d.obj[0]["scroll" + y], d.ratio = p[i.axis] / d[i.axis], f.obj.toggleClass("disable", d.ratio >= 1), m[i.axis] = "auto" === i.size ? p[i.axis] : i.size, _[i.axis] = Math.min(m[i.axis], Math.max(0, "auto" === i.sizethumb ? m[i.axis] * d.ratio : i.sizethumb)), f.ratio = "auto" === i.sizethumb ? d[i.axis] / m[i.axis] : (d[i.axis] - p[i.axis]) / (m[i.axis] - _[i.axis]), x = "relative" === t && d.ratio <= 1 ? Math.min(d[i.axis] - p[i.axis], Math.max(0, x)) : 0, x = "bottom" === t && d.ratio <= 1 ? d[i.axis] - p[i.axis] : isNaN(parseInt(t, 10)) ? x : parseInt(t, 10), s()
            }, n()
        }
        t.tiny = t.tiny || {}, t.tiny.scrollbar = {
            options: {
                axis: "y",
                wheel: 40,
                scroll: !0,
                lockscroll: !0,
                size: "auto",
                sizethumb: "auto",
                invertscroll: !1
            }
        }, t.fn.tinyscrollbar = function(i) {
            var n = t.extend({}, t.tiny.scrollbar.options, i);
            return this.each(function() {
                t(this).data("tsb", new e(t(this), n))
            }), this
        }, t.fn.tinyscrollbar_update = function(e) {
            return t(this).data("tsb").update(e)
        }
    }(jQuery), jQuery.easing.jswing = jQuery.easing.swing, jQuery.extend(jQuery.easing, {
        def: "easeOutQuad",
        swing: function(t, e, i, n, s) {
            return jQuery.easing[jQuery.easing.def](t, e, i, n, s)
        },
        easeInQuad: function(t, e, i, n, s) {
            return n * (e /= s) * e + i
        },
        easeOutQuad: function(t, e, i, n, s) {
            return -n * (e /= s) * (e - 2) + i
        },
        easeInOutQuad: function(t, e, i, n, s) {
            return (e /= s / 2) < 1 ? n / 2 * e * e + i : -n / 2 * (--e * (e - 2) - 1) + i
        },
        easeInCubic: function(t, e, i, n, s) {
            return n * (e /= s) * e * e + i
        },
        easeOutCubic: function(t, e, i, n, s) {
            return n * ((e = e / s - 1) * e * e + 1) + i
        },
        easeInOutCubic: function(t, e, i, n, s) {
            return (e /= s / 2) < 1 ? n / 2 * e * e * e + i : n / 2 * ((e -= 2) * e * e + 2) + i
        },
        easeInQuart: function(t, e, i, n, s) {
            return n * (e /= s) * e * e * e + i
        },
        easeOutQuart: function(t, e, i, n, s) {
            return -n * ((e = e / s - 1) * e * e * e - 1) + i
        },
        easeInOutQuart: function(t, e, i, n, s) {
            return (e /= s / 2) < 1 ? n / 2 * e * e * e * e + i : -n / 2 * ((e -= 2) * e * e * e - 2) + i
        },
        easeInQuint: function(t, e, i, n, s) {
            return n * (e /= s) * e * e * e * e + i
        },
        easeOutQuint: function(t, e, i, n, s) {
            return n * ((e = e / s - 1) * e * e * e * e + 1) + i
        },
        easeInOutQuint: function(t, e, i, n, s) {
            return (e /= s / 2) < 1 ? n / 2 * e * e * e * e * e + i : n / 2 * ((e -= 2) * e * e * e * e + 2) + i
        },
        easeInSine: function(t, e, i, n, s) {
            return -n * Math.cos(e / s * (Math.PI / 2)) + n + i
        },
        easeOutSine: function(t, e, i, n, s) {
            return n * Math.sin(e / s * (Math.PI / 2)) + i
        },
        easeInOutSine: function(t, e, i, n, s) {
            return -n / 2 * (Math.cos(Math.PI * e / s) - 1) + i
        },
        easeInExpo: function(t, e, i, n, s) {
            return 0 == e ? i : n * Math.pow(2, 10 * (e / s - 1)) + i
        },
        easeOutExpo: function(t, e, i, n, s) {
            return e == s ? i + n : n * (-Math.pow(2, -10 * e / s) + 1) + i
        },
        easeInOutExpo: function(t, e, i, n, s) {
            return 0 == e ? i : e == s ? i + n : (e /= s / 2) < 1 ? n / 2 * Math.pow(2, 10 * (e - 1)) + i : n / 2 * (-Math.pow(2, -10 * --e) + 2) + i
        },
        easeInCirc: function(t, e, i, n, s) {
            return -n * (Math.sqrt(1 - (e /= s) * e) - 1) + i
        },
        easeOutCirc: function(t, e, i, n, s) {
            return n * Math.sqrt(1 - (e = e / s - 1) * e) + i
        },
        easeInOutCirc: function(t, e, i, n, s) {
            return (e /= s / 2) < 1 ? -n / 2 * (Math.sqrt(1 - e * e) - 1) + i : n / 2 * (Math.sqrt(1 - (e -= 2) * e) + 1) + i
        },
        easeInElastic: function(t, e, i, n, s) {
            var r = 1.70158,
                o = 0,
                a = n;
            if (0 == e) return i;
            if (1 == (e /= s)) return i + n;
            if (o || (o = .3 * s), a < Math.abs(n)) {
                a = n;
                var r = o / 4
            } else var r = o / (2 * Math.PI) * Math.asin(n / a);
            return -(a * Math.pow(2, 10 * (e -= 1)) * Math.sin(2 * (e * s - r) * Math.PI / o)) + i
        },
        easeOutElastic: function(t, e, i, n, s) {
            var r = 1.70158,
                o = 0,
                a = n;
            if (0 == e) return i;
            if (1 == (e /= s)) return i + n;
            if (o || (o = .3 * s), a < Math.abs(n)) {
                a = n;
                var r = o / 4
            } else var r = o / (2 * Math.PI) * Math.asin(n / a);
            return a * Math.pow(2, -10 * e) * Math.sin(2 * (e * s - r) * Math.PI / o) + n + i
        },
        easeInOutElastic: function(t, e, i, n, s) {
            var r = 1.70158,
                o = 0,
                a = n;
            if (0 == e) return i;
            if (2 == (e /= s / 2)) return i + n;
            if (o || (o = .3 * s * 1.5), a < Math.abs(n)) {
                a = n;
                var r = o / 4
            } else var r = o / (2 * Math.PI) * Math.asin(n / a);
            return 1 > e ? -.5 * a * Math.pow(2, 10 * (e -= 1)) * Math.sin(2 * (e * s - r) * Math.PI / o) + i : a * Math.pow(2, -10 * (e -= 1)) * Math.sin(2 * (e * s - r) * Math.PI / o) * .5 + n + i
        },
        easeInBack: function(t, e, i, n, s, r) {
            return void 0 == r && (r = 1.70158), n * (e /= s) * e * ((r + 1) * e - r) + i
        },
        easeOutBack: function(t, e, i, n, s, r) {
            return void 0 == r && (r = 1.70158), n * ((e = e / s - 1) * e * ((r + 1) * e + r) + 1) + i
        },
        easeInOutBack: function(t, e, i, n, s, r) {
            return void 0 == r && (r = 1.70158), (e /= s / 2) < 1 ? n / 2 * e * e * (((r *= 1.525) + 1) * e - r) + i : n / 2 * ((e -= 2) * e * (((r *= 1.525) + 1) * e + r) + 2) + i
        },
        easeInBounce: function(t, e, i, n, s) {
            return n - jQuery.easing.easeOutBounce(t, s - e, 0, n, s) + i
        },
        easeOutBounce: function(t, e, i, n, s) {
            return (e /= s) < 1 / 2.75 ? 7.5625 * n * e * e + i : 2 / 2.75 > e ? n * (7.5625 * (e -= 1.5 / 2.75) * e + .75) + i : 2.5 / 2.75 > e ? n * (7.5625 * (e -= 2.25 / 2.75) * e + .9375) + i : n * (7.5625 * (e -= 2.625 / 2.75) * e + .984375) + i
        },
        easeInOutBounce: function(t, e, i, n, s) {
            return s / 2 > e ? .5 * jQuery.easing.easeInBounce(t, 2 * e, 0, n, s) + i : .5 * jQuery.easing.easeOutBounce(t, 2 * e - s, 0, n, s) + .5 * n + i
        }
    }), ! function(t) {
        var e = {},
            n = {
                mode: "horizontal",
                slideSelector: "",
                infiniteLoop: !0,
                hideControlOnEnd: !1,
                speed: 500,
                easing: null,
                slideMargin: 0,
                startSlide: 0,
                randomStart: !1,
                captions: !1,
                ticker: !1,
                tickerHover: !1,
                adaptiveHeight: !1,
                adaptiveHeightSpeed: 500,
                video: !1,
                useCSS: !0,
                preloadImages: "visible",
                responsive: !0,
                slideZIndex: 50,
                touchEnabled: !0,
                swipeThreshold: 50,
                oneToOneTouch: !0,
                preventDefaultSwipeX: !0,
                preventDefaultSwipeY: !1,
                pager: !0,
                pagerType: "full",
                pagerShortSeparator: " / ",
                pagerSelector: null,
                buildPager: null,
                pagerCustom: null,
                controls: !0,
                nextText: "Next",
                prevText: "Prev",
                nextSelector: null,
                prevSelector: null,
                autoControls: !1,
                startText: "Start",
                stopText: "Stop",
                autoControlsCombine: !1,
                autoControlsSelector: null,
                auto: !1,
                pause: 4e3,
                autoStart: !0,
                autoDirection: "next",
                autoHover: !1,
                autoDelay: 0,
                minSlides: 1,
                maxSlides: 1,
                moveSlides: 0,
                slideWidth: 0,
                onSliderLoad: function() {},
                onSlideBefore: function() {},
                onSlideAfter: function() {},
                onSlideNext: function() {},
                onSlidePrev: function() {},
                onSliderResize: function() {}
            };
        t.fn.bxSlider = function(s) {
            if (0 == this.length) return this;
            if (this.length > 1) return this.each(function() {
                t(this).bxSlider(s)
            }), this;
            var r = {},
                o = this;
            e.el = this;
            var a = t(window).width(),
                l = t(window).height(),
                h = function() {
                    r.settings = t.extend({}, n, s), r.settings.slideWidth = parseInt(r.settings.slideWidth), r.children = o.children(r.settings.slideSelector), r.children.length < r.settings.minSlides && (r.settings.minSlides = r.children.length), r.children.length < r.settings.maxSlides && (r.settings.maxSlides = r.children.length), r.settings.randomStart && (r.settings.startSlide = Math.floor(Math.random() * r.children.length)), r.active = {
                        index: r.settings.startSlide
                    }, r.carousel = r.settings.minSlides > 1 || r.settings.maxSlides > 1, r.carousel && (r.settings.preloadImages = "all"), r.minThreshold = r.settings.minSlides * r.settings.slideWidth + (r.settings.minSlides - 1) * r.settings.slideMargin, r.maxThreshold = r.settings.maxSlides * r.settings.slideWidth + (r.settings.maxSlides - 1) * r.settings.slideMargin, r.working = !1, r.controls = {}, r.interval = null, r.animProp = "vertical" == r.settings.mode ? "top" : "left", r.usingCSS = r.settings.useCSS && "fade" != r.settings.mode && function() {
                        var t = document.createElement("div"),
                            e = ["WebkitPerspective", "MozPerspective", "OPerspective", "msPerspective"];
                        for (var i in e)
                            if (void 0 !== t.style[e[i]]) return r.cssPrefix = e[i].replace("Perspective", "").toLowerCase(), r.animProp = "-" + r.cssPrefix + "-transform", !0;
                        return !1
                    }(), "vertical" == r.settings.mode && (r.settings.maxSlides = r.settings.minSlides), o.data("origStyle", o.attr("style")), o.children(r.settings.slideSelector).each(function() {
                        t(this).data("origStyle", t(this).attr("style"))
                    }), u()
                },
                u = function() {
                    o.wrap('<div class="bx-wrapper"><div class="bx-viewport"></div></div>'), r.viewport = o.parent(), r.loader = t('<div class="bx-loading" />'), r.viewport.prepend(r.loader), o.css({
                        width: "horizontal" == r.settings.mode ? 100 * r.children.length + 215 + "%" : "auto",
                        position: "relative"
                    }), r.usingCSS && r.settings.easing ? o.css("-" + r.cssPrefix + "-transition-timing-function", r.settings.easing) : r.settings.easing || (r.settings.easing = "swing"), _(), r.viewport.css({
                        width: "100%",
                        overflow: "hidden",
                        position: "relative"
                    }), r.viewport.parent().css({
                        maxWidth: f()
                    }), r.settings.pager || r.viewport.parent().css({
                        margin: "0 auto 0px"
                    }), r.children.css({
                        "float": "horizontal" == r.settings.mode ? "left" : "none",
                        listStyle: "none",
                        position: "relative"
                    }), r.children.css("width", m()), "horizontal" == r.settings.mode && r.settings.slideMargin > 0 && r.children.css("marginRight", r.settings.slideMargin), "vertical" == r.settings.mode && r.settings.slideMargin > 0 && r.children.css("marginBottom", r.settings.slideMargin), "fade" == r.settings.mode && (r.children.css({
                        position: "absolute",
                        zIndex: 0,
                        display: "none"
                    }), r.children.eq(r.settings.startSlide).css({
                        zIndex: r.settings.slideZIndex,
                        display: "block"
                    })), r.controls.el = t('<div class="bx-controls" />'), r.settings.captions && P(), r.active.last = r.settings.startSlide == g() - 1, r.settings.video && o.fitVids();
                    var e = r.children.eq(r.settings.startSlide);
                    "all" == r.settings.preloadImages && (e = r.children), r.settings.ticker ? r.settings.pager = !1 : (r.settings.pager && b(), r.settings.controls && w(), r.settings.auto && r.settings.autoControls && S(), (r.settings.controls || r.settings.autoControls || r.settings.pager) && r.viewport.after(r.controls.el)), c(e, p)
                },
                c = function(e, i) {
                    var n = e.find("img, iframe").length;
                    if (0 == n) return void i();
                    var s = 0;
                    e.find("img, iframe").each(function() {
                        t(this).one("load", function() {
                            ++s == n && i()
                        }).each(function() {
                            this.complete && t(this).load()
                        })
                    })
                },
                p = function() {
                    if (r.settings.infiniteLoop && "fade" != r.settings.mode && !r.settings.ticker) {
                        var e = "vertical" == r.settings.mode ? r.settings.minSlides : r.settings.maxSlides,
                            i = r.children.slice(0, e).clone().addClass("bx-clone"),
                            n = r.children.slice(-e).clone().addClass("bx-clone");
                        o.append(i).prepend(n)
                    }
                    r.loader.remove(), y(), "vertical" == r.settings.mode && (r.settings.adaptiveHeight = !0), r.viewport.height(d()), o.redrawSlider(), r.settings.onSliderLoad(r.active.index), r.initialized = !0, r.settings.responsive && t(window).bind("resize", X), r.settings.auto && r.settings.autoStart && L(), r.settings.ticker && D(), r.settings.pager && M(r.settings.startSlide), r.settings.controls && A(), r.settings.touchEnabled && !r.settings.ticker && F()
                },
                d = function() {
                    var e = 0,
                        n = t();
                    if ("vertical" == r.settings.mode || r.settings.adaptiveHeight)
                        if (r.carousel) {
                            var s = 1 == r.settings.moveSlides ? r.active.index : r.active.index * v();
                            for (n = r.children.eq(s), i = 1; i <= r.settings.maxSlides - 1; i++) n = n.add(s + i >= r.children.length ? r.children.eq(i - 1) : r.children.eq(s + i))
                        } else n = r.children.eq(r.active.index);
                    else n = r.children;
                    return "vertical" == r.settings.mode ? (n.each(function() {
                        e += t(this).outerHeight()
                    }), r.settings.slideMargin > 0 && (e += r.settings.slideMargin * (r.settings.minSlides - 1))) : e = Math.max.apply(Math, n.map(function() {
                        return t(this).outerHeight(!1)
                    }).get()), e
                },
                f = function() {
                    var t = "100%";
                    return r.settings.slideWidth > 0 && (t = "horizontal" == r.settings.mode ? r.settings.maxSlides * r.settings.slideWidth + (r.settings.maxSlides - 1) * r.settings.slideMargin : r.settings.slideWidth), t
                },
                m = function() {
                    var t = r.settings.slideWidth,
                        e = r.viewport.width();
                    return 0 == r.settings.slideWidth || r.settings.slideWidth > e && !r.carousel || "vertical" == r.settings.mode ? t = e : r.settings.maxSlides > 1 && "horizontal" == r.settings.mode && (e > r.maxThreshold || e < r.minThreshold && (t = (e - r.settings.slideMargin * (r.settings.minSlides - 1)) / r.settings.minSlides)), t
                },
                _ = function() {
                    var t = 1;
                    if ("horizontal" == r.settings.mode && r.settings.slideWidth > 0)
                        if (r.viewport.width() < r.minThreshold) t = r.settings.minSlides;
                        else if (r.viewport.width() > r.maxThreshold) t = r.settings.maxSlides;
                    else {
                        var e = r.children.first().width();
                        t = Math.floor(r.viewport.width() / e)
                    } else "vertical" == r.settings.mode && (t = r.settings.minSlides);
                    return t
                },
                g = function() {
                    var t = 0;
                    if (r.settings.moveSlides > 0)
                        if (r.settings.infiniteLoop) t = r.children.length / v();
                        else
                            for (var e = 0, i = 0; e < r.children.length;) ++t, e = i + _(), i += r.settings.moveSlides <= _() ? r.settings.moveSlides : _();
                    else t = Math.ceil(r.children.length / _());
                    return t
                },
                v = function() {
                    return r.settings.moveSlides > 0 && r.settings.moveSlides <= _() ? r.settings.moveSlides : _()
                },
                y = function() {
                    if (r.children.length > r.settings.maxSlides && r.active.last && !r.settings.infiniteLoop) {
                        if ("horizontal" == r.settings.mode) {
                            var t = r.children.last(),
                                e = t.position();
                            x(-(e.left - (r.viewport.width() - t.width())), "reset", 0)
                        } else if ("vertical" == r.settings.mode) {
                            var i = r.children.length - r.settings.minSlides,
                                e = r.children.eq(i).position();
                            x(-e.top, "reset", 0)
                        }
                    } else {
                        var e = r.children.eq(r.active.index * v()).position();
                        r.active.index == g() - 1 && (r.active.last = !0), void 0 != e && ("horizontal" == r.settings.mode ? x(-e.left, "reset", 0) : "vertical" == r.settings.mode && x(-e.top, "reset", 0))
                    }
                },
                x = function(t, e, i, n) {
                    if (r.usingCSS) {
                        var s = "vertical" == r.settings.mode ? "translate3d(0, " + t + "px, 0)" : "translate3d(" + t + "px, 0, 0)";
                        o.css("-" + r.cssPrefix + "-transition-duration", i / 1e3 + "s"), "slide" == e ? (o.css(r.animProp, s), o.bind("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd", function() {
                            o.unbind("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd"), E()
                        })) : "reset" == e ? o.css(r.animProp, s) : "ticker" == e && (o.css("-" + r.cssPrefix + "-transition-timing-function", "linear"), o.css(r.animProp, s), o.bind("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd", function() {
                            o.unbind("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd"), x(n.resetValue, "reset", 0), j()
                        }))
                    } else {
                        var a = {};
                        a[r.animProp] = t, "slide" == e ? o.animate(a, i, r.settings.easing, function() {
                            E()
                        }) : "reset" == e ? o.css(r.animProp, t) : "ticker" == e && o.animate(a, speed, "linear", function() {
                            x(n.resetValue, "reset", 0), j()
                        })
                    }
                },
                T = function() {
                    for (var e = "", i = g(), n = 0; i > n; n++) {
                        var s = "";
                        r.settings.buildPager && t.isFunction(r.settings.buildPager) ? (s = r.settings.buildPager(n), r.pagerEl.addClass("bx-custom-pager")) : (s = n + 1, r.pagerEl.addClass("bx-default-pager")), e += '<div class="bx-pager-item"><a href="" data-slide-index="' + n + '" class="bx-pager-link">' + s + "</a></div>"
                    }
                    r.pagerEl.html(e)
                },
                b = function() {
                    r.settings.pagerCustom ? r.pagerEl = t(r.settings.pagerCustom) : (r.pagerEl = t('<div class="bx-pager" />'), r.settings.pagerSelector ? t(r.settings.pagerSelector).html(r.pagerEl) : r.controls.el.addClass("bx-has-pager").append(r.pagerEl), T()), r.pagerEl.on("click", "a", R)
                },
                w = function() {
                    r.controls.next = t('<a class="bx-next" href="">' + r.settings.nextText + "</a>"), r.controls.prev = t('<a class="bx-prev" href="">' + r.settings.prevText + "</a>"), r.controls.next.bind("click", C), r.controls.prev.bind("click", I), r.settings.nextSelector && t(r.settings.nextSelector).append(r.controls.next), r.settings.prevSelector && t(r.settings.prevSelector).append(r.controls.prev), r.settings.nextSelector || r.settings.prevSelector || (r.controls.directionEl = t('<div class="bx-controls-direction" />'), r.controls.directionEl.append(r.controls.prev).append(r.controls.next), r.controls.el.addClass("bx-has-controls-direction").append(r.controls.directionEl))
                },
                S = function() {
                    r.controls.start = t('<div class="bx-controls-auto-item"><a class="bx-start" href="">' + r.settings.startText + "</a></div>"), r.controls.stop = t('<div class="bx-controls-auto-item"><a class="bx-stop" href="">' + r.settings.stopText + "</a></div>"), r.controls.autoEl = t('<div class="bx-controls-auto" />'), r.controls.autoEl.on("click", ".bx-start", z), r.controls.autoEl.on("click", ".bx-stop", O), r.settings.autoControlsCombine ? r.controls.autoEl.append(r.controls.start) : r.controls.autoEl.append(r.controls.start).append(r.controls.stop), r.settings.autoControlsSelector ? t(r.settings.autoControlsSelector).html(r.controls.autoEl) : r.controls.el.addClass("bx-has-controls-auto").append(r.controls.autoEl), k(r.settings.autoStart ? "stop" : "start")
                },
                P = function() {
                    r.children.each(function() {
                        var e = t(this).find("img:first").attr("title");
                        void 0 != e && ("" + e).length && t(this).append('<div class="bx-caption"><span>' + e + "</span></div>")
                    })
                },
                C = function(t) {
                    r.settings.auto && o.stopAuto(), o.goToNextSlide(), t.preventDefault()
                },
                I = function(t) {
                    r.settings.auto && o.stopAuto(), o.goToPrevSlide(), t.preventDefault()
                },
                z = function(t) {
                    o.startAuto(), t.preventDefault()
                },
                O = function(t) {
                    o.stopAuto(), t.preventDefault()
                },
                R = function(e) {
                    r.settings.auto && o.stopAuto();
                    var i = t(e.currentTarget),
                        n = parseInt(i.attr("data-slide-index"));
                    n != r.active.index && o.goToSlide(n), e.preventDefault()
                },
                M = function(e) {
                    var i = r.children.length;
                    return "short" == r.settings.pagerType ? (r.settings.maxSlides > 1 && (i = Math.ceil(r.children.length / r.settings.maxSlides)), void r.pagerEl.html(e + 1 + r.settings.pagerShortSeparator + i)) : (r.pagerEl.find("a").removeClass("active"), void r.pagerEl.each(function(i, n) {
                        t(n).find("a").eq(e).addClass("active")
                    }))
                },
                E = function() {
                    if (r.settings.infiniteLoop) {
                        var t = "";
                        0 == r.active.index ? t = r.children.eq(0).position() : r.active.index == g() - 1 && r.carousel ? t = r.children.eq((g() - 1) * v()).position() : r.active.index == r.children.length - 1 && (t = r.children.eq(r.children.length - 1).position()), t && ("horizontal" == r.settings.mode ? x(-t.left, "reset", 0) : "vertical" == r.settings.mode && x(-t.top, "reset", 0))
                    }
                    r.working = !1, r.settings.onSlideAfter(r.children.eq(r.active.index), r.oldIndex, r.active.index)
                },
                k = function(t) {
                    r.settings.autoControlsCombine ? r.controls.autoEl.html(r.controls[t]) : (r.controls.autoEl.find("a").removeClass("active"), r.controls.autoEl.find("a:not(.bx-" + t + ")").addClass("active"))
                },
                A = function() {
                    1 == g() ? (r.controls.prev.addClass("disabled"), r.controls.next.addClass("disabled")) : !r.settings.infiniteLoop && r.settings.hideControlOnEnd && (0 == r.active.index ? (r.controls.prev.addClass("disabled"), r.controls.next.removeClass("disabled")) : r.active.index == g() - 1 ? (r.controls.next.addClass("disabled"), r.controls.prev.removeClass("disabled")) : (r.controls.prev.removeClass("disabled"), r.controls.next.removeClass("disabled")))
                },
                L = function() {
                    r.settings.autoDelay > 0 ? setTimeout(o.startAuto, r.settings.autoDelay) : o.startAuto(), r.settings.autoHover && o.hover(function() {
                        r.interval && (o.stopAuto(!0), r.autoPaused = !0)
                    }, function() {
                        r.autoPaused && (o.startAuto(!0), r.autoPaused = null)
                    })
                },
                D = function() {
                    var e = 0;
                    if ("next" == r.settings.autoDirection) o.append(r.children.clone().addClass("bx-clone"));
                    else {
                        o.prepend(r.children.clone().addClass("bx-clone"));
                        var i = r.children.first().position();
                        e = "horizontal" == r.settings.mode ? -i.left : -i.top
                    }
                    x(e, "reset", 0), r.settings.pager = !1, r.settings.controls = !1, r.settings.autoControls = !1, r.settings.tickerHover && !r.usingCSS && r.viewport.hover(function() {
                        o.stop()
                    }, function() {
                        var e = 0;
                        r.children.each(function() {
                            e += "horizontal" == r.settings.mode ? t(this).outerWidth(!0) : t(this).outerHeight(!0)
                        });
                        var i = r.settings.speed / e,
                            n = "horizontal" == r.settings.mode ? "left" : "top",
                            s = i * (e - Math.abs(parseInt(o.css(n))));
                        j(s)
                    }), j()
                },
                j = function(t) {
                    speed = t ? t : r.settings.speed;
                    var e = {
                            left: 0,
                            top: 0
                        },
                        i = {
                            left: 0,
                            top: 0
                        };
                    "next" == r.settings.autoDirection ? e = o.find(".bx-clone").first().position() : i = r.children.first().position();
                    var n = "horizontal" == r.settings.mode ? -e.left : -e.top,
                        s = "horizontal" == r.settings.mode ? -i.left : -i.top,
                        a = {
                            resetValue: s
                        };
                    x(n, "ticker", speed, a)
                },
                F = function() {
                    r.touch = {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        }
                    }, r.viewport.bind("touchstart", N)
                },
                N = function(t) {
                    if (r.working) t.preventDefault();
                    else {
                        r.touch.originalPos = o.position();
                        var e = t.originalEvent;
                        r.touch.start.x = e.changedTouches[0].pageX, r.touch.start.y = e.changedTouches[0].pageY, r.viewport.bind("touchmove", W), r.viewport.bind("touchend", q)
                    }
                },
                W = function(t) {
                    var e = t.originalEvent,
                        i = Math.abs(e.changedTouches[0].pageX - r.touch.start.x),
                        n = Math.abs(e.changedTouches[0].pageY - r.touch.start.y);
                    if (3 * i > n && r.settings.preventDefaultSwipeX ? t.preventDefault() : 3 * n > i && r.settings.preventDefaultSwipeY && t.preventDefault(), "fade" != r.settings.mode && r.settings.oneToOneTouch) {
                        var s = 0;
                        if ("horizontal" == r.settings.mode) {
                            var o = e.changedTouches[0].pageX - r.touch.start.x;
                            s = r.touch.originalPos.left + o
                        } else {
                            var o = e.changedTouches[0].pageY - r.touch.start.y;
                            s = r.touch.originalPos.top + o
                        }
                        x(s, "reset", 0)
                    }
                },
                q = function(t) {
                    r.viewport.unbind("touchmove", W);
                    var e = t.originalEvent,
                        i = 0;
                    if (r.touch.end.x = e.changedTouches[0].pageX, r.touch.end.y = e.changedTouches[0].pageY, "fade" == r.settings.mode) {
                        var n = Math.abs(r.touch.start.x - r.touch.end.x);
                        n >= r.settings.swipeThreshold && (r.touch.start.x > r.touch.end.x ? o.goToNextSlide() : o.goToPrevSlide(), o.stopAuto())
                    } else {
                        var n = 0;
                        "horizontal" == r.settings.mode ? (n = r.touch.end.x - r.touch.start.x, i = r.touch.originalPos.left) : (n = r.touch.end.y - r.touch.start.y, i = r.touch.originalPos.top), !r.settings.infiniteLoop && (0 == r.active.index && n > 0 || r.active.last && 0 > n) ? x(i, "reset", 200) : Math.abs(n) >= r.settings.swipeThreshold ? (0 > n ? o.goToNextSlide() : o.goToPrevSlide(), o.stopAuto()) : x(i, "reset", 200)
                    }
                    r.viewport.unbind("touchend", q)
                },
                X = function() {
                    var e = t(window).width(),
                        i = t(window).height();
                    (a != e || l != i) && (a = e, l = i, o.redrawSlider(), r.settings.onSliderResize.call(o, r.active.index))
                };
            return o.goToSlide = function(e, i) {
                if (!r.working && r.active.index != e)
                    if (r.working = !0, r.oldIndex = r.active.index, r.active.index = 0 > e ? g() - 1 : e >= g() ? 0 : e, r.settings.onSlideBefore(r.children.eq(r.active.index), r.oldIndex, r.active.index), "next" == i ? r.settings.onSlideNext(r.children.eq(r.active.index), r.oldIndex, r.active.index) : "prev" == i && r.settings.onSlidePrev(r.children.eq(r.active.index), r.oldIndex, r.active.index), r.active.last = r.active.index >= g() - 1, r.settings.pager && M(r.active.index), r.settings.controls && A(), "fade" == r.settings.mode) r.settings.adaptiveHeight && r.viewport.height() != d() && r.viewport.animate({
                        height: d()
                    }, r.settings.adaptiveHeightSpeed), r.children.filter(":visible").fadeOut(r.settings.speed).css({
                        zIndex: 0
                    }), r.children.eq(r.active.index).css("zIndex", r.settings.slideZIndex + 1).fadeIn(r.settings.speed, function() {
                        t(this).css("zIndex", r.settings.slideZIndex), E()
                    });
                    else {
                        r.settings.adaptiveHeight && r.viewport.height() != d() && r.viewport.animate({
                            height: d()
                        }, r.settings.adaptiveHeightSpeed);
                        var n = 0,
                            s = {
                                left: 0,
                                top: 0
                            };
                        if (!r.settings.infiniteLoop && r.carousel && r.active.last)
                            if ("horizontal" == r.settings.mode) {
                                var a = r.children.eq(r.children.length - 1);
                                s = a.position(), n = r.viewport.width() - a.outerWidth()
                            } else {
                                var l = r.children.length - r.settings.minSlides;
                                s = r.children.eq(l).position()
                            }
                        else if (r.carousel && r.active.last && "prev" == i) {
                            var h = 1 == r.settings.moveSlides ? r.settings.maxSlides - v() : (g() - 1) * v() - (r.children.length - r.settings.maxSlides),
                                a = o.children(".bx-clone").eq(h);
                            s = a.position()
                        } else if ("next" == i && 0 == r.active.index) s = o.find("> .bx-clone").eq(r.settings.maxSlides).position(), r.active.last = !1;
                        else if (e >= 0) {
                            var u = e * v();
                            s = r.children.eq(u).position()
                        }
                        if ("undefined" != typeof s) {
                            var c = "horizontal" == r.settings.mode ? -(s.left - n) : -s.top;
                            x(c, "slide", r.settings.speed)
                        }
                    }
            }, o.goToNextSlide = function() {
                if (r.settings.infiniteLoop || !r.active.last) {
                    var t = parseInt(r.active.index) + 1;
                    o.goToSlide(t, "next")
                }
            }, o.goToPrevSlide = function() {
                if (r.settings.infiniteLoop || 0 != r.active.index) {
                    var t = parseInt(r.active.index) - 1;
                    o.goToSlide(t, "prev")
                }
            }, o.startAuto = function(t) {
                r.interval || (r.interval = setInterval(function() {
                    "next" == r.settings.autoDirection ? o.goToNextSlide() : o.goToPrevSlide()
                }, r.settings.pause), r.settings.autoControls && 1 != t && k("stop"))
            }, o.stopAuto = function(t) {
                r.interval && (clearInterval(r.interval), r.interval = null, r.settings.autoControls && 1 != t && k("start"))
            }, o.getCurrentSlide = function() {
                return r.active.index
            }, o.getCurrentSlideElement = function() {
                return r.children.eq(r.active.index)
            }, o.getSlideCount = function() {
                return r.children.length
            }, o.redrawSlider = function() {
                r.children.add(o.find(".bx-clone")).outerWidth(m()), r.viewport.css("height", d()), r.settings.ticker || y(), r.active.last && (r.active.index = g() - 1), r.active.index >= g() && (r.active.last = !0), r.settings.pager && !r.settings.pagerCustom && (T(), M(r.active.index))
            }, o.destroySlider = function() {
                r.initialized && (r.initialized = !1, t(".bx-clone", this).remove(), r.children.each(function() {
                    void 0 != t(this).data("origStyle") ? t(this).attr("style", t(this).data("origStyle")) : t(this).removeAttr("style")
                }), void 0 != t(this).data("origStyle") ? this.attr("style", t(this).data("origStyle")) : t(this).removeAttr("style"), t(this).unwrap().unwrap(), r.controls.el && r.controls.el.remove(), r.controls.next && r.controls.next.remove(), r.controls.prev && r.controls.prev.remove(), r.pagerEl && r.settings.controls && r.pagerEl.remove(), t(".bx-caption", this).remove(), r.controls.autoEl && r.controls.autoEl.remove(), clearInterval(r.interval), r.settings.responsive && t(window).unbind("resize", X))
            }, o.reloadSlider = function(t) {
                void 0 != t && (s = t), o.destroySlider(), h()
            }, h(), this
        }
    }(jQuery);