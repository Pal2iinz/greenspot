
$(document).ready(function(){

	$('#VitamilkYoutube').modal('show');
	$("#VitamilkYoutube").on('hidden.bs.modal', function (e) { // on closing the modal
	  // stop the video
	  $("#VitamilkYoutube iframe").attr("src", null);
	});
	
	var screensize = document.documentElement.clientWidth;
    if (screensize  > 1400) {
         	$('#frmbox').css({ width: "100%" });
         	$('#Background img').css({ width: "100%" });
         	$('#sectionfrm1').css({
		    'bottom': '833px',
		    'left': '29px',
			});
			$('#sectionfrm2').css({
		    'bottom': '830px',
		    'left': '34px',
			});
			$('#sectionfrm3').css({
		    'bottom': '836px',
		    'left': '31px',
			});
			$('#sectionfrm4').css({
		    'bottom': '844px',
		    'left': '34px',
			});
			$('.upload-btn-wrapper').css({
		    'bottom': '825px',
		    'left': '270px',
			});
			$('.bsubmit').css({
		    'bottom': '789px',
		    'right': '262px',
			});
			$('.bsharefb').css({
		    'bottom': '789px',
		    'right': '275px',
			});
			$('#conditionfrm1').css({
		    'bottom': '50px',
		    'top': '0px',
			});
				$('#contentcondition').css({
		    'top': '-270px',
			});
			$('#conditionfrm2').css({
				'position' : 'relative',
		    'top': '-300px',
			});
	 } else if(screensize >= 320 && screensize <= 361){
	 	    $('#frmbox').css({
				'position' : 'absolute',
				'top' : '150px',
				'width' : '488px',
			});
			$('#frmbox img').css({ width: "473px" });
	 	  $('#Background').css({ width: "488px" });
     	$('#Background img').css({ width: "100%" });
     	$('.footer').css({ width: "488px" });
      $('input.textbox').css({
				'height' : '14px',
				'width' : '55%',
			});
      		$('select.textbox').css({
				'height' : '14px',
				'width' : '56%',
			});
      $('#r1').css({
				'position' : 'relative',
				'bottom' : '95px',
				'left' : '110px',
			});
			$('#r2').css({
				'position' : 'relative',
				'bottom' : '109px',
				'left' : '110px',
			});
			$('#r3').css({
				'position' : 'relative',
				'bottom' : '123px',
				'left' : '110px',
			});
			$('#r4').css({
				'position' : 'relative',
				'bottom' : '137px',
				'left' : '110px',
			});
			$('#r5').css({
				'position' : 'relative',
				'bottom' : '149px',
				'left' : '113px',
			});
			$('#r6').css({
				'position' : 'relative',
				'bottom' : '160px',
				'left' : '110px',
			});
			$('#r7').css({
				'position' : 'relative',
				'bottom' : '172px',
				'left' : '113px',
			});
			$('.Mupload-btn-wrapper').css({
				'position' : 'relative',
				'width' : '55px',
				'height' : '22px',

			});
			$('.Mupload-btn-wrapper input[type=file]').css({
				'position' : 'absolute',
				'left' : '0',
				'top' : '0',
				'opacity' :'0',
				'width' : '55px',
				'height' : '22px',
			});
			$('.btn2').css({
				'border' : '1px solid #58380b',
				'height' : '14px',
				'width' : '55px',
				'font-size' : '10px',
			});

			$('#r8').css({
				'position' : 'relative',
				'bottom' : '192px',
				'left' : '85px',
			});

			$('.bsubmit').css({
				'position' : 'relative',
				'bottom' : '173px',
				'left' : '-11px',
				'width' : '70px',
			});

			$('.bsharefb').css({
				'position' : 'relative',
				'bottom' : '199px',
				'left' : '224px',
				'width' : '78px',
			});

			$('#conditionfrm1').css({
				'width' : '100%',
				'position' : 'relative',
				'top' : '230px',
				'left' : '70px',
			});

			$('#contentcondition').css({
				'position' : 'relative',
				'top' : '15px',
				'left' : '70px',
			});

			$('#conditionfrm2').css({
				'position' : 'relative',
				'width' : '100%',
				'left' : '70px',
			});
	 } else if(screensize >= 390 && screensize <= 410){
	     $('#frmbox').css({
				'position' : 'absolute',
				'top' : '150px',
				'width' : '488px',
			});
			$('#frmbox img').css({ width: "473px" });
	 	  $('#Background').css({ width: "488px" });
     	$('#Background img').css({ width: "100%" });
     	$('.footer').css({ width: "488px" });
      $('input.textbox').css({
				'height' : '14px',
				'width' : '55%',
			});
      		$('select.textbox').css({
				'height' : '14px',
				'width' : '54%',
			});
      $('#r1').css({
				'position' : 'relative',
				'bottom' : '99px',
				'left' : '100px',
			});
			$('#r2').css({
				'position' : 'relative',
				'bottom' : '113px',
				'left' : '100px',
			});
			$('#r3').css({
				'position' : 'relative',
				'bottom' : '127px',
				'left' : '100px',
			});
			$('#r4').css({
				'position' : 'relative',
				'bottom' : '142px',
				'left' : '100px',
			});
			$('#r5').css({
				'position' : 'relative',
				'bottom' : '154px',
				'left' : '104px',
			});
			$('#r6').css({
				'position' : 'relative',
				'bottom' : '165px',
				'left' : '100px',
			});
			$('#r7').css({
				'position' : 'relative',
				'bottom' : '177px',
				'left' : '104px',
			});
			$('.Mupload-btn-wrapper').css({
				'position' : 'relative',
				'width' : '55px',
				'height' : '22px',

			});
			$('.Mupload-btn-wrapper input[type=file]').css({
				'position' : 'absolute',
				'left' : '0',
				'top' : '0',
				'opacity' :'0',
				'width' : '55px',
				'height' : '22px',
			});
			$('.btn2').css({
				'border' : '1px solid #58380b',
				'height' : '14px',
				'width' : '55px',
				'font-size' : '10px',
			});

			$('#r8').css({
				'position' : 'relative',
				'bottom' : '195px',
				'left' : '75px',
			});

			$('.bsubmit').css({
				'position' : 'relative',
				'bottom' : '176px',
				'left' : '-36px',
				'width' : '70px',
			});

			$('.bsharefb').css({
				'position' : 'relative',
				'bottom' : '171px',
				'left' : '-57px',
				'width' : '78px',
			});

			$('#conditionfrm1').css({
				'width' : '100%',
				'position' : 'relative',
				'top' : '230px',
				'left' : '60px',
			});

			$('#contentcondition').css({
				'position' : 'relative',
				'top' : '15px',
				'left' : '60px',
			});

			$('#conditionfrm2').css({
				'position' : 'relative',
				'width' : '100%',
				'left' : '60px',
			});
		} else if(screensize >= 375 && screensize <= 410){
			 $('#frmbox').css({
				'position' : 'absolute',
				'top' : '150px',
				'width' : '488px',
			});
			$('#frmbox img').css({ width: "473px" });
	 	  $('#Background').css({ width: "488px" });
     	$('#Background img').css({ width: "100%" });
     	$('.footer').css({ width: "488px" });
      $('input.textbox').css({
				'height' : '14px',
				'width' : '55%',
			});
      		$('select.textbox').css({
				'height' : '14px',
				'width' : '54%',
			});
      $('#r1').css({
				'position' : 'relative',
				'bottom' : '99px',
				'left' : '105px',
			});
			$('#r2').css({
				'position' : 'relative',
				'bottom' : '113px',
				'left' : '105px',
			});
			$('#r3').css({
				'position' : 'relative',
				'bottom' : '127px',
				'left' : '105px',
			});
			$('#r4').css({
				'position' : 'relative',
				'bottom' : '142px',
				'left' : '105px',
			});
			$('#r5').css({
				'position' : 'relative',
				'bottom' : '154px',
				'left' : '109px',
			});
			$('#r6').css({
				'position' : 'relative',
				'bottom' : '165px',
				'left' : '105px',
			});
			$('#r7').css({
				'position' : 'relative',
				'bottom' : '175px',
				'left' : '109px',
			});
			$('.Mupload-btn-wrapper').css({
				'position' : 'relative',
				'width' : '55px',
				'height' : '22px',

			});
			$('.Mupload-btn-wrapper input[type=file]').css({
				'position' : 'absolute',
				'left' : '0',
				'top' : '0',
				'opacity' :'0',
				'width' : '55px',
				'height' : '22px',
			});
			$('.btn2').css({
				'border' : '1px solid #58380b',
				'height' : '14px',
				'width' : '55px',
				'font-size' : '10px',
			});

			$('#r8').css({
				'position' : 'relative',
				'bottom' : '195px',
				'left' : '75px',
			});

			$('.bsubmit').css({
				'position' : 'relative',
				'bottom' : '175px',
				'left' : '-30px',
				'width' : '70px',
			});

			$('.bsharefb').css({
				'position' : 'relative',
				'bottom' : '200px',
				'left' : '218px',
				'width' : '78px',
			});

			$('#conditionfrm1').css({
				'width' : '100%',
				'position' : 'relative',
				'top' : '230px',
				'left' : '60px',
			});

			$('#contentcondition').css({
				'position' : 'relative',
				'top' : '15px',
				'left' : '60px',
			});

			$('#conditionfrm2').css({
				'position' : 'relative',
				'width' : '100%',
				'left' : '60px',
			});
		} else if(screensize >= 414 && screensize <= 736){

			   $('#frmbox').css({
				'position' : 'absolute',
				'top' : '150px',
				'width' : '488px',
			});
			$('#frmbox img').css({ width: "518px" });
	 	  $('#Background').css({ width: "518px" });
     	$('#Background img').css({ width: "100%" });
     	$('.footer').css({ width: "518px" });
      $('input.textbox').css({
				'height' : '14px',
				'width' : '55%',
			});
      		$('select.textbox').css({
				'height' : '14px',
				'width' : '54%',
			});
      $('#r1').css({
				'position' : 'relative',
				'bottom' : '126px',
				'left' : '100px',
			});
			$('#r2').css({
				'position' : 'relative',
				'bottom' : '140px',
				'left' : '100px',
			});
			$('#r3').css({
				'position' : 'relative',
				'bottom' : '155px',
				'left' : '100px',
			});
			$('#r4').css({
				'position' : 'relative',
				'bottom' : '169px',
				'left' : '100px',
			});
			$('#r5').css({
				'position' : 'relative',
				'bottom' : '181px',
				'left' : '104px',
			});
			$('#r6').css({
				'position' : 'relative',
				'bottom' : '192px',
				'left' : '100px',
			});
			$('#r7').css({
				'position' : 'relative',
				'bottom' : '204px',
				'left' : '104px',
			});
			$('.Mupload-btn-wrapper').css({
				'position' : 'relative',
				'width' : '55px',
				'height' : '22px',

			});
			$('.Mupload-btn-wrapper input[type=file]').css({
				'position' : 'absolute',
				'left' : '0',
				'top' : '0',
				'opacity' :'0',
				'width' : '55px',
				'height' : '22px',
			});
			$('.btn2').css({
				'border' : '1px solid #58380b',
				'height' : '14px',
				'width' : '55px',
				'font-size' : '10px',
			});

			$('#r8').css({
				'position' : 'relative',
				'bottom' : '224px',
				'left' : '70px',
			});

			$('.bsubmit').css({
				'position' : 'relative',
				'bottom' : '205px',
				'left' : '-42px',
				'width' : '70px',
			});

			$('.bsharefb').css({
				'position' : 'relative',
				'bottom' : '200px',
				'left' : '-69px',
				'width' : '78px',
			});

			$('#conditionfrm1').css({
				'width' : '100%',
				'position' : 'relative',
				'top' : '230px',
				'left' : '60px',
			});

			$('#contentcondition').css({
				'position' : 'relative',
				'top' : '15px',
				'left' : '60px',
			});

			$('#conditionfrm2').css({
				'position' : 'relative',
				'width' : '100%',
				'left' : '60px',
			});

	  } else if(screensize >= 1280 && screensize < 1300){
	  	$('#sectionfrm1').css({
				'bottom' : '379px',
				'left' : '26px',
			});
			$('#sectionfrm2').css({
				'bottom' : '378px',
				'left' : '33px',
			});

			$('#sectionfrm3').css({
				'bottom' : '385px',
				'left' : '30px',
			});

			$('#sectionfrm4').css({
				'bottom' : '392px',
				'left' : '33px',
			});

			$('.upload-btn-wrapper').css({
				'bottom' : '380px',
				'left' : '156px',
			});

			$('.bsubmit').css({
				'bottom' : '340px',
				'right' : '222px',
			});

			$('.bsharefb').css({
				'bottom' : '340px',
				'right' : '226px',
			});

			$('.navbar-collapse').css({
				'padding-left' : '130px',
			});


	  }
	 



	$('input[name=sAccountFB]').on('keyup',function(event){
		event.preventDefault();
		var sFacebook = $(this).val();
		var sFacebookData = CheckvalidateFB(sFacebook);
		 sFacebookData.done(function( data ) {
				if(data != ''){
					alert('ชื่อ Facebook ของคุณมีอยู่ในระบบแล้ว');
					$('input[name=sAccountFB]').val('');
				}
				return false;
	    });
	});

		$('input[name=sPhone]').on('keyup',function(event){
			event.preventDefault();
			var sPhone = $(this).val();
			var sPhoneData = CheckvalidatePhone(sPhone);
			 sPhoneData.done(function( data ) {
				if(data != ''){
					alert('เบอร์โทรศัพท์ของคุณมีอยู่ในระบบแล้ว');
					$('input[name=sPhone]').val('');
				}
					return false;
		    });
		});

});


	function CheckvalidatePhone(sPhone) {
	  var request = $.ajax({
	    url: "CheckValidatorPhone",
	    type: "POST",
	    data: { sPhone  : sPhone},
	    dataType: "json",
	  });
	  return request;
	}

	function CheckvalidateFB(sFacebook) {
	  var request = $.ajax({
	    url: "CheckValidatorFacebook",
	    type: "POST",
	    data: { sFacebook  : sFacebook},
	    dataType: "json",
	  });
	  return request;
	}

