

function getParameterByName(e) {
	e = e.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	var t = new RegExp("[\\?&]" + e + "=([^&#]*)"),
		a = t.exec(location.search);
	return null == a ? "" : decodeURIComponent(a[1].replace(/\+/g, " "))
}

function paging(e, t) {
	t != currentPage && (currentPage = t, $.ajax({
		url: e + ".php",
		success: function(e) {
			$(".wrapper").html(e), $(".paging .page").html(t)
		}
	}).then(function() {
		$("#filters button").removeClass("active"), $(".wrapper").isotope("destroy"), $(".wrapper").isotope({
			itemSelector: ".box",
			layoutMode: "fitRows"
		})
	}))
}

function readContent(e) {
	$.ajax({
		url: e + ".html",
		success: function(e) {
			$(".lightbox > div").html(e), openContent()
		}
	})
}

function openContent() {
	$(".overlay").addClass("opened"), $(".lightbox").addClass("opened")
}

function closeContent() {
	$(".overlay").attr("class", "overlay closed"), $(".lightbox").attr("class", "lightbox closed"), setTimeout(function() {
		$(".overlay, .lightbox").removeClass("closed"), $(".lightbox > div").empty()
	}, 700)
}

function shareFacebook(o) {
    var t = baseURL +  o ;

    newwindowtw = window.open("https://www.facebook.com/sharer/sharer.php?u=" + encodeURIComponent(t), "sharer", "toolbar=0, status=0, width=626, height=436"), window.focus && newwindowtw.focus()
}

function shareTwitter(o) {
     var t = baseURL +  o ;
    switch (o) {
        case "o":
            t = title, e = "http://goo.gl/tkwmMX";
            break;
    }
    newwindowtw = window.open("http://twitter.com/share?url=" + encodeURIComponent(t), "sharer", "toolbar=0, status=0, width=626, height=436"), window.focus && newwindowtw.focus()
}

function shareGooglePlus(o) {
      var t = baseURL +  o ;
    newwindowtw = window.open("https://plus.google.com/share?url=" + encodeURIComponent(t), "sharer", "toolbar=0, status=0, width=626, height=436"), window.focus && newwindowtw.focus()
}

var currentPage = 1,
	baseURL = "http://www2.greenspot.co.th/healthy-menu/form/",
	chkParam = getParameterByName("page");
chkParam && readContent(chkParam), $(document).ready(function() {
	$(".wrapper").isotope({
		itemSelector: ".box",
		layoutMode: "fitRows"
	}), $("#filters").on("click", "button", function() {
		var e = $(this).attr("data-filter");
		$(".wrapper").isotope({
			filter: e
		}), $("#filters button").removeClass("active"), $(this).addClass("active")
	}), $(".wrapper").on("click", ".box", function(e) {
		var t = $(this).attr("data-id");
		var title = $(this).attr('data-title');
		"fb" != e.target.className && "tw" != e.target.className && "gp" != e.target.className && readContent(t)
	}), $(document).on("click", ".wrapShare .fb", function(e) {
		e.preventDefault(), shareFacebook($(this).attr("data-share"))
	}), $(document).on("click", ".wrapShare .tw", function(e) {
		e.preventDefault(), shareTwitter($(this).attr("data-share"))
	}), $(document).on("click", ".wrapShare .gp", function(e) {
		e.preventDefault()
	})
});


