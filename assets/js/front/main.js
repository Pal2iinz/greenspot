$(document).ready(function() {
  $('#HeroBanner').owlCarousel({
    loop: true,
    autoplayHoverPause: true,
    autoplayTimeout: 5000,
    items: 1,
    dots: true,
    animateOut: 'slideOutUp',
    animateIn: 'slideInUp'
  });

   $('#MoblieHero').owlCarousel({
    loop: true,
    autoplay: true,
    autoplayHoverPause: true,
    autoplayTimeout: 5000,
    items: 1,
    dots: true,
    animateOut: 'slideOutUp',
    animateIn: 'slideInUp'
  });

  var imageSwap = function () {
        var $this = $(this);
        var newSource = $this.data('swap');
        $this.data('swap', $this.attr('src'));
        $this.attr('src', newSource);
    }

    $(function () {
      $('img.playgif').stop();
      $('img.playgif').mouseenter(imageSwap, imageSwap);
    });

});




