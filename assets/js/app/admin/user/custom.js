$(document).ready(function(){
  $('.datatable').dataTable();
});

// delete bank case
$('.delete').click(function(){
  // get value from table
  var id = $(this).attr("data-id");
  // create form json
  var formData = {id:id};
  // check user confirm
  var cf = confirm("คุณต้องการลบรายการนี้ออกจากระบบ");
  if(cf == true){
    // ajax post
    var request = $.ajax({
      url: "user/delete",
      type: "POST",
      data: formData,
      success: function( data, textStatus, jQxhr ){
        location.reload();
      },
        error: function( jqXhr, textStatus, errorThrown ){
          console.log( errorThrown );
        }
    }); // end ajax
  } else {
    console.log('cancle');
  } // end else
}); // end delete
