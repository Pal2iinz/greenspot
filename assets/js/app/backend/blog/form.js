$('document').ready(function(){
  $('input, textarea, select')
  .not('input[type=checkbox]')
  .not('input[type=submit]')
  .not('input[type=radio]')
  .addClass('form-control');
  $('#post').summernote({
      height: 300,
  });
});

// delete Photo case
$('.delimage').click(function(){
  // get value from table
  var id = $(this).attr("data-id");
  var position = $(this).attr("data-position");
	// create form json
  var formData = {id:id,position:position};
  // check user confirm
  var cf = confirm("คุณต้องการลบรูปภาพนี้ออกจากระบบ");
  if(cf == true){
    // ajax post
    var request = $.ajax({
        url: "../deleteimage",
        type: "POST",
        data: formData,
        success: function( data, textStatus, jQxhr ){
          location.reload();
        },
        error: function( jqXhr, textStatus, errorThrown ){
          console.log( errorThrown );
        }
    }); // end ajax
  } else {
    console.log('cancle');
  } // end else
}); // end delete
