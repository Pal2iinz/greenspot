$('document').ready(function(){
  $('input, textarea, select')
  .not('input[type=checkbox]')
  .not('input[type=submit]')
  .not('input[type=radio]')
  .addClass('form-control');
});
