$('document').ready(function(){
    $('.datatable').dataTable();
});

// open banner case
$('.openintro').click(function(){
  // get value from table
  var id   = $(this).attr("data-id");
  // create form json
  var formData = {id:id};
  // ajax post
  var request = $.ajax({
    url: "open",
    type: "POST",
    data: formData,
    success: function( data, textStatus, jQxhr ){
      location.reload();
    },
    error: function( jqXhr, textStatus, errorThrown ){
      console.log( errorThrown );
      }
    }); // end ajax
}); // end open

// close banner case
$('.closeintro').click(function(){
  // get value from table
  var id  = $(this).attr("data-id");
  // create form json
  var formData = {id:id};
  // ajax post
  var request = $.ajax({
    url: "close",
    type: "POST",
    data: formData,
    success: function( data, textStatus, jQxhr ){
      location.reload();
      },
      error: function( jqXhr, textStatus, errorThrown ){
        console.log( errorThrown );
      }
    }); // end ajax
}); // end close

// delete banner case
$('.delete').click(function(){
  // get value from table
  var id   = $(this).attr("data-id");
	// create form json
  var formData = {id:id};
  // check user confirm
  var cf = confirm("คุณต้องการลบรายการนี้ออกจากระบบ");
  if(cf == true){
    // ajax post
    var request = $.ajax({
        url: "intro/delete",
        type: "POST",
        data: formData,
        success: function( data, textStatus, jQxhr ){
          location.reload();
        },
        error: function( jqXhr, textStatus, errorThrown ){
          console.log( errorThrown );
        }
    }); // end ajax
  } else {
    console.log('cancle');
  } // end else
}); // end delete


// delete Photo case
$('.delimage').click(function(){
  // get value from table
  var id   = $(this).attr("data-id");
	// create form json
  var formData = {id:id};
  // check user confirm
  var cf = confirm("คุณต้องการลบรูปภาพนี้ออกจากระบบ");
  if(cf == true){
    // ajax post
    var request = $.ajax({
        url: "../delimage",
        type: "POST",
        data: formData,
        success: function( data, textStatus, jQxhr ){
          location.reload();
        },
        error: function( jqXhr, textStatus, errorThrown ){
          console.log( errorThrown );
        }
    }); // end ajax
  } else {
    console.log('cancle');
  } // end else
}); // end delete
