
var heroBannerSlide;

    heroBannerSlide = $(".heroBannerSlide").bxSlider({
        mode: "vertical",
        pause: 5e3,
        speed: 720,
        controls: !1,
        onSliderLoad: function() {
            $("#heroBanner").css("visibility", "visible")
        }
    });

function getParameterByName(e) {
	e = e.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	var t = new RegExp("[\\?&]" + e + "=([^&#]*)"),
		a = t.exec(location.search);
	return null == a ? "" : decodeURIComponent(a[1].replace(/\+/g, " "))
}

function paging(e, t) {
	t != currentPage && (currentPage = t, $.ajax({
		url: e + ".html",
		success: function(e) {
			$(".wrapper").html(e), $(".paging .page").html(t)
		}
	}).then(function() {
		$("#filters button").removeClass("active"), $(".wrapper").isotope("destroy"), $(".wrapper").isotope({
			itemSelector: ".box",
			layoutMode: "fitRows"
		})
	}))
}

function readContent(e) {
	$.ajax({
		url: e + ".html",
		success: function(e) {
			$(".lightbox > div").html(e), openContent()
		}
	})
}

function openContent() {
	$(".overlay").addClass("opened"), $(".lightbox").addClass("opened")
}

function closeContent() {
	$(".overlay").attr("class", "overlay closed"), $(".lightbox").attr("class", "lightbox closed"), setTimeout(function() {
		$(".overlay, .lightbox").removeClass("closed"), $(".lightbox > div").empty()
	}, 700)
}
var currentPage = 1,
	baseURL = "http://www.greenspot.co.th/share/",
	chkParam = getParameterByName("page");
chkParam && readContent(chkParam), $(document).ready(function() {
	$(".wrapper").isotope({
		itemSelector: ".box",
		layoutMode: "fitRows"
	}), $("#filters").on("click", "button", function() {
		var e = $(this).attr("data-filter");
		$(".wrapper").isotope({
			filter: e
		}), $("#filters button").removeClass("active"), $(this).addClass("active")
	}), $(".wrapper").on("click", ".box", function(e) {
		var t = $(this).attr("data-id");
		"fb" != e.target.className && "tw" != e.target.className && "gp" != e.target.className && readContent(t)
	}), $(document).on("click", ".wrapShare .fb", function(e) {
		e.preventDefault(), shareFacebook($(this).attr("data-share"))
	}), $(document).on("click", ".wrapShare .tw", function(e) {
		e.preventDefault(), shareTwitter($(this).attr("data-share"))
	}), $(document).on("click", ".wrapShare .gp", function(e) {
		e.preventDefault()
	})
});